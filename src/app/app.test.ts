import { HttpLoaderFactory, RosieMissingTranslationHandler } from './app.module';
import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';
import { EventsMock } from './../core/mocks/app.mock';
import { SyncDataProvider } from './../providers/sync-data/sync-data';
import { SyncDataMock } from './../core/mocks/sync-data.mock';
import { TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import {
  App, MenuController, NavController, Platform, Config, Keyboard,
  Form, LoadingController, GestureController, DomController, Events, DeepLinker, DeepLinkConfig, DeepLinkMetadata,
} from 'ionic-angular';
import { mockNavController, mockDeepLinker } from 'ionic-angular/util/mock-providers';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';

export class TestsProviders {
  public getProviders() {
    return [
      DeepLinkMetadata,
      {
        provide: DeepLinker,
        useFactory: (deepLinkConfig: DeepLinkConfig, app: App) => {
          return mockDeepLinker(deepLinkConfig, app);
        },
        deps: [DeepLinkMetadata, App],
      },
      MockBackend,
      BaseRequestOptions,
      {
        provide: Http,
        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
          return new Http(backendInstance, defaultOptions);
        },
        deps: [MockBackend, BaseRequestOptions],
      },
      App, Platform, Form, Keyboard, MenuController, GestureController, Config,
      LoadingController, DomController,
      { provide: Events, useClass: EventsMock },
      { provide: NavController, useClass: mockNavController },
      { provide: SyncDataProvider, useClass: SyncDataMock },
    ];
  }
}

export class TestUtils {

  static readonly TEST_PROVIDERS: Array<any> = new TestsProviders().getProviders();

  static TEST_RUNNING_PROVIDERS: Array<any> = TestUtils.TEST_PROVIDERS;

  static addProvider(provider: any) {
    TestUtils.TEST_RUNNING_PROVIDERS.push(provider);
  }

  public static beforeEachCompiler(components: Array<any>): Promise<{ fixture: any, instance: any }> {

    return TestUtils.configureIonicTestingModule(components)
      .compileComponents().then(() => {
        let fixture: any;
        if (components.length > 0) {
          /*try {
            fixture = TestBed.createComponent(components[0]);
          } catch (e) {
            console.log('ERRO AO CRIAR COMPONENTE', e);
          }*/

          fixture = TestBed.createComponent(components[0]);

          // jasmine.DEFAULT_TIMEOUT_INTERVAL = 30000;
          return {
            fixture: fixture,
            instance: fixture.debugElement.componentInstance,
          };
        }
        return null;
      });
  }

  public static configureIonicTestingModule(components: Array<any>): typeof TestBed {
    return TestBed.configureTestingModule({
      declarations: [
        ...components,
      ],
      imports: [
        NoopAnimationsModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: (HttpLoaderFactory),
            deps: [Http],
          },
          missingTranslationHandler: {
            provide: MissingTranslationHandler, useClass: RosieMissingTranslationHandler,
          },
        }),
      ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: TestUtils.TEST_RUNNING_PROVIDERS,
    });
  }

  public static destruirProviders() {
    TestUtils.TEST_RUNNING_PROVIDERS = new TestsProviders().getProviders();
  }

  // http://stackoverflow.com/questions/2705583/how-to-simulate-a-click-with-javascript
  public static eventFire(el: any, etype: string): void {
    if (el.fireEvent) {
      el.fireEvent('on' + etype);
    } else {
      let evObj: any = document.createEvent('Events');
      evObj.initEvent(etype, true, false);
      el.dispatchEvent(evObj);
    }
  }
}
