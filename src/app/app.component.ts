import { Session } from './../core/models/chat-session.model';
import { AppHelper } from './../core/helpers/app.helper';
import { SyncDataProvider } from './../providers/sync-data/sync-data';
import { AuthProvider } from './../providers/auth/auth';
import { Globalization } from '@ionic-native/globalization';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';

@Component({
  templateUrl: 'app.html',
})
export class RosieApp {

  rootPage: any = 'TutorialPage';
  // rootPage: any = 'ChatPage';

  constructor(private _globalization: Globalization, private translate: TranslateService, private _platform: Platform, private _statusBar: StatusBar, private _splashScreen: SplashScreen,
    private _authService: AuthProvider, private _syncDataProvider: SyncDataProvider) {

    this.translate.addLangs(['en', 'de', 'fr', 'pt']);
    /*this.translate.onLangChange.subscribe((event: any) => {
      console.log('onLangChange()~translate', event);
    });*/
    this._initializeApp().then(
      () => {
        this._platform.ready().then(
          () => {
            this._statusBar.styleDefault();
            this._splashScreen.hide();
            this._authService.startupTokenRefresh();
          },
        );
      },
    );

    // this._authService._saveToken('eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1MjYxODE2NjEwNTk3OTkiLCJkYm5hbWUiOiJ1c2VyLTUyNjE4MTY2MTA1OTc5OSIsImlhdCI6MTUxMDE1ODQ1MywiZXhwIjoxNTEyNzUwNDUzfQ.kjL
    // 2xUf_FqRdYD6PdmamF2dLMpGdk7FlnWztvdxfs00');
  }

  ionViewWillUnload() {
    this._authService.unscheduleRefreshToken();
  }

  private async _initializeApp(): Promise<any> {
    let isValid: boolean = false;
    isValid = await this._authService.isValidToken();

    if (isValid) {
      this.rootPage = 'ChatPage';

      await this._syncDataProvider.prepararUsuario().then(
        () => {
          console.log('preparado com sucesso', this._syncDataProvider.getDatabase());
        },
        () => {
          console.log('falha ao preparar usuário', this._syncDataProvider.getDatabase());
        },
      );
    }

    await this._initTranslate().catch((err) => { return err; });

    if (this._syncDataProvider.getDatabase()) {
      await this._updateLastSession();
    }
  }

  private async _initTranslate() {
    // this.translate.setDefaultLang('en');
    this.translate.setDefaultLang('pt');

    await this._platform.ready();
    await this._globalization.getPreferredLanguage().then(
      (res: any) => {
        console.log('Globalization Lang', res.value);
        if (this.translate.getBrowserLang() !== undefined) {

          this.translate.use(this.translate.getBrowserLang());
        } else {

          this.translate.use('en');
        }
      },
    );
  }

  private async _updateLastSession() {
    let sessionDoc: Session;

    await this._syncDataProvider.getDocumentById('dc636ba569551879357f366092fd789c').then(
      (result: any) => {
        if (result.error && result.status === 404) {
          // não encontrei o doc
        } else if (result.error === undefined) {
          sessionDoc = result;
          sessionDoc.lastSession = moment().format(AppHelper.FORMATO_DATA_ISO_8601);
        } else {
          // erro desconhecido
        }
      },
    );

    await this._syncDataProvider.addOrUpdate(sessionDoc).then(
      () => {
        console.log('sucesso ao atualizar lastSession');
      },
      (err: any) => {
        console.log('falha ao atualizar lastSession', err);
      },
    );
  }
}
