import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { RosieApp } from './app.component';
import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule } from 'ionic-angular';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule, Http } from '@angular/http';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Geolocation } from '@ionic-native/geolocation';
import { Health } from '@ionic-native/health';
import { Camera } from '@ionic-native/camera';
import { Globalization } from '@ionic-native/globalization';
import { TranslateModule, MissingTranslationHandler, MissingTranslationHandlerParams, TranslateLoader } from '@ngx-translate/core';

import * as Raven from 'raven-js';
import { SyncDataProvider } from '../providers/sync-data/sync-data';
import { AuthenticationProvider } from '../providers/authentication/authentication';
import { ApiProvider } from '../providers/api/api';
import { AuthProvider } from '../providers/auth/auth';
import { AuthHttp, AuthConfig, JwtHelper } from 'angular2-jwt';

Raven
  .config('https://b037256b5f034f1cb2248aec07103ab3@sentry.io/136503')
  .install();

export class RavenErrorHandler implements ErrorHandler {
  handleError(err: any): void {
    Raven.captureException(err.originalError);
  }
}

export function getAuthHttp(http: Http, storage: Storage) {
  return new AuthHttp(new AuthConfig({
    noJwtError: true,
    globalHeaders: [{ 'Accept': 'application/json' }],
    tokenGetter: (() => storage.get('jwt_token')),
  }), http);
}

export function HttpLoaderFactory(http: Http) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export class RosieMissingTranslationHandler implements MissingTranslationHandler {
  handle(params: MissingTranslationHandlerParams) {
    console.log('Não encontrei LANG', params.key);
    return 'some value';
  }
}

@NgModule({
  declarations: [
    RosieApp,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [Http],
      },
      missingTranslationHandler: {
        provide: MissingTranslationHandler, useClass: RosieMissingTranslationHandler,
      },
    }),
    IonicModule.forRoot(RosieApp),
    IonicStorageModule.forRoot(),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    RosieApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    Health,
    Camera,
    Globalization,
    SyncDataProvider,
    AuthenticationProvider,
    ApiProvider,
    AuthProvider,
    JwtHelper,
    {
      provide: AuthHttp,
      useFactory: getAuthHttp,
      deps: [Http, Storage],
    },
  ],
})
export class AppModule { }
