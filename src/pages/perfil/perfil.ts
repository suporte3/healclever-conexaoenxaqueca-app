import { TranslateService } from '@ngx-translate/core';
import { ValidationHelper } from './../../core/helpers/validation.helper';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { AppHelper } from './../../core/helpers/app.helper';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Usuario } from './../../core/models/usuario/usuario.model';
import { SexoUsuario } from './../../core/models/usuario/sexo.enum';
import { Component, trigger, transition, style, animate, OnInit, ViewChild } from '@angular/core';
import { IonicPage, Platform, ActionSheetController, ActionSheet } from 'ionic-angular';
import * as moment from 'moment';
import * as blobUtil from 'blob-util';

@IonicPage()
@Component({
  selector: 'ib-page-perfil',
  templateUrl: 'perfil.html',
  animations: [
    trigger(
      'enterAnimation', [
        transition(':enter', [
          style({ transform: 'translateX(100%)', opacity: 0 }),
          animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
        ]),
        transition(':leave', [
          style({ transform: 'translateX(0)', opacity: 1 }),
          animate('500ms', style({ transform: 'translateX(-100%)', opacity: 0 })),
        ]),
      ],
    ),
  ],
})
export class PerfilPage implements OnInit {

  public usuario: Usuario = {
    _id: SyncDataProvider.DOCUMENT_PERFIL_ID,
    dataAdmissao: null,
    email: null,
    first_name: null,
    last_name: null,
    birthday: null,
    genero: SexoUsuario.UKNOW,
    sessao: {},
    sincronizado: false,
    logado: true,
    facebook: false,
  };
  public opcoesGenero: Array<any> = [];
  public minDate: any = moment().subtract(100, 'years').toISOString();
  public atualizando: boolean = false;
  public vinculandoComFacebook: boolean = false;
  public temAvatar: boolean = false;
  public imgSrcAvatar: string = null;
  public documentoImagemPerfil: any = null;

  @ViewChild('perfilForm') perfilForm: FormGroup;

  private _imgDataUrlAvatar: string;

  private MSG_INTERNET_FAIL: string = null;
  private MSG_FACEBOOK_FAIL: string = null;
  private MSG_IMAGE_FACEBOOK_FAIL: string = null;

  private ACTION_SHEET_TITULO: string = null;
  private ACTION_SHEET_LABEL_GALERIA: string = null;
  private ACTION_SHEET_LABEL_FOTO_FB: string = null;
  private ACTION_SHEET_LABEL_TIRAR_FOTO: string = null;

  private appHelper: AppHelper;
  private _cameraOptions: CameraOptions = {
    destinationType: this._camera.DestinationType.DATA_URL,
    quality: 100,
    targetWidth: 200,
    targetHeight: 200,
    encodingType: this._camera.EncodingType.JPEG,
    correctOrientation: true,
  };

  constructor(private _translate: TranslateService, private _syncDataProvider: SyncDataProvider, private _formBuilder: FormBuilder,
    private _actionSheetCtrl: ActionSheetController, private _camera: Camera, private _platform: Platform) {

    this.opcoesGenero = [
      { value: SexoUsuario.UKNOW, descricao: null },
      { value: SexoUsuario.FEMININO, descricao: null },
      { value: SexoUsuario.MASCULINO, descricao: null },
    ];

    this.appHelper = new AppHelper();
    this._obterMensagens();
  }

  public ngOnInit() {
    this.perfilForm = this._formBuilder.group({
      email: ['', Validators.compose([ValidationHelper.validarFormatoEmailNullTrue])],
      firstName: ['', Validators.maxLength(100)],
      lastName: ['', Validators.maxLength(100)],
    });
  }

  public ionViewDidEnter() {
    let scope = this;
    this._syncDataProvider.getDocumentById(SyncDataProvider.DOCUMENT_PERFIL_ID)
      .then(
      (document) => {
        this.usuario = document;

        if (this.usuario.email) {
          this.perfilForm = this._formBuilder.group({
            email: ['', Validators.compose([ValidationHelper.validarFormatoEmail])],
            firstName: ['', Validators.maxLength(100)],
            lastName: ['', Validators.maxLength(100)],
          });
        }
      },
      (error) => {
        console.log('ERRO', error);
        // track
      },
    );

    this._syncDataProvider.getAttachmentsById(SyncDataProvider.DOCUMENT_PERFIL_IMAGEM_ID)
      .then(
      (result) => {
        if (result.error && result.status === 404) {
          scope.documentoImagemPerfil = null;
          scope.temAvatar = false;
        } else if (result.error === undefined) {
          scope.documentoImagemPerfil = result;
          let urlProfileImage = 'data:' + result._attachments.perfil.content_type + ';base64,' + result._attachments.perfil.data;
          scope.temAvatar = true;
          scope.imgSrcAvatar = urlProfileImage;
        } else {
          // erro desconhecido
        }

        // scope.temAvatar = true;
        // scope.srcAvatar = +
        // "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/13494865_10201837716621358_8056038554301631899_n. +
        // jpg?oh=e6bc2eb976b38f245357e05e5a6224b6&oe=590140A4";
      },
      () => {
        // track
      },
    );
  }

  public onSubmitAtualizarPerfil(event: any) {
    if (!this.atualizando) {
      if (this.perfilForm.valid) {
        this.atualizando = true;
        this._atualizarDadosPerfilUsuario();
      }
    }

    event.preventDefault();
  }

  public onClickOpenChooser() {
    let actionSheet: ActionSheet = this._actionSheetCtrl.create({
      title: this.ACTION_SHEET_TITULO,
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: this.ACTION_SHEET_LABEL_GALERIA,
          icon: !this._platform.is('ios') ? 'images' : null,
          handler: () => {
            this._escolherGaleria();
          },
        },
        {
          text: this.ACTION_SHEET_LABEL_FOTO_FB,
          icon: !this._platform.is('ios') ? 'image' : null,
          handler: () => {
            this._atualizarImagemPerfil(null, 'facebook');
          },
        },
        {
          text: this.ACTION_SHEET_LABEL_TIRAR_FOTO,
          icon: !this._platform.is('ios') ? 'camera' : null,
          handler: () => {
            this._tirarFoto();
          },
        },
      ],
    });
    actionSheet.present();
  }

  public callbackEnterAnimationDone() {
    if (!this.temAvatar) {
      this.imgSrcAvatar = this._imgDataUrlAvatar;
      this.temAvatar = true;
    }
  }

  public onSincronizarOuAtualizarFacebook() {
    // this.temAvatar = true;
    /*if (!this.vinculandoComFacebook) {
      // this.temAvatar = false;
      this.vinculandoComFacebook = true;
      let permissoesDoUsuario: string[] = ['email', 'public_profile', 'user_friends', 'user_birthday'];
      new Facebook().login(permissoesDoUsuario).then(
        (facebookResponse: FacebookLoginResponse) => {
          let credenciais = { access_token: facebookResponse.authResponse.accessToken };
          this._vincularContaFacebook(credenciais, facebookResponse);
        },
        (error) => {
          console.log('FBLOGIN REJEITADO 1', error);
          this._manipularErroVinculacaoFacebook();
        },
      );
    }*/
  }

  private _escolherGaleria() {
    this._cameraOptions['sourceType'] = this._camera.PictureSourceType.PHOTOLIBRARY;

    this._camera.getPicture(this._cameraOptions)
      .then(
      dataBase64 => this._atualizarImagemPerfil(dataBase64, ''),
      (err: any) => console.log(err),
    );
  }

  private _tirarFoto() {
    this._cameraOptions['sourceType'] = this._camera.PictureSourceType.CAMERA;

    this._camera.getPicture(this._cameraOptions).then((dataBase64) => {
      this._atualizarImagemPerfil(dataBase64, '');
    }, (err: any) => {
      console.log(err);
    });
  }

  private _atualizarImagemPerfil(dataBase64: string, provider: string) {
    if (provider === 'facebook') {
      /*let permissoesDoUsuario: string[] = ['email', 'public_profile', 'user_friends', 'user_birthday'];
      new Facebook().login(permissoesDoUsuario).then(
        (facebookResponse: FacebookLoginResponse) => {
          this._atualizarImagemPerfilByFacebook(facebookResponse.authResponse.userID);
        },
        (error) => {
          let network = new Network();
          console.log('FBLOGIN REJEITADO 1', error);
          if (network.type === 'none') {
            this.appHelper.toastMensagem(this._toastCtrl, this.MSG_INTERNET_FAIL);
          } else {
            this.appHelper.toastMensagem(this._toastCtrl, this.MSG_IMAGE_FACEBOOK_FAIL);
          }
        },
      );*/
    } else {

      let scope = this;

      blobUtil.base64StringToBlob(dataBase64).then(function (blob: Blob) {
        scope._syncDataProvider.addOrUpdateProfileImage(blob, blob.type, null)
          .then(
          () => {
            scope._atualizarSrcProfileImage(blob, dataBase64);
          },
          (err) => {
            console.log('TRACK', err);
          },
        );
      }).catch(function (err: any) {
        console.log('TRACK', err);
      });
    }
  }
/*
  private _vincularContaFacebook(credenciais: any, facebookResponse: any) {
    let headers = new Headers();
    headers.append(
      'Content-Type',
      'application/json',
    );
    headers.append(
      'Authorization',
      'Bearer ' + this.usuario.sessao.token + ':' + this.usuario.sessao.password,
    );

    this._http.post(SyncDataProvider.BASE_API_URL + '/auth/link/facebook/token',
      JSON.stringify(credenciais), { headers: headers })
      .subscribe(
      (res) => {
        console.log('CONTA ADICIONADA', res);
        // animação
        this._getFacebookUserData(facebookResponse);
      },
      (err) => {
        console.log('_adicionarConta()', err);
        this._manipularErroVinculacaoFacebook();
      },
    );
  }

  private _getFacebookUserData(facebookResponse: any) {
    new Facebook().api(facebookResponse.authResponse.userID + '/?fields=id,email,first_name,last_name, birthday, gender', []).then(
      (facebookDados) => {
        // this.usuario.email = facebookDados.email;
        this.usuario.first_name = facebookDados.first_name;
        this.usuario.last_name = facebookDados.last_name;
        this.usuario.birthday = facebookDados.birthday;
        this.usuario.genero = this.appHelper.getFacebookGenero(facebookDados.gender);
        this.usuario.facebook = true;

        this._atualizarImagemPerfilByFacebook(facebookDados.id);
        this._atualizarDadosPerfilUsuario();
      },
      (error) => {
        console.log('_getFacebookUserData', error);
        // TRACK
        this._manipularErroVinculacaoFacebook();
      },
    );
  }

  private _atualizarImagemPerfilByFacebook(facebookId: string) {
    let scope = this;
    let _rev = ((scope.documentoImagemPerfil) ? scope.documentoImagemPerfil._rev : null);
    this._syncDataProvider.addOrUpdateProfileImageByFacebook(facebookId, _rev)
      .then(
      (blob: Blob) => {
        blobUtil.blobToBase64String(blob).then(function (base64String: string) {
          scope._atualizarSrcProfileImage(blob, base64String);
        }).catch(function (err: any) {
          console.log('BASE64 ERRO', err);
        });
      },
      (err) => {
        console.log('ERRO', err);
      },
    );
  }
*/
  private _atualizarSrcProfileImage(blob: Blob, base64String: string) {
    let urlProfileImage = 'data:' + blob.type + ';base64,' + base64String;
    this._imgDataUrlAvatar = urlProfileImage;
    this.temAvatar = (!this.temAvatar);
    if (this.temAvatar) {
      this.imgSrcAvatar = this._imgDataUrlAvatar;
    }
  }
/*
  private _manipularErroVinculacaoFacebook() {
    let network = new Network();
    if (network.type === 'none') {
      this.appHelper.toastMensagem(this._toastCtrl, this.MSG_INTERNET_FAIL);
    } else {
      this.appHelper.toastMensagem(this._toastCtrl, this.MSG_FACEBOOK_FAIL);
    }
    this.vinculandoComFacebook = false;
  }
*/
  private _atualizarDadosPerfilUsuario() {
    this.usuario.first_name = ((this.usuario.first_name) ? this.usuario.first_name.toLowerCase() : this.usuario.first_name);
    this.usuario.last_name = ((this.usuario.last_name) ? this.usuario.last_name.toLowerCase() : this.usuario.last_name);
    this.usuario.birthday = ((this.usuario.birthday) ? moment(this.usuario.birthday).toISOString() : this.usuario.birthday);

    this._syncDataProvider.addOrUpdate(this.usuario)
      .then(
      () => {
        if (!this.usuario.sincronizado) {
          // this.syncDataService.setDatabaseContinuous(false);
          // this.syncDataService.sincronizarDatabase();
        }

        let dadosAtualizarPerfilUsuario: any = {
          email: this.usuario.email,
          birthday: this.usuario.birthday,
          first_name: this.usuario.first_name,
          last_name: this.usuario.last_name,
          genero: this.usuario.genero,
        };
        if (this.usuario.facebook) {
          dadosAtualizarPerfilUsuario.facebook = true;
        }

        this._syncDataProvider.setDatabaseContinuous(false);
        /*this._syncDataProvider.sincronizarDatabase(this.usuario.sessao);
        this.appHelper.atualizarPerfilUsuario(this.usuario, dadosAtualizarPerfilUsuario)
          .then(
          () => {
            this.atualizando = false;
            this.vinculandoComFacebook = false;
          },
          () => {
            this.atualizando = false;
            this.vinculandoComFacebook = false;
          },
          );*/
      },
      () => {
        this.atualizando = false;
        this.vinculandoComFacebook = false;
      },
    );
  }

  private _obterMensagens() {
    this._translate.get('MENSAGENS.SEM_CONEXAO').subscribe(
      (value: string) => {
        this.MSG_INTERNET_FAIL = value;
      },
    );

    this._translate.get('PERFIL.MSG.FACEBOOK_FAIL').subscribe(
      (value: string) => {
        this.MSG_FACEBOOK_FAIL = value;
      },
    );

    this._translate.get('PERFIL.MSG.IMAGE_FACEBOOK_FAIL').subscribe(
      (value: string) => {
        this.MSG_IMAGE_FACEBOOK_FAIL = value;
      },
    );

    this._translate.get('PERFIL.ACTION_SHEET.TITULO').subscribe(
      (value: string) => {
        this.ACTION_SHEET_TITULO = value;
      },
    );

    this._translate.get('PERFIL.ACTION_SHEET.LABEL_GALERIA').subscribe(
      (value: string) => {
        this.ACTION_SHEET_LABEL_GALERIA = value;
      },
    );

    this._translate.get('PERFIL.ACTION_SHEET.LABEL_FOTO_FB').subscribe(
      (value: string) => {
        this.ACTION_SHEET_LABEL_FOTO_FB = value;
      },
    );

    this._translate.get('PERFIL.ACTION_SHEET.LABEL_TIRAR_FOTO').subscribe(
      (value: string) => {
        this.ACTION_SHEET_LABEL_TIRAR_FOTO = value;
      },
    );

    this._translate.get('PERFIL.FORMULARIO.GENERO.OPCAO.NAO_ESPECIFICADO').subscribe(
      (value: string) => {
        this.opcoesGenero[0].descricao = value;
      },
    );

    this._translate.get('PERFIL.FORMULARIO.GENERO.OPCAO.FEMININO').subscribe(
      (value: string) => {
        this.opcoesGenero[1].descricao = value;
      },
    );

    this._translate.get('PERFIL.FORMULARIO.GENERO.OPCAO.MASCULINO').subscribe(
      (value: string) => {
        this.opcoesGenero[2].descricao = value;
      },
    );
  }

}
