import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistroSonoPage } from './registro-sono';

@NgModule({
  declarations: [
    RegistroSonoPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistroSonoPage),
    PipesModule,
    TranslateModule,
  ],
})
export class RegistroSonoPageModule { }
