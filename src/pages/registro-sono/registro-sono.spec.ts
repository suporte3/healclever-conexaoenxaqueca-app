import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Diario } from './../../core/models/diario.model';
import { Sono } from './../../core/models/sono.model';
import { AppHelper } from './../../core/helpers/app.helper';
import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { NavParamsMock } from './../../core/mocks/nav-params.mock';
import { NavParams, ToastController } from 'ionic-angular';
import { TestUtils } from './../../app/app.test';
import { RegistroSonoPage } from './registro-sono';
import { ComponentFixture, async, fakeAsync, tick } from '@angular/core/testing';
import * as moment from 'moment';
import 'moment/min/locales';

describe('Registro Sono Page:', () => {

    let fixture: ComponentFixture<RegistroSonoPage> = null;
    let instance: any = null;
    let _instance: any = RegistroSonoPage.prototype;

    let spyInicializarValoresTempo: jasmine.Spy;
    let spyDefinirPeriodoMinSono: jasmine.Spy;
    let spyDefinirPeriodoMaxSono: jasmine.Spy;
    let spyInicializarValorSono: jasmine.Spy;
    let spyConsultarSono: jasmine.Spy;

    let dataAtual: any = moment().local();
    let dataAnterior: any = moment(dataAtual).subtract(1, 'days');
    let diaAtual: number = parseInt(dataAtual.format('D'), 10);
    let mesAtual: number = parseInt(dataAtual.format('M'), 10) - 1;
    let anoAtual: number = parseInt(dataAtual.format('YYYY'), 10);
    let diaAnterior: number = parseInt(dataAnterior.format('D'), 10);
    let mesDoDiaAnterior: number = parseInt(dataAnterior.format('M'), 10) - 1;

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.addProvider(ToastController);
        TestUtils.beforeEachCompiler([RegistroSonoPage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            },
        );

        spyOn(_instance, '_iniciar').and.callThrough();
        spyInicializarValoresTempo = spyOn(_instance, '_inicializarValoresTempo').and.stub();
        spyDefinirPeriodoMinSono = spyOn(_instance, '_definirPeriodoMinSono').and.stub();
        spyDefinirPeriodoMaxSono = spyOn(_instance, '_definirPeriodoMaxSono').and.stub();
        spyInicializarValorSono = spyOn(_instance, '_inicializarValorSono').and.stub();
        spyConsultarSono = spyOn(_instance, '_consultarSono').and.returnValue(Promise.resolve());
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o componente ao carregar', () => {
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#onClickCancelar - Deve cancelar a operação e voltar tela', () => {
        spyOn(instance._navCtrl, 'pop').and.stub();

        instance.onClickCancelar();
        expect(instance._navCtrl.pop).toHaveBeenCalled();
    });

    it('#onClickRegistrar - Deve registrar com sucesso', fakeAsync(() => {
        spyOn(instance._syncDataProvider, 'addOrUpdate').and.returnValue(Promise.resolve());
        spyOn(instance._navCtrl, 'pop').and.stub();

        instance.onClickRegistrar();
        expect(instance._syncDataProvider.addOrUpdate).toHaveBeenCalledWith(instance.diario);
        tick();
        expect(instance._navCtrl.pop).toHaveBeenCalled();
    }));

    it('#onClickRegistrar - Deve realizar tratamento quando houver erro no cadastro', fakeAsync(() => {
        spyOn(instance._syncDataProvider, 'addOrUpdate').and.returnValue(Promise.reject(null));
        spyOn(AppHelper.prototype, 'toastMensagem').and.stub();
        spyOn(instance, 'onClickCancelar').and.stub();

        instance.onClickRegistrar();
        expect(instance._syncDataProvider.addOrUpdate).toHaveBeenCalledWith(instance.diario);
        tick();
        expect(AppHelper.prototype.toastMensagem).toHaveBeenCalledWith(instance._toastCtrl, instance.MSG_ERRO_CADASTRO);
        expect(instance.onClickCancelar).toHaveBeenCalled();
    }));

    it('#onClickAddQualidadeSubjetivaSono - Deve adicionar uma qualidade subjetiva ao sono', () => {
        let item: any = {
            icone: 'mm-qualidade-dor-peso',
            qualidadeSubjetiva: 'ruim',
        };

        instance.onClickAddQualidadeSubjetivaSono(item);
        expect(instance.itemAtual).toEqual(item);
        expect(instance.diario.sono.qualidadeSubjetiva).toEqual(item.qualidadeSubjetiva);
        expect(instance.animationState).toBe('active');
    });

    it('#onChangeTempoSono - Deve tratar mudanças de animation', () => {
        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();

        instance.onChangeTempoSono();
        expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('#callbackAnimationScaleDone - Deve alterar animação para scala zero', () => {

        instance.callbackAnimationScaleDone();
        expect(instance.animationState).toBe('inactive');
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        spyOn(moment, 'locale').and.stub();

        let itens = [
            {
                qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.MUITO_BOA',
                icone: 'rosie-sono-muito-boa',
            },
            {
                qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.BOA',
                icone: 'rosie-sono-boa',
            },
            {
                qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.RUIM',
                icone: 'rosie-sono-ruim',
            },
            {
                qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.MUITO_RUIM',
                icone: 'rosie-sono-muito-ruim',
            },
        ];

        spyInicializarValoresTempo.calls.reset();
        spyDefinirPeriodoMinSono.calls.reset();
        spyDefinirPeriodoMaxSono.calls.reset();
        spyInicializarValorSono.calls.reset();
        spyConsultarSono.calls.reset();

        instance._iniciar();
        expect(moment.locale).toHaveBeenCalledWith('pt-BR');
        expect(instance.items).toEqual(itens);
        expect(spyInicializarValoresTempo).toHaveBeenCalled();
        expect(spyDefinirPeriodoMinSono).toHaveBeenCalled();
        expect(spyDefinirPeriodoMaxSono).toHaveBeenCalled();
        expect(spyInicializarValorSono).toHaveBeenCalled();
        expect(spyConsultarSono).toHaveBeenCalled();
    });

    it('#_consultarSono - Deve atualizar id do documento quando não encontrado ', fakeAsync(() => {
        let result: any = {
            error: true,
            status: 404,
        };

        spyOn(instance._syncDataProvider, 'getDocumentById').and.returnValue(Promise.resolve(result));
        spyConsultarSono.calls.reset();
        spyConsultarSono.and.callThrough();

        instance._consultarSono();
        expect(instance._syncDataProvider.getDocumentById).toHaveBeenCalledWith(instance._DIARIO_ID);
        tick();
        expect(instance.diario._id).toBe(instance._DIARIO_ID);
    }));

    it('#_consultarSono - Deve consultar documento sono atual', fakeAsync(() => {
        let sono: Sono = {
            inicio: null,
            fim: null,
            qualidadeSubjetiva: instance.itemAtual.qualidadeSubjetiva,
            type: 'sono',
        };
        let result: Diario = {
            _id: null,
            alimentos: [],
            sono: sono,
            passos: 0,
            googleFitSync: false,
            type: 'diario-saude',
        };

        spyOn(instance, '_definirItemAtual').and.stub();
        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();
        spyOn(instance._syncDataProvider, 'getDocumentById').and.returnValue(Promise.resolve(result));
        spyConsultarSono.calls.reset();
        spyConsultarSono.and.callThrough();

        instance._consultarSono();
        expect(instance._syncDataProvider.getDocumentById).toHaveBeenCalledWith(instance._DIARIO_ID);
        tick();
        expect(instance.diario).toBe(result);
        expect(instance._definirItemAtual).toHaveBeenCalled();
        expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
    }));

    it('#_inicializarValoresTempo - Deve iniciar valores do tempo de sono', () => {
        spyInicializarValoresTempo.calls.reset();
        spyInicializarValoresTempo.and.callThrough();

        let DIARIO_ID: string = SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID + moment().date() + '-' + moment().month() + '-' + moment().year();
        instance._DIARIO_ID = null;
        instance._navParams = new NavParams({ 'diarioSaudeMoment': undefined });

        instance._inicializarValoresTempo();
        expect(instance._DIARIO_ID).toEqual(DIARIO_ID);
        expect(instance._dataAtual).toBeDefined();
        expect(instance._dataAnterior).toBeDefined();
        expect(instance._diaAtual).toEqual(diaAtual);
        expect(instance._mesAtual).toEqual(mesAtual);
        expect(instance._anoAtual).toEqual(anoAtual);
        expect(instance._diaAnterior).toEqual(diaAnterior);
        expect(instance._mesDoDiaAnterior).toEqual(mesDoDiaAnterior);
    });

    it('#_definirPeriodoMinSono - Deve definir período mínimo de sono', () => {
        let minTime: any = new Date(anoAtual, mesDoDiaAnterior, diaAnterior, 0, 0, 0);
        let minDate: string = moment(minTime).format(AppHelper.FORMATO_DATA_ISO_8601);
        spyInicializarValoresTempo.calls.reset();
        spyInicializarValoresTempo.and.callThrough();
        spyDefinirPeriodoMinSono.calls.reset();
        spyDefinirPeriodoMinSono.and.callThrough();

        instance._DIARIO_ID = null;
        instance._navParams = new NavParams({ 'diarioSaudeMoment': undefined });

        instance._inicializarValoresTempo();
        instance._definirPeriodoMinSono();
        expect(instance.minDate).toEqual(minDate);
    });

    it('#_definirPeriodoMaxSono - Deve definir período máximo de sono', () => {
        let maxTime: any = new Date(anoAtual, mesAtual, diaAtual, 23, 59, 59);
        let maxDate: string = moment(maxTime).format(AppHelper.FORMATO_DATA_ISO_8601);
        spyInicializarValoresTempo.calls.reset();
        spyInicializarValoresTempo.and.callThrough();
        spyDefinirPeriodoMaxSono.calls.reset();
        spyDefinirPeriodoMaxSono.and.callThrough();

        instance._DIARIO_ID = null;
        instance._navParams = new NavParams({ 'diarioSaudeMoment': undefined });

        instance._inicializarValoresTempo();
        instance._definirPeriodoMaxSono();
        expect(instance.maxDate).toEqual(maxDate);
    });

    it('#_definirItemAtual - Deve definir item atual a partir do valor atual do documento sono', () => {
        let itemMuitoRuim: any = {
            qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.MUITO_RUIM',
            icone: 'rosie-sono-muito-ruim',
        };
        instance.diario.sono.qualidadeSubjetiva = itemMuitoRuim.qualidadeSubjetiva;
        instance._iniciar();
        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();

        instance._definirItemAtual();
        expect(instance.itemAtual).toEqual(itemMuitoRuim);
        expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('#_inicializarValorSono - Deve iniciar valor inicial de sono', () => {
        spyInicializarValorSono.calls.reset();
        spyInicializarValorSono.and.callThrough();

        let iniTime: any = new Date(this._anoAtual, this._mesDoDiaAnterior, this._diaAnterior, 22, 0, 0);
        let iniSono: string = moment(iniTime).format(AppHelper.FORMATO_DATA_ISO_8601);

        let maxTime: any = new Date(this._anoAtual, this._mesAtual, this._diaAtual, 6, 0, 0);
        let fimSono: string = moment(maxTime).format(AppHelper.FORMATO_DATA_ISO_8601);

        instance._inicializarValorSono();
        expect(instance.diario.sono.inicio).toEqual(iniSono);
        expect(instance.diario.sono.fim).toEqual(fimSono);
    });
});
