import { TranslateService } from '@ngx-translate/core';
import { AppHelper } from './../../core/helpers/app.helper';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Diario } from './../../core/models/diario.model';
import { Sono } from './../../core/models/sono.model';
import { Component, ChangeDetectionStrategy, trigger, state, style, transition, animate, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import * as moment from 'moment';
import 'moment/min/locales';

@IonicPage()
@Component({
  selector: 'ib-page-registro-sono',
  templateUrl: 'registro-sono.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('scale', [
      state('inactive', style({
        transform: 'scale(1)',
      })),
      state('active', style({
        transform: 'scale(1.1)',
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out')),
    ]),
  ],
})
export class RegistroSonoPage {

  public animationState: string = 'inactive';
  public nomeMes: Array<string>;
  public minDate: any;
  public maxDate: any;
  public items: Array<any> = [];
  public itemAtual: any = {
    icone: 'rosie-sono-boa',
    qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.BOA',
  };
  public sono: Sono = {
    inicio: null,
    fim: null,
    qualidadeSubjetiva: this.itemAtual.qualidadeSubjetiva,
    type: 'sono',
  };
  public diario: Diario = {
    _id: null,
    alimentos: [],
    sono: this.sono,
    passos: 0,
    googleFitSync: false,
    type: 'diario-saude',
  };

  private _DIARIO_ID: string = null;
  private _dataAtual: any = null;
  private _dataAnterior: any;
  private _diaAtual: number;
  private _mesAtual: number;
  private _anoAtual: number;
  private _diaAnterior: number;
  private _mesDoDiaAnterior: number;
  private MSG_ERRO_CADASTRO: string = null;

  constructor(private _translate: TranslateService, private _navCtrl: NavController, private _syncDataProvider: SyncDataProvider, private _toastCtrl: ToastController,
    private _changeDetectorRef: ChangeDetectorRef, private _navParams: NavParams) {

    this._iniciar();
  }

  onClickCancelar() {
    this._navCtrl.pop();
  }

  onClickRegistrar() {

    this._syncDataProvider.addOrUpdate(this.diario).then(
      () => {
        this._navCtrl.pop();
      },
      (err: any) => {
        console.log('ERRO NO CADASTRO', err);
        new AppHelper().toastMensagem(this._toastCtrl, this.MSG_ERRO_CADASTRO);
        this.onClickCancelar();
      },
    );
  }

  onClickAddQualidadeSubjetivaSono(item: any) {
    this.itemAtual = item;
    this.diario.sono.qualidadeSubjetiva = item.qualidadeSubjetiva;
    this.animationState = 'active';
  }

  onChangeTempoSono() {
    this._changeDetectorRef.markForCheck();
  }

  public callbackAnimationScaleDone() {
    this.animationState = 'inactive';
  }

  private _iniciar() {
    this._obterMensagens();

    moment.locale('pt-BR'); // Definir por translate service
    this.nomeMes = moment.months();

    this.items = [
      {
        qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.MUITO_BOA',
        icone: 'rosie-sono-muito-boa',
      },
      {
        qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.BOA',
        icone: 'rosie-sono-boa',
      },
      {
        qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.RUIM',
        icone: 'rosie-sono-ruim',
      },
      {
        qualidadeSubjetiva: 'REGISTRO_SONO.QUALIDADE_SUBJETIVA.MUITO_RUIM',
        icone: 'rosie-sono-muito-ruim',
      },
    ];

    this._inicializarValoresTempo();
    this._definirPeriodoMinSono();
    this._definirPeriodoMaxSono();
    this._inicializarValorSono();
    this._consultarSono();
  }

  private _consultarSono(): Promise<any> {
    let _this: RegistroSonoPage = this;

    return new Promise(function (resolve, reject) {
      _this._syncDataProvider.getDocumentById(_this._DIARIO_ID).then(
        (result: any) => {
          if (result.error && result.status === 404) {
            // Documento vazio
            _this.diario._id = _this._DIARIO_ID;
            console.log('DOCUMENTO 404', _this.diario._id);
            resolve();
          } else if (result.error === undefined) {
            console.log('DOCUMENTO ENCONTRADO', result);
            _this.diario = result;

            if (!_this.diario.sono) {
              _this.diario.sono = _this.sono;
            }

            _this._definirItemAtual();
            _this._changeDetectorRef.markForCheck();
            resolve();
          } else {
            console.log('ERRO DESCONHECIMENTO', result);
            // Tratar este erro de maneira que o usuário entenda e compreenda
            // Para isso, tratar junta com a Rosie no "chatbox"
            reject();
          }
        },
      );
    });
  }

  private _inicializarValoresTempo() {
    this._dataAtual = (!this._navParams.get('diarioSaudeMoment') ? moment() : this._navParams.get('diarioSaudeMoment'));
    this._DIARIO_ID = SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID + this._dataAtual.date() + '-' + this._dataAtual.month() + '-' + this._dataAtual.year();

    this._dataAnterior = moment(this._dataAtual).subtract(1, 'days');
    this._diaAtual = parseInt(this._dataAtual.format('D'), 10);
    this._mesAtual = parseInt(this._dataAtual.format('M'), 10) - 1;
    this._anoAtual = parseInt(this._dataAtual.format('YYYY'), 10);
    this._diaAnterior = parseInt(this._dataAnterior.format('D'), 10);
    this._mesDoDiaAnterior = parseInt(this._dataAnterior.format('M'), 10) - 1;
  }

  private _definirPeriodoMinSono() {
    let minTime: any = new Date(this._anoAtual, this._mesDoDiaAnterior, this._diaAnterior, 0, 0, 0);
    this.minDate = moment(minTime).format(AppHelper.FORMATO_DATA_ISO_8601);
  }

  private _definirPeriodoMaxSono() {
    let maxTime: any = new Date(this._anoAtual, this._mesAtual, this._diaAtual, 23, 59, 59);
    this.maxDate = moment(maxTime).format(AppHelper.FORMATO_DATA_ISO_8601);
  }

  private _definirItemAtual() {
    this.items.every(
      (item: any) => {
        if (this.diario.sono.qualidadeSubjetiva === item.qualidadeSubjetiva) {
          this.itemAtual = item;
          this._changeDetectorRef.markForCheck();
          return false;
        } else {
          return true;
        }
      },
    );
  }

  private _inicializarValorSono() {
    let iniTime: any = new Date(this._anoAtual, this._mesDoDiaAnterior, this._diaAnterior, 22, 0, 0);
    this.diario.sono.inicio = moment(iniTime).format(AppHelper.FORMATO_DATA_ISO_8601);

    let maxTime: any = new Date(this._anoAtual, this._mesAtual, this._diaAtual, 6, 0, 0);
    this.diario.sono.fim = moment(maxTime).format(AppHelper.FORMATO_DATA_ISO_8601);
  }

  private _obterMensagens() {
    this._translate.get('REGISTRO_SONO.MSG.ERRO_CADASTRO').subscribe(
      (value: string) => {
        this.MSG_ERRO_CADASTRO = value;
      },
    );
  }

}
