import { ApiProvider } from './../../providers/api/api';
import { Observable, Subscription } from 'rxjs';
import { Component, ViewChild, trigger, transition, style, animate, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ActionSheet, ActionSheetController, Platform } from 'ionic-angular';
import { JwtHelper } from 'angular2-jwt';

@IonicPage()
@Component({
    selector: 'ib-page-chat',
    templateUrl: 'chat.html',
    changeDetection: ChangeDetectionStrategy.OnPush,
    animations: [
        trigger(
            'enterAnimation', [
                transition(':enter', [
                    style({ transform: 'translateX(100%)', opacity: 0 }),
                    animate('500ms', style({ transform: 'translateX(0)', opacity: 1 })),
                ]),
            ],
        ),
    ],
})
export class ChatPage {

    @ViewChild(Content) content: Content;

    public opcoesPaciente: Array<any> = [];
    public messages: Array<any> = [];
    /*private _dialogo: Array<any> = [
        {
            message: 'Olá! Eu sou a Rosie.',
            from: 'rosie',
        },
        {
            message: 'Qual é o seu nome?',
            from: 'rosie',
        },
        {
            message: 'Daniel',
            from: 'paciente',
            payload: 'payload_1',
        },
        {
            message: 'Oi Daniel. Prazer em conhece-lo!',
            from: 'rosie',
        },
        {
            message: 'Igualmente :)',
            from: 'paciente',
            payload: 'payload_2',
        },
        {
            message: 'Então, você gosta de basquete?',
            from: 'rosie',
        },
        {
            message: 'Sim',
            from: 'paciente',
            payload: 'payload_3',
        },
        {
            message: 'Hmmm',
            from: 'rosie',
        },
        {
            message: 'E de futebol?',
            from: 'rosie',
        },
        {
            message: 'Também',
            from: 'paciente',
            payload: 'payload_4',
        },
        {
            message: 'Ué, mas você tem algum esporte favorito?',
            from: 'rosie',
        },
        {
            message: 'Vai saber',
            from: 'paciente',
            payload: 'payload_5',
        },
        {
            message: 'Ok Daniel.',
            from: 'rosie',
        },
        {
            message: 'Até a próxima.',
            from: 'rosie',
        },
        {
            message: 'E uma última coisa...',
            from: 'rosie',
        },
        {
            message: 'Sim?',
            from: 'paciente',
            payload: 'payload_6',
        },
        {
            message: 'Não esqueça de treinar para dar um pau no gordo!',
            from: 'rosie',
        },
        {
            message: 'Pode deixar',
            from: 'paciente',
            payload: 'payload_7',
        },
        {
            message: 'Conto com você!',
            from: 'rosie',
        },
    ];*/

    private _messageTimer: Subscription;
    private _autoSroll: Subscription;
    private _endpoint: string = 'https://service.us.apiconnect.ibmcloud.com/gws/apigateway/api/321c788a37912aeded69af2b85a98a3abc4dc268e1cfc3e3e512848d903e0d31/chatbot/bot/webhook';

    constructor(private _changeDetectorRef: ChangeDetectorRef, private _platform: Platform, private _actionSheetCtrl: ActionSheetController,
        public navCtrl: NavController, public navParams: NavParams, private _api: ApiProvider) {
    }

    ionViewDidLoad() {
    }

    ionViewDidEnter() {
        this.content.resize();

        let payload: any = {
            postback: {
                payload: 'EVENT_GET_STARTED',
            },
        };
        this._executarDialogo(payload);
    }

    onClickDiario() {
        this.navCtrl.setRoot('DiarioPage');
    }

    onTapSendPatientMessage(opcao: any) {
        this._addMensagemPaciente(opcao);
    }

    public onClickOpenChooser() {
        let actionSheet: ActionSheet = this._actionSheetCtrl.create({
            title: 'Novo Chat',
            cssClass: 'action-sheets-basic-page',
            buttons: [
                {
                    text: 'Diga me...',
                    icon: !this._platform.is('ios') ? 'information-circle' : null,
                    handler: () => {
                        // Handler
                    },
                },
                {
                    text: 'Adicionar dados',
                    icon: !this._platform.is('ios') ? 'checkmark-circle' : null,
                    handler: () => {
                        // Handler
                    },
                },
                {
                    text: 'Me sinto...',
                    icon: !this._platform.is('ios') ? 'happy' : null,
                    handler: () => {
                        // Handler
                    },
                },
            ],
        });
        actionSheet.present();
    }

    private async _executarDialogo(payload: any) {
        let response: Array<any> = [];

        await this._callSendApi(payload).then(
            (res: Array<any>) => {
                if (res instanceof Array) {
                    response = res;
                }
            },
        );

        for (let i = 0; i < response.length; ++i) {
            let responseMessage: any = response[i];
            if (responseMessage.from === 'rosie') {
                this.messages.push({
                    from: responseMessage.from,
                    message: null,
                });
                this._changeDetectorRef.detectChanges();

                await this._addMensagemRosie(responseMessage.text);
                this._updateScroll();
            } else if (responseMessage.from === 'paciente') {
                let opcaoPaciente: any = {
                    message: responseMessage.text,
                    payload: responseMessage.payload,
                    from: responseMessage.from,
                };

                this.opcoesPaciente.push(opcaoPaciente);
                console.log('opcoes', this.opcoesPaciente);
                this.content.resize();
                this._changeDetectorRef.detectChanges();
                this._updateScroll();
            } else {
                console.log('mensagem não reconhecida');
            }
        }
    }

    private _callSendApi(payload: any) {
        let scope: ChatPage = this;

        return new Promise(
            (resolve) => {
                scope._api.post(scope._endpoint, payload)
                    .map(res => res.json())
                    .subscribe(
                    (response: any) => {
                        console.log('response API', response);
                        resolve(response);
                    },
                );
            },
        );
    }

    private _addMensagemRosie(message: string): Promise<any> {
        let scope: ChatPage = this;

        return new Promise(
            (resolve, reject) => {
                let timerMillis: number = scope._getRandomInt(1000, 3000);
                let messageTimer: Observable<any> = Observable.timer(timerMillis);
                scope._messageTimer = messageTimer.subscribe(
                    (x: any) => {
                        console.log('Next: ' + x);

                        let index: number = scope.messages.length - 1;
                        scope.messages[index].message = message;
                        scope._changeDetectorRef.detectChanges();

                        scope._updateScroll();
                    },
                    (err: any) => {
                        console.log('Error: ' + err);
                        reject();
                    },
                    () => {
                        scope._messageTimer.unsubscribe();
                        resolve();
                        // this._executarDialogo();
                    },
                );
            },
        );
    }

    private _addMensagemPaciente(opcaoPaciente: any) {
        this.messages.push(opcaoPaciente);
        this._changeDetectorRef.detectChanges();
        this._updateScroll();

        this.opcoesPaciente = [];
        this.content.resize();
        this._changeDetectorRef.detectChanges();
        this._updateScroll();

        let token: any = null;
        try {
            token = new JwtHelper().decodeToken(opcaoPaciente.payload);
        } catch (err) {
            token = null; // token não é um JWT
        }

        let isInternAction: boolean = false;
        if (token && token.actions) {
            token.actions.every(
                (action: any) => {
                    if (action.type === 'app') {
                        this._runAction(action.name);
                        isInternAction = true;
                        return false;
                    } else {
                        return true;
                    }
                },
            );
        }

        if (!isInternAction) {
            // run event
            let payload: any = {
                postback: {
                    payload: opcaoPaciente.payload,
                },
            };
            this._executarDialogo(payload);
        }
    }

    private _runAction(name: string) {
        switch (name) {
            case 'CRISIS_REGISTER':
                alert('executar ação' + name);
                break;
            default:
                console.log('ação desconhcida', name);
                break;
        }
    }

    private _updateScroll() {
        let autoSroll: Observable<any> = Observable.timer(100);
        this._autoSroll = autoSroll.subscribe(
            () => {
                this.content.scrollToBottom();
            },
            () => {

            },
            () => {
                this._autoSroll.unsubscribe();
            },
        );
    }

    private _getRandomInt(min: number, max: number) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}
