import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Diario } from './../../core/models/diario.model';
import { ModeloAlimento } from './../../core/models/pojo/alimento.pojo';
import { ConfiguracaoNavBar } from './../../core/models/pojo/configuracao-nav-bar.pojo';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, FabContainer, ModalController, Events, Modal } from 'ionic-angular';
import findIndex from 'lodash-es/findIndex';
import clone from 'lodash-es/clone';
import * as moment from 'moment';
import 'moment/min/locales';

@IonicPage()
@Component({
  selector: 'ib-page-registro-alimento',
  templateUrl: 'registro-alimento.html',
})
export class RegistroAlimentoPage {

  public readonly MODO_ADD: number = 0;
  public readonly MODO_EDICAO: number = 1;
  public readonly MODO_DELETE: number = 2;

  public descricaoDataAtual: string = null;
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public navBarConfigurado: boolean = false;
  public modoRegistro: number = this.MODO_ADD;
  public arrayOpcoesDoGrid: Array<ModeloAlimento> = [];
  public alimentosSelecionados: Array<ModeloAlimento> = [];

  public CAFE_DA_MANHA: number = 0;
  public ALMOCO: number = 1;
  public JANTAR: number = 2;
  public LANCHES: number = 3;

  public categoriasNome: Array<string> = [];

  @ViewChild(Slides) slideCategorias: Slides;
  @ViewChild('fab') fab: FabContainer;

  public categoriaAtual: number = this.CAFE_DA_MANHA;
  public categorias: Array<any> = [];
  public bindOpcoesDoGrid: Array<Array<ModeloAlimento>> = [];

  private readonly NUM_COLUNS: number = 3;
  private _DIARIO_ID: string = null;
  private _diario: Diario = {
    _id: null,
    alimentos: [],
    sono: null,
    passos: 0,
    googleFitSync: false,
    type: 'diario-saude',
  };
  private _documentoOpcoesAlimento: any = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_ALIMENTOS_ID,
    opcoes: [],
  };

  constructor(private _modalCtrl: ModalController, private _syncDataProvider: SyncDataProvider,
    private _events: Events, private _navCtrl: NavController, private _navParams: NavParams) {

    this._iniciar();
  }

  ionViewWillEnter() {
    this.slideCategorias.update();
  }

  public onClickAcoesGridAlimentos(alimento: ModeloAlimento) {
    switch (this.modoRegistro) {
      case this.MODO_ADD:
        this._selecionarOuDesmarcarOpcao(alimento);
        break;
      case this.MODO_EDICAO:
        let parametro: any = { 'opcoes': this._documentoOpcoesAlimento, 'modeloAlimento': alimento };
        this._abrirModalEditar(parametro);
        break;
      case this.MODO_DELETE:
        this._removerOpcao(alimento);
        break;
      default:
        break;
    }
  }

  public onChangeCategoria(categoria: any) {
    this.categoriaAtual = categoria;
    this._atualizarLista();
  }

  public onClickModoAdd() {
    if (this.modoRegistro === this.MODO_EDICAO) {
      this._events.publish('ativarModoEdicao:' + this.configuracaoNavBar.getIdentificadorGrid());
    }

    if (this.modoRegistro === this.MODO_DELETE) {
      this._events.publish('ativarModoRemover:' + this.configuracaoNavBar.getIdentificadorGrid());
    }

    this.modoRegistro = this.MODO_ADD;
  }

  public onClickModoDelete() {
    this.modoRegistro = this.MODO_DELETE;
    this._events.publish('ativarModoRemover:' + this.configuracaoNavBar.getIdentificadorGrid());

    this.fab.close();
  }

  public onClickModoEdicao() {
    this.modoRegistro = this.MODO_EDICAO;
    this._events.publish('ativarModoEdicao:' + this.configuracaoNavBar.getIdentificadorGrid());

    this.fab.close();
  }

  public onClickAddNovoAlimento() {
    this.fab.close();

    let parametro: any = { 'opcoes': this._documentoOpcoesAlimento };
    this._abrirModalEditar(parametro);
  }

  public isModoAdd() {
    return (this.modoRegistro === this.MODO_ADD);
  }

  public isModoEdicao() {
    return (this.modoRegistro === this.MODO_EDICAO);
  }

  public isModoDelete() {
    return (this.modoRegistro === this.MODO_DELETE);
  }

  public getCategoriaAtual() {
    return this.categoriasNome[this.categoriaAtual];
  }

  public onClickAddAlimentos() {
    this._prepararDados().then(
      () => {
        this._syncDataProvider.addOrUpdate(this._diario).then(
          () => {
            this._navCtrl.pop();
          },
          (err: any) => {
            console.log('ERRO NO CADASTRO', err);
          },
        );
      },
    );
  }

  public onClickAddPrimeiroAlimento() {
    this.fab.toggleList();
  }

  public isEnableConfirmButton(): boolean {
    return (this.alimentosSelecionados.length === 0 || !this.isModoAdd());
  }

  public retirarModeloAlimentoDoArray(array: Array<any>, opcao: ModeloAlimento): Array<any> {
    array.every(
      (element: any, indice: number) => {
        if (element.nome === opcao.getNome()) {
          array.splice(indice, 1);
          return false;
        } else {
          return true;
        }
      },
    );

    return array;
  }

  public retirarModeloOpcaoDoArraySelecionado(modeloOpcao: ModeloAlimento): Array<ModeloAlimento> {

    this.alimentosSelecionados.every(
      (opcao: ModeloAlimento, indice: number) => {
        if (opcao.getId() === modeloOpcao.getId()) {
          this.alimentosSelecionados.splice(indice, 1);
          return false;
        } else {
          return true;
        }
      },
    );

    return this.alimentosSelecionados;
  }

  private _atualizarOpcaoDoGrid(modeloAlimentoParaAtualizar: ModeloAlimento) {
    let indiceParaAtualizar: number = -1;
    this.arrayOpcoesDoGrid.every(
      (modeloOpcaoGrid: ModeloAlimento, i: number) => {
        if (modeloOpcaoGrid.getId() === modeloAlimentoParaAtualizar.getId()) {
          indiceParaAtualizar = i;
          return false;
        } else {
          return true;
        }
      },
    );

    if (indiceParaAtualizar > -1) {
      this.arrayOpcoesDoGrid[indiceParaAtualizar] = modeloAlimentoParaAtualizar;
    }

    this._atualizarLista();
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('REGISTRO_ALIMENTO.TITULO_INICIAL');
    this.configuracaoNavBar.setTextoModoRemover('REGISTRO_ALIMENTO.TITULO_REMOVER');
    this.configuracaoNavBar.setTextoModoEdicao('REGISTRO_ALIMENTO.TITULO_EDICAO');
    this.configuracaoNavBar.setIdentificadorGrid('idGridAlimentos');

    this.navBarConfigurado = true;
  }

  private _abrirModalEditar(parametro: any) {
    let modalNovoAlimento: Modal = this._modalCtrl.create('NovoAlimentoPage', parametro);
    modalNovoAlimento.present();
    modalNovoAlimento.onDidDismiss(
      (data: any) => {
        this._modalAlimentoDissmiss(data);
      },
    );
  }

  private _atualizarStatusOpcao(array: Array<any>, opcao: ModeloAlimento): Array<any> {
    array.every(
      (element: any) => {
        if (element.nome === opcao.getNome()) {
          element.ativo = false;
          return false;
        } else {
          return true;
        }
      },
    );

    return array;
  }

  private _removerOpcao(alimento: ModeloAlimento) {
    let cloneModeloAlimento: ModeloAlimento = clone(alimento);

    this._diario.alimentos = this.retirarModeloAlimentoDoArray(
      this._diario.alimentos,
      cloneModeloAlimento,
    );

    this.retirarModeloOpcaoDoArraySelecionado(cloneModeloAlimento);
    this._documentoOpcoesAlimento.opcoes = this._atualizarStatusOpcao(this._documentoOpcoesAlimento.opcoes, cloneModeloAlimento);

    let manyDocuments: any = [this._documentoOpcoesAlimento, this._diario];

    this._syncDataProvider.addOrUpdateManyDocuments(manyDocuments);
    this._montarOpcoesDoGrid();
  }

  private _prepararDados(): Promise<any> {
    let _this: RegistroAlimentoPage = this;

    return new Promise(function (resolve) {

      _this._addOpcao();
      _this._retirarOpcao();

      resolve();
    });
  }

  private _addOpcao() {
    for (let i = 0; i < this.alimentosSelecionados.length; i++) {
      let opcaoSelecionada = this.alimentosSelecionados[i];
      let index: number = -1;

      if (this._diario.alimentos) {
        index = findIndex(
          this._diario.alimentos,
          function (opcao: any) {
            return opcao._id === opcaoSelecionada.getId();
          },
        );
      }

      if (index < 0) {
        let novaOpcao: any = this._converterModeloAlimentoParaAlimento(opcaoSelecionada);
        this._diario.alimentos.push(novaOpcao);
      }
    }
  }

  private _retirarOpcao() {
    let opcoesDiario: any = this._diario.alimentos;

    for (let i = opcoesDiario.length - 1; i >= 0; i -= 1) {
      let indiceOpcaoSelecionada: number = -1;
      indiceOpcaoSelecionada = findIndex(
        this.alimentosSelecionados,
        function (o: ModeloAlimento) {
          return o.getId() === opcoesDiario[i]._id;
        },
      );

      if (indiceOpcaoSelecionada === -1) {
        opcoesDiario.splice(i, 1);
      }
    }
  }

  private _converterModeloAlimentoParaAlimento(opcaoSelecionada: ModeloAlimento) {
    let alimento: any = {};
    alimento._id = opcaoSelecionada.getId();
    alimento.nome = opcaoSelecionada.getNome();
    alimento.dataCriacao = opcaoSelecionada.getDataCriacao();
    alimento.ativo = opcaoSelecionada.isAtivo();
    alimento.cafeDaManha = opcaoSelecionada.isCafeDaManha();
    alimento.almoco = opcaoSelecionada.isAlmoco();
    alimento.jantar = opcaoSelecionada.isJantar();
    alimento.lanches = opcaoSelecionada.isLanches();

    return alimento;
  }

  private _selecionarOuDesmarcarOpcao(modeloAlimento: ModeloAlimento) {
    modeloAlimento.setSelecionado(!modeloAlimento.isSelecionado());

    if (modeloAlimento.isSelecionado()) {
      this._selecionarOpcao(modeloAlimento);
    } else {
      this._desmarcarOpcao(modeloAlimento);
    }
  }

  private _selecionarOpcao(modeloAlimento: ModeloAlimento) {
    let cloneModeloAlimento: ModeloAlimento = clone(modeloAlimento);
    this.alimentosSelecionados.push(cloneModeloAlimento);
  }

  private _desmarcarOpcao(modeloAlimento: ModeloAlimento) {
    let cloneModeloAlimento: ModeloAlimento = clone(modeloAlimento);

    this.retirarModeloOpcaoDoArraySelecionado(cloneModeloAlimento);

    this._atualizarOpcaoDoGrid(cloneModeloAlimento);
  }

  private _modalAlimentoDissmiss(data: any) {
    if (data) {

      if (data.opcoes) {
        this._documentoOpcoesAlimento = data.opcoes;
      }

      if (data.modeloAlimento) {
        // add no array selecionados
      }

      this._montarOpcoesDoGrid();
      // this.opcoesAlimento.unshift(alimento);

    }
  }

  private _iniciar() {
    moment.locale('pt-BR');
    this.categoriasNome = [
      'REGISTRO_ALIMENTO.CATEGORIA.CAFE_DA_MANHA',
      'REGISTRO_ALIMENTO.CATEGORIA.ALMOCO',
      'REGISTRO_ALIMENTO.CATEGORIA.JANTAR',
      'REGISTRO_ALIMENTO.CATEGORIA.LANCHES',
    ];
    this.categorias = [
      {
        nome: this.categoriasNome[this.CAFE_DA_MANHA],
        cor: 'bg-cafe-da-manha',
        icone: 'rosie-alimentos-cafe',
        cat: this.CAFE_DA_MANHA,
      },
      {
        nome: this.categoriasNome[this.ALMOCO],
        cor: 'bg-almoco',
        icone: 'rosie-alimentos-almoco',
        cat: this.ALMOCO,
      },
      {
        nome: this.categoriasNome[this.JANTAR],
        cor: 'bg-jantar',
        icone: 'rosie-alimentos-jantar',
        cat: this.JANTAR,
      },
      {
        nome: this.categoriasNome[this.LANCHES],
        cor: 'bg-lanches',
        icone: 'rosie-alimentos-lanches',
        cat: this.LANCHES,
      },
    ];

    this._iniciarDadosDeTempo();
    this._configurarNavBar();

    this._consultarOpcoes().then(
      () => {
        this._consultarAlimentos().then(
          () => {
            this._montarOpcoesDoGrid();
          },
          () => {
            // Tratar este erro de maneira que o usuário entenda e compreenda
            // Para isso, tratar junta com a Rosie no "chatbox"
          },
        );
      },
      () => {
        // Tratar este erro de maneira que o usuário entenda e compreenda
        // Para isso, tratar junta com a Rosie no "chatbox"
      },
    );
  }

  private _iniciarDadosDeTempo() {
    let momentDiarioSaude: any = (!this._navParams.get('diarioSaudeMoment') ? moment() : this._navParams.get('diarioSaudeMoment'));
    this._DIARIO_ID = SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID + momentDiarioSaude.date() + '-' + momentDiarioSaude.month() + '-' + momentDiarioSaude.year();
    this.descricaoDataAtual = momentDiarioSaude.format('dddd, LL');
  }

  private _consultarAlimentos(): Promise<any> {
    let _this: RegistroAlimentoPage = this;

    return new Promise(function (resolve, reject) {
      _this._syncDataProvider.getDocumentById(_this._DIARIO_ID).then(
        (result: any) => {
          if (result.error && result.status === 404) {
            // Documento vazio
            _this._diario._id = _this._DIARIO_ID;
            resolve();
          } else if (result.error === undefined) {
            _this._diario = result;
            resolve();
          } else {
            console.log('ERRO DESCONHECIMENTO', result);
            // Apenas capturar o erro com a ferramenta
            reject();
          }
        },
      );
    });
  }

  private _consultarOpcoes(): Promise<any> {
    let _this: RegistroAlimentoPage = this;

    return new Promise(function (resolve, reject) {
      _this._syncDataProvider.getDocumentById(SyncDataProvider.DOCUMENT_OPCOES_ALIMENTOS_ID).then(
        (result: any) => {
          if (result.error && result.status === 404) {
            console.log('Não foi encontrado o documento com o id passado');
            // Tratamento
            // Documento não existe
            resolve();
          } else if (result.error === undefined) {
            _this._documentoOpcoesAlimento = result;
            resolve();
          } else {
            console.log('ERRO DESCONHECIMENTO', result);
            // Apenas capturar o erro com a ferramenta
            reject();
          }
        },
      );
    });
  }

  private _montarOpcoesDoGrid() {
    this.arrayOpcoesDoGrid = [];

    this._documentoOpcoesAlimento.opcoes.forEach(
      (opcaoAlimento: any) => {
        if (opcaoAlimento.ativo) {
          let modeloAlimento: ModeloAlimento = new ModeloAlimento();
          modeloAlimento.setId(opcaoAlimento._id);
          modeloAlimento.setNome(opcaoAlimento.nome);
          modeloAlimento.setDataCriacao(opcaoAlimento.dataCriacao);
          modeloAlimento.setAtivo(opcaoAlimento.ativo);
          modeloAlimento.setCafeDaManha(opcaoAlimento.cafeDaManha);
          modeloAlimento.setAlmoco(opcaoAlimento.almoco);
          modeloAlimento.setJantar(opcaoAlimento.jantar);
          modeloAlimento.setLanches(opcaoAlimento.lanches);
          modeloAlimento.definirIcone(this.categoriaAtual);

          this._diario.alimentos.every(
            (opcao: any) => {
              if (opcao._id === modeloAlimento.getId()) {
                modeloAlimento.setSelecionado(true);

                let index: number = -1;
                index = findIndex(
                  this.alimentosSelecionados,
                  function (opcaoSelecionada: ModeloAlimento) {
                    return modeloAlimento.getId() === opcaoSelecionada.getId();
                  },
                );

                if (index < 0) {
                  this.alimentosSelecionados.push(modeloAlimento);
                }

                return false;
              } else {
                return true;
              }
            },
          );

          this.alimentosSelecionados.every(
            (opcao: ModeloAlimento) => {
              if (opcao.getId() === modeloAlimento.getId()) {
                modeloAlimento.setSelecionado(true);
                return false;
              } else {
                return true;
              }
            },
          );

          this.arrayOpcoesDoGrid.push(modeloAlimento);
        }
      },
    );

    this._atualizarLista();
  }

  private _buildGrid(opcoesGrid: Array<ModeloAlimento>) {

    let indiceColunaRemovida: number = -1;
    let numeroDeLinhas = Math.ceil(opcoesGrid.length / this.NUM_COLUNS);
    this.bindOpcoesDoGrid = Array(numeroDeLinhas);
    let dataIndex = 0;
    for (let linha = 0; linha < numeroDeLinhas; linha++) {
      this.bindOpcoesDoGrid[linha] = Array(this.NUM_COLUNS);
      for (let coluna: number = 0; coluna < this.NUM_COLUNS; coluna++) {
        if (opcoesGrid[dataIndex]) {
          this.bindOpcoesDoGrid[linha][coluna] = opcoesGrid[dataIndex];
          dataIndex++;
        } else {
          if (!(indiceColunaRemovida > -1)) {
            indiceColunaRemovida = coluna;
          }
          this.bindOpcoesDoGrid[linha].splice(indiceColunaRemovida, 1);
        }
      }
    }
  }

  private _atualizarLista() {
    if (this.arrayOpcoesDoGrid.length > 0) {
      this._filtrarOpcoes().then(
        (opcoesGrid: Array<ModeloAlimento>) => {
          this._buildGrid(opcoesGrid);
        },
      );
    }
  }

  private _filtrarOpcoes(): Promise<any> {
    let _this: RegistroAlimentoPage = this;

    return new Promise(function (resolve) {
      let opcoesGrid: Array<ModeloAlimento> = [];

      _this.arrayOpcoesDoGrid.forEach(
        (modeloAlimento: ModeloAlimento) => {
          if (modeloAlimento.getValueCategoryStatusByType(_this.categoriaAtual)) {
            modeloAlimento.definirIcone(_this.categoriaAtual);
            opcoesGrid.push(modeloAlimento);
          }
        },
      );

      resolve(opcoesGrid);
    });
  }

}
