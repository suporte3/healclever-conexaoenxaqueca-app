import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from './../../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NovoAlimentoPage } from './novo-alimento';

@NgModule({
  declarations: [
    NovoAlimentoPage,
  ],
  imports: [
    IonicPageModule.forChild(NovoAlimentoPage),
    PipesModule,
    TranslateModule,
  ],
})
export class NovoAlimentoPageModule { }
