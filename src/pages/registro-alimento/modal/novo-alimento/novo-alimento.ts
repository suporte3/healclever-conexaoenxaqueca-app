import { TranslateService } from '@ngx-translate/core';
import { ModeloAlimento } from './../../../../core/models/pojo/alimento.pojo';
import { AppHelper } from './../../../../core/helpers/app.helper';
import { ValidationHelper } from './../../../../core/helpers/validation.helper';
import { SyncDataProvider } from './../../../../providers/sync-data/sync-data';
import { Opcoes } from './../../../../core/models/opcoes.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Alimento } from './../../../../core/models/alimento.model';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavParams, ViewController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-novo-alimento',
  templateUrl: 'novo-alimento.html',
})
export class NovoAlimentoPage implements OnInit {

  public CAFE_DA_MANHA: number = 0;
  public ALMOCO: number = 1;
  public JANTAR: number = 2;
  public LANCHES: number = 3;

  public submitted: boolean = false;
  public categorias: Array<any> = [];
  public modoEdicao: boolean = false;
  public modeloAlimento: Alimento = {
    _id: null,
    nome: null,
    dataCriacao: null,
    cafeDaManha: false,
    almoco: false,
    jantar: false,
    lanches: false,
    ativo: true,
  };
  @ViewChild('novoAlimentoForm') novoAlimentoForm: FormGroup;

  private MSG_ERRO_CADASTRO: string = null;
  private _opcoesAlimentos: Opcoes = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_ALIMENTOS_ID,
    opcoes: [],
  };

  constructor(private _translate: TranslateService, private _viewCtrl: ViewController, private _syncDataProvider: SyncDataProvider,
    private _navParams: NavParams, private _formBuilder: FormBuilder, private _toastCtrl: ToastController) {

    this._iniciar();
  }

  ngOnInit() {

    let validatorsArray: Array<any> = [
      Validators.required,
      Validators.minLength(3),
    ];

    if (!this.modoEdicao) {
      validatorsArray.push(ValidationHelper.opcaoJaExiste.bind(this, this._opcoesAlimentos.opcoes));
    }

    this.novoAlimentoForm = this._formBuilder.group({
      nome: [
        '',
        Validators.compose(validatorsArray),
      ],
    });
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  public onClickRegistrar() {
    this.submitted = true;
    if (this.novoAlimentoForm.valid && !this.categoriaIsEmpty()) {

      let appHelper: AppHelper = new AppHelper();
      let opcaoAlimento: any = this.modeloAlimento;
      if (this.modoEdicao) {
        this._atualizarCategorias(opcaoAlimento);

        let indice: number = -1;
        this._opcoesAlimentos.opcoes.every(
          (opcao: any, i: number) => {
            if (opcao._id === opcaoAlimento._id) {
              indice = i;
              return false;
            } else {
              return true;
            }
          },
        );

        if (indice > -1) {
          this._opcoesAlimentos.opcoes[indice] = opcaoAlimento;
        }
      } else {
        let opcaoInativa: any = appHelper.isOpcaoInativa(opcaoAlimento.nome, this._opcoesAlimentos.opcoes);
        if (opcaoInativa) {
          opcaoAlimento = appHelper.ativarOpcao(opcaoInativa);
          this._atualizarCategorias(opcaoAlimento);
        } else {
          opcaoAlimento = appHelper.criarNovaOpcao(opcaoAlimento.nome);
          delete opcaoAlimento.tipo;

          this._atualizarCategorias(opcaoAlimento);
          this._opcoesAlimentos.opcoes.push(opcaoAlimento);
        }
      }

      let modeloAlimento: ModeloAlimento = new ModeloAlimento().criarNovoAlimento(opcaoAlimento);

      this._syncDataProvider.addOrUpdate(this._opcoesAlimentos).then(
        () => {
          this._viewCtrl.dismiss({ 'opcoes': this._opcoesAlimentos, 'modeloAlimento': modeloAlimento });
        },
        (err: any) => {
          console.log('ERRO AO CADASTRAR NOVO ALIMENTO', err);
          appHelper.toastMensagem(this._toastCtrl, this.MSG_ERRO_CADASTRO);
          appHelper.retirarOpcaoDoArray(this._opcoesAlimentos.opcoes, opcaoAlimento);
          this.onClickCancelar();
          // TRACK
        },
      ).catch(
        (e: any) => {
          console.log('erro catch novo alimento', e);
        },
      );

    } else {
      // Executar validação
    }
  }

  public categoriaIsEmpty() {
    let isEmpty: boolean = false;
    if (this.submitted) {
      this.categorias.every(
        (categoria: any) => {
          if (categoria.model) {
            isEmpty = false;
            return false;
          } else {
            isEmpty = true;
            return true;
          }
        },
      );
    }

    return isEmpty;
  }

  private _iniciar() {
    this._obterMensagens();

    let opcoes: any = this._navParams.get('opcoes');
    if (opcoes) {
      this._opcoesAlimentos = opcoes;
    }

    let modeloAlimento: ModeloAlimento = this._navParams.get('modeloAlimento');
    if (modeloAlimento) {
      this.modeloAlimento._id = modeloAlimento.getId();
      this.modeloAlimento.nome = modeloAlimento.getNome();
      this.modeloAlimento.dataCriacao = modeloAlimento.getDataCriacao();
      this.modeloAlimento.ativo = modeloAlimento.isAtivo();
      this.modeloAlimento.cafeDaManha = modeloAlimento.isCafeDaManha();
      this.modeloAlimento.almoco = modeloAlimento.isAlmoco();
      this.modeloAlimento.jantar = modeloAlimento.isJantar();
      this.modeloAlimento.lanches = modeloAlimento.isLanches();

      this.modoEdicao = true;
    }

    this.categorias = [
      {
        nome: 'REGISTRO_ALIMENTO.CATEGORIA.CAFE_DA_MANHA',
        model: this.modeloAlimento.cafeDaManha,
      },
      {
        nome: 'REGISTRO_ALIMENTO.CATEGORIA.ALMOCO',
        model: this.modeloAlimento.almoco,
      },
      {
        nome: 'REGISTRO_ALIMENTO.CATEGORIA.JANTAR',
        model: this.modeloAlimento.jantar,
      },
      {
        nome: 'REGISTRO_ALIMENTO.CATEGORIA.LANCHES',
        model: this.modeloAlimento.lanches,
      },
    ];
  }

  private _atualizarCategorias(opcaoAlimento: any) {
    opcaoAlimento.cafeDaManha = this.categorias[this.CAFE_DA_MANHA].model;
    opcaoAlimento.almoco = this.categorias[this.ALMOCO].model;
    opcaoAlimento.jantar = this.categorias[this.JANTAR].model;
    opcaoAlimento.lanches = this.categorias[this.LANCHES].model;

    return opcaoAlimento;
  }

  private _obterMensagens() {
    this._translate.get('NOVO_ALIMENTO.MSG.ERRO_CADASTRO').subscribe(
      (value: string) => {
        this.MSG_ERRO_CADASTRO = value;
      },
    );
  }

}
