import { Alimento } from './../../core/models/alimento.model';
import { FabContainerMock } from './../../core/mocks/app.mock';
import { ModeloAlimento } from './../../core/models/pojo/alimento.pojo';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { NavParamsMock } from './../../core/mocks/nav-params.mock';
import { TestUtils } from './../../app/app.test';
import { RegistroAlimentoPage } from './registro-alimento';
import { ModalController, Modal, NavParams } from 'ionic-angular';
import { ComponentFixture, async, fakeAsync, tick } from '@angular/core/testing';
import * as moment from 'moment';

describe('Registro Alimento Page:', () => {

    let CAFE_DA_MANHA: number = 0;
    let ALMOCO: number = 1;
    let JANTAR: number = 2;
    let LANCHES: number = 3;

    let fixture: ComponentFixture<RegistroAlimentoPage> = null;
    let instance: any = null;
    let _instance: any = RegistroAlimentoPage.prototype;

    let spyConsultarOpcoes: jasmine.Spy;
    let spyConsultarAlimentos: jasmine.Spy;
    let spyMontarOpcoesGrid: jasmine.Spy;
    let spyConfigurarNavBar: jasmine.Spy;
    let spyIniciarDadosTempo: jasmine.Spy;

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.addProvider(ModalController);
        TestUtils.beforeEachCompiler([RegistroAlimentoPage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
        spyConsultarOpcoes = spyOn(_instance, '_consultarOpcoes').and.returnValue(Promise.resolve());
        spyConsultarAlimentos = spyOn(_instance, '_consultarAlimentos').and.returnValue(Promise.resolve());
        spyMontarOpcoesGrid = spyOn(_instance, '_montarOpcoesDoGrid').and.stub();
        spyConfigurarNavBar = spyOn(_instance, '_configurarNavBar').and.stub();
        spyIniciarDadosTempo = spyOn(_instance, '_iniciarDadosDeTempo').and.stub();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o componente ao carregar', () => {
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#_iniciar - Deve iniciar corretamente', fakeAsync(() => {
        let categoriasNome: Array<any> = [
            'REGISTRO_ALIMENTO.CATEGORIA.CAFE_DA_MANHA',
            'REGISTRO_ALIMENTO.CATEGORIA.ALMOCO',
            'REGISTRO_ALIMENTO.CATEGORIA.JANTAR',
            'REGISTRO_ALIMENTO.CATEGORIA.LANCHES',
        ];
        let categorias: Array<any> = [
            {
                nome: categoriasNome[CAFE_DA_MANHA],
                cor: 'bg-cafe-da-manha',
                icone: 'rosie-alimentos-cafe',
                cat: CAFE_DA_MANHA,
            },
            {
                nome: categoriasNome[ALMOCO],
                cor: 'bg-almoco',
                icone: 'rosie-alimentos-almoco',
                cat: ALMOCO,
            },
            {
                nome: categoriasNome[JANTAR],
                cor: 'bg-jantar',
                icone: 'rosie-alimentos-jantar',
                cat: JANTAR,
            },
            {
                nome: categoriasNome[LANCHES],
                cor: 'bg-lanches',
                icone: 'rosie-alimentos-lanches',
                cat: LANCHES,
            },
        ];

        instance.categoriasNome = [];
        instance.categorias = [];

        instance._iniciar();
        expect(instance.categoriasNome).toEqual(categoriasNome);
        expect(instance.categorias).toEqual(categorias);
        expect(instance._configurarNavBar).toHaveBeenCalled();
        expect(instance._consultarOpcoes).toHaveBeenCalled();

        tick();
        expect(instance._consultarAlimentos).toHaveBeenCalled();

        tick();
        expect(instance._montarOpcoesDoGrid).toHaveBeenCalled();
    }));

    it('#_iniciarDadosDeTempo - Deve iniciar dados de tempo', () => {
        spyIniciarDadosTempo.calls.reset();
        spyIniciarDadosTempo.and.callThrough();

        let DIARIO_ID: string = SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID + moment().date() + '-' + moment().month() + '-' + moment().year();
        instance._DIARIO_ID = null;
        instance._navParams = new NavParams({ 'diarioSaudeMoment': undefined });

        instance._iniciarDadosDeTempo();

        expect(instance._DIARIO_ID).toEqual(DIARIO_ID);
        expect(instance.descricaoDataAtual).toBe(moment().local().format('dddd, LL'));
    });

    it('Deve selecionar opção ao executar ação de usuário', () => {
        spyOn(instance, '_selecionarOuDesmarcarOpcao').and.stub();
        spyOn(instance, '_abrirModalEditar').and.stub();
        spyOn(instance, '_removerOpcao').and.stub();

        let modeloAlimento: ModeloAlimento = new ModeloAlimento();
        instance.modoRegistro = instance.MODO_ADD;
        instance.onClickAcoesGridAlimentos(modeloAlimento);

        expect(instance._selecionarOuDesmarcarOpcao).toHaveBeenCalled();
        expect(instance._abrirModalEditar).not.toHaveBeenCalled();
        expect(instance._removerOpcao).not.toHaveBeenCalled();
    });

    it('Deve remover opção ao executar ação de usuário', () => {
        spyOn(instance, '_selecionarOuDesmarcarOpcao').and.stub();
        spyOn(instance, '_abrirModalEditar').and.stub();
        spyOn(instance, '_removerOpcao').and.stub();

        let modeloAlimento: ModeloAlimento = new ModeloAlimento();
        instance.modoRegistro = instance.MODO_DELETE;
        instance.onClickAcoesGridAlimentos(modeloAlimento);

        expect(instance._selecionarOuDesmarcarOpcao).not.toHaveBeenCalled();
        expect(instance._abrirModalEditar).not.toHaveBeenCalled();
        expect(instance._removerOpcao).toHaveBeenCalled();
    });

    it('Deve abrir modal editar ao executar ação de usuário', () => {
        spyOn(instance, '_selecionarOuDesmarcarOpcao').and.stub();
        spyOn(instance, '_abrirModalEditar').and.stub();
        spyOn(instance, '_removerOpcao').and.stub();

        let modeloAlimento: ModeloAlimento = new ModeloAlimento();
        instance.modoRegistro = instance.MODO_EDICAO;
        instance.onClickAcoesGridAlimentos(modeloAlimento);

        expect(instance._selecionarOuDesmarcarOpcao).not.toHaveBeenCalled();
        expect(instance._abrirModalEditar).toHaveBeenCalled();
        expect(instance._removerOpcao).not.toHaveBeenCalled();
    });

    it('#onChangeCategoria - Deve atualizar lista de grid ao mudar categoria atual', () => {
        let categoria: number = 0;
        spyOn(instance, '_atualizarLista').and.stub();

        instance.onChangeCategoria(categoria);
        expect(instance.categoriaAtual).toBe(categoria);
        expect(instance._atualizarLista).toHaveBeenCalled();
    });

    it('#onClickModoAdd - Deve alterar para MODO ADD', () => {
        let spyEventsPublish: jasmine.Spy = spyOn(instance._events, 'publish').and.stub();

        instance.modoRegistro = instance.MODO_DELETE;
        instance.onClickModoAdd();
        expect(spyEventsPublish.calls.argsFor(0)[0]).not.toEqual('ativarModoEdicao:' + instance.configuracaoNavBar.getIdentificadorGrid());
        expect(spyEventsPublish.calls.argsFor(0)[0]).toEqual('ativarModoRemover:' + instance.configuracaoNavBar.getIdentificadorGrid());
        expect(instance.modoRegistro).toBe(instance.MODO_ADD);

        spyEventsPublish.calls.reset();

        instance.modoRegistro = instance.MODO_EDICAO;
        instance.onClickModoAdd();
        expect(spyEventsPublish.calls.argsFor(0)[0]).toEqual('ativarModoEdicao:' + instance.configuracaoNavBar.getIdentificadorGrid());
        expect(spyEventsPublish.calls.argsFor(0)[0]).not.toEqual('ativarModoRemover:' + instance.configuracaoNavBar.getIdentificadorGrid());
        expect(instance.modoRegistro).toBe(instance.MODO_ADD);
    });

    it('#onClickModoEdicao - Deve alterar para MODO EDICAO', () => {
        instance.fab = new FabContainerMock();

        spyOn(instance._events, 'publish').and.stub();
        spyOn(instance.fab, 'close').and.stub();

        instance.onClickModoEdicao();
        expect(instance.modoRegistro).toBe(instance.MODO_EDICAO);
        expect(instance._events.publish).toHaveBeenCalledWith('ativarModoEdicao:' + instance.configuracaoNavBar.getIdentificadorGrid());
        expect(instance.fab.close).toHaveBeenCalled();
    });

    it('#onClickModoDelete - Deve alterar para MODO DELETE', () => {
        instance.fab = new FabContainerMock();

        spyOn(instance._events, 'publish').and.stub();
        spyOn(instance.fab, 'close').and.stub();

        instance.onClickModoDelete();
        expect(instance.modoRegistro).toBe(instance.MODO_DELETE);
        expect(instance._events.publish).toHaveBeenCalledWith('ativarModoRemover:' + instance.configuracaoNavBar.getIdentificadorGrid());
        expect(instance.fab.close).toHaveBeenCalled();
    });

    it('#onClickAddNovoAlimento - Deve abrir o modal adicionar', () => {
        let parametro = { 'opcoes': instance._documentoOpcoesAlimento };
        instance.fab = new FabContainerMock();

        spyOn(instance.fab, 'close').and.stub();
        spyOn(instance, '_abrirModalEditar').and.stub();

        instance.onClickAddNovoAlimento();
        expect(instance.fab.close).toHaveBeenCalled();
        expect(instance._abrirModalEditar).toHaveBeenCalledWith(parametro);
    });

    it('#isModoAdd - Deve retornar se está em modo add', () => {
        instance.modoRegistro = instance.MODO_ADD;
        expect(instance.isModoAdd()).toBe(true);

        instance.modoRegistro = instance.MODO_DELETE;
        expect(instance.isModoAdd()).toBe(false);
    });

    it('#isModoDelete - Deve retornar se está em modo add', () => {
        instance.modoRegistro = instance.MODO_DELETE;
        expect(instance.isModoDelete()).toBe(true);

        instance.modoRegistro = instance.MODO_ADD;
        expect(instance.isModoDelete()).toBe(false);
    });

    it('#isModoEdicao - Deve retornar se está em modo add', () => {
        instance.modoRegistro = instance.MODO_EDICAO;
        expect(instance.isModoEdicao()).toBe(true);

        instance.modoRegistro = instance.MODO_DELETE;
        expect(instance.isModoEdicao()).toBe(false);
    });

    it('#getCategoriaAtual - Deve obter o texto da categoria atual', () => {
        instance.categoriaAtual = instance.CAFE_DA_MANHA;
        expect(instance.getCategoriaAtual()).toBe('REGISTRO_ALIMENTO.CATEGORIA.CAFE_DA_MANHA');

        instance.categoriaAtual = instance.ALMOCO;
        expect(instance.getCategoriaAtual()).toBe('REGISTRO_ALIMENTO.CATEGORIA.ALMOCO');

        instance.categoriaAtual = instance.JANTAR;
        expect(instance.getCategoriaAtual()).toBe('REGISTRO_ALIMENTO.CATEGORIA.JANTAR');

        instance.categoriaAtual = instance.LANCHES;
        expect(instance.getCategoriaAtual()).toBe('REGISTRO_ALIMENTO.CATEGORIA.LANCHES');
    });

    it('#onClickAddAlimentos - Deve adicionar alimentos', fakeAsync(() => {
        // instance._alimentosDiario.opcoes = ['', ''];

        spyOn(instance, '_prepararDados').and.returnValue(Promise.resolve());
        spyOn(instance._syncDataProvider, 'addOrUpdate').and.returnValue(Promise.resolve());
        spyOn(instance._navCtrl, 'pop').and.stub();

        instance.onClickAddAlimentos();
        expect(instance._prepararDados).toHaveBeenCalled();

        tick();
        expect(instance._syncDataProvider.addOrUpdate).toHaveBeenCalled();
        tick();
        expect(instance._navCtrl.pop).toHaveBeenCalled();
    }));

    it('#onClickAddPrimeiroAlimento - Deve fazer toggleList do FAB', () => {
        instance.fab = new FabContainerMock();

        spyOn(instance.fab, 'toggleList').and.stub();

        instance.onClickAddPrimeiroAlimento();

        expect(instance.fab.toggleList).toHaveBeenCalled();
    });

    it('#isEnableConfirmButton - Deve verificar se o botão confirmar está habilitado', () => {
        instance.alimentosSelecionados = [];
        instance.modoRegistro = instance.MODO_ADD;
        expect(instance.isEnableConfirmButton()).toBe(true);

        instance.alimentosSelecionados = [{}, {}];
        instance.modoRegistro = instance.MODO_EDICAO;
        expect(instance.isEnableConfirmButton()).toBe(true);

        instance.alimentosSelecionados = [{}, {}];
        instance.modoRegistro = instance.MODO_ADD;
        expect(instance.isEnableConfirmButton()).toBe(false);
    });

    it('#retirarModeloAlimentoDoArray - Deve retirar um modelo de alimento de um array', () => {
        let opcao: any = { nome: 'laranja' };
        let arrayQualquer: Array<any> = [];
        arrayQualquer.push(opcao);
        arrayQualquer.push({ nome: 'maçã' });
        arrayQualquer.push({ nome: 'melancia' });

        let modeloAlimento: ModeloAlimento = new ModeloAlimento();
        modeloAlimento.setNome('laranja');

        instance.retirarModeloAlimentoDoArray(arrayQualquer, modeloAlimento);
        expect(arrayQualquer).not.toContain(opcao);
    });

    it('#retirarModeloOpcaoDoArraySelecionado - Deve retirar um modelo de alimento das opções selecionadas', () => {

        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setId('laranja');

        let melancia: ModeloAlimento = new ModeloAlimento();
        melancia.setId('melancia');

        let queijo: ModeloAlimento = new ModeloAlimento();
        queijo.setId('queijo');

        instance.alimentosSelecionados.push(queijo);
        instance.alimentosSelecionados.push(laranja);
        instance.alimentosSelecionados.push(melancia);

        instance.retirarModeloOpcaoDoArraySelecionado(laranja);
        expect(instance.alimentosSelecionados).not.toContain(laranja);
    });

    it('#_atualizarOpcaoDoGrid - Deve atualizar apenas uma opção do grid', () => {
        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setId('idLaranja');
        laranja.setNome('laranja');

        let melancia: ModeloAlimento = new ModeloAlimento();
        melancia.setId('idMelancia');
        melancia.setNome('melancia');

        let queijo: ModeloAlimento = new ModeloAlimento();
        queijo.setId('idQueijo');
        queijo.setNome('queijo');

        let modeloAlimentoParaAtualizar: ModeloAlimento = new ModeloAlimento();
        modeloAlimentoParaAtualizar.setId('idMelancia');
        modeloAlimentoParaAtualizar.setNome('melancia2');

        instance.arrayOpcoesDoGrid.push(queijo);
        instance.arrayOpcoesDoGrid.push(laranja);
        instance.arrayOpcoesDoGrid.push(melancia);

        spyOn(instance, '_atualizarLista').and.stub();

        instance._atualizarOpcaoDoGrid(modeloAlimentoParaAtualizar);
        expect(instance.arrayOpcoesDoGrid[2].getNome()).toBe(modeloAlimentoParaAtualizar.getNome());
        expect(instance._atualizarLista).toHaveBeenCalled();
    });

    it('#_configurarNavBar - Deve configurar corretamente', () => {

        spyConfigurarNavBar.calls.reset();
        spyConfigurarNavBar.and.callThrough();

        instance._configurarNavBar();
        expect(instance.configuracaoNavBar.getTextoInicial()).toBe('REGISTRO_ALIMENTO.TITULO_INICIAL');
        expect(instance.configuracaoNavBar.getTextoModoRemover()).toBe('REGISTRO_ALIMENTO.TITULO_REMOVER');
        expect(instance.configuracaoNavBar.getTextoModoEdicao()).toBe('REGISTRO_ALIMENTO.TITULO_EDICAO');
        expect(instance.configuracaoNavBar.getIdentificadorGrid()).toBe('idGridAlimentos');
        expect(instance.navBarConfigurado).toBe(true);
    });

    it('#_abrirModalEditar - Deve abrir modal corretamente', () => {
        let modeloAlimento: ModeloAlimento = new ModeloAlimento();
        modeloAlimento.setId('idQueijo');

        spyOn(instance, '_modalAlimentoDissmiss').and.stub();
        spyOn(instance._modalCtrl, 'create').and.callThrough();
        spyOn(Modal.prototype, 'present').and.stub();
        spyOn(Modal.prototype, 'onDidDismiss').and.stub();
        let parametro = { 'opcoes': instance._documentoOpcoesAlimento };
        instance._abrirModalEditar(parametro);
        expect(instance._modalCtrl.create).toHaveBeenCalledWith('NovoAlimentoPage', parametro);
        expect(Modal.prototype.present).toHaveBeenCalled();
        expect(Modal.prototype.onDidDismiss).toHaveBeenCalled();
    });

    it('#_atualizarStatusOpcao - Deve atualizar status da opção', () => {
        let arrayOpcoes = [{
            nome: 'laranja',
            ativo: true,
        },
        {
            nome: 'queijo',
            ativo: true,
        }];

        let modeloAlimento: ModeloAlimento = new ModeloAlimento();
        modeloAlimento.setId('idQueijo');
        modeloAlimento.setNome('queijo');
        modeloAlimento.setAtivo(true);

        let opcoes = instance._atualizarStatusOpcao(arrayOpcoes, modeloAlimento);
        expect(opcoes[1].ativo).toBe(false);
    });

    it('#_removerOpcao - Deve remover a opção', () => {
        let documentOptions: any = {
            _id: SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID,
            opcoes: [],
        };

        let laranja: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };
        let laranjaDesativado: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: false,
        };

        let melancia: Alimento = {
            _id: 'idMelancia',
            nome: 'melancia',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        let queijo: Alimento = {
            _id: 'idQueijo',
            nome: 'queijo',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        documentOptions.opcoes.push(laranja);
        documentOptions.opcoes.push(melancia);
        documentOptions.opcoes.push(queijo);
        instance._diario.alimentos.push(laranja);
        instance._documentoOpcoesAlimento = documentOptions;

        let alimentoParaExcluir: ModeloAlimento = new ModeloAlimento();
        alimentoParaExcluir.setId(laranja._id);
        alimentoParaExcluir.setNome(laranja.nome);
        alimentoParaExcluir.setDataCriacao(laranja.dataCriacao);
        alimentoParaExcluir.setCafeDaManha(laranja.cafeDaManha);
        alimentoParaExcluir.setAlmoco(laranja.almoco);
        alimentoParaExcluir.setJantar(laranja.jantar);
        alimentoParaExcluir.setLanches(laranja.lanches);
        alimentoParaExcluir.setAtivo(laranja.ativo);

        spyOn(instance._syncDataProvider, 'addOrUpdateManyDocuments').and.stub();

        instance._removerOpcao(alimentoParaExcluir);
        let manyDocuments: any = [instance._documentoOpcoesAlimento, instance._diario];

        expect(instance._diario.alimentos).not.toContain(laranja);
        expect(instance.alimentosSelecionados).not.toContain(alimentoParaExcluir);
        expect(instance._documentoOpcoesAlimento.opcoes).toContain(laranjaDesativado);
        expect(instance._syncDataProvider.addOrUpdateManyDocuments).toHaveBeenCalledWith(manyDocuments);
        expect(instance._montarOpcoesDoGrid).toHaveBeenCalled();
    });

    it('#_prepararDados - Deve preparar os dados para salvar', fakeAsync(() => {
        spyOn(instance, '_addOpcao').and.stub();
        spyOn(instance, '_retirarOpcao').and.stub();

        instance._prepararDados();

        tick();
        expect(instance._addOpcao).toHaveBeenCalled();
        expect(instance._retirarOpcao).toHaveBeenCalled();
    }));

    it('#_addOpcao - Deve adicionar um alimento selecionado', () => {
        let laranja: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };
        let alimentoSelecionado: ModeloAlimento = new ModeloAlimento();
        alimentoSelecionado.setId(laranja._id);
        alimentoSelecionado.setNome(laranja.nome);
        alimentoSelecionado.setDataCriacao(laranja.dataCriacao);
        alimentoSelecionado.setCafeDaManha(laranja.cafeDaManha);
        alimentoSelecionado.setAlmoco(laranja.almoco);
        alimentoSelecionado.setJantar(laranja.jantar);
        alimentoSelecionado.setLanches(laranja.lanches);
        alimentoSelecionado.setAtivo(laranja.ativo);

        instance.alimentosSelecionados.push(alimentoSelecionado);

        spyOn(instance, '_converterModeloAlimentoParaAlimento').and.callThrough();

        instance._addOpcao();
        expect(instance._converterModeloAlimentoParaAlimento).toHaveBeenCalled();
        expect(instance._diario.alimentos).toContain(laranja);
    });

    it('#_retirarOpcao - Deve retirar um objeto alimento selecionado que está no documento cadastrado', () => {
        let laranja: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        let melancia: Alimento = {
            _id: 'idMelancia',
            nome: 'melancia',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        let queijo: Alimento = {
            _id: 'idQueijo',
            nome: 'queijo',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        instance._diario.alimentos.push(laranja);
        instance._diario.alimentos.push(melancia);
        instance._diario.alimentos.push(queijo);

        let modeloAlimentoQueijo: ModeloAlimento = new ModeloAlimento();
        modeloAlimentoQueijo.setId(queijo._id);
        instance.alimentosSelecionados.push(modeloAlimentoQueijo);

        instance._retirarOpcao();
        expect(instance._diario.alimentos).not.toContain(laranja);
        expect(instance._diario.alimentos).not.toContain(melancia);
    });

    it('#_converterModeloAlimentoParaAlimento - Deve converter um objeto ModeloAlimeto para uma opção válida de cadastrado', () => {
        let laranja: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };
        let alimentoLaranja: ModeloAlimento = new ModeloAlimento();
        alimentoLaranja.setId(laranja._id);
        alimentoLaranja.setNome(laranja.nome);
        alimentoLaranja.setDataCriacao(laranja.dataCriacao);
        alimentoLaranja.setCafeDaManha(laranja.cafeDaManha);
        alimentoLaranja.setAlmoco(laranja.almoco);
        alimentoLaranja.setJantar(laranja.jantar);
        alimentoLaranja.setLanches(laranja.lanches);
        alimentoLaranja.setAtivo(laranja.ativo);

        let alimentoConvertido: any = instance._converterModeloAlimentoParaAlimento(alimentoLaranja);
        expect(alimentoConvertido).toEqual(laranja);
    });

    it('#_selecionarOuDesmarcarOpcao - Deve selecionar opção', () => {
        spyOn(instance, '_selecionarOpcao').and.stub();
        spyOn(ModeloAlimento.prototype, 'setSelecionado').and.callThrough();

        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setSelecionado(false);

        instance._selecionarOuDesmarcarOpcao(laranja);
        let isSelecionado: boolean = !laranja.isSelecionado();
        expect(ModeloAlimento.prototype.setSelecionado).toHaveBeenCalledWith(isSelecionado);
        expect(instance._selecionarOpcao).toHaveBeenCalledWith(laranja);
    });

    it('#_selecionarOuDesmarcarOpcao - Deve desmarcar opção', () => {
        spyOn(instance, '_desmarcarOpcao').and.stub();
        spyOn(ModeloAlimento.prototype, 'setSelecionado').and.callThrough();

        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setSelecionado(true);

        instance._selecionarOuDesmarcarOpcao(laranja);
        let isSelecionado: boolean = !laranja.isSelecionado();
        expect(ModeloAlimento.prototype.setSelecionado).toHaveBeenCalledWith(isSelecionado);
        expect(instance._desmarcarOpcao).toHaveBeenCalledWith(laranja);
    });

    it('#_selecionarOpcao - Deve selecionar um alimento', () => {
        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setId('idLaranja');

        instance._selecionarOpcao(laranja);
        expect(instance.alimentosSelecionados).toContain(laranja);
    });

    it('#_desmarcarOpcao - Deve desmarcar um alimento', () => {
        let laranja: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };
        let alimentoLaranja: ModeloAlimento = new ModeloAlimento();
        alimentoLaranja.setId(laranja._id);
        alimentoLaranja.setNome(laranja.nome);
        alimentoLaranja.setDataCriacao(laranja.dataCriacao);
        alimentoLaranja.setCafeDaManha(laranja.cafeDaManha);
        alimentoLaranja.setAlmoco(laranja.almoco);
        alimentoLaranja.setJantar(laranja.jantar);
        alimentoLaranja.setLanches(laranja.lanches);
        alimentoLaranja.setAtivo(laranja.ativo);

        instance.alimentosSelecionados.push(alimentoLaranja);
        instance._desmarcarOpcao(alimentoLaranja);
        expect(instance.alimentosSelecionados).not.toContain(laranja);
    });

    it('#_modalAlimentoDissmiss - Deve executar modo onDissmiss', () => {
        let documentOptions: any = {
            _id: SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID,
            opcoes: [],
        };

        let laranja: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        let melancia: Alimento = {
            _id: 'idMelancia',
            nome: 'melancia',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        let queijo: Alimento = {
            _id: 'idQueijo',
            nome: 'queijo',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        documentOptions.opcoes.push(laranja);
        documentOptions.opcoes.push(melancia);
        documentOptions.opcoes.push(queijo);

        let data: any = {
            opcoes: documentOptions,
        };

        instance._modalAlimentoDissmiss(data);
        expect(instance._documentoOpcoesAlimento).toEqual(documentOptions);
        expect(instance._montarOpcoesDoGrid).toHaveBeenCalled();
    });

    it('#_consultarAlimentos - Deve atualizar id do documento quando não encontrado', fakeAsync(() => {
        let result: any = {
            error: true,
            status: 404,
        };
        spyOn(instance._syncDataProvider, 'getDocumentById').and.returnValue(Promise.resolve(result));
        spyConsultarAlimentos.calls.reset();
        spyConsultarAlimentos.and.callThrough();

        instance._consultarAlimentos();
        expect(instance._syncDataProvider.getDocumentById).toHaveBeenCalledWith(instance._DIARIO_ID);
        tick();
        expect(instance._diario._id).toBe(instance._DIARIO_ID);
    }));

    it('#_consultarAlimentos - Deve atualizar id do documento quando não encontrado', fakeAsync(() => {
        let result: any = {
            _id: null,
            opcoes: [],
        };
        spyOn(instance._syncDataProvider, 'getDocumentById').and.returnValue(Promise.resolve(result));
        spyConsultarAlimentos.calls.reset();
        spyConsultarAlimentos.and.callThrough();

        instance._consultarAlimentos();
        expect(instance._syncDataProvider.getDocumentById).toHaveBeenCalledWith(instance._DIARIO_ID);
        tick();
        expect(instance._diario).toBe(result);
    }));

    it('#_consultarOpcoes - Deve consultar opções fixas', fakeAsync(() => {
        let result: any = {
            _id: null,
            opcoes: [],
        };
        spyOn(instance._syncDataProvider, 'getDocumentById').and.returnValue(Promise.resolve(result));
        spyConsultarOpcoes.calls.reset();
        spyConsultarOpcoes.and.callThrough();

        instance._consultarOpcoes();
        expect(instance._syncDataProvider.getDocumentById).toHaveBeenCalledWith(SyncDataProvider.DOCUMENT_OPCOES_ALIMENTOS_ID);
        tick();
        expect(instance._documentoOpcoesAlimento).toBe(result);
    }));

    it('#_montarOpcoesDoGrid - Deve montar array de opções do grid apenas com opções ativas', () => {
        let documentOptions: any = {
            _id: SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID,
            opcoes: [],
        };

        let laranja: Alimento = {
            _id: 'idLaranja',
            nome: 'laranja',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: true,
        };

        let alimentoLaranja: ModeloAlimento = new ModeloAlimento();
        alimentoLaranja.setId(laranja._id);
        alimentoLaranja.setNome(laranja.nome);
        alimentoLaranja.setDataCriacao(laranja.dataCriacao);
        alimentoLaranja.setCafeDaManha(laranja.cafeDaManha);
        alimentoLaranja.setAlmoco(laranja.almoco);
        alimentoLaranja.setJantar(laranja.jantar);
        alimentoLaranja.setLanches(laranja.lanches);
        alimentoLaranja.setAtivo(laranja.ativo);
        alimentoLaranja.definirIcone(instance.categoriaAtual);

        let melancia: Alimento = {
            _id: 'idMelancia',
            nome: 'melancia',
            dataCriacao: '13/10/2017',
            cafeDaManha: true,
            almoco: true,
            jantar: false,
            lanches: false,
            ativo: false,
        };

        let alimentoMelancia: ModeloAlimento = new ModeloAlimento();
        alimentoMelancia.setId(melancia._id);
        alimentoMelancia.setNome(melancia.nome);
        alimentoMelancia.setDataCriacao(melancia.dataCriacao);
        alimentoMelancia.setCafeDaManha(melancia.cafeDaManha);
        alimentoMelancia.setAlmoco(melancia.almoco);
        alimentoMelancia.setJantar(melancia.jantar);
        alimentoMelancia.setLanches(melancia.lanches);
        alimentoMelancia.setAtivo(melancia.ativo);
        alimentoMelancia.definirIcone(instance.categoriaAtual);

        documentOptions.opcoes.push(laranja);
        documentOptions.opcoes.push(melancia);
        instance._documentoOpcoesAlimento = documentOptions;
        instance._diario.alimentos.push(laranja);

        spyMontarOpcoesGrid.and.callThrough();
        instance._montarOpcoesDoGrid();
        expect(instance.arrayOpcoesDoGrid).not.toContain(alimentoMelancia);
        expect(instance._diario.alimentos).toContain(laranja);
        expect(instance.arrayOpcoesDoGrid[0].isSelecionado()).toBe(true);
    });

    // Testar #_montarOpcoesDoGrid - Não deve duplicar um modeloAlimento que está no array selecionadas
    // Testar #_montarOpcoesDoGrid - Deve montar o status como selecionado de uma opção que está no array selecionadas

    it('#_buildGrid - Deve montar o grid', () => {
        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setId('idLaranja');
        laranja.setNome('laranja');

        let melancia: ModeloAlimento = new ModeloAlimento();
        melancia.setId('idMelancia');
        melancia.setNome('melancia');

        let queijo: ModeloAlimento = new ModeloAlimento();
        queijo.setId('idQueijo');
        queijo.setNome('queijo');

        let arroz: ModeloAlimento = new ModeloAlimento();
        arroz.setId('idArroz');
        arroz.setNome('arroz');

        let opcoesGrid: Array<ModeloAlimento> = [];
        opcoesGrid.push(laranja);
        opcoesGrid.push(melancia);
        opcoesGrid.push(queijo);
        opcoesGrid.push(arroz);

        instance._buildGrid(opcoesGrid);
        expect(instance.bindOpcoesDoGrid.length).toBe(Math.ceil(opcoesGrid.length / 3));
    });

    it('#_atualizarLista - Não deve atualizar lista de dados quando não houver opções', () => {
        instance.arrayOpcoesDoGrid = [];
        spyOn(instance, '_filtrarOpcoes').and.returnValue(Promise.resolve());

        instance._atualizarLista();
        expect(instance._filtrarOpcoes).not.toHaveBeenCalled();
    });

    it('#_atualizarLista - Deve atualizar lista de dados', fakeAsync(() => {
        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setId('idLaranja');
        instance.arrayOpcoesDoGrid.push(laranja);

        spyOn(instance, '_filtrarOpcoes').and.returnValue(Promise.resolve());
        spyOn(instance, '_buildGrid').and.stub();

        instance._atualizarLista();
        expect(instance._filtrarOpcoes).toHaveBeenCalled();
        tick();
        expect(instance._buildGrid).toHaveBeenCalled();
    }));

    it('#_filtrarOpcoes - Deve filtrar as opções por tipo', () => {
        let pao: ModeloAlimento = new ModeloAlimento();
        pao.setId('idPao');
        pao.setCafeDaManha(true);
        pao.setAlmoco(false);
        pao.setJantar(false);
        pao.setLanches(false);

        let arroz: ModeloAlimento = new ModeloAlimento();
        arroz.setId('idArroz');
        arroz.setCafeDaManha(false);
        arroz.setAlmoco(true);
        arroz.setJantar(false);
        arroz.setLanches(false);

        let peixe: ModeloAlimento = new ModeloAlimento();
        peixe.setId('idPeixe');
        peixe.setCafeDaManha(false);
        peixe.setAlmoco(false);
        peixe.setJantar(true);
        peixe.setLanches(false);

        let laranja: ModeloAlimento = new ModeloAlimento();
        laranja.setId('idLaranja');
        laranja.setCafeDaManha(false);
        laranja.setAlmoco(false);
        laranja.setJantar(false);
        laranja.setLanches(true);

        instance.arrayOpcoesDoGrid.push(pao);
        instance.arrayOpcoesDoGrid.push(arroz);
        instance.arrayOpcoesDoGrid.push(peixe);
        instance.arrayOpcoesDoGrid.push(laranja);

        instance.categoriaAtual = ALMOCO;
        instance._filtrarOpcoes().then(
            (opcoes: Array<ModeloAlimento>) => {
                expect(opcoes).not.toContain(pao);
                expect(opcoes).toContain(arroz);
                expect(opcoes).not.toContain(peixe);
                expect(opcoes).not.toContain(laranja);
            },
        );
    });

});
