import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from './../../components/components.module';
import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RegistroAlimentoPage } from './registro-alimento';

@NgModule({
  declarations: [
    RegistroAlimentoPage,
  ],
  imports: [
    IonicPageModule.forChild(RegistroAlimentoPage),
    ComponentsModule,
    PipesModule,
    TranslateModule,
  ],
})
export class RegistroAlimentoPageModule { }
