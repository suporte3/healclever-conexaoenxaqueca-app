import { HealthGoal } from './../../core/models/health-goal.model';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-eu',
  templateUrl: 'eu.html',
})
export class EuPage {

  public goal: HealthGoal;

  constructor(private _navCtrl: NavController, private _syncDataProvider: SyncDataProvider) {
    this._iniciar();
  }

  public onClickPerfil() {
    this._navCtrl.push('PerfilPage');
  }

  public onClickMinhaCefaleia() {
    this._navCtrl.push('MinhaCefaleiaPage');
  }

  private _iniciar() {
    this._syncDataProvider.getDocumentById('dc636ba569551879357f366092fd789c').then(
      (result: any) => {
        this.goal = result.goal;
      },
    );
  }

}
