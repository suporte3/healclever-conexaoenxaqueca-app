import { PipesModule } from './../../pipes/pipes.module';
import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EuPage } from './eu';

@NgModule({
  declarations: [
    EuPage,
  ],
  imports: [
    IonicPageModule.forChild(EuPage),
    ComponentsModule,
    TranslateModule,
    PipesModule,
  ],
})
export class EuPageModule { }
