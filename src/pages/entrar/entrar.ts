import { ValidationHelper } from './../../core/helpers/validation.helper';
import { Network } from '@ionic-native/network';
import { AuthenticationProvider } from './../../providers/authentication/authentication';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Http } from '@angular/http';
import { AuthenticationEventHandlerInterface } from './../../core/models/interface/authentication-event-handler.interface';
import { Component, OnInit, trigger, state, style, transition, animate, ViewChild } from '@angular/core';
import { IonicPage, NavController, Events, LoadingController, Loading, Platform } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare var AccountKitPlugin: any;

@IonicPage()
@Component({
  selector: 'ib-page-entrar',
  templateUrl: 'entrar.html',
  animations: [
    trigger('fadeInOut', [
      state('in', style({ opacity: 1 })),
      state('out', style({ opacity: 0 })),
      transition('* => *', animate('.5s')),
    ]),
  ],
  providers: [ValidationHelper],
})
export class EntrarPage implements OnInit, AuthenticationEventHandlerInterface {

  public readonly NOVO_USUARIO: number = 1;
  public readonly USUARIO_VETERANO: number = 2;

  public readonly SEM_MENSAGENS: number = 0;
  public readonly MSG_PROBLEMA_INTERNET: number = 1;
  public readonly MSG_DADOS_USUARIO_INVALIDO: number = 2;
  public readonly MSG_PROBLEMA_DESCONHECIDO: number = 3;
  public readonly MSG_EMAIL_EM_USO: number = 4;

  public tipoUsuario: number = 0;
  public submitted: boolean = false;
  public request: boolean = true;
  public loadingPopup: Loading;
  public formState: string = 'out';
  public tipoDaMensagem: number = 0;

  @ViewChild('entrarForm') entrarForm: FormGroup;

  constructor(private _platforma: Platform, private _navCtrl: NavController, private _http: Http, private _syncData: SyncDataProvider,
    private _formBuilder: FormBuilder, private _validationHelper: ValidationHelper, private _events: Events,
    private _authenticationProvider: AuthenticationProvider, public loadingCtrl: LoadingController) {

    this._iniciar();
  }

  ngOnInit() {
    this._validationHelper = null; // Temporariamente validar TSLint
    this.entrarForm = this._formBuilder.group({
      // email: ['', Validators.compose([this.validationService.validarFormatoEVerificarEmail.bind(this)]) ],
      email: ['', Validators.compose([ValidationHelper.validarFormatoEmail])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  public onClickUsuarioAcao(tipoUsuario: number) {
    this.tipoUsuario = tipoUsuario;
    this.formState = 'in';
  }

  public onSubmitEntrarNoApp(event: any) {
    this.submitted = true;
    this._definirMensagemParaUsuario(this.SEM_MENSAGENS);

    if (this.entrarForm.valid) {
      this.loadingPopup = this.loadingCtrl.create({
        content: '<strong>Autenticando usuário...</strong><p>Aguarde alguns instantes.</p>',
        spinner: 'dots',
      });
      this.loadingPopup.present();
      this._definirAcaoUsuario();
    }

    event.preventDefault();
  }

  public onErrorAtualizarPerfilUsuario(error: any) {
    console.log('TRACK~onErrorAtualizarPerfilUsuario', error);
    if (this.loadingPopup) {
      this.loadingPopup.dismiss();
      this.loadingPopup.onDidDismiss(() => {
        this._events.publish('user:logout', { noRedirect: true });
        this._definirMensagemParaUsuario(500);
      });
    } else {
      this._definirMensagemParaUsuario(500);
      this._events.publish('user:logout', { noRedirect: true });
    }
  }

  public onErrorSincronizarDatabase() {
    if (this.loadingPopup) {
      this.loadingPopup.dismiss();
      this.loadingPopup.onDidDismiss(() => {
        this._events.publish('user:logout', { noRedirect: true });
        this._definirMensagemParaUsuario(500);
      });
    } else {
      this._definirMensagemParaUsuario(500);
      this._events.publish('user:logout', { noRedirect: true });
    }
  }

  public onSuccessLogin(tipoAutenticacao: number) {
    console.log('TRACK~onSuccessLogin', tipoAutenticacao);
    if (this.loadingPopup) {
      this.loadingPopup.dismiss();
      this.loadingPopup.onDidDismiss(() => {
        this._events.publish('user:login');
        this._navCtrl.setRoot('DiarioPage', null, { animate: true });
      });
    } else {
      this._events.publish('user:login');
      this._navCtrl.setRoot('DiarioPage');
    }
  }

  public onErrorDatabaseInit(tipoAutenticacao: number) {
    console.log('TRACK~onErrorDatabaseInit', tipoAutenticacao);
    if (this.loadingPopup) {
      this.loadingPopup.dismiss();
      this.loadingPopup.onDidDismiss(() => {
        this._events.publish('user:logout', { noRedirect: true });
        this._definirMensagemParaUsuario(500);
      });
    } else {
      this.loadingPopup.dismiss();
      this.loadingPopup.onDidDismiss(() => {
        this._events.publish('user:logout', { noRedirect: true });
        this._definirMensagemParaUsuario(500);
      });
    }

  }

  public onLoginBySmartphoneUserNotFound() {
    if (this.loadingPopup) {
      this.loadingPopup.dismiss();
    }

    this.tipoUsuario = this.MSG_DADOS_USUARIO_INVALIDO;
  }

  public onErrorRequest(error: any, tipoRequisicao: number) {
    console.log('TRACK~onErrorRequest', [error, tipoRequisicao]);
    this._manipularErroAcaoUsuario(error);
  }

  public smsLogin() {

    this._platforma.ready().then(
      () => {
        console.log('platform ready');
        if (AccountKitPlugin) {

          console.log('cordova.AccountKitPlugin');

          let options: any = {
            useAccessToken: false,
            defaultCountryCode: 'BR',
            facebookNotificationsEnabled: true,
          };

          AccountKitPlugin.loginWithPhoneNumber(
            options,
            (response: any) => {
              console.log(response);
              // let authCode: string = response.code;
              // passar codigo para Api
            },
            (err: any) => {
              console.log('ERR', err);
            },
          );
        }
      },
    );
  }

  public emailLogin() {
    this._platforma.ready().then(
      () => {
        if (AccountKitPlugin) {
          let options: any = {
            useAccessToken: false,
            defaultCountryCode: 'BR',
            facebookNotificationsEnabled: true,
          };

          AccountKitPlugin.loginWithEmail(
            options,
            (response: any) => {
              // let authCode: string = response.code;
              console.log(response);
            },
            (err: any) => {
              console.log('ERR', err);
            },
          );
        }
      },
    );
  }

  public logout() {
    AccountKitPlugin.logout();
  }

  private _iniciar() {
    this._authenticationProvider.setPage(this);
    this._authenticationProvider.setSyncDataServiceReference(this._syncData);
    this._authenticationProvider.setHttpReference(this._http);
  }

  private _definirAcaoUsuario() {
    switch (this.tipoUsuario) {
      case this.NOVO_USUARIO:
        let email: string = this.entrarForm.controls['email'].value;
        this._authenticationProvider.verificarSeUsuarioExiste(email)
          .subscribe(
          () => {
            let credenciais = {
              email: email,
              password: this.entrarForm.controls['password'].value,
              confirmPassword: this.entrarForm.controls['password'].value,
            };
            this._authenticationProvider.setCredenciaisAutenticacao(credenciais);
            this._authenticationProvider.requisitarCadastroUsuario();
          },
          (error) => {
            this.loadingPopup.dismiss();
            this._definirMensagemParaUsuario(error.status);
          },
        );
        break;
      case this.USUARIO_VETERANO:
        this._requisitarLogin();
        break;
      default:
        // Track error - Sentry - message: '_definirAcaoUsuario()~unknow'
        this.loadingPopup.dismiss();
        this.loadingPopup.onDidDismiss(() => {
          this._navCtrl.setRoot('TutorialPage');
        });
    }
  }

  private _requisitarLogin() {
    let network = new Network();
    if (network.type === 'none') {
      let emailUsuarioFormulario = this.entrarForm.controls['email'].value;
      this._authenticationProvider.requisitarLoginBySmarthone(emailUsuarioFormulario);
    } else {
      let credenciais = {
        username: this.entrarForm.controls['email'].value,
        password: this.entrarForm.controls['password'].value,
      };
      this._authenticationProvider.setCredenciaisAutenticacao(credenciais);
      this._authenticationProvider.requisitarLoginComServidor();
    }
  }

  private _manipularErroAcaoUsuario(error: any) {
    this.request = false;
    if (this.loadingPopup) {
      this.loadingPopup.dismiss();
      this.loadingPopup.onDidDismiss(() => {
        this._definirMensagemParaUsuario(error.status);
      });
    }
  }

  private _definirMensagemParaUsuario(status: number) {
    let network = new Network();
    if (network.type === 'none') {
      this.tipoDaMensagem = this.MSG_PROBLEMA_INTERNET;
    } else if (status === this.SEM_MENSAGENS) {
      this.tipoDaMensagem = this.SEM_MENSAGENS;
    } else if (status === 401) {
      this.tipoDaMensagem = this.MSG_DADOS_USUARIO_INVALIDO;
      // TRACK ERROR
    } else if (status === 409) {
      this.tipoDaMensagem = this.MSG_EMAIL_EM_USO;
    } else {
      this.tipoDaMensagem = this.MSG_PROBLEMA_DESCONHECIDO;
      // TRACK ERROR
    }
  }

}
