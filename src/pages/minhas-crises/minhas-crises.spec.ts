import { Cor } from './../../core/models/pojo/cor.pojo';
import { ModeloListaCrise } from './../../core/models/pojo/lista-crise.pojo';
import { AppHelper } from './../../core/helpers/app.helper';
import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { NavParamsMock } from './../../core/mocks/nav-params.mock';
import { NavParams } from 'ionic-angular';
import { TestUtils } from './../../app/app.test';
import { Crise } from './../../core/models/crise.model';
import { Usuario } from './../../core/models/usuario/usuario.model';
import { MinhasCrisesPage } from './minhas-crises';
import * as moment from 'moment';
import 'moment/min/locales';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Minhas Crises Page:', () => {

    let fixture: ComponentFixture<MinhasCrisesPage> = null;
    let instance: any = null;
    let _instance: any = MinhasCrisesPage.prototype;
    let usuarioObjTest: Usuario = {
        _id: 'usuarioId',
        dataAdmissao: '',
        email: 'usuario@email.com',
        first_name: 'joão',
        last_name: 'silva',
        birthday: null,
        genero: 0,
        sessao: null,
        sincronizado: false,
        logado: false,
        facebook: false,
    };
    let crise: Crise = {
        _id: 'criseId0',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };

    let spyInicializarListaCrise: jasmine.Spy;

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.beforeEachCompiler([MinhasCrisesPage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            },
        );

        spyInicializarListaCrise = spyOn(_instance, '_inicializarListaDeCrises').and.stub();
        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        moment.locale('tr');

        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(instance).toBeDefined();
        expect(fixture).toBeDefined();
        done();
    });

    it('Deve iniciar o componente ao carregar', () => {
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('Deve iniciar corretamente', (done) => {
        let dataAtual: any = moment().local();
        let spyAppUtilsGetUsuarioLogado: jasmine.Spy = spyOn(AppHelper.prototype, 'getUsuarioLogado').and.returnValue(Promise.resolve(usuarioObjTest));
        spyOn(moment, 'locale').and.stub();
        spyInicializarListaCrise.calls.reset();
        spyInicializarListaCrise.and.callThrough();
        instance._navParams = new NavParams({ 'dataAtual': dataAtual });

        instance._iniciar();
        expect(moment.locale).toHaveBeenCalledWith('pt-BR');
        expect(instance.carregandoDados).toBe(true);
        expect(instance._appHelper).toEqual(new AppHelper());
        expect(spyAppUtilsGetUsuarioLogado).toHaveBeenCalled();
        expect(instance._dataAtual).toBe(dataAtual);
        expect(spyInicializarListaCrise).toHaveBeenCalled();

        fixture.whenStable().then(
            () => {
                expect(instance._usuario).toEqual(usuarioObjTest);
                done();
            },
        );
    });

    it('#ionViewWillEnter - Deve atualizar a lista ao entrar na tela', () => {

        spyOn(instance, '_atualizarLista').and.stub();
        instance.ionViewWillEnter();
        expect(instance.carregandoDados).toBe(true);
        expect(instance._atualizarLista).toHaveBeenCalled();
    });

    it('#onClickRegistrarPrimeiraCrise - Deve ir para questionário', () => {
        spyOn(instance._navCtrl, 'push').and.stub();
        instance.onClickRegistrarPrimeiraCrise();
        expect(instance._navCtrl.push).toHaveBeenCalledWith('DuracaoDorPage');
    });

    it('#onClickExcluirCrise - Deve apresentar Dialog para confirmar exclusão de crise', (done) => {
        spyOn(instance._appHelper, 'sweetAlert').and.returnValue(Promise.resolve());
        spyOn(instance, '_showDialogConfirmarExcluirCrise').and.stub();

        let elementRefCard: any = null;
        let modeloListaCrise: ModeloListaCrise = new ModeloListaCrise();
        modeloListaCrise.setCrise(crise);
        let grupoIndex: number = 0;
        let criseIndex: number = 0;

        let opcoesDialog: any = {
            title: instance.TITULO_EXCLUIR,
            text: instance.DESCRICAO_EXCLUIR,
            type: null,
            showCancelButton: true,
            confirmButtonText: instance.BTN_EXCLUIR_SIM,
            cancelButtonText: instance.BTN_EXCLUIR_CANCELAR,
        };
        instance.onClickExcluirCrise(elementRefCard, modeloListaCrise, grupoIndex, criseIndex);
        expect(instance._appHelper.sweetAlert).toHaveBeenCalledWith(opcoesDialog);

        fixture.whenStable().then(
            () => {
                expect(instance._showDialogConfirmarExcluirCrise).toHaveBeenCalled();
                done();
            },
        );
    });

    it('#_showDialogConfirmarExcluirCrise - Deve apresentar Dialog para confirmar operação ao excluir crise, corretamente', () => {
        spyOn(instance._appHelper, 'sweetAlert').and.returnValue(Promise.resolve());
        spyOn(instance, '_triggerDeleteAnimation').and.stub();
        spyOn(instance, '_excluirCrise').and.stub();

        let elementRefCard: any = null;
        let modeloListaCrise: ModeloListaCrise = new ModeloListaCrise();
        modeloListaCrise.setCrise(crise);
        let grupoIndex: number = 0;
        let criseIndex: number = 0;
        let opcoesDialog: any = {
            title: instance.TITULO_EXCLUIR,
            text: instance.SUCESSO_EXCLUIR,
            type: 'success',
        };

        instance._showDialogConfirmarExcluirCrise(elementRefCard, modeloListaCrise, grupoIndex, criseIndex);
        expect(instance._appHelper.sweetAlert).toHaveBeenCalledWith(opcoesDialog);

        fixture.whenStable().then(
            () => {
                expect(instance._triggerDeleteAnimation).toHaveBeenCalled();
                // expect(instance._excluirCrise).toHaveBeenCalled();
            },
        );
    }, 600);

    it('#_triggerDeleteAnimation - Deve executar corretamente animação', () => {
        let cardElement: any = null;
        let opcoes: any = [
            [
                { transform: 'translate3d(0, 0, 0)' },
                { transform: 'translate3d(100%, 0, 0)' },
            ],
            {
                duration: 500,
                delay: 0,
                fill: 'forwards',
            },
        ];
        spyOn(instance._renderer, 'invokeElementMethod').and.stub();
        instance._triggerDeleteAnimation(cardElement);
        expect(instance._renderer.invokeElementMethod).toHaveBeenCalledWith(cardElement, 'animate', opcoes);
    });

    it('#_excluirCrise - Deve excluir registro da crise', (done) => {
        let modeloListaCrise: ModeloListaCrise = new ModeloListaCrise();
        modeloListaCrise.setCrise(crise);
        let grupoIndex: number = 0;
        let criseIndex: number = 0;

        instance._inicializarGrupoDeDados();
        instance.bindListaDeCrises[grupoIndex].crises.push(modeloListaCrise);
        instance.crises.push(crise);
        spyOn(instance.bindListaDeCrises[grupoIndex].crises, 'splice').and.stub();
        spyOn(instance.crises, 'splice').and.stub();
        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();
        spyOn(instance._syncDataProvider, 'deleteDocById').and.stub();
        instance._excluirCrise(modeloListaCrise, grupoIndex, criseIndex).then(
            () => {
                expect(instance.bindListaDeCrises[grupoIndex].crises.splice).toHaveBeenCalledWith(criseIndex, 1);
                expect(instance.crises.splice).toHaveBeenCalledWith(criseIndex, 1);
                expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
            },
        ).then(
            () => {
                expect(instance._syncDataProvider.deleteDocById).toHaveBeenCalled();
                done();
            },
        );
    });

    it('Deve atualizar lista', (done) => {
        let crises: any = [{}, {}];
        spyOn(instance, '_construirLista').and.stub();
        let spyCarregarDados: jasmine.Spy = spyOn(instance, '_carregarDados').and.returnValue(Promise.resolve(crises));
        instance._atualizarLista();
        expect(instance.carregandoDados).toBe(true);
        expect(spyInicializarListaCrise).toHaveBeenCalled();
        expect(spyCarregarDados).toHaveBeenCalled();

        fixture.whenStable().then(
            () => {
                expect(instance._construirLista).toHaveBeenCalledWith(crises);
                done();
            },
        );
    });

    it('Deve inicializar a lista de crises', () => {
        spyOn(instance, '_definirPeriodoConsulta').and.stub();
        spyOn(instance, '_inicializarGrupoDeDados').and.stub();
        spyInicializarListaCrise.calls.reset();
        spyInicializarListaCrise.and.callThrough();

        instance._inicializarListaDeCrises();
        expect(instance._definirPeriodoConsulta).toHaveBeenCalled();
        expect(instance._inicializarGrupoDeDados).toHaveBeenCalled();
    });

    it('#_carregarDados - Deve funcionar sem retorno de dados', (done) => {

        instance.crises = [];
        let result: any = { docs: [] };
        let index: any = {
            index: {
                fields: ['type', 'dataInicial'],
            },
        };
        let query: any = {
            selector: {
                type: 'crise',
                dataInicial: {
                    $gte: instance._dataIni,
                    $lte: instance._dataFim,
                },
            },
            sort: [
                { type: 'desc' },
                { dataInicial: 'desc' },
            ],
        };

        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();
        let spyCreateIndex: jasmine.Spy = spyOn(instance._syncDataProvider, 'createIndex').and.returnValue(Promise.resolve(result));
        let spyFindQuery: jasmine.Spy = spyOn(instance._syncDataProvider, 'find').and.returnValue(Promise.resolve(result));
        instance._carregarDados().then(
            () => {
                expect(spyCreateIndex).toHaveBeenCalledWith(index);
                expect(spyFindQuery).toHaveBeenCalledWith(query);
                expect(instance.crises.length).toBe(0);
                expect(instance.carregandoDados).toBe(false);
                expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
                done();
            },
        );
    });

    it('#_carregarDados - Deve funcionar com retorno de dados', (done) => {
        let _crise: Crise = {
            _id: 'idCrise0',
            dataInicial: '2017-08-01',
            dataFinal: null,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: 'notas...',
            clima: null,
            gps: null,
            type: 'crise',
        };

        instance.crises = [];
        let result: any = { docs: [_crise] };
        let index: any = {
            index: {
                fields: ['type', 'dataInicial'],
            },
        };
        let query: any = {
            selector: {
                type: 'crise',
                dataInicial: {
                    $gte: instance._dataIni,
                    $lte: instance._dataFim,
                },
            },
            sort: [
                { type: 'desc' },
                { dataInicial: 'desc' },
            ],
        };

        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();
        let spyCreateIndex: jasmine.Spy = spyOn(instance._syncDataProvider, 'createIndex').and.returnValue(Promise.resolve(result));
        let spyFindQuery: jasmine.Spy = spyOn(instance._syncDataProvider, 'find').and.returnValue(Promise.resolve(result));
        instance._carregarDados().then(
            () => {
                expect(spyCreateIndex).toHaveBeenCalledWith(index);
                expect(spyFindQuery).toHaveBeenCalledWith(query);

                expect(instance.crises.length).toBeGreaterThan(0);
                expect(instance.carregandoDados).toBe(false);
                expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
                done();
            },
        );
    });

    it('Deve construir a lista', () => {
        let dataInicial: any = new Date(2017, 0, 1);
        dataInicial = moment(dataInicial).format(AppHelper.FORMATO_DATA_ISO_8601);
        let dataFinal: any = new Date(2017, 0, 1, 23, 59, 59);
        dataFinal = moment(dataFinal).format(AppHelper.FORMATO_DATA_ISO_8601);
        let crise1: Crise = {
            _id: 'idCrise0',
            dataInicial: dataInicial,
            dataFinal: dataFinal,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: 'notas...',
            clima: null,
            gps: null,
            type: 'crise',
        };
        let crise2: Crise = {
            _id: 'idCrise0',
            dataInicial: dataInicial,
            dataFinal: null,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: 'notas...',
            clima: null,
            gps: null,
            type: 'crise',
        };
        let arrayCrises: Array<Crise> = [crise1, crise2];

        spyOn(instance, '_addCriseParaLista').and.stub();
        instance._construirLista(arrayCrises);

        arrayCrises.forEach(
            (_crise) => {
                let textoDataCrise = moment.utc(_crise.dataInicial).calendar(moment().subtract(1, 'days').endOf('day'));
                let modeloListaCrise: ModeloListaCrise = new ModeloListaCrise();
                modeloListaCrise.setCrise(_crise);
                modeloListaCrise.setTextoData(textoDataCrise);
                let porcentagem: number = instance._appHelper.getPorcentagemCriseCompleta(_crise, instance._usuario);
                porcentagem = parseInt(porcentagem.toFixed(2), 10);
                modeloListaCrise.setPorcentagem(porcentagem);

                if (_crise.dataFinal) {
                    modeloListaCrise.setProgressoCor();
                    modeloListaCrise.setOcorrendo(false);

                    let duracaoEmMilisegundos: number = moment(_crise.dataInicial).diff(_crise.dataFinal);
                    modeloListaCrise.setDuracao(moment.duration(duracaoEmMilisegundos).humanize());
                } else {
                    modeloListaCrise.setProgressoCorFixo(Cor.LARANJA);
                    modeloListaCrise.setOcorrendo(true);
                }
                modeloListaCrise.definirDetalhesIntensidade();

                expect(instance._addCriseParaLista).toHaveBeenCalledWith(modeloListaCrise);
            },
        );

    });

    it('Deve definir período da consulta', () => {
        instance._dataAtual = moment().local();
        let dataIni: any = instance._dataAtual.startOf('day').format(AppHelper.FORMATO_DATA_ISO_8601);
        let dataFim: any = instance._dataAtual.endOf('day').format(AppHelper.FORMATO_DATA_ISO_8601);

        instance._definirPeriodoConsulta();
        expect(instance._dataIni).toBe(dataIni);
        expect(instance._dataFim).toBe(dataFim);
    });

    it('Deve definir inicializar grupo de dados da lista', () => {
        let modeloListaCrise: ModeloListaCrise = new ModeloListaCrise();
        modeloListaCrise.setOcorrendo(true);

        let crisesEmAberto: any = {
            label: 'MINHAS_CRISES.LABEL_OCORRENDO',
            cor: 'laranja',
            crises: [],
        };

        let crisesCompletas: any = {
            label: 'MINHAS_CRISES.LABEL_FINALIZADAS',
            cor: 'verde-claro',
            crises: [],
        };

        let bindListaDeCrises: Array<any> = [];
        bindListaDeCrises.push(crisesEmAberto);
        bindListaDeCrises.push(crisesCompletas);

        instance._inicializarGrupoDeDados();
        instance.bindListaDeCrises[0].crises.push(modeloListaCrise);
        instance._inicializarGrupoDeDados();
        expect(instance.bindListaDeCrises[0].crises).not.toContain(modeloListaCrise);

        expect(instance.bindListaDeCrises[0].label).toEqual('MINHAS_CRISES.LABEL_OCORRENDO');
        expect(instance.bindListaDeCrises[0].cor).toEqual('laranja');

        expect(instance.bindListaDeCrises[1].label).toEqual('MINHAS_CRISES.LABEL_FINALIZADAS');
        expect(instance.bindListaDeCrises[1].cor).toEqual('verde-claro');
    });

    it('Deve definir adicionar um modelo de crise para a lista de dados', () => {
        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();

        instance.bindListaDeCrises = [];
        let modeloListaCrise: ModeloListaCrise = new ModeloListaCrise();
        modeloListaCrise.setOcorrendo(true);
        instance._inicializarGrupoDeDados();
        instance._addCriseParaLista(modeloListaCrise);
        expect(instance.bindListaDeCrises[0].crises).toContain(modeloListaCrise);

        instance._inicializarGrupoDeDados();
        modeloListaCrise.setOcorrendo(false);
        instance._addCriseParaLista(modeloListaCrise);
        expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
        expect(instance.bindListaDeCrises[1].crises).toContain(modeloListaCrise);
    });
});
