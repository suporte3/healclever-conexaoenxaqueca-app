import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { Cor } from './../../core/models/pojo/cor.pojo';
import { ModeloListaCrise } from './../../core/models/pojo/lista-crise.pojo';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { AppHelper } from './../../core/helpers/app.helper';
import { Usuario } from './../../core/models/usuario/usuario.model';
import { Crise } from './../../core/models/crise.model';
import { Component, ChangeDetectorRef, Renderer } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import findIndex from 'lodash-es/findIndex';
import * as moment from 'moment';
import 'moment/min/locales';

@IonicPage()
@Component({
  selector: 'ib-page-minhas-crises',
  templateUrl: 'minhas-crises.html',
})
export class MinhasCrisesPage {

  public crises: Array<Crise> = [];
  public bindListaDeCrises: Array<any>;
  public carregandoDados: boolean;

  private _dataAtual: any;
  private _dataIni: string = null;
  private _dataFim: string = null;
  private _usuario: Usuario;
  private _appHelper: AppHelper;

  private TITULO_EXCLUIR: string = null;
  private DESCRICAO_EXCLUIR: string = null;
  private SUCESSO_EXCLUIR: string = null;
  private BTN_EXCLUIR_SIM: string = null;
  private BTN_EXCLUIR_CANCELAR: string = null;

  constructor(private _translate: TranslateService, private _syncDataProvider: SyncDataProvider, private _changeDetectorRef: ChangeDetectorRef,
    private _renderer: Renderer, private _navCtrl: NavController, private _navParams: NavParams) {

    this._iniciar();
  }

  ionViewWillEnter() {
    this.carregandoDados = true;
    this._atualizarLista();
  }

  public onClickRegistrarPrimeiraCrise() {
    this._navCtrl.push('DuracaoDorPage');
  }

  public onClickIrParaSumario(crise: Crise) {
    this._navCtrl.push('SumarioPage', { crise: crise });
  }

  public onClickExcluirCrise(card: any, modeloListaCrise: ModeloListaCrise, grupoIndex: number, criseIndex: number) {
    this._appHelper.sweetAlert({
      title: this.TITULO_EXCLUIR,
      text: this.DESCRICAO_EXCLUIR,
      type: null,
      showCancelButton: true,
      confirmButtonText: this.BTN_EXCLUIR_SIM,
      cancelButtonText: this.BTN_EXCLUIR_CANCELAR,
    }).then(
      () => {
        this._showDialogConfirmarExcluirCrise(card, modeloListaCrise, grupoIndex, criseIndex);
      },
    ).catch(
      (e) => {
        console.log(e);
        // TRACK ERROR
      },
    );
  }

  private _obterMensagens() {
    this._translate.get('MENSAGENS.EXCLUIR_TITULO').subscribe(
      (value: string) => {
        this.TITULO_EXCLUIR = value;
      },
    );

    this._translate.get('MENSAGENS.EXCLUIR_DESCRICAO').subscribe(
      (value: string) => {
        this.DESCRICAO_EXCLUIR = value;
      },
    );

    this._translate.get('MENSAGENS.EXCLUIR_SUCESSO').subscribe(
      (value: string) => {
        this.SUCESSO_EXCLUIR = value;
      },
    );

    this._translate.get('MENSAGENS.BTN_EXCLUIR_SIM').subscribe(
      (value: string) => {
        this.BTN_EXCLUIR_SIM = value;
      },
    );

    this._translate.get('MENSAGENS.BTN_EXCLUIR_CANCEL').subscribe(
      (value: string) => {
        this.BTN_EXCLUIR_CANCELAR = value;
      },
    );
  }

  private _showDialogConfirmarExcluirCrise(card: any, modeloListaCrise: ModeloListaCrise, grupoIndex: number, criseIndex: number) {

    this._appHelper.sweetAlert({
      title: this.TITULO_EXCLUIR,
      text: this.SUCESSO_EXCLUIR,
      type: 'success',
    }).then(
      () => {
        this._triggerDeleteAnimation(card);

        Observable.of(null).delay(505).subscribe(
          () => {
            this._excluirCrise(modeloListaCrise, grupoIndex, criseIndex);
          },
        );
      },
    ).catch(
      (e) => {
        console.log(e);
        // TRACK ERROR
      },
    );
  }

  private _iniciar() {
    this._obterMensagens();
    moment.locale('pt-BR');
    this.carregandoDados = true;
    this._appHelper = new AppHelper();
    this._appHelper.getUsuarioLogado()
      .then(
      (usuario: Usuario) => {
        this._usuario = usuario;
      },
      () => {

      },
    );

    this._dataAtual = this._navParams.get('dataAtual');
    this._inicializarListaDeCrises();
  }

  private _triggerDeleteAnimation(elementCard: any) {
    this._renderer.invokeElementMethod(
      elementCard,
      'animate',
      [
        [
          { transform: 'translate3d(0, 0, 0)' },
          { transform: 'translate3d(100%, 0, 0)' },
        ],
        {
          duration: 500,
          delay: 0,
          fill: 'forwards',
        },
      ],
    );
  }

  private _excluirCrise(modeloListaCrise: ModeloListaCrise, grupoIndex: number, criseIndex: number): Promise<any> {
    let scope: MinhasCrisesPage = this;

    return new Promise(
      function (resolve) {
        let listCriseIndex: number = findIndex(scope.crises, function (crise: Crise) {
          return crise._id === modeloListaCrise.getCrise()._id;
        });
        scope.bindListaDeCrises[grupoIndex].crises.splice(criseIndex, 1);
        if (listCriseIndex > -1) {
          scope.crises.splice(listCriseIndex, 1);
        }
        scope._changeDetectorRef.markForCheck();

        return resolve();
      },
    ).then(
      () => {
        scope._syncDataProvider.deleteDocById(modeloListaCrise.getCrise()._id);
      },
    );
  }

  private _atualizarLista() {
    this.carregandoDados = true;
    this._inicializarListaDeCrises();
    this._carregarDados().then(
      (crises: Crise[]) => {
        if (crises.length > 0) {
          this._construirLista(crises);
        }
      },
    );
  }

  private _inicializarListaDeCrises() {
    this._definirPeriodoConsulta();
    this._inicializarGrupoDeDados();
  }

  private _carregarDados(): Promise<any> {
    let scope: MinhasCrisesPage = this;

    return new Promise(
      function (resolve) {

        scope._syncDataProvider.createIndex({
          index: {
            fields: ['type', 'dataInicial'],
          },
        }).then(function () {
          scope._syncDataProvider.find({
            selector: {
              type: 'crise',
              dataInicial: {
                $gte: scope._dataIni,
                $lte: scope._dataFim,
              },
            },
            sort: [
              { type: 'desc' },
              { dataInicial: 'desc' },
            ],
          }).then(
            (result: any) => {

              if (result.docs.length > 0) {
                result.docs.forEach(
                  (doc: Crise) => {
                    scope.crises.push(doc);
                  },
                );
              }

              scope.carregandoDados = false;
              scope._changeDetectorRef.markForCheck();
              resolve(result.docs);
            },
          ).catch(
            (err: any) => {
              console.log('ERRO FIND', err);
            },
          );
        }).catch(function (err) {
          console.log(err);
        });
      },
    );
  }

  private _construirLista(crises: Array<Crise>) {

    crises.forEach(
      (crise: Crise) => {
        let momentCrise: any = moment.utc(crise.dataInicial);
        let textoDataCrise = momentCrise.calendar(moment().subtract(1, 'days').endOf('day'));
        let modeloListaCrise: ModeloListaCrise = new ModeloListaCrise();
        modeloListaCrise.setCrise(crise);
        modeloListaCrise.setTextoData(textoDataCrise);
        let porcentagem: number = this._appHelper.getPorcentagemCriseCompleta(crise, this._usuario);
        porcentagem = parseInt(porcentagem.toFixed(2), 10);
        modeloListaCrise.setPorcentagem(porcentagem);

        if (crise.dataFinal) {
          modeloListaCrise.setProgressoCor();
          modeloListaCrise.setOcorrendo(false);

          let duracaoEmMilisegundos: number = moment(crise.dataInicial).diff(crise.dataFinal);
          modeloListaCrise.setDuracao(moment.duration(duracaoEmMilisegundos).humanize());
        } else {
          modeloListaCrise.setProgressoCorFixo(Cor.LARANJA);
          modeloListaCrise.setOcorrendo(true);
        }

        modeloListaCrise.definirDetalhesIntensidade();
        this._addCriseParaLista(modeloListaCrise);
      });
  }

  private _definirPeriodoConsulta() {
    this._dataIni = this._dataAtual.startOf('day').format(AppHelper.FORMATO_DATA_ISO_8601);
    this._dataFim = this._dataAtual.endOf('day').format(AppHelper.FORMATO_DATA_ISO_8601);
  }

  private _inicializarGrupoDeDados() {
    this.bindListaDeCrises = [];
    this.crises = [];

    let crisesEmAberto: any = {
      label: 'MINHAS_CRISES.LABEL_OCORRENDO',
      cor: 'laranja',
      crises: [],
    };

    let crisesCompletas: any = {
      label: 'MINHAS_CRISES.LABEL_FINALIZADAS',
      cor: 'verde-claro',
      crises: [],
    };

    this.bindListaDeCrises.push(crisesEmAberto);
    this.bindListaDeCrises.push(crisesCompletas);
  }

  /**
   * Adiciona um objeto ModeloListaCrise para o bindListaDeCrises. Caso a crise esteja ocorrendo é adicionado para o primeiro grupo de dados, que são
   *   crises em andamento. Caso contrário é adicionado em grupo mês a mês.
   *   @param mes Indica o mês da crise, que é o identificador para o Stick Header
   *   @param modeloListaCrise Objeto modelo da crise que serve como View
   */
  private _addCriseParaLista(modeloListaCrise: ModeloListaCrise) {

    if (this.bindListaDeCrises && this.bindListaDeCrises.length > 0) {
      if (modeloListaCrise.isOcorrendo()) {
        this.bindListaDeCrises[0].crises.push(modeloListaCrise);
      } else {
        this.bindListaDeCrises[1].crises.push(modeloListaCrise);
      }
    }

    this._changeDetectorRef.markForCheck();
  }

}
