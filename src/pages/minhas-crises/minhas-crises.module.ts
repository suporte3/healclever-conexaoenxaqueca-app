import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhasCrisesPage } from './minhas-crises';

@NgModule({
  declarations: [
    MinhasCrisesPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhasCrisesPage),
    PipesModule,
    TranslateModule,
  ],
})
export class MinhasCrisesPageModule { }
