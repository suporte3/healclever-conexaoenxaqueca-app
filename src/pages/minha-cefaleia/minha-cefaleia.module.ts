import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from './../../components/components.module';
import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MinhaCefaleiaPage } from './minha-cefaleia';

@NgModule({
  declarations: [
    MinhaCefaleiaPage,
  ],
  imports: [
    IonicPageModule.forChild(MinhaCefaleiaPage),
    ComponentsModule,
    PipesModule,
    TranslateModule,
  ],
})
export class MinhaCefaleiaPageModule { }
