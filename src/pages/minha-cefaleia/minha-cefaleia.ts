import { CefaleiaEstatisticasService } from './../../services/cefaleia-estatisticas/cefaleia-estatisticas.service';
import { LocalizacaoPart } from './../../core/models/pojo/localizacao-dor-part.pojo';
import { AppHelper } from './../../core/helpers/app.helper';
import { Usuario } from './../../core/models/usuario/usuario.model';
import { SexoUsuario } from './../../core/models/usuario/sexo.enum';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { ConfiguracaoLocalizacaoDorTouch } from './../../core/models/pojo/configuracao-localizacao-dor-touch.pojo';
import { Component } from '@angular/core';
import { IonicPage } from 'ionic-angular';
import filter from 'lodash-es/filter';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'ib-page-minha-cefaleia',
  templateUrl: 'minha-cefaleia.html',
})
export class MinhaCefaleiaPage {

  public componenteConfigurado: boolean = false;
  public nrCrises: number = 0;
  public textoDuracaoDor: string = null;
  public dadosQuandoAcontece: Array<any> = [];
  public gatilhos: Array<any> = [];
  public sintomas: Array<any> = [];
  public dadosMenstruacao: Array<any> = [];
  public medicamentos: Array<any> = [];
  public metodosEficientes: Array<any> = [];
  public dadosSono: Array<any> = [];
  public localizacoesSelecionadas: Array<string> = [];
  public configuracaoLocalizacaoCanvasTouch: ConfiguracaoLocalizacaoDorTouch = new ConfiguracaoLocalizacaoDorTouch();

  private _dadosEstatisticaCrise: Array<any> = null;
  private _dadosEstatisticaSono: Array<any> = null;

  constructor(private _syncDataProvider: SyncDataProvider) {

    this._iniciar();
  }

  private _iniciar() {
    this._consultarDadosEstatisticos().then(
      () => {
        this._renderizarEstatisticas();
      },
      () => {

      },
    );
  }

  private _renderizarEstatisticas() {
    let cefaleiaEstatisticaService: CefaleiaEstatisticasService = new CefaleiaEstatisticasService();

    cefaleiaEstatisticaService.setDadosCrise(this._dadosEstatisticaCrise);
    cefaleiaEstatisticaService.setDadosSono(this._dadosEstatisticaSono);

    new AppHelper().getUsuarioLogado()
      .then(
      (usuario: Usuario) => {
        if (!usuario || usuario.genero === SexoUsuario.UKNOW || usuario.genero === SexoUsuario.FEMININO) {
          this.dadosMenstruacao = cefaleiaEstatisticaService.buildMenstruacao();
        }
      },
      () => {

      },
    );

    this.textoDuracaoDor = cefaleiaEstatisticaService.buildDuracaoDor();
    this.nrCrises = this._dadosEstatisticaCrise.length;

    this.dadosQuandoAcontece = cefaleiaEstatisticaService.buildQuandoAcontece();
    this.gatilhos = cefaleiaEstatisticaService.buildItensBarGraph('gatilhos');
    this.sintomas = cefaleiaEstatisticaService.buildItensBarGraph('sintomas');

    this.medicamentos = cefaleiaEstatisticaService.buildMedicamentos();
    this.metodosEficientes = cefaleiaEstatisticaService.buildMetodoAlivio();
    this.dadosSono = cefaleiaEstatisticaService.buildSono();

    this.localizacoesSelecionadas = cefaleiaEstatisticaService.buildLocalizacaoDor();
    let localizacoes: Array<LocalizacaoPart> = [];
    let extensao: string = '.svg';
    for (let i = 1; i < 20; i++) {
      let caminhoImagem = 'assets/img/localizacao-dor/localizacao_dor_' + i + extensao;
      let stringAreaId: string = i.toString();
      let isSelecionado: boolean = false;

      let localizacao: string[] = (filter(this.localizacoesSelecionadas, _id => _id === stringAreaId));
      if (localizacao.length > 0) {
        isSelecionado = true;
      }
      localizacoes[i] = new LocalizacaoPart(caminhoImagem, isSelecionado);
    }

    this.configuracaoLocalizacaoCanvasTouch.setImagemInicial('assets/img/localizacao-dor/localizacao_dor_0' + extensao);
    this.configuracaoLocalizacaoCanvasTouch.setImagemMapa('assets/img/localizacao-dor/localizacao_dor_map' + extensao);
    this.configuracaoLocalizacaoCanvasTouch.setLocalizacoes(localizacoes);
    this.configuracaoLocalizacaoCanvasTouch.setHasTouch(false);
    this.componenteConfigurado = true;
  }

  private _consultarDadosEstatisticos(): Promise<any> {
    let _this: MinhaCefaleiaPage = this;

    return new Promise(
      function (resolve, reject) {
        _this._consultarDadosCrise().then(
          (dadosCrise: any) => {
            _this._dadosEstatisticaCrise = dadosCrise.docs;
            _this._consultarDadosSono().then(
              (dadosSono: any) => {
                _this._dadosEstatisticaSono = dadosSono.docs;
                resolve();
              },
              () => {
                reject();
              },
            );
          },
          () => {
            reject();
          },
        );
      },
    );
  }

  private _consultarDadosCrise() {
    let dataFinal: string = moment().endOf('day').format(AppHelper.FORMATO_DATA_ISO_8601);
    let dataInicial: string = moment(dataFinal).startOf('day').subtract(90, 'days').format(AppHelper.FORMATO_DATA_ISO_8601);
    let _this: MinhaCefaleiaPage = this;

    return new Promise(
      function (resolve, reject) {
        _this._syncDataProvider.find(
          {
            selector: {
              type: 'crise',
              dataInicial: {
                $gte: dataInicial,
                $lte: dataFinal,
              },
              dataFinal: {
                $ne: null,
              },
            },
          },
        ).then(
          (result: any) => {

            resolve(result);
          },
        ).catch(
          (err: any) => {
            console.log('ERRO FIND CRISE', err);
            reject();
          },
        );
      },
    );
  }

  private _consultarDadosSono() {
    let dataFinal: string = moment().endOf('day').format(AppHelper.FORMATO_DATA_ISO_8601);
    let dataInicial: string = moment.utc(dataFinal).startOf('day').subtract(91, 'days').format(AppHelper.FORMATO_DATA_ISO_8601);
    let _this: MinhaCefaleiaPage = this;

    return new Promise(
      function (resolve, reject) {
        _this._syncDataProvider.find(
          {
            selector: {
              type: 'diario-saude',
              'sono.type': { $exists: true },
              'sono.inicio': {
                $gte: dataInicial,
                $lte: dataFinal,
              },
            },
          },
        ).then(
          (result: any) => {
            resolve(result);
          },
        ).catch(
          (err: any) => {
            console.log('ERRO FIND SONO', err);
            reject();
          },
        );
      },
    );
  }

}
