import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { TipoOpcao } from './../../core/models/tipo-opcao.enum';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Opcao } from './../../core/models/opcao.model';
import { AppHelper } from './../../core/helpers/app.helper';
import { NavParamsMock } from './../../core/mocks/nav-params.mock';
import { NavParams } from 'ionic-angular';
import { TestUtils } from './../../app/app.test';
import { Usuario } from './../../core/models/usuario/usuario.model';
import { SexoUsuario } from './../../core/models/usuario/sexo.enum';
import { Crise } from './../../core/models/crise.model';
import { SumarioPage } from './sumario';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Sumário Page:', () => {

    let fixture: ComponentFixture<SumarioPage> = null;
    let instance: any = null;
    let _instance: any = SumarioPage.prototype;
    let crise: Crise = {
        _id: 'criseId0',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };

    let usuario: Usuario = {
        _id: 'idUsuario',
        dataAdmissao: null,
        email: 'email@server.com',
        first_name: 'joão',
        last_name: 'silva sauro',
        birthday: null,
        genero: SexoUsuario.UKNOW,
        sessao: null,
        sincronizado: true,
        logado: true,
        facebook: false,
    };

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.beforeEachCompiler([SumarioPage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();

                instance._crise = crise;
            },
        );

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(instance).toBeDefined();
        expect(fixture).toBeDefined();
        done();
    });

    it('Deve iniciar o componente ao carregar', () => {
        expect(instance._crise).toEqual(crise);
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#_iniciar - Deve iniciar corretamente', (done) => {
        spyOn(AppHelper.prototype, 'getUsuarioLogado').and.returnValue(Promise.resolve(usuario));
        spyOn(instance, '_gerarPorcentagem').and.stub();
        instance._iniciar();
        expect(instance._appHelper).toEqual(new AppHelper());

        fixture.whenStable().then(
            () => {
                expect(instance._usuario).toEqual(usuario);
                expect(instance._gerarPorcentagem).toHaveBeenCalled();
                done();
            },
        );
    });

    it('#_gerarPorcentagem - Deve gerar o percentual somente se existir _usuario alocado em memória', () => {
        let porcentagem: number = 87.2345;
        let porcentagemArredondada: number = parseInt(porcentagem.toFixed(2), 10);
        let spyGetPorcentagemCriseCompleta: jasmine.Spy = spyOn(AppHelper.prototype, 'getPorcentagemCriseCompleta').and.returnValue(porcentagem);
        let spyGetIndiceRespostaCriseCor: jasmine.Spy = spyOn(AppHelper.prototype, 'getIndiceRespostaCriseCor').and.returnValue('#000000');
        instance._usuario = null;
        instance._gerarPorcentagem();
        expect(spyGetPorcentagemCriseCompleta).not.toHaveBeenCalled();
        expect(spyGetIndiceRespostaCriseCor).not.toHaveBeenCalled();

        spyGetPorcentagemCriseCompleta.calls.reset();
        spyGetIndiceRespostaCriseCor.calls.reset();
        instance._usuario = usuario;
        instance._gerarPorcentagem();
        expect(spyGetPorcentagemCriseCompleta).toHaveBeenCalledWith(crise, usuario);
        expect(spyGetIndiceRespostaCriseCor).toHaveBeenCalledWith(porcentagemArredondada);

    });

    it('#ionViewWillEnter - Deve montar o sumário ao entrar na tela somente se o componente estiver iniciado', () => {
        let spyMontarSumario: jasmine.Spy = spyOn(instance, '_montarSumario').and.stub();

        instance._componenteIniciado = false;
        instance.ionViewWillEnter();
        expect(spyMontarSumario).not.toHaveBeenCalled();

        spyMontarSumario.calls.reset();
        instance._componenteIniciado = true;
        instance.ionViewWillEnter();
        expect(spyMontarSumario).toHaveBeenCalled();
    });

    it('#onClickConfirmar - Deve voltar para a tela anterior', () => {
        spyOn(instance._navCtrl, 'pop').and.stub();
        instance.onClickConfirmar();
        expect(instance._navCtrl.pop).toHaveBeenCalled();
    });

    it('#onClickExcluirRegistroCrise - Deve apresentar Dialog para confirmar exclusão de crise', (done) => {
        spyOn(instance._appHelper, 'sweetAlert').and.returnValue(Promise.resolve());
        spyOn(instance, '_showDialogConfirmarExcluirCrise').and.stub();

        let opcoesDialog: any = {
            title: instance.TITULO_EXCLUIR,
            text: instance.DESCRICAO_EXCLUIR,
            type: null,
            showCancelButton: true,
            confirmButtonText: instance.BTN_EXCLUIR_SIM,
            cancelButtonText: instance.BTN_EXCLUIR_CANCELAR,
        };
        instance.onClickExcluirRegistroCrise();
        expect(instance._appHelper.sweetAlert).toHaveBeenCalledWith(opcoesDialog);

        fixture.whenStable().then(
            () => {
                expect(instance._showDialogConfirmarExcluirCrise).toHaveBeenCalled();
                done();
            },
        );
    });

    it('#_showDialogConfirmarExcluirCrise - Deve apresentar Dialog para confirmar operação ao excluir crise', (done) => {
        spyOn(instance._appHelper, 'sweetAlert').and.returnValue(Promise.resolve());
        spyOn(instance, '_excluirCrise').and.stub();
        spyOn(instance._navCtrl, 'pop').and.stub();

        let opcoesDialog: any = {
            title: instance.TITULO_EXCLUIR,
            text: instance.SUCESSO_EXCLUIR,
            type: 'success',
        };

        instance._showDialogConfirmarExcluirCrise();
        expect(instance._appHelper.sweetAlert).toHaveBeenCalledWith(opcoesDialog);
        fixture.whenStable().then(
            () => {
                expect(instance._excluirCrise).toHaveBeenCalled();
            },
        ).then(
            () => {
                expect(instance._navCtrl.pop).toHaveBeenCalled();
                done();
            },
            () => {

            },
        );
    });

    it('#_excluirCrise - Deve excluir registro da crise', () => {
        spyOn(instance._syncDataProvider, 'deleteDocById').and.stub();
        instance._excluirCrise();
        expect(instance._syncDataProvider.deleteDocById).toHaveBeenCalled();
    });

    it('Deve montar sumário', () => {
        spyOn(instance, '_montarItem').and.stub();

        let opcao: Opcao = {
            _id: 'idOpcao0',
            nome: 'nomeOpcao',
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };

        usuario.genero = SexoUsuario.MASCULINO;
        instance._crise = crise;
        instance._usuario = usuario;
        instance._montarSumario();
        expect(instance.bindItensSumario).toEqual([]);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.DURACAO);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.INTENSIDADE);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.MEDICAMENTO);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.QUALIDADE_DOR);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.SINTOMA);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.GATILHO);
        expect(instance._arraySortItens).not.toContain(ItemSumarioCrise.ESFORCO_FISICO);
        expect(instance._arraySortItens).not.toContain(ItemSumarioCrise.BEBIDA);
        expect(instance._arraySortItens).not.toContain(ItemSumarioCrise.COMIDA);
        expect(instance._arraySortItens).not.toContain(ItemSumarioCrise.CICLO_MENSTRUAL);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.LOCALIZACAO_DOR);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.METODO_ALIVIO);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.METODO_EFICIENTE);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.CONSEQUENCIAS);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.LOCAIS);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.ANOTACOES);

        expect(instance._montarItem).toHaveBeenCalled();

        usuario.genero = SexoUsuario.FEMININO;
        instance._usuario = usuario;
        instance._crise.gatilhos = [opcao];
        instance._montarSumario();
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.ESFORCO_FISICO);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.BEBIDA);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.COMIDA);
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.CICLO_MENSTRUAL);

        usuario.genero = SexoUsuario.UKNOW;
        instance._usuario = usuario;
        instance._montarSumario();
        expect(instance._arraySortItens).toContain(ItemSumarioCrise.CICLO_MENSTRUAL);
    });

    it('#_montarItem - Deve montar um item', () => {
        instance.bindItensSumario = [];
        spyOn(instance.bindItensSumario, 'push').and.stub();
        instance._crise = crise;
        let item: ItemSumarioCrise = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.DURACAO);
        item.setCrise(crise);

        instance._montarItem(ItemSumarioCrise.DURACAO);
        expect(instance.bindItensSumario.push).toHaveBeenCalledWith(item);
    });
});
