import { TranslateService } from '@ngx-translate/core';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { AppHelper } from './../../core/helpers/app.helper';
import { Usuario } from './../../core/models/usuario/usuario.model';
import { SexoUsuario } from './../../core/models/usuario/sexo.enum';
import { Crise } from './../../core/models/crise.model';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-sumario',
  templateUrl: 'sumario.html',
})
export class SumarioPage {

  public porcentagem: number;
  public porcentagemCor: string;
  public bindItensSumario: Array<ItemSumarioCrise>;
  private _crise: Crise;
  private _usuario: Usuario = null;
  private _appHelper: AppHelper;
  private _arraySortItens: Array<any>;
  private _componenteIniciado: boolean;

  private TITULO_EXCLUIR: string = null;
  private DESCRICAO_EXCLUIR: string = null;
  private SUCESSO_EXCLUIR: string = null;
  private BTN_EXCLUIR_SIM: string = null;
  private BTN_EXCLUIR_CANCELAR: string = null;

  constructor(private _translate: TranslateService, private _navCtrl: NavController, private _navParams: NavParams, private _syncDataProvider: SyncDataProvider) {
    this._crise = this._navParams.get('crise');

    this._iniciar().then(
      () => {
        this._montarSumario();
        this._componenteIniciado = true;
      },
    );
  }

  ionViewWillEnter() {
    if (this._componenteIniciado) {
      this._montarSumario();
    }
  }

  public onClickConfirmar() {
    this._navCtrl.pop();
  }

  public onClickExcluirRegistroCrise() {
    this._appHelper.sweetAlert({
      title: this.TITULO_EXCLUIR,
      text: this.DESCRICAO_EXCLUIR,
      type: null,
      showCancelButton: true,
      confirmButtonText: this.BTN_EXCLUIR_SIM,
      cancelButtonText: this.BTN_EXCLUIR_CANCELAR,
    }).then(
      () => {
        this._showDialogConfirmarExcluirCrise();
      },
    ).catch(
      (e) => {
        console.log(e);
        // TRACK ERROR
      },
    );
  }

  private _iniciar(): Promise<any> {
    this._obterMensagens();

    let scope: SumarioPage = this;
    return new Promise(
      function (resolve) {
        scope._appHelper = new AppHelper();
        scope._appHelper.getUsuarioLogado()
          .then(
          (usuario: Usuario) => {
            scope._usuario = usuario;
            scope._gerarPorcentagem();
            resolve();
          },
          () => {
            // Track error
            resolve();
          },
        );
      },
    );
  }

  private _obterMensagens() {
    this._translate.get('MENSAGENS.EXCLUIR_TITULO').subscribe(
      (value: string) => {
        this.TITULO_EXCLUIR = value;
      },
    );

    this._translate.get('MENSAGENS.EXCLUIR_DESCRICAO').subscribe(
      (value: string) => {
        this.DESCRICAO_EXCLUIR = value;
      },
    );

    this._translate.get('MENSAGENS.EXCLUIR_SUCESSO').subscribe(
      (value: string) => {
        this.SUCESSO_EXCLUIR = value;
      },
    );

    this._translate.get('MENSAGENS.BTN_EXCLUIR_SIM').subscribe(
      (value: string) => {
        this.BTN_EXCLUIR_SIM = value;
      },
    );

    this._translate.get('MENSAGENS.BTN_EXCLUIR_CANCEL').subscribe(
      (value: string) => {
        this.BTN_EXCLUIR_CANCELAR = value;
      },
    );
  }

  private _gerarPorcentagem() {
    if (this._usuario) {
      this.porcentagem = this._appHelper.getPorcentagemCriseCompleta(this._crise, this._usuario);
      this.porcentagem = parseInt(this.porcentagem.toFixed(2), 10);
      this.porcentagemCor = this._appHelper.getIndiceRespostaCriseCor(this.porcentagem);
    }
  }

  private _montarSumario() {
    let scope: SumarioPage = this;
    this.bindItensSumario = [];
    this._arraySortItens = [];

    this._arraySortItens.push(ItemSumarioCrise.DURACAO);
    this._arraySortItens.push(ItemSumarioCrise.CLIMA);
    this._arraySortItens.push(ItemSumarioCrise.INTENSIDADE);
    this._arraySortItens.push(ItemSumarioCrise.MEDICAMENTO);
    this._arraySortItens.push(ItemSumarioCrise.QUALIDADE_DOR);
    this._arraySortItens.push(ItemSumarioCrise.SINTOMA);
    this._arraySortItens.push(ItemSumarioCrise.GATILHO);
    if (this._crise.gatilhos[0]) {
      this._arraySortItens.push(ItemSumarioCrise.ESFORCO_FISICO);
      this._arraySortItens.push(ItemSumarioCrise.BEBIDA);
      this._arraySortItens.push(ItemSumarioCrise.COMIDA);
    }

    if (this._usuario) {
      if (typeof this._usuario.genero === 'number') {
        if (this._usuario.genero === SexoUsuario.UKNOW || this._usuario.genero === SexoUsuario.FEMININO) {
          this._arraySortItens.push(ItemSumarioCrise.CICLO_MENSTRUAL);
        }
      }
    }

    this._arraySortItens.push(ItemSumarioCrise.LOCALIZACAO_DOR);
    this._arraySortItens.push(ItemSumarioCrise.METODO_ALIVIO);
    this._arraySortItens.push(ItemSumarioCrise.METODO_EFICIENTE);
    this._arraySortItens.push(ItemSumarioCrise.CONSEQUENCIAS);
    this._arraySortItens.push(ItemSumarioCrise.LOCAIS);
    this._arraySortItens.push(ItemSumarioCrise.ANOTACOES);

    this._arraySortItens.forEach(
      (tipoItem) => {
        scope._montarItem(tipoItem);
      },
    );
  }

  private _montarItem(tipoItem: string) {
    let item: ItemSumarioCrise = new ItemSumarioCrise();

    item.setTipo(tipoItem);
    item.setCrise(this._crise);

    this.bindItensSumario.push(item);
  }

  private _showDialogConfirmarExcluirCrise() {

    this._appHelper.sweetAlert({
      title: this.TITULO_EXCLUIR,
      text: this.SUCESSO_EXCLUIR,
      type: 'success',
    }).then(
      () => {
        return this._excluirCrise();
      },
    ).then(
      () => {
        this._navCtrl.pop();
      },
      () => { },
    ).catch(
      (e) => {
        console.log(e);
        // TRACK ERROR
      },
    );
  }

  private _excluirCrise(): Promise<any> {

    return this._syncDataProvider.deleteDocById(this._crise._id);
  }

}
