import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from './../../pipes/pipes.module';
import { ComponentsModule } from './../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SumarioPage } from './sumario';

@NgModule({
  declarations: [
    SumarioPage,
  ],
  imports: [
    IonicPageModule.forChild(SumarioPage),
    ComponentsModule,
    PipesModule,
    TranslateModule,
  ],
})
export class SumarioPageModule { }
