import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { AuthProvider } from './../../providers/auth/auth';
import { TranslateService } from '@ngx-translate/core';
import { Component, ViewChild, ChangeDetectionStrategy, trigger, state, animate, transition, style } from '@angular/core';
import { IonicPage, NavController, Slides, Content, Platform, LoadingController, Loading, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';

export interface Slide {
  title: string;
  description: string;
  image: string;
}

@IonicPage()
@Component({
  selector: 'ib-page-tutorial',
  templateUrl: 'tutorial.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('focus', [
      state('inactive', style({
        transform: 'scale(1)',
      })),
      state('active', style({
        transform: 'scale(1.1)',
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out')),
    ]),
  ],
})
export class TutorialPage {

  @ViewChild('slides') slides: Slides;
  @ViewChild('content') content: Content;

  public bodySlides: Slide[];
  public isLastSlide = false;
  public isFirstSlide = true;
  public animationState: string = 'inactive';
  public facebookLoading: boolean = false;

  private MSG_FACEBOOK_FAIL: string = '';
  private MSG_SEM_CONEXAO: string = '';

  private _loadingPopup: Loading;

  constructor(private _translate: TranslateService, private _navCtrl: NavController, private _authService: AuthProvider,
    private _syncdataProvider: SyncDataProvider, private _platform: Platform, private _loadingCtrl: LoadingController,
    private _toastCtrl: ToastController) {

    this.bodySlides = [
      {
        title: null,
        description: null,
        image: 'assets/img/onboarding/dor-entendida-prevenida.png',
      },
      {
        title: null,
        description: null,
        image: 'assets/img/onboarding/descubra-causa-dor.png',
      },
      {
        title: null,
        description: null,
        image: 'assets/img/onboarding/conecte-se-medico.png',
      },
      {
        title: null,
        description: null,
        image: 'assets/img/onboarding/informacoes-em-segurancao.png',
      },
    ];

    this._obterMensagens();
    this._montarConteudoSlides();
  }

  ionViewWillEnter() {
    this.slides.update();
  }

  onSlideChangeStart(slider: Slides) {
    this.slides.update();
    this.isLastSlide = slider.isEnd();
    this.isFirstSlide = slider.isBeginning();
  }

  public callbackAnimationFocusDone() {
    this.animationState = 'inactive';
  }

  public onClickVamosComecar() {
    this.animationState = 'active';
  }

  public onClickEntrar() {
    // this._navCtrl.push('DiarioPage');
  }

  public onClickLoginEmail() {

    this._platform.ready().then(
      () => {
        if (AccountKitPlugin) {
          this._createAuthModal();
          this._loadingPopup.present();

          let options: any = {
            useAccessToken: false,
            defaultCountryCode: 'BR',
            facebookNotificationsEnabled: true,
          };

          AccountKitPlugin.loginWithEmail(
            options,
            (response: any) => {
              let authCode: string = response.code;
              this._authService.signup(authCode)
                .then(
                (token: any) => {
                  if (token) {

                    this._syncdataProvider.prepararUsuario().then(
                      () => {
                        this._loadingPopup.dismiss({ success: true });
                      },
                      () => {
                        let message: string = 'Ops! Ocorreu algum problema ao preparar seu usuário. Que tal tentar?';
                        this._loadingPopup.dismiss({ message: message });
                        // error - Problemas ao preparar usuário
                        // remover jwt_token
                      },
                    );
                  } else {
                    let message: string = 'Ops! Ocorreu algum problema interno ao autenticar seu usuário. Que tal tentar daqui a 30 minutos?';
                    this._loadingPopup.dismiss({ message: message });
                    // error - Erro no login de e-mail
                  }
                },
              );
            },
            (err: any) => {
              console.log('ERR', err);
              // Algum erro no servidor - instabilidade de internet
              let message: string = 'Ops! Ocorreu algum problema interno ao autenticar seu usuário. Que tal tentar daqui a 30 minutos?';
              this._loadingPopup.dismiss({ message: message });
            },
          );
        }
      },
    );
  }

  public onClickLoginSMS() {
    this._platform.ready().then(
      () => {
        if (AccountKitPlugin) {
          this._createAuthModal();
          this._loadingPopup.present();

          let options: any = {
            useAccessToken: false,
            defaultCountryCode: 'BR',
            facebookNotificationsEnabled: true,
          };

          AccountKitPlugin.loginWithPhoneNumber(
            options,
            (response: any) => {
              let authCode: string = response.code;
              this._authService.signup(authCode)
                .then(
                (token: any) => {
                  if (token) {

                    this._syncdataProvider.prepararUsuario().then(
                      () => {
                        this._loadingPopup.dismiss({ success: true });
                      },
                      () => {
                        let message: string = 'Ops! Ocorreu algum problema ao preparar seu usuário. Que tal tentar?';
                        this._loadingPopup.dismiss({ message: message });
                        // error - Problemas ao preparar usuário
                        // remover jwt_token
                      },
                    );
                  } else {
                    let message: string = 'Ops! Ocorreu algum problema interno ao autenticar seu usuário. Que tal tentar daqui a 30 minutos?';
                    this._loadingPopup.dismiss({ message: message });
                    // error - Erro no login de e-mail
                  }
                },
              );
            },
            (err: any) => {
              console.log('ERR', err);
              let message: string = 'Ops! Ocorreu algum problema interno ao autenticar seu usuário. Que tal tentar daqui a 30 minutos?';
              this._loadingPopup.dismiss({ message: message });
            },
          );
        }
      },
    );
  }

  public onClickLoginFacebook() {
    /*this.facebookLoading = true;
    let permissoesDoUsuario: string[] = ['email', 'public_profile', 'user_friends', 'user_birthday'];
    new Facebook().login(permissoesDoUsuario).then(
      (response: FacebookLoginResponse) => {
        this._getFacebookUserData(response);
      },
      (error) => {
        console.log('TRACK~onClickLoginFacebook~FACEBOOK', error);
        // TRACK
        this.facebookLoading = false;
        let network = new Network();
        if (network.type === 'none') {

          this.toastMensagem('Verifique sua conexão com a internet');
        } else {
          this.toastMensagem(this.MSG_FACEBOOK_FAIL);
        }
      },
    );*/
  }

  private _toastMensagem(mensagem: string) {
    let toast = this._toastCtrl.create({
      message: mensagem,
      position: 'bottom',
      showCloseButton: true,
      closeButtonText: 'Fechar',
    });

    toast.onDidDismiss(() => { });

    toast.present();
  }

  private _createAuthModal() {
    this._loadingPopup = this._loadingCtrl.create({
      content: '<strong>PREPARANDO USUÁRIO</strong><p>Aguarde alguns instantes.</p>',
      spinner: 'dots',
    });
    this._loadingPopup.onDidDismiss(
      (data: any) => {
        if (data.success) {
          this._goToRosie();
        } else {
          this._toastMensagem(data.message);
        }
      },
    );
  }

  private _goToRosie() {
    this._navCtrl.setRoot('ChatPage', null, { animate: true });
  }

  private _montarConteudoSlides() {

    this.bodySlides.forEach(
      (itemConteudo: any, i: number) => {
        itemConteudo.title = 'TUTORIAL.TITULO' + i;
        itemConteudo.description = 'TUTORIAL.DESCRICAO' + i;
      },
    );
  }

  private _obterMensagens() {
    this._translate.get('MENSAGENS.SEM_CONEXAO').subscribe(
      (value: string) => {
        this.MSG_SEM_CONEXAO = value;
      },
    );

    this._translate.get('MENSAGENS.FALHA_FACEBOOK').subscribe(
      (value: string) => {
        this.MSG_FACEBOOK_FAIL = value;
      },
    );
  }
  /*
    private _getFacebookUserData(facebookResponse: any) {
      new Facebook().api(facebookResponse.authResponse.userID + '/?fields=id,email,first_name,last_name, birthday', []).then(
        (facebookDados) => {
          let credenciais = { access_token: facebookResponse.authResponse.accessToken };
          this._authenticationService.setCredenciaisAutenticacao(credenciais);
          this._authenticationService.requisitarCadastroComFacebook(facebookDados);
        },
        (error) => {
          console.log('_getFacebookUserData', error);
          // TRACK
          this.facebookLoading = false;
          let network = new Network();
          if (network.type === 'none') {
            this.toastMensagem('Verifique sua conexão com a internet');
          } else {
            this.toastMensagem(this.MSG_FACEBOOK_FAIL);
          }
        },
      );
    }
  */
}
