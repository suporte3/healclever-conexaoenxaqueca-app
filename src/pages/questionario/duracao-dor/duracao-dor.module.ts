import { PipesModule } from './../../../pipes/pipes.module';
import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DuracaoDorPage } from './duracao-dor';

@NgModule({
  declarations: [
    DuracaoDorPage,
  ],
  imports: [
    IonicPageModule.forChild(DuracaoDorPage),
    PipesModule,
    ComponentsModule,
  ],
})
export class DuracaoDorPageModule { }
