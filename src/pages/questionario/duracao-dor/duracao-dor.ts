import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { SeedService } from './../../../services/seed/seed';
import { NotificationService } from './../../../services/notification/notification.service';
import { QuestaoPersonalizadaInterface } from './../../../core/models/interface/questao-personalizada.interface';
import { ConfiguracaoFooterBar } from './../../../core/models/pojo/configuracao-footer-bar.pojo';
import { ConfiguracaoNavBar } from './../../../core/models/pojo/configuracao-nav-bar.pojo';
import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { AppParam } from './../../../core/models/pojo/app-param.pojo';
import { Crise } from './../../../core/models/crise.model';
import { AppHelper } from './../../../core/helpers/app.helper';
import { Http } from '@angular/http';
import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { Component, ViewChildren, QueryList, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, Events, DateTime } from 'ionic-angular';
import * as moment from 'moment';
import 'moment/min/locales';

@IonicPage()
@Component({
  selector: 'ib-page-duracao-dor',
  templateUrl: 'duracao-dor.html',
})
export class DuracaoDorPage implements OnDestroy, QuestaoPersonalizadaInterface {

  @ViewChildren('datePicker') datePicker: QueryList<DateTime>;
  @ViewChildren('timePicker') timePicker: QueryList<DateTime>;

  public diasSemanaShort: Array<string>;
  public nomeMes: Array<string>;
  public minDate: any = moment().subtract(10, 'years').format(AppHelper.FORMATO_DATA_ISO_8601);
  public itensTempoInicial: Array<any>;
  public itensTempoFinal: Array<any>;
  public itens: Array<any>;
  public questaoPersonalizada: QuestaoPersonalizadaService = new QuestaoPersonalizadaService();
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public configuracaoFooterBar: ConfiguracaoFooterBar;
  public criseEmProgresso: boolean = false;

  private _appHelper: AppHelper;
  private _newDoc: boolean;
  private _docCrise: Crise;
  private readonly corOpcaoPadrao: string = 'verde-claro';
  private readonly corOpcaoSelecionada: string = 'laranja';
  private _tipoOpcao: string = null;

  constructor(private _navCtrl: NavController, private _navParams: NavParams, private _syncDataService: SyncDataProvider, private _platform: Platform,
    private _http: Http, private _events: Events) {

    this._iniciar();
  }

  ngOnDestroy() {
    this._destroyEvents();
  }

  ionViewWillEnter() {
    this._configurarQuestao();

    this._platform.ready().then(
      () => {
        this._prepararQuestao();
      });
  }

  public onChangeHandler(index: number) {
    if (this._tipoOpcao === 'input') {
      this.timePicker.toArray()[index].open();
      let item: any = this.itens[index];
      item.momentModel = moment(item.model).utc();
      this._atualizarDescricaoDeTempo(item);
    }
  }

  public onClickHandlerOpcoes(index: number, opcao: any) {
    this.questaoPersonalizada.setHouveInteracao(true);

    let array: Array<any> = this.itens[index].opcoes;
    array.forEach(
      (itemOpcao) => {
        itemOpcao.cor = this.corOpcaoPadrao;
      },
    );
    opcao.cor = this.corOpcaoSelecionada;

    this._definirAcaoTempo(opcao, index);
  }

  onClickHelp() {
    console.log('click help');
  }

  onClickNext() {
    this.questaoPersonalizada.registrarQuestao();
  }

  onClickDissmissQuestions() {
    this._navCtrl.setRoot('DiarioPage');
  }

  onClickConfirm() {
    this.questaoPersonalizada.registrarQuestao();
  }

  public onErrorSaveQuestion(err: any) { console.log(err); }
  public onSavingQuestion(estadoQuestao: boolean) {
    this.configuracaoFooterBar.setSaving(estadoQuestao);
  }

  public onCallbackPreRegistrarQuestao(): Promise<any> {
    let scope: DuracaoDorPage = this;

    return new Promise(
      function (resolve) {
        scope.questaoPersonalizada.getCrise().dataInicial = scope.itens[0].model;

        if (scope.criseEmProgresso) {
          scope.questaoPersonalizada.getCrise().dataFinal = null;
        } else {
          scope.questaoPersonalizada.getCrise().dataFinal = scope.itens[1].model;
        }

        if (scope._newDoc) {
          scope.questaoPersonalizada.getCrise()._id = AppHelper.generateUniqueID();
          scope._appHelper.setCriseId(scope.questaoPersonalizada.getCrise()._id);
          scope._newDoc = false;
        }

        resolve();
      },
    );
  }

  onCallbackPreAtualizarQuestao(): Promise<any> {
    return Promise.resolve();
  }

  onAtualizarQuestao() {
    this.itens[0].model = this.questaoPersonalizada.getCrise().dataInicial;
    this.itens[1].model = this.questaoPersonalizada.getCrise().dataFinal;

    this._setSelecionarOpcao(this.itensTempoInicial);
    this._setSelecionarOpcao(this.itensTempoFinal);
  }

  private _iniciar() {
    moment.locale('pt-BR');
    this.diasSemanaShort = moment.weekdaysShort();
    this.nomeMes = moment.months();

    let seedService = new SeedService(this._syncDataService, this._http);
    seedService.adicionarOpces();

    this._docCrise = (this._navParams.get(AppParam.CRISE_ID) ? this._navParams.get(AppParam.CRISE_ID) : null);
    this._newDoc = (this._docCrise ? false : true);

    this._iniciarConteudo();
    this._configurarNavBar();
    this._configurarFooterBar();

    this._listenEvents();
  }

  private _configurarQuestao() {
    this.questaoPersonalizada.setPage(this);
    this.questaoPersonalizada.setSyncDataServiceReference(this._syncDataService);
    this.questaoPersonalizada.setProximaPagina('IntensidadePage');

    if (this._navParams.get(AppParam.ROUTE) === 'sumario') {
      this.questaoPersonalizada.setSumario(true);
    }
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('Quando ocorreu a dor?');
    this.configuracaoNavBar.setIdentificadorGrid(TipoQuestao.DURACAO_DOR);
  }

  private _configurarFooterBar() {
    this.configuracaoFooterBar = new ConfiguracaoFooterBar();
    this.configuracaoFooterBar.setModoAtualizar(false);
    this.configuracaoFooterBar.setSaving(false);
    this.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.DURACAO_DOR);
  }

  private _iniciarConteudo() {
    this.itensTempoInicial = [
      {
        descricao: 'agora',
        icone: 'mm-duracao-agora',
        time: 'now',
        cor: this.corOpcaoSelecionada,
      },
      {
        descricao: '15 min. atrás',
        icone: 'mm-duracao-15',
        time: 15,
        cor: this.corOpcaoPadrao,
      },
      /*{
        descricao: '30 min. atrás',
        icone: 'mm-duracao-30',
        time: 30,
        cor: this.corOpcaoPadrao,
      },*/
      {
        descricao: '1 hora atrás',
        icone: 'mm-duracao-60',
        time: 60,
        cor: this.corOpcaoPadrao,
      },
      {
        descricao: 'outro tempo',
        icone: 'mm-duracao-input',
        time: 'input',
        cor: this.corOpcaoPadrao,
      },
    ];

    this.itensTempoFinal = [
      {
        descricao: 'não acabou',
        icone: 'mm-duracao-agora',
        time: 'isOpen',
        cor: this.corOpcaoSelecionada,
      },
      {
        descricao: 'agora',
        icone: 'mm-duracao-agora',
        time: 'now',
        cor: this.corOpcaoPadrao,
      },
      {
        descricao: '15 min. atrás',
        icone: 'mm-duracao-15',
        time: 15,
        cor: this.corOpcaoPadrao,
      },
      /*{
        descricao: '30 min. atrás',
        icone: 'mm-duracao-30',
        time: 30,
        cor: this.corOpcaoPadrao,
      },*/
      {
        descricao: 'outro tempo',
        icone: 'mm-duracao-input',
        time: 'input',
        cor: this.corOpcaoPadrao,
      },
    ];

    let momentModel: any = moment().local();

    this.itens = [
      {
        titulo: 'começou:',
        model: momentModel.format(AppHelper.FORMATO_DATA_ISO_8601),
        momentModel: momentModel,
        descricao: momentModel.calendar(),
        opcoes: this.itensTempoInicial,
        emProgresso: false,
      },
      {
        titulo: 'terminou:',
        model: momentModel.format(AppHelper.FORMATO_DATA_ISO_8601),
        momentModel: momentModel,
        descricao: momentModel.calendar(),
        opcoes: this.itensTempoFinal,
        emProgresso: true,
      },
    ];
  }

  private _prepararQuestao() {
    this._appHelper = new AppHelper();

    if (this._newDoc) {

      this.questaoPersonalizada.setHouveInteracao(true);
    } else {
      if (this._docCrise) {
        this.questaoPersonalizada.setCrise(this._docCrise);
        this.questaoPersonalizada.triggerAtualizarQuestao();
        this.configuracaoFooterBar.setModoAtualizar(true);
      } else {
        this.questaoPersonalizada.getCriseId()
          .then(
          (criseId: string) => {
            this.questaoPersonalizada.getCrise()._id = criseId;
          },
          (err) => {
            console.log('TRACK ERROR', err);
            // TRACK
          },
        );
      }

      this.questaoPersonalizada.setHouveInteracao(false);
    }
  }

  private _definirAcaoTempo(opcao: any, index: number) {
    if (opcao.time === 'input') {

      if (index === 1) {
        this.criseEmProgresso = false;
        this.itens[index].emProgresso = this.criseEmProgresso;
      }

      this._tipoOpcao = opcao.time;

      this.datePicker.toArray()[index].open();
    } else {
      this._tipoOpcao = opcao.time;
      this._definirTempo(index, opcao.time);
    }
  }

  private _definirTempo(index: number, time: any) {
    switch (time) {
      case 'now':
        this._definirTempoAtual(index);
        break;
      case 'isOpen':
        this._definirTempoEmProgresso(index);
        break;
      default:
        this._definirTempoMinutosAtras(index, time);
    }
  }

  private _definirTempoAtual(index: number) {
    let item: any = this.itens[index];
    item.momentModel = moment().local();
    item.model = item.momentModel.format(AppHelper.FORMATO_DATA_ISO_8601);
    this._atualizarDescricaoDeTempo(item);

    if (index === 1) {
      this.criseEmProgresso = false;
      this.itens[index].emProgresso = this.criseEmProgresso;
    }

    NotificationService.cancelarNotificaticao(NotificationService.CRISE_EM_PROGRESSO_NOTIFICATION_ID);
  }

  private _definirTempoEmProgresso(index: number) {
    let item: any = this.itens[index];

    if (index === 1) {
      this.criseEmProgresso = true;
      item.emProgresso = this.criseEmProgresso;
    }
  }

  private _definirTempoMinutosAtras(index: number, time: any) {
    let item: any = this.itens[index];

    if (index === 1) {
      this.criseEmProgresso = false;
      item.emProgresso = this.criseEmProgresso;
    }

    item.momentModel = moment(item.momentModel).subtract(time, 'minutes');
    item.model = item.momentModel.format(AppHelper.FORMATO_DATA_ISO_8601);
    this._atualizarDescricaoDeTempo(item);

    NotificationService.cancelarNotificaticao(NotificationService.CRISE_EM_PROGRESSO_NOTIFICATION_ID);
  }

  private _atualizarDescricaoDeTempo(item: any) {
    item.descricao = item.momentModel.calendar(null, {
      sameElse: 'L LT',
    });
  }

  private _setSelecionarOpcao(array: Array<any>) {
    let selecionado: boolean = false;
    array.forEach(
      (itemOpcao) => {
        if (!this.itens[1].model && itemOpcao.time === 'isOpen' && !selecionado) {
          itemOpcao.cor = this.corOpcaoSelecionada;
          selecionado = true;
        } else if (itemOpcao.time === 'input' && !selecionado) {
          itemOpcao.cor = this.corOpcaoSelecionada;
          selecionado = true;
        } else {
          itemOpcao.cor = this.corOpcaoPadrao;
        }
      },
    );
  }

  private _listenEvents() {
    this._events.subscribe('footer:onClickDissmissQuestions' + TipoQuestao.DURACAO_DOR,
      () => {
        this.onClickDissmissQuestions();
      },
    );

    this._events.subscribe('footer:onClickNext' + TipoQuestao.DURACAO_DOR,
      () => {
        this.onClickNext();
      },
    );

    this._events.subscribe('footer:onClickConfirm' + TipoQuestao.DURACAO_DOR,
      () => {
        this.onClickConfirm();
      },
    );
  }

  private _destroyEvents() {
    this._events.unsubscribe('footer:onClickDissmissQuestions' + TipoQuestao.DURACAO_DOR);
    this._events.unsubscribe('footer:onClickNext' + TipoQuestao.DURACAO_DOR);
    this._events.unsubscribe('footer:onClickConfirm' + TipoQuestao.DURACAO_DOR);
  }

}
