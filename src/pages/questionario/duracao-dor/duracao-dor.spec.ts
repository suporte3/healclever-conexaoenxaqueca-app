import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { NotificationService } from './../../../services/notification/notification.service';
import { SeedService } from './../../../services/seed/seed';
import { CapitalizePipe } from './../../../pipes/capitalize/capitalize';
import { NavParamsMock } from './../../../core/mocks/nav-params.mock';
import { NavParams } from 'ionic-angular';
import { TestUtils } from './../../../app/app.test';
import { Crise } from './../../../core/models/crise.model';
import { DuracaoDorPage } from './duracao-dor';
import { ComponentFixture, async, fakeAsync, tick } from '@angular/core/testing';

describe('Duração Dor Page:', () => {

    let fixture: ComponentFixture<DuracaoDorPage> = null;
    let instance: any = null;
    let _instance: any = DuracaoDorPage.prototype;
    let criseObjetoTest: Crise = {
        _id: 'idCrise',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );

        TestUtils.beforeEachCompiler([DuracaoDorPage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(instance).toBeDefined();
        expect(fixture).toBeDefined();
        done();
    });

    it('Deve iniciar o componente ao carregar', (done) => {
        expect(instance._iniciar).toHaveBeenCalled();
        done();
    });

    it('#ngOnDestroy - Deve destruir o componente', () => {
        spyOn(instance, '_destroyEvents').and.stub();

        instance.ngOnDestroy();

        expect(instance._destroyEvents).toHaveBeenCalled();
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        spyOn(SeedService.prototype, 'adicionarOpces').and.stub();
        spyOn(instance, '_iniciarConteudo').and.stub();
        spyOn(instance, '_configurarNavBar').and.stub();
        spyOn(instance, '_configurarFooterBar').and.stub();
        spyOn(instance, '_listenEvents').and.stub();

        instance._iniciar();

        expect(instance.diasSemanaShort).toBeDefined();
        expect(instance.nomeMes).toBeDefined();
        expect(SeedService.prototype.adicionarOpces).toHaveBeenCalled();
        expect(instance._iniciarConteudo).toHaveBeenCalled();
        expect(instance._configurarNavBar).toHaveBeenCalled();
        expect(instance._configurarFooterBar).toHaveBeenCalled();
        expect(instance._listenEvents).toHaveBeenCalled();
    });

    it('Deve configurar a questão sempre ao entrar na questão', (done) => {
        spyOn(instance, '_configurarQuestao').and.stub();
        spyOn(instance._platform, 'ready').and.returnValue(Promise.resolve());
        spyOn(instance, '_prepararQuestao').and.stub();
        instance.ionViewWillEnter();

        expect(instance._configurarQuestao).toHaveBeenCalled();
        expect(instance._platform.ready).toHaveBeenCalled();
        fixture.whenStable().then(
            () => {
                expect(instance._prepararQuestao).toHaveBeenCalled();
                done();
            },
        );
    });

    it('#onClickDissmissQuestions - Deve ir para o Painel', () => {
        spyOn(instance._navCtrl, 'setRoot').and.stub();
        instance.onClickDissmissQuestions();
        expect(instance._navCtrl.setRoot).toHaveBeenCalledWith('DiarioPage');
    });

    it('Deve selecionar uma opção e definir ação de tempo', () => {
        spyOn(instance, '_definirAcaoTempo').and.stub();
        let opcao: any = instance.itensTempoFinal[1];
        let index: number = 1;

        instance._configurarQuestao();
        spyOn(instance.questaoPersonalizada, 'setHouveInteracao').and.stub();
        instance.onClickHandlerOpcoes(index, opcao);

        let selecionados: number = 0;
        instance.itensTempoFinal.forEach(
            (item: any) => {
                if (item.cor === 'laranja') {
                    selecionados++;
                }
            },
        );

        expect(instance.questaoPersonalizada.setHouveInteracao).toHaveBeenCalledWith(true);
        expect(selecionados).toEqual(1);
        expect(instance._definirAcaoTempo).toHaveBeenCalledWith(opcao, index);
    });

    it('Deve preparar questão para documento existente', (done) => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'setHouveInteracao').and.stub();
        spyOn(instance.questaoPersonalizada, 'getCriseId').and.returnValue(Promise.resolve('criseId'));
        instance._docCrise = null;
        instance._newDoc = false;
        instance._prepararQuestao();
        expect(instance.questaoPersonalizada.getCriseId).toHaveBeenCalled();
        expect(instance.questaoPersonalizada.setHouveInteracao).toHaveBeenCalledWith(false);
        fixture.whenStable().then(
            () => {
                expect(instance.questaoPersonalizada.getCrise()._id).toEqual('criseId');
                done();
            },
        );
    });

    it('Deve preparar questão para documento existente - sumário', () => {
        instance._configurarQuestao();
        instance._configurarFooterBar();

        spyOn(instance.configuracaoFooterBar, 'setModoAtualizar').and.stub();
        spyOn(instance.questaoPersonalizada, 'setCrise').and.stub();
        spyOn(instance.questaoPersonalizada, 'triggerAtualizarQuestao').and.stub();
        instance._docCrise = criseObjetoTest;
        instance._newDoc = false;
        instance._prepararQuestao();
        expect(instance.questaoPersonalizada.triggerAtualizarQuestao).toHaveBeenCalled();
        expect(instance.questaoPersonalizada.setCrise).toHaveBeenCalledWith(instance._docCrise);
        expect(instance.configuracaoFooterBar.setModoAtualizar).toHaveBeenCalledWith(true);
    });

    it('Deve preparar questão para novo documento', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'setHouveInteracao').and.stub();
        spyOn(instance.questaoPersonalizada, 'getCriseId').and.callThrough();
        instance._newDoc = true;
        instance._prepararQuestao();
        expect(instance.questaoPersonalizada.getCriseId).not.toHaveBeenCalled();
        expect(instance.questaoPersonalizada.setHouveInteracao).toHaveBeenCalledWith(true);
    });

    it('#_definirTempo - Deve definir o tempo de acordo com a opção', () => {
        spyOn(instance, '_definirTempoEmProgresso').and.stub();
        spyOn(instance, '_definirTempoAtual').and.stub();
        spyOn(instance, '_definirTempoMinutosAtras').and.stub();
        let opcao: any;
        let index: number = 1;

        opcao = instance.itensTempoFinal[0];
        instance._definirTempo(index, opcao.time);
        expect(instance._definirTempoEmProgresso).toHaveBeenCalled();

        opcao = instance.itensTempoFinal[1];
        instance._definirTempo(index, opcao.time);
        expect(instance._definirTempoAtual).toHaveBeenCalled();

        opcao = instance.itensTempoFinal[2];
        instance._definirTempo(index, opcao.time);
        expect(instance._definirTempoMinutosAtras).toHaveBeenCalled();
    });

    it('#_definirTempoAtual - Deve efetuar tempo atual', () => {
        spyOn(NotificationService, 'cancelarNotificaticao').and.stub();
        spyOn(instance, '_atualizarDescricaoDeTempo').and.stub();

        let index: number = 1;

        instance._definirTempoAtual(index);
        expect(NotificationService.cancelarNotificaticao).toHaveBeenCalled();
        expect(instance.criseEmProgresso).toBe(false);
        expect(instance.itens[index].emProgresso).toBe(false);
        expect(instance._atualizarDescricaoDeTempo).toHaveBeenCalled();
    });

    it('#_definirTempoEmProgresso - Deve efetuar tempo em progresso', () => {
        let index: number = 1;

        instance._definirTempoEmProgresso(index);
        expect(instance.criseEmProgresso).toBe(true);
        expect(instance.itens[1].emProgresso).toBe(true);
    });

    it('#_definirTempoMinutosAtras - Deve efetuar tempo em minutos atrás', () => {
        spyOn(NotificationService, 'cancelarNotificaticao').and.stub();
        spyOn(instance, '_atualizarDescricaoDeTempo').and.stub();
        let index: number = 1;

        instance._definirTempoMinutosAtras(index);
        expect(NotificationService.cancelarNotificaticao).toHaveBeenCalled();
        expect(instance.criseEmProgresso).toBe(false);
        expect(instance.itens[index].emProgresso).toBe(false);
        expect(instance._atualizarDescricaoDeTempo).toHaveBeenCalled();
    });

    it('#onClickNext - Deve registrar a questão', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'registrarQuestao').and.stub();

        instance.onClickNext();
        expect(instance.questaoPersonalizada.registrarQuestao).toHaveBeenCalled();
    });

    it('#onClickConfirm - Deve registrar a questão', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'registrarQuestao').and.stub();

        instance.onClickConfirm();
        expect(instance.questaoPersonalizada.registrarQuestao).toHaveBeenCalled();
    });

    it('#onSavingQuestion - Deve alterar o estado da questão', () => {

        let estadoQuestao: boolean = true;
        instance.configuracaoFooterBar.setSaving(false);
        instance.onSavingQuestion(estadoQuestao);

        expect(instance.configuracaoFooterBar.isSaving()).toBe(estadoQuestao);
    });
    /*
        it('#onErrorSaveQuestion - Deve tratar ...', () => {
            // ...
        });
    */

    it('#onCallbackPreRegistrarQuestao - Deve atualizar os dados da crise antes de salvar - Crise em progresso', fakeAsync(() => {

        instance.criseEmProgresso = false;
        instance.onCallbackPreRegistrarQuestao();
        tick();

        expect(instance.questaoPersonalizada.getCrise().dataInicial).toEqual(instance.itens[0].model);
        expect(instance.questaoPersonalizada.getCrise().dataFinal).not.toBeNull();
    }));

    it('#onCallbackPreRegistrarQuestao - Deve atualizar os dados da crise antes de salvar - Crise finalizado', fakeAsync(() => {

        instance.criseEmProgresso = true;
        instance.onCallbackPreRegistrarQuestao();
        tick();

        expect(instance.questaoPersonalizada.getCrise().dataInicial).toEqual(instance.itens[0].model);
        expect(instance.questaoPersonalizada.getCrise().dataInicial).toEqual(instance.itens[1].model);
    }));

    it('#onCallbackPreRegistrarQuestao - Deve atualizar os dados da crise antes de salvar - Com novo documento', fakeAsync(() => {

        instance._newDoc = true;
        instance._prepararQuestao();
        spyOn(instance._appHelper, 'setCriseId').and.stub();

        instance.onCallbackPreRegistrarQuestao();
        tick();

        expect(instance.questaoPersonalizada.getCrise()._id).not.toBeNull();
        expect(instance._appHelper.setCriseId).toHaveBeenCalled();
        expect(instance._newDoc).toBe(false);
    }));

    it('#_configurarQuestao - Deve configurar a questão corretamente', () => {
        spyOn(QuestaoPersonalizadaService.prototype, 'setSumario').and.stub();

        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncDataService);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('IntensidadePage');
        expect(QuestaoPersonalizadaService.prototype.setSumario).not.toHaveBeenCalled();
    });

    it('#_configurarQuestao - Deve configurar a questão corretamente - sumário', () => {

        instance._navParams = new NavParams({ 'com.healclever.myMigraine.route': 'sumario' });
        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncDataService);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('IntensidadePage');
        expect(instance.questaoPersonalizada.isSumario()).toBe(true);
    });

    it('Deve configurar o <nav-bar /> corretamente', () => {
        instance._configurarNavBar();
        expect(instance.configuracaoNavBar.getTextoInicial()).toBe('Quando ocorreu a dor?');
    });

    it('Deve configurar o <footer-bar /> corretamente', () => {
        instance._configurarFooterBar();
        expect(instance.configuracaoFooterBar.isModoAtualizar()).toBe(false);
        expect(instance.configuracaoFooterBar.isSaving()).toBe(false);
        expect(instance.configuracaoFooterBar.getIdentificadorGrid()).toBe(TipoQuestao.DURACAO_DOR);
    });

    it('Deve atualizar opções da questão', () => {
        spyOn(instance, '_setSelecionarOpcao').and.stub();

        instance._configurarQuestao();
        instance.onAtualizarQuestao();
        expect(instance._setSelecionarOpcao).toHaveBeenCalledWith(instance.itensTempoInicial);
        expect(instance._setSelecionarOpcao).toHaveBeenCalledWith(instance.itensTempoFinal);
    });

    it('Deve selecionar itens iniciais e finais da questão', () => {
        instance._setSelecionarOpcao(instance.itensTempoInicial);
        let itensIniciais: number = 0;
        instance.itensTempoFinal.forEach(
            (item: any) => {
                if (item.cor === 'laranja') {
                    itensIniciais++;
                }
            },
        );
        expect(itensIniciais).toEqual(1);

        instance._setSelecionarOpcao(instance.itensTempoFinal);
        let itensFinais: number = 0;
        instance.itensTempoFinal.forEach(
            (item: any) => {
                if (item.cor === 'laranja') {
                    itensFinais++;
                }
            },
        );
        expect(itensFinais).toEqual(1);
    });

    it('#_listenEvents - Deve escutar eventos', () => {
        let spySubscribe: jasmine.Spy = spyOn(instance._events, 'subscribe').and.stub();

        instance._listenEvents();

        expect(spySubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.DURACAO_DOR);
        expect(spySubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.DURACAO_DOR);
        expect(spySubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.DURACAO_DOR);
    });

    it('#_destroyEvents - Deve destruir eventos que estão sendo escutados', () => {
        let spyUnsubscribe: jasmine.Spy = spyOn(instance._events, 'unsubscribe').and.stub();

        instance._destroyEvents();

        expect(spyUnsubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.DURACAO_DOR);
        expect(spyUnsubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.DURACAO_DOR);
        expect(spyUnsubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.DURACAO_DOR);
    });

});
