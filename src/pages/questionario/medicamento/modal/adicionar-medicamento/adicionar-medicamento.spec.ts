import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { AppHelper } from './../../../../../core/helpers/app.helper';
import { ViewControllerMock } from './../../../../../core/mocks/app.mock';
import { NavParamsMock } from './../../../../../core/mocks/nav-params.mock';
import { FormBuilder } from '@angular/forms';
import { ToastController, NavParams, ViewController } from 'ionic-angular';
import { TestUtils } from './../../../../../app/app.test';
import { AdicionarMedicamentoPage } from './adicionar-medicamento';
import { ComponentFixture, async, fakeAsync, tick } from '@angular/core/testing';

describe('Adicionar Medicamento Page:', () => {

    let fixture: ComponentFixture<AdicionarMedicamentoPage> = null;
    let instance: any = null;
    let _instance: any = AdicionarMedicamentoPage.prototype;

    beforeEach(async(() => {

        TestUtils.addProvider(ToastController);
        TestUtils.addProvider(FormBuilder);
        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.addProvider(
            { provide: ViewController, useClass: ViewControllerMock },
        );
        TestUtils.beforeEachCompiler([AdicionarMedicamentoPage]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o modal ao carregar o componente', () => {
        expect(instance._iniciar).toBeTruthy();
    });

    it('Deve iniciar o modal', () => {
        instance._iniciar();
        expect(instance._opcoesMedicamento).toBeDefined();
    });

    it('Deve fechar o modal', () => {
        spyOn(instance._viewCtrl, 'dismiss');
        instance.onClickCancelar();
        expect(instance._viewCtrl.dismiss).toHaveBeenCalled();
    });

    it('#onClickRegistrar - Deve registrar uma nova opção', fakeAsync(() => {
        // Configurações
        let spyAppUtilsAtivarOpcao: jasmine.Spy = spyOn(AppHelper.prototype, 'ativarOpcao').and.stub();
        let spyAppUtilsCriarNovaOpcao: jasmine.Spy = spyOn(AppHelper.prototype, 'criarNovaOpcao').and.callThrough();
        let spyAppUtilsIsOpcaoNativa: jasmine.Spy = spyOn(AppHelper.prototype, 'isOpcaoInativa').and.stub();
        let spyModeloOpcaoCriarModeloDeNovoMedicamento: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'criarModeloDeNovoMedicamento').and.callThrough();
        let spyDissmiss: jasmine.Spy = spyOn(instance._viewCtrl, 'dismiss').and.stub();
        spyOn(instance._syncDataProvider, 'addOrUpdate').and.returnValue(Promise.resolve());
        instance._opcoesMedicamento = {};
        instance._opcoesMedicamento.opcoes = [];

        spyAppUtilsIsOpcaoNativa.and.returnValue(null);
        spyOn(instance._opcoesMedicamento.opcoes, 'push').and.stub();
        spyModeloOpcaoCriarModeloDeNovoMedicamento.and.callThrough();
        spyAppUtilsAtivarOpcao.and.callThrough();
        instance.adicionarMedicamentoForm.get('nome').clearValidators();
        instance.adicionarMedicamentoForm.get('nome').setValue('opção medicamento teste');
        instance.adicionarMedicamentoForm.get('quantidade').clearValidators();
        instance.adicionarMedicamentoForm.get('quantidade').setValue(2);
        instance.medicamento.nome = 'opção medicamento teste';

        // Teste
        instance.onClickRegistrar();
        let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovoMedicamento(instance.opcao, instance.medicamento.quantidade);
        expect(spyAppUtilsAtivarOpcao).not.toHaveBeenCalled();
        expect(spyAppUtilsCriarNovaOpcao).toHaveBeenCalled();
        expect(instance._opcoesMedicamento.opcoes.push).toHaveBeenCalled();
        expect(instance._syncDataProvider.addOrUpdate).toHaveBeenCalled();
        tick();
        expect(spyDissmiss).toHaveBeenCalledWith({ 'opcao': instance._opcoesMedicamento, 'modeloOpcao': modeloOpcao });
    }));

    it('#onClickRegistrar - Deve ativar uma opção', fakeAsync(() => {
        // Configurações
        let spyAppUtilsAtivarOpcao: jasmine.Spy = spyOn(AppHelper.prototype, 'ativarOpcao').and.stub();
        let spyAppUtilsCriarNovaOpcao: jasmine.Spy = spyOn(AppHelper.prototype, 'criarNovaOpcao').and.stub();
        let spyAppUtilsIsOpcaoNativa: jasmine.Spy = spyOn(AppHelper.prototype, 'isOpcaoInativa').and.stub();
        let spyModeloOpcaoCriarModeloDeNovoMedicamento: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'criarModeloDeNovoMedicamento').and.stub();
        let spyDissmiss: jasmine.Spy = spyOn(instance._viewCtrl, 'dismiss').and.stub();
        spyOn(instance._syncDataProvider, 'addOrUpdate').and.returnValue(Promise.resolve());
        instance._opcoesMedicamento = {};
        instance._opcoesMedicamento.opcoes = [];

        spyModeloOpcaoCriarModeloDeNovoMedicamento.and.callThrough();
        spyAppUtilsAtivarOpcao.and.callThrough();
        instance.adicionarMedicamentoForm.get('nome').clearValidators();
        instance.adicionarMedicamentoForm.get('nome').setValue('opção medicamento teste');
        instance.adicionarMedicamentoForm.get('quantidade').clearValidators();
        instance.adicionarMedicamentoForm.get('quantidade').setValue(2);
        instance.opcao.ativo = false;
        spyAppUtilsIsOpcaoNativa.and.returnValue(instance.opcao);

        // Teste
        instance.onClickRegistrar();
        let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovoMedicamento(instance.opcao, instance.medicamento.quantidade);
        expect(spyAppUtilsAtivarOpcao).toHaveBeenCalled();
        expect(spyAppUtilsCriarNovaOpcao).not.toHaveBeenCalled();
        expect(spyModeloOpcaoCriarModeloDeNovoMedicamento).toHaveBeenCalled();
        expect(instance._syncDataProvider.addOrUpdate).toHaveBeenCalled();
        tick();
        expect(spyDissmiss).toHaveBeenCalledWith({ 'opcao': instance._opcoesMedicamento, 'modeloOpcao': modeloOpcao });
    }));

});
