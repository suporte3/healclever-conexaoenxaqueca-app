import { ValidationHelper } from './../../../../../core/helpers/validation.helper';
import { TipoOpcao } from './../../../../../core/models/tipo-opcao.enum';
import { AppHelper } from './../../../../../core/helpers/app.helper';
import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { Opcoes } from './../../../../../core/models/opcoes.model';
import { Medicamento } from './../../../../../core/models/medicamento.model';
import { Opcao } from './../../../../../core/models/opcao.model';
import { SyncDataProvider } from './../../../../../providers/sync-data/sync-data';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavParams, ViewController, ToastController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-medicamento',
  templateUrl: 'adicionar-medicamento.html',
})
export class AdicionarMedicamentoPage implements OnInit {

  @ViewChild('adicionarMedicamentoForm') adicionarMedicamentoForm: FormGroup;

  medicamento: Medicamento = <Medicamento>{
    nome: null,
    quantidade: null,
  };
  possuiMedicamentoIgual: boolean = false;

  private opcao: Opcao = {
    _id: null,
    nome: null,
    dataCriacao: null,
    ativo: true,
    tipo: TipoOpcao.DINAMICO,
  };

  private _opcoesMedicamento: Opcoes = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_MEDICAMENTO_ID,
    opcoes: [],
  };

  constructor(private _viewCtrl: ViewController, private _syncDataProvider: SyncDataProvider,
    private _navParams: NavParams, private _formBuilder: FormBuilder, private _toastCtrl: ToastController) {

    this._iniciar();
  }

  ngOnInit() {
    this.adicionarMedicamentoForm = this._formBuilder.group({
      nome: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          ValidationHelper.opcaoJaExiste.bind(this, this._opcoesMedicamento.opcoes),
        ],
        ),
      ],
      quantidade: [
        '',
        Validators.compose(
          [
            Validators.required,
            ValidationHelper.validarInputNumber,
          ],
        ),
      ],
    });
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  public onClickRegistrar() {
    if (this.adicionarMedicamentoForm.valid) {

      let appHelper: AppHelper = new AppHelper();
      let opcaoInativa: any = appHelper.isOpcaoInativa(this.medicamento.nome, this._opcoesMedicamento.opcoes);
      if (opcaoInativa) {
        this.opcao = appHelper.ativarOpcao(opcaoInativa);
      } else {
        this.opcao = appHelper.criarNovaOpcao(this.medicamento.nome);
        this._opcoesMedicamento.opcoes.push(this.opcao);
      }

      let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovoMedicamento(this.opcao, this.medicamento.quantidade);

      this._syncDataProvider.addOrUpdate(this._opcoesMedicamento).then(
        () => {
          this._viewCtrl.dismiss({ 'opcao': this._opcoesMedicamento, 'modeloOpcao': modeloOpcao });
        },
        (err) => {
          console.log('ERRO AO CADASTRAR MEDICAMENTO', err);
          appHelper.toastMensagem(this._toastCtrl, 'Não foi possível cadastrar o medicamento');
          appHelper.retirarOpcaoDoArray(this._opcoesMedicamento.opcoes, this.opcao);
          this.onClickCancelar();
          // TRACK
        },
      ).catch(
        (e) => {
          console.log('erro catch', e);
        },
      );
    }
  }

  private _iniciar() {
    let opcoes: Opcoes = this._navParams.get('opcoes');
    if (opcoes) {
      this._opcoesMedicamento = opcoes;
    }
  }

}
