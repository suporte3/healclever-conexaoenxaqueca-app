import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarMedicamentoPage } from './adicionar-medicamento';

@NgModule({
  declarations: [
    AdicionarMedicamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarMedicamentoPage),
  ],
})
export class AdicionarMedicamentoPageModule {}
