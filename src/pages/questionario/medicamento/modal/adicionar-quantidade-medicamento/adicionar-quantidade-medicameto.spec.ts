import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { CapitalizePipe } from './../../../../../pipes/capitalize/capitalize';
import { ViewControllerMock } from './../../../../../core/mocks/app.mock';
import { NavParamsMock } from './../../../../../core/mocks/nav-params.mock';
import { NavParams, ViewController } from 'ionic-angular';
import { TestUtils } from './../../../../../app/app.test';
import { AdicionarQuantidadeMedicamentoPage } from './adicionar-quantidade-medicamento';
import { ComponentFixture, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';

describe('Adicionar Quantidade Medicamento Page:', () => {

    let fixture: ComponentFixture<AdicionarQuantidadeMedicamentoPage> = null;
    let instance: any = null;
    let _instance: any = AdicionarQuantidadeMedicamentoPage.prototype;
    let spyCallbackAnimationFocusDone: jasmine.Spy = null;

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.addProvider(
            { provide: ViewController, useClass: ViewControllerMock },
        );
        TestUtils.beforeEachCompiler([AdicionarQuantidadeMedicamentoPage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
        spyCallbackAnimationFocusDone = spyOn(AdicionarQuantidadeMedicamentoPage.prototype, 'callbackAnimationFocusDone');
        spyCallbackAnimationFocusDone.and.stub();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o modal ao carregar o componente', () => {
        expect(instance._iniciar).toBeTruthy();
    });

    it('Deve iniciar o modal', () => {
        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setQuantidade(1);
        instance.modeloOpcao = undefined;
        instance.menuOpcoes = undefined;
        instance._navParams = new NavParams({ 'modeloOpcao': modeloOpcao });

        instance._iniciar();
        expect(instance.modeloOpcao).toBeDefined();
        expect(instance.menuOpcoes).toBeDefined();
        expect(instance._goingOut).toBe(false);
    });

    it('Deve fechar o modal', () => {
        spyOn(instance, '_sairComParamentro').and.stub();
        instance.onClickCancelar();
        expect(instance._sairComParamentro).toHaveBeenCalledWith(false);
    });

    it('Deve adicionar uma pirula', () => {
        instance.quantidadeMedicamento = 0;
        instance.animationState = undefined;

        instance.addUmaPirula();
        expect(instance.quantidadeMedicamento).toEqual(1);
        expect(instance.animationState).toEqual('active');
    });

    it('Deve adicionar meia pirula', () => {
        instance.quantidadeMedicamento = 0;
        instance.animationState = undefined;

        instance.addMeiaPirula();
        expect(instance.quantidadeMedicamento).toEqual(0.5);
        expect(instance.animationState).toEqual('active');
    });

    it('Deve retirar uma pirula', () => {
        instance.quantidadeMedicamento = 2;
        instance.animationState = undefined;

        instance.retirarUmaPirula();
        expect(instance.quantidadeMedicamento).toEqual(1);
        expect(instance.animationState).toEqual('active');

        instance.quantidadeMedicamento = 0.5;
        instance.retirarUmaPirula();
        expect(instance.quantidadeMedicamento).toEqual(0);

        instance.quantidadeMedicamento = 0;
        instance.retirarUmaPirula();
        expect(instance.quantidadeMedicamento).toEqual(0);
    });

    it('Deve retirar meia pirula', () => {
        instance.quantidadeMedicamento = 2;
        instance.animationState = undefined;

        instance.retirarMeiaPirula();
        expect(instance.quantidadeMedicamento).toEqual(1.5);
        expect(instance.animationState).toEqual('active');

        instance.quantidadeMedicamento = 0;
        instance.retirarMeiaPirula();
        expect(instance.quantidadeMedicamento).toEqual(0);
    });
    it('#onClickRegistrar - Deve registrar', () => {
        let spyModeloOpcaoSetQuantidade: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setQuantidade');
        spyOn(instance, '_sairComParamentro').and.stub();
        instance.quantidadeMedicamento = 1;
        instance._quantidadeMedicamentoAntigo = 0;

        instance.onClickRegistrar();
        expect(spyModeloOpcaoSetQuantidade).toHaveBeenCalledWith(instance.quantidadeMedicamento);
        expect(instance._sairComParamentro).toHaveBeenCalledWith(true);

        spyModeloOpcaoSetQuantidade.calls.reset();
        instance.quantidadeMedicamento = 0;
        instance._quantidadeMedicamentoAntigo = 0;
        instance.onClickRegistrar();
        expect(spyModeloOpcaoSetQuantidade).not.toHaveBeenCalled();
        expect(instance._sairComParamentro).toHaveBeenCalledWith(false);
    });

    it('#callbackAnimationFocusDone - Deve definir callback de animação', () => {
        spyCallbackAnimationFocusDone.calls.reset();
        spyCallbackAnimationFocusDone.and.callThrough();
        instance.animationState = 'active';
        instance.callbackAnimationFocusDone();
        instance.animationState = 'inactive';
    });

    it('#onClickAcaoMenu - Deve definir ação de click do usuário', () => {
        let spyAddUmaPilula: jasmine.Spy = spyOn(instance, 'addUmaPirula').and.stub();
        let spyAddMeiaPilula: jasmine.Spy = spyOn(instance, 'addMeiaPirula').and.stub();
        let spyRetirarMeiaPilula: jasmine.Spy = spyOn(instance, 'retirarMeiaPirula').and.stub();
        let spyRetirarUmaPilula: jasmine.Spy = spyOn(instance, 'retirarUmaPirula').and.stub();

        instance.onClickAcaoMenu('addUmaPirula');
        expect(spyAddUmaPilula).toHaveBeenCalled();
        expect(spyAddMeiaPilula).not.toHaveBeenCalled();
        expect(spyRetirarMeiaPilula).not.toHaveBeenCalled();
        expect(spyRetirarUmaPilula).not.toHaveBeenCalled();

        spyAddUmaPilula.calls.reset();
        spyAddMeiaPilula.calls.reset();
        spyRetirarMeiaPilula.calls.reset();
        spyRetirarUmaPilula.calls.reset();
        instance.onClickAcaoMenu('addMeiaPirula');
        expect(spyAddUmaPilula).not.toHaveBeenCalled();
        expect(spyAddMeiaPilula).toHaveBeenCalled();
        expect(spyRetirarMeiaPilula).not.toHaveBeenCalled();
        expect(spyRetirarUmaPilula).not.toHaveBeenCalled();

        spyAddUmaPilula.calls.reset();
        spyAddMeiaPilula.calls.reset();
        spyRetirarMeiaPilula.calls.reset();
        spyRetirarUmaPilula.calls.reset();
        instance.onClickAcaoMenu('retirarMeiaPirula');
        expect(spyAddUmaPilula).not.toHaveBeenCalled();
        expect(spyAddMeiaPilula).not.toHaveBeenCalled();
        expect(spyRetirarMeiaPilula).toHaveBeenCalled();
        expect(spyRetirarUmaPilula).not.toHaveBeenCalled();

        spyAddUmaPilula.calls.reset();
        spyAddMeiaPilula.calls.reset();
        spyRetirarMeiaPilula.calls.reset();
        spyRetirarUmaPilula.calls.reset();
        instance.onClickAcaoMenu('retirarUmaPirula');
        expect(spyAddUmaPilula).not.toHaveBeenCalled();
        expect(spyAddMeiaPilula).not.toHaveBeenCalled();
        expect(spyRetirarMeiaPilula).not.toHaveBeenCalled();
        expect(spyRetirarUmaPilula).toHaveBeenCalled();
    });

    it('#_sairComParamentro - Deve sair do modal corretamente com parametro', (done) => {
        let paramentro: boolean = true;
        instance._goingOut = false;
        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('id-opcao');
        instance.modeloOpcao = modeloOpcao;
        spyOn(instance._viewCtrl, 'dismiss');

        instance._sairComParamentro(paramentro);
        expect(instance._goingOut).toBe(true);

        let obs = Observable.of(1).delay(500);
        obs.subscribe(
            () => {
                expect(instance._viewCtrl.dismiss).toHaveBeenCalledWith({ 'modeloOpcao': instance.modeloOpcao });
                done();
            },
        );
    });

    it('#_sairComParamentro - Deve sair do modal corretamente sem parametro', (done) => {
        let paramentro: boolean = false;
        instance._goingOut = false;
        spyOn(instance._viewCtrl, 'dismiss');

        instance._sairComParamentro(paramentro);
        expect(instance._goingOut).toBe(true);

        let obs = Observable.of(1).delay(500);
        obs.subscribe(
            () => {
                expect(instance._viewCtrl.dismiss).toHaveBeenCalled();
                done();
            },
        );
    });
});
