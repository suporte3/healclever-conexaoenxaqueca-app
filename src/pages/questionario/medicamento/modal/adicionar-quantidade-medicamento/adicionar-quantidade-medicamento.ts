import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { Component, trigger, state, style, transition, animate } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-quantidade-medicamento',
  templateUrl: 'adicionar-quantidade-medicamento.html',
  animations: [
    trigger('focus', [
      state('inactive', style({
        transform: 'scale(1)',
      })),
      state('active', style({
        transform: 'scale(1.1)',
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out')),
    ]),
  ],
})
export class AdicionarQuantidadeMedicamentoPage {

  public quantidadeMedicamento: number = 0;
  public modeloOpcao: ModeloOpcao = new ModeloOpcao();
  public menuOpcoes: Array<any>;
  public animationState: string = 'inactive';
  private _goingOut: boolean;
  private _quantidadeMedicamentoAntigo: number;

  constructor(private _viewCtrl: ViewController, private _navParams: NavParams) {

    this._iniciar();
  }

  onClickCancelar() {
    this._sairComParamentro(false);
  }

  onClickRegistrar() {
    let isMedicamento: boolean = false;
    if (this.quantidadeMedicamento > 0 || this._quantidadeMedicamentoAntigo > 0) {
      this.quantidadeMedicamento = (this.quantidadeMedicamento === 0 ? null : this.quantidadeMedicamento);
      this.modeloOpcao.setQuantidade(this.quantidadeMedicamento);
      isMedicamento = true;
    }

    this._sairComParamentro(isMedicamento);
  }

  public callbackAnimationFocusDone() {
    this.animationState = 'inactive';
  }

  public onClickAcaoMenu(acao: string) {
    switch (acao) {
      case 'addUmaPirula':
        this.addUmaPirula();
        break;
      case 'addMeiaPirula':
        this.addMeiaPirula();
        break;
      case 'retirarMeiaPirula':
        this.retirarMeiaPirula();
        break;
      case 'retirarUmaPirula':
        this.retirarUmaPirula();
        break;
      default:
        console.log('TRACK ERROR');
    }
  }

  public addUmaPirula() {
    this.quantidadeMedicamento = this.quantidadeMedicamento + 1;
    this.animationState = 'active';
  }

  public addMeiaPirula() {
    this.quantidadeMedicamento = this.quantidadeMedicamento + 0.5;
    this.animationState = 'active';
  }

  public retirarMeiaPirula() {
    if ((this.quantidadeMedicamento - 0.5) < 0) {
      return;
    }
    this.animationState = 'active';
    this.quantidadeMedicamento = this.quantidadeMedicamento - 0.5;
  }

  public retirarUmaPirula() {
    if ((this.quantidadeMedicamento - 1) < 0) {
      this.quantidadeMedicamento = 0;
      return;
    }
    this.animationState = 'active';
    this.quantidadeMedicamento = this.quantidadeMedicamento - 1;
  }

  private _iniciar() {
    this._goingOut = false;

    if (this._navParams.get('modeloOpcao') !== 'default') {
      this.modeloOpcao = this._navParams.get('modeloOpcao');

      if (this.modeloOpcao) {
        if (this.modeloOpcao.getQuantidade()) {
          this.quantidadeMedicamento = Number(this.modeloOpcao.getQuantidade());
          this._quantidadeMedicamentoAntigo = this.quantidadeMedicamento;
        }
      }

      this.menuOpcoes = [
        {
          backgroundColor: 'verde-claro-background',
          acao: 'addUmaPirula',
          texto: '+1',
        },
        {
          backgroundColor: 'verde-claro-background',
          acao: 'addMeiaPirula',
          texto: '+0.5',
        },
        {
          backgroundColor: 'laranja-background',
          acao: 'retirarMeiaPirula',
          texto: '-0.5',
        },
        {
          backgroundColor: 'laranja-background',
          acao: 'retirarUmaPirula',
          texto: '-1',
        },
      ];
    }
  }

  private _sairComParamentro(parametro: boolean) {
    if (!this._goingOut) {
      document.getElementById('menu-toggler').remove();
      this._goingOut = true;
      let scope: AdicionarQuantidadeMedicamentoPage = this;
      setTimeout(function () {
        if (parametro) {
          scope._viewCtrl.dismiss({ 'modeloOpcao': scope.modeloOpcao });
        } else {
          scope._viewCtrl.dismiss();
        }
      }, 500);
    }
  }

}
