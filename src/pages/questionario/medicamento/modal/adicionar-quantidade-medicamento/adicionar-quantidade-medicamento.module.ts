import { PipesModule } from './../../../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarQuantidadeMedicamentoPage } from './adicionar-quantidade-medicamento';

@NgModule({
  declarations: [
    AdicionarQuantidadeMedicamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarQuantidadeMedicamentoPage),
    PipesModule,
  ],
})
export class AdicionarQuantidadeMedicamentoPageModule {}
