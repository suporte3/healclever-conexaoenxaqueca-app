import { ComponentsModule } from './../../../components/components.module';
import { PipesModule } from './../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MedicamentoPage } from './medicamento';

@NgModule({
  declarations: [
    MedicamentoPage,
  ],
  imports: [
    IonicPageModule.forChild(MedicamentoPage),
    PipesModule,
    ComponentsModule,
  ],
})
export class MedicamentoPageModule { }
