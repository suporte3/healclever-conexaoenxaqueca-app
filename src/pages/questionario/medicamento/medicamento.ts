import { GridQuestionarioComponent } from './../../../components/grid-questionario/grid-questionario';
import { QuestaoGridService } from './../../../services/questao-grid/questao-grid.service';
import { ConfiguracaoGrid } from './../../../core/models/pojo/configuracao-grid.pojo';
import { Crise } from './../../../core/models/crise.model';
import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { AppParam } from './../../../core/models/pojo/app-param.pojo';
import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { ConfiguracaoFooterBar } from './../../../core/models/pojo/configuracao-footer-bar.pojo';
import { ConfiguracaoNavBar } from './../../../core/models/pojo/configuracao-nav-bar.pojo';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-medicamento',
  templateUrl: 'medicamento.html',
})
export class MedicamentoPage implements OnDestroy {

  public medicamentos: Array<any>;
  public componentesConfigurado: boolean;
  public configuracaoGrid: ConfiguracaoGrid = new ConfiguracaoGrid();
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public primeiroCadastro: boolean = false;
  public configuracaoFooterBar: ConfiguracaoFooterBar = new ConfiguracaoFooterBar();
  @ViewChild('grid') grid: GridQuestionarioComponent;

  private _questaoGrid: QuestaoGridService = new QuestaoGridService();
  private _docCrise: Crise;

  constructor(private _navCtrl: NavController, private _modalCtrl: ModalController, private _navParams: NavParams,
    private _events: Events, private _syncDataService: SyncDataProvider) {

    this._iniciar();
  }

  ngOnDestroy() {
    this._destroyEvents();
  }

  ionViewDidEnter() {
    this._questaoGrid.setGrid(this.grid);

    this._questaoGrid.iniciarGrid(this._docCrise).then(
      () => {
        this._questaoGrid.montarGrid();
        this.componentesConfigurado = this._questaoGrid.componentesConfigurado;
      },
      () => {
        // Não foi possível iniciar, devido a não ser possível obter os dados relacionado a este registro
        // Pensar em um tratamento
        console.log('Não foi possível iniciar, devido a não ser possível obter os dados relacionado a este registro');
      },
    );
  }

  onClickConfirm() {
    this._questaoGrid.salvarOpcoes();
  }

  onClickNext() {
    this._questaoGrid.salvarOpcoes();
  }

  onClickDissmissQuestions() {
    this._navCtrl.setRoot('DiarioPage');
  }

  onPrimeiroCadastroMedicamento(estado: boolean) {
    this.primeiroCadastro = estado;
  }

  private _iniciar() {
    this.componentesConfigurado = this._questaoGrid.componentesConfigurado;
    this._docCrise = (this._navParams.get(AppParam.CRISE_ID) ? this._navParams.get(AppParam.CRISE_ID) : null);

    this._configurarNavBar();
    this._configurarFooterBar();
    this._configurarGrid();

    this._listenEvents();
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('Tomou algum medicamento?');
    this.configuracaoNavBar.setTextoModoRemover('Toque no medicamento que deseja remover:');
    this.configuracaoNavBar.setIdentificadorGrid(TipoQuestao.MEDICAMENTO);
  }

  private _configurarFooterBar() {
    this.configuracaoFooterBar.setModoAtualizar(false);
    this.configuracaoFooterBar.setSaving(false);
    this.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.MEDICAMENTO);
  }

  private _configurarGrid() {
    this._questaoGrid.setNavCtrlReference(this._navCtrl);
    this._questaoGrid.setEventsReference(this._events);
    this._questaoGrid.setSyncDataServiceReference(this._syncDataService);
    this._questaoGrid.setConfiguracaoFooterBarReference(this.configuracaoFooterBar);

    this.configuracaoGrid.setModalEventoClique('AdicionarQuantidadeMedicamentoPage');
    this.configuracaoGrid.setPossuiEventoClique(true);
    this.configuracaoGrid.setPossuiModal(true);
    this.configuracaoGrid.setIdentificadorGrid(TipoQuestao.MEDICAMENTO);
    // this.configuracaoGrid.setQuestao(this);
    this.configuracaoGrid.setModalCtrl(this._modalCtrl);
    this.configuracaoGrid.setModalAdicionarOpcaoReferencia('AdicionarMedicamentoPage');
    this.configuracaoGrid.setProximaPagina('LocalizacaoDorPage');
    this.configuracaoGrid.setDocumentoOptionId(SyncDataProvider.DOCUMENT_OPCOES_MEDICAMENTO_ID);
    this.configuracaoGrid.setNomeDoObjetoOpcao('medicamentos');

    if (this._navParams.get(AppParam.ROUTE) === 'sumario') {
      this.configuracaoGrid.setRota('sumario');
    }
  }

  private _listenEvents() {
    this._events.subscribe('footer:onClickDissmissQuestions' + TipoQuestao.MEDICAMENTO,
      () => {
        this.onClickDissmissQuestions();
      },
    );

    this._events.subscribe('footer:onClickNext' + TipoQuestao.MEDICAMENTO,
      () => {
        this.onClickNext();
      },
    );

    this._events.subscribe('footer:onClickConfirm' + TipoQuestao.MEDICAMENTO,
      () => {
        this.onClickConfirm();
      },
    );

    this._events.subscribe('medicamento:onPrimeiroCadastroMedicamento', (estado: boolean) => {
      this.onPrimeiroCadastroMedicamento(estado);
    });
  }

  private _destroyEvents() {
    this._questaoGrid.destroyListenGridEvents();
    this._events.unsubscribe('footer:onClickDissmissQuestions' + TipoQuestao.MEDICAMENTO);
    this._events.unsubscribe('footer:onClickNext' + TipoQuestao.MEDICAMENTO);
    this._events.unsubscribe('footer:onClickConfirm' + TipoQuestao.MEDICAMENTO);
    this._events.unsubscribe('medicamento:onPrimeiroCadastroMedicamento');
  }

}
