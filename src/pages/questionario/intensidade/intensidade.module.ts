import { ComponentsModule } from './../../../components/components.module';
import { PipesModule } from './../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { IntensidadePage } from './intensidade';

@NgModule({
  declarations: [
    IntensidadePage,
  ],
  imports: [
    IonicPageModule.forChild(IntensidadePage),
    PipesModule,
    ComponentsModule,
  ],
})
export class IntensidadePageModule {}
