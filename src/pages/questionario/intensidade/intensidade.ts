import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { AppParam } from './../../../core/models/pojo/app-param.pojo';
import { Cor } from './../../../core/models/pojo/cor.pojo';
import { Intensidade } from './../../../core/models/intensidade.model';
import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { QuestaoPersonalizadaInterface } from './../../../core/models/interface/questao-personalizada.interface';
import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { ConfiguracaoFooterBar } from './../../../core/models/pojo/configuracao-footer-bar.pojo';
import { ConfiguracaoNavBar } from './../../../core/models/pojo/configuracao-nav-bar.pojo';
import { Crise } from './../../../core/models/crise.model';
import { AppHelper } from './../../../core/helpers/app.helper';
import { Component, ChangeDetectionStrategy, ChangeDetectorRef, trigger, state, style, transition, animate, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'ib-page-intensidade',
  templateUrl: 'intensidade.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('scale', [
      state('inactive', style({
        transform: 'scale(1)',
      })),
      state('active', style({
        transform: 'scale(1.1)',
      })),
      transition('inactive => active', animate('100ms ease-in')),
      transition('active => inactive', animate('100ms ease-out')),
    ]),
  ],
})
export class IntensidadePage implements OnDestroy, QuestaoPersonalizadaInterface {

  public dadosIntensidade: Array<any> = [];
  public items: Array<any> = [];
  public itemAtual: any = {};
  public animationsScaleState: string = 'inactive';
  public questaoPersonalizada: QuestaoPersonalizadaService = new QuestaoPersonalizadaService();
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public configuracaoFooterBar: ConfiguracaoFooterBar;
  public isSumario: boolean = false;

  private _indiceAtual: number = 0;
  private _appHelper: AppHelper;
  private _docCrise: Crise;

  constructor(private _navCtrl: NavController, private _navParams: NavParams, private _syncData: SyncDataProvider,
    private _changeDetectorRef: ChangeDetectorRef, private _events: Events) {

    this._iniciar();
  }

  ngOnDestroy() {
    this._destroyEvents();
  }

  ionViewWillEnter() {
    this._configurarQuestao();
    this._prepararQuestao();
  }

  onClickHelp() {
    console.log('click help');
  }

  public onClickChangeIntensidade(itemIntensidade: any) {
    this.itemAtual = itemIntensidade;
    this.dadosIntensidade[this._indiceAtual] = {
      valor: itemIntensidade.intensidade,
      selecionado: true,
    };
    this.animationsScaleState = 'active';
    this.questaoPersonalizada.setHouveInteracao(true);
  }

  public callbackAnimationScaleDone() {
    this.animationsScaleState = 'inactive';
  }

  onClickNext() {
    this.questaoPersonalizada.registrarQuestao();
  }

  onClickDissmissQuestions() {
    this._navCtrl.setRoot('DiarioPage');
  }

  onClickConfirm() {
    this.questaoPersonalizada.registrarQuestao();
  }

  public onClickSelecionarIntensidade(i: number) {
    this._indiceAtual = i;
    this._resetarEstadoSelecionado();

    this.dadosIntensidade[i].selecionado = true;

    this.itemAtual = this.items[this.dadosIntensidade[i].valor - 1];
    this.animationsScaleState = 'active';
  }

  public onClickAddNewIntensidade() {
    this.questaoPersonalizada.setHouveInteracao(true);
    let newIntensidade: any = {
      valor: 1,
      selecionado: true,
    };

    this._resetarEstadoSelecionado();

    this.dadosIntensidade.push(newIntensidade);
    this.itemAtual = this.items[0];
    this.animationsScaleState = 'active';
    this._indiceAtual = this.dadosIntensidade.length - 1;
  }

  public onClickExcluirIntensidade() {
    this._appHelper.sweetAlert({
      title: 'Tem certeza que deseja excluir?',
      text: 'Após esta operação não será possível recuperar o registro',
      type: null,
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Cancelar',
    }).then(
      () => {
        this.questaoPersonalizada.setHouveInteracao(true);
        this.dadosIntensidade.splice(this._indiceAtual, 1);
        this.itemAtual = this.items[this.dadosIntensidade[0].valor - 1];
        this.dadosIntensidade[0].selecionado = true;
        this.animationsScaleState = 'active';
        this._indiceAtual = 0;
        this._changeDetectorRef.markForCheck();
      },
    );
  }

  onErrorSaveQuestion(err: any) { console.log(err); }
  onSavingQuestion(estadoQuestao: boolean) {
    this.configuracaoFooterBar.setSaving(estadoQuestao);
  }

  onCallbackPreRegistrarQuestao(): Promise<any> {
    let scope: IntensidadePage = this;

    return new Promise(
      function (resolve) {
        let dadosCriseIntensidade: Array<Intensidade> = [];
        scope.dadosIntensidade.forEach(
          (dadoIntensidade: any, i: number) => {

            let intensidade: Intensidade = {
              valor: 0,
              dataCriacao: null,
            };

            let dataCriacao: string = null;
            if (scope.questaoPersonalizada.getCrise().intensidade.length > 0) {
              dataCriacao = scope.questaoPersonalizada.getCrise().intensidade[i].dataCriacao;
            }

            if (!dataCriacao) {
              dataCriacao = moment().local().format(AppHelper.FORMATO_DATA_ISO_8601);
            }

            intensidade.dataCriacao = dataCriacao;
            intensidade.valor = dadoIntensidade.valor;

            dadosCriseIntensidade.push(intensidade);
          },
        );

        scope.questaoPersonalizada.getCrise().intensidade = dadosCriseIntensidade;
        /*
                if (scope.questaoPersonalizada.isSumario()) {
                  let indice: number = scope.questaoPersonalizada.getCrise().intensidade.length;
                  scope.questaoPersonalizada.getCrise().intensidade[indice] = scope._intensidade;
                } else {
                  scope.questaoPersonalizada.getCrise().intensidade[0] = scope._intensidade;
                }*/

        // scope.questaoPersonalizada.setCrise(scope.questaoPersonalizada.getCrise()); // Verificar necessidade
        resolve();
      },
    );
  }

  onCallbackPreAtualizarQuestao(): Promise<any> {
    let scope: IntensidadePage = this;

    return new Promise(
      function (resolve) {
        if (scope.questaoPersonalizada.getCrise().intensidade.length > 0) {
          scope.dadosIntensidade = scope.questaoPersonalizada.getCrise().intensidade;
          scope.dadosIntensidade[0].selecionado = true;

          scope.itemAtual = scope.items[scope.questaoPersonalizada.getCrise().intensidade[0].valor - 1];
          scope._changeDetectorRef.markForCheck();
        }

        resolve();
      },
    );
  }

  onAtualizarQuestao() { }

  public getCorIcone(valorIntensidade: number) {
    let corInicial: string;
    let corFinal: string;
    let proporcao: number;

    if (valorIntensidade <= 5) {
      corInicial = Cor.CRISE_FRACA;
      corFinal = Cor.CRISE_MEDIA;
      proporcao = (valorIntensidade * 20) / 100;
    } else {
      corInicial = Cor.CRISE_MEDIA;
      corFinal = Cor.CRISE_FORTE;
      proporcao = (((valorIntensidade + 1) - 5) * 20) / 100;
    }

    return this._appHelper.linearInterpolationColor(corInicial, corFinal, proporcao);
  }

  private _iniciar() {
    this._appHelper = new AppHelper();
    this._docCrise = (this._navParams.get(AppParam.CRISE_ID) ? this._navParams.get(AppParam.CRISE_ID) : null);

    this._iniciarConteudo();
    // this.onAtualizarQuestao();
    this._configurarNavBar();
    this._configurarFooterBar();

    this._listenEvents();
  }

  private _iniciarConteudo() {
    this.items = [
      {
        intensidade: 1,
        titulo: 'dói pouco',
        descricao: 'a dor não limita nenhuma de minhas atividades',
        dica: 'esta é mais uma questão de um leve desconforto que dor; é semelhante à mordida de um mosquito (mutuca) na praia.',
        icone: 'mm-intensidade-fraca',

      },
      {
        intensidade: 2,
        titulo: 'dói pouco',
        descricao: 'a dor não limita nenhuma de minhas atividades',
        dica: 'neste nível a dor é perceptível, mas muito pouco e pode ser facilmente esquecido.',
        icone: 'mm-intensidade-fraca',
      },
      {
        intensidade: 3,
        titulo: 'dói um pouco mais',
        descricao: 'posso fazer minhas atividades, mas com dificuldades',
        dica: 'esta é a dor aguda, mas controlável que não dura por muito tempo.',
        icone: 'mm-intensidade-fraca-media',
      },
      {
        intensidade: 4,
        titulo: 'dói um pouco mais',
        descricao: 'posso fazer minhas atividades, mas com dificuldades',
        dica: 'a diferença entre os níveis três e quatro é o nível de angústia. Dores de dentes são frequentemente avaliado a este nível.',
        icone: 'mm-intensidade-fraca-media',
      },
      {
        intensidade: 5,
        titulo: 'dói bastante',
        descricao: 'a dor está me limitando em algumas atividades',
        dica: 'um exemplo de dor a este nível seria uma entorse de tornozelo que dói cada vez que um passo é dado.',
        icone: 'mm-intensidade-media',
      },
      {
        intensidade: 6,
        titulo: 'dói bastante',
        descricao: 'a dor está me limitando em algumas atividades',
        dica: 'algumas das piores dores de cabeça tensionais são classificadas neste nível, que envolve o sentimento de uma força dolorosa ' +
        'perfurando o corpo.',
        icone: 'mm-intensidade-media',
      },
      {
        intensidade: 7,
        titulo: 'dor severa',
        descricao: 'não consigo fazer praticamente nada',
        dica: 'este é o nível onde enxaquecas e cefaleias em salvas normalmente começam. Dor muito intensa, é frequentemente debilitante ao ' +
        'ponto de não permitir pensar claramente.',
        icone: 'mm-intensidade-media-forte',
      },
      {
        intensidade: 8,
        titulo: 'dor severa',
        descricao: 'não consigo fazer praticamente nada',
        dica: 'as mulheres que sofreram parto sabem tudo sobre essa dor muito intensa. O que pode levar ao transtorno de personalidade, se ' +
        'continuar sem tratamento.',
        icone: 'mm-intensidade-media-forte',
      },
      {
        intensidade: 9,
        titulo: 'dor insuportável',
        descricao: 'possível estado de inconsciência. Não consigo fazer qualquer atividade por causa da dor',
        dica: 'câncer de garganta é uma das condições de doença mais insuportáveis que podem acontecer a uma pessoa, e muitas vezes é ' +
        'classificado neste nível de dor.',
        icone: 'mm-intensidade-forte',
      },
      {
        intensidade: 10,
        titulo: 'dor insuportável',
        descricao: 'possível estado de inconsciência. Não consigo fazer qualquer atividade por causa da dor',
        dica: 'a este nível, a dor é tão grande que as pessoas perdem a consciência ou chegam a entrar e sair do estado de choque.',
        icone: 'mm-intensidade-forte',
      },
    ];

    this.itemAtual = this.items[0];
  }

  private _resetarEstadoSelecionado() {
    this.dadosIntensidade.forEach(
      (menu: any) => {
        if (menu.selecionado) {
          delete menu.selecionado;
        }
      },
    );
  }

  private _prepararQuestao() {
    if (this._docCrise) {
      this.questaoPersonalizada.setCrise(this._docCrise);
      this.questaoPersonalizada.triggerAtualizarQuestao();
      this.configuracaoFooterBar.setModoAtualizar(true);
      this.isSumario = true;
    } else {
      this.isSumario = false;
      this.questaoPersonalizada.getCriseId()
        .then(
        (criseId: string) => {
          this.questaoPersonalizada.getCrise()._id = criseId;
        },
        (err) => {
          console.log('TRACK ERROR', err);
          // TRACK
        },
      );
    }
  }

  private _configurarQuestao() {
    this.questaoPersonalizada.setPage(this);
    this.questaoPersonalizada.setSyncDataServiceReference(this._syncData);
    this.questaoPersonalizada.setProximaPagina('MedicamentoPage');
    this.questaoPersonalizada.setHouveInteracao(false);

    if (this._navParams.get(AppParam.ROUTE) === 'sumario') {
      this.questaoPersonalizada.setSumario(true);
    }
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('Qual seu nível de dor?');
  }

  private _configurarFooterBar() {
    this.configuracaoFooterBar = new ConfiguracaoFooterBar();
    this.configuracaoFooterBar.setModoAtualizar(false);
    this.configuracaoFooterBar.setSaving(false);
    this.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.INTENSIDADE);
  }

  private _listenEvents() {
    this._events.subscribe('footer:onClickDissmissQuestions' + TipoQuestao.INTENSIDADE,
      () => {
        this.onClickDissmissQuestions();
      },
    );

    this._events.subscribe('footer:onClickNext' + TipoQuestao.INTENSIDADE,
      () => {
        this.onClickNext();
      },
    );

    this._events.subscribe('footer:onClickConfirm' + TipoQuestao.INTENSIDADE,
      () => {
        this.onClickConfirm();
      },
    );
  }

  private _destroyEvents() {
    this._events.unsubscribe('footer:onClickDissmissQuestions' + TipoQuestao.INTENSIDADE);
    this._events.unsubscribe('footer:onClickNext' + TipoQuestao.INTENSIDADE);
    this._events.unsubscribe('footer:onClickConfirm' + TipoQuestao.INTENSIDADE);
  }

}
