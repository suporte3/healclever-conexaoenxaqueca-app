import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { Intensidade } from './../../../core/models/intensidade.model';
import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { CapitalizePipe } from './../../../pipes/capitalize/capitalize';
import { NavParamsMock } from './../../../core/mocks/nav-params.mock';
import { NavParams, ModalController } from 'ionic-angular';
import { TestUtils } from './../../../app/app.test';
import { Crise } from './../../../core/models/crise.model';
import { IntensidadePage } from './intensidade';
import { ComponentFixture, async, fakeAsync, tick } from '@angular/core/testing';

describe('Intensidade Page:', () => {

    let fixture: ComponentFixture<IntensidadePage> = null;
    let instance: any = null;
    let _instance: any = IntensidadePage.prototype;
    let criseObjetoTest: Crise = {
        _id: 'idCrise',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    let items: Array<any> = [
        {
            intensidade: 1,
            titulo: 'dói pouco',
            descricao: 'a dor não limita nenhuma de minhas atividades',
            dica: 'esta é mais uma questão de um leve desconforto que dor; é semelhante à mordida de um mosquito (mutuca) na praia.',
            icone: 'mm-intensidade-fraca',

        },
        {
            intensidade: 2,
            titulo: 'dói pouco',
            descricao: 'a dor não limita nenhuma de minhas atividades',
            dica: 'neste nível a dor é perceptível, mas muito pouco e pode ser facilmente esquecido.',
            icone: 'mm-intensidade-fraca',
        },
        {
            intensidade: 3,
            titulo: 'dói um pouco mais',
            descricao: 'posso fazer minhas atividades, mas com dificuldades',
            dica: 'esta é a dor aguda, mas controlável que não dura por muito tempo.',
            icone: 'mm-intensidade-fraca-media',
        },
        {
            intensidade: 4,
            titulo: 'dói um pouco mais',
            descricao: 'posso fazer minhas atividades, mas com dificuldades',
            dica: 'a diferença entre os níveis três e quatro é o nível de angústia. Dores de dentes são frequentemente avaliado a este nível.',
            icone: 'mm-intensidade-fraca-media',
        },
        {
            intensidade: 5,
            titulo: 'dói bastante',
            descricao: 'a dor está me limitando em algumas atividades',
            dica: 'um exemplo de dor a este nível seria uma entorse de tornozelo que dói cada vez que um passo é dado.',
            icone: 'mm-intensidade-media',
        },
        {
            intensidade: 6,
            titulo: 'dói bastante',
            descricao: 'a dor está me limitando em algumas atividades',
            dica: 'algumas das piores dores de cabeça tensionais são classificadas neste nível, que envolve o sentimento de uma força dolorosa ' +
            'perfurando o corpo.',
            icone: 'mm-intensidade-media',
        },
        {
            intensidade: 7,
            titulo: 'dor severa',
            descricao: 'não consigo fazer praticamente nada',
            dica: 'este é o nível onde enxaquecas e cefaleias em salvas normalmente começam. Dor muito intensa, é frequentemente debilitante ao ' +
            'ponto de não permitir pensar claramente.',
            icone: 'mm-intensidade-media-forte',
        },
        {
            intensidade: 8,
            titulo: 'dor severa',
            descricao: 'não consigo fazer praticamente nada',
            dica: 'as mulheres que sofreram parto sabem tudo sobre essa dor muito intensa. O que pode levar ao transtorno de personalidade, se ' +
            'continuar sem tratamento.',
            icone: 'mm-intensidade-media-forte',
        },
        {
            intensidade: 9,
            titulo: 'dor insuportável',
            descricao: 'possível estado de inconsciência. Não consigo fazer qualquer atividade por causa da dor',
            dica: 'câncer de garganta é uma das condições de doença mais insuportáveis que podem acontecer a uma pessoa, e muitas vezes é ' +
            'classificado neste nível de dor.',
            icone: 'mm-intensidade-forte',
        },
        {
            intensidade: 10,
            titulo: 'dor insuportável',
            descricao: 'possível estado de inconsciência. Não consigo fazer qualquer atividade por causa da dor',
            dica: 'a este nível, a dor é tão grande que as pessoas perdem a consciência ou chegam a entrar e sair do estado de choque.',
            icone: 'mm-intensidade-forte',
        },
    ];

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.addProvider(ModalController);

        TestUtils.beforeEachCompiler([IntensidadePage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o componente ao carregar', (done) => {
        expect(instance._iniciar).toHaveBeenCalled();
        done();
    });

    it('#ngOnDestroy - Deve destruir o componente', () => {
        spyOn(instance, '_destroyEvents').and.stub();

        instance.ngOnDestroy();

        expect(instance._destroyEvents).toHaveBeenCalled();
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        spyOn(instance, '_iniciarConteudo').and.stub();
        // spyOn(instance, 'onAtualizarQuestao').and.stub();
        spyOn(instance, '_configurarNavBar').and.stub();
        spyOn(instance, '_configurarFooterBar').and.stub();
        spyOn(instance, '_listenEvents').and.stub();

        instance._iniciar();

        expect(instance._iniciarConteudo).toHaveBeenCalled();
        // expect(instance.onAtualizarQuestao).toHaveBeenCalled();
        expect(instance._configurarNavBar).toHaveBeenCalled();
        expect(instance._configurarFooterBar).toHaveBeenCalled();
        expect(instance._listenEvents).toHaveBeenCalled();
    });

    it('Deve configurar a questão sempre ao entrar na questão', () => {
        spyOn(instance, '_configurarQuestao').and.stub();
        spyOn(instance, '_prepararQuestao').and.stub();
        instance.ionViewWillEnter();

        expect(instance._configurarQuestao).toHaveBeenCalled();
        expect(instance._prepararQuestao).toHaveBeenCalled();
    });

    it('#onClickChangeIntensidade - Deve alterar o valor da intensidade atual', () => {
        instance.itemAtual = {
            intensidade: 1,
            titulo: 'dói pouco',
            descricao: 'a dor não limita nenhuma de minhas atividades',
            dica: 'esta é mais uma questão de um leve desconforto que dor; é semelhante à mordida de um mosquito (mutuca) na praia.',
            icone: 'mm-intensidade-fraca',

        };
        let intensidadeAlterada: any = {
            intensidade: 9,
            titulo: 'dor insuportável',
            descricao: 'possível estado de inconsciência. Não consigo fazer qualquer atividade por causa da dor',
            dica: 'câncer de garganta é uma das condições de doença mais insuportáveis que podem acontecer a uma pessoa, e muitas vezes é ' +
            'classificado neste nível de dor.',
            icone: 'mm-intensidade-forte',
        };

        spyOn(instance.questaoPersonalizada, 'setHouveInteracao').and.stub();

        instance.onClickChangeIntensidade(intensidadeAlterada);

        expect(instance.itemAtual).toEqual(intensidadeAlterada);
        expect(instance.animationsScaleState).toBe('active');
        expect(instance.questaoPersonalizada.setHouveInteracao).toHaveBeenCalled();
    });

    it('#_prepararQuestao - Deve preparar questão', (done) => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'getCriseId').and.returnValue(Promise.resolve('criseId'));
        instance._docCrise = null;
        instance._prepararQuestao();

        expect(instance.questaoPersonalizada.getCriseId).toHaveBeenCalled();
        fixture.whenStable().then(
            () => {
                expect(instance.questaoPersonalizada.getCrise()._id).toEqual('criseId');
                done();
            },
        );
    });

    it('#_prepararQuestao - Deve preparar questão - sumário', () => {
        instance._configurarQuestao();
        instance._configurarFooterBar();

        spyOn(instance.configuracaoFooterBar, 'setModoAtualizar').and.stub();
        spyOn(instance.questaoPersonalizada, 'setCrise').and.stub();
        spyOn(instance.questaoPersonalizada, 'triggerAtualizarQuestao').and.stub();
        instance._docCrise = criseObjetoTest;
        instance._prepararQuestao();

        expect(instance.questaoPersonalizada.triggerAtualizarQuestao).toHaveBeenCalled();
        expect(instance.questaoPersonalizada.setCrise).toHaveBeenCalledWith(instance._docCrise);
        expect(instance.configuracaoFooterBar.setModoAtualizar).toHaveBeenCalledWith(true);
    });

    it('#_configurarQuestao - Deve configurar a questão corretamente', () => {
        // spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();
        spyOn(QuestaoPersonalizadaService.prototype, 'setSumario').and.stub();

        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncData);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('MedicamentoPage');
        expect(instance.questaoPersonalizada.getHouveInteracao()).toBe(false);
        // expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
        expect(QuestaoPersonalizadaService.prototype.setSumario).not.toHaveBeenCalled();
    });

    it('#_configurarQuestao - Deve configurar a questão corretamente - sumário', () => {
        // spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();

        instance._navParams = new NavParams({ 'com.healclever.myMigraine.route': 'sumario' });
        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncData);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('MedicamentoPage');
        expect(instance.questaoPersonalizada.getHouveInteracao()).toBe(false);
        // expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
        expect(instance.questaoPersonalizada.isSumario()).toBe(true);
    });

    it('#onClickDissmissQuestions - Deve ir para o Painel', () => {
        spyOn(instance._navCtrl, 'setRoot').and.stub();
        instance.onClickDissmissQuestions();
        expect(instance._navCtrl.setRoot).toHaveBeenCalledWith('DiarioPage');
    });

    it('#onClickNext - Deve registrar a questão', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'registrarQuestao').and.stub();

        instance.onClickNext();
        expect(instance.questaoPersonalizada.registrarQuestao).toHaveBeenCalled();
    });

    it('#onClickConfirm - Deve registrar a questão', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'registrarQuestao').and.stub();

        instance.onClickConfirm();
        expect(instance.questaoPersonalizada.registrarQuestao).toHaveBeenCalled();
    });

    it('#onCallbackPreRegistrarQuestao - Deve atualizar valores antes de registrar', fakeAsync(() => {
        instance.dadosIntensidade = [
            {
                valor: 4,
                selecionado: true,
            },
            {
                valor: 5,
            },
            {
                valor: 6,
            },
        ];
        instance.onCallbackPreRegistrarQuestao();
        tick();
        expect(instance.questaoPersonalizada.getCrise().intensidade[0].valor).toBe(instance.dadosIntensidade[0].valor);
        expect(instance.questaoPersonalizada.getCrise().intensidade[1].valor).toBe(instance.dadosIntensidade[1].valor);
        expect(instance.questaoPersonalizada.getCrise().intensidade[2].valor).toBe(instance.dadosIntensidade[2].valor);
    }));

    it('#onCallbackPreAtualizarQuestao - Deve atualizar valores antes de apresentar questão', () => {
        let intensidade: Intensidade = {
            valor: 4,
            dataCriacao: null,
        };
        let intensidade1: Intensidade = {
            valor: 2,
            dataCriacao: null,
        };
        instance.questaoPersonalizada.getCrise().intensidade = [intensidade, intensidade1];
        spyOn(instance._changeDetectorRef, 'markForCheck').and.stub();

        instance.onCallbackPreAtualizarQuestao();
        expect(instance.itemAtual).toEqual(items[intensidade.valor - 1]);
        expect(instance._changeDetectorRef.markForCheck).toHaveBeenCalled();
    });

    it('#onSavingQuestion - Deve alterar o estado da questão', () => {
        let estadoQuestao: boolean = true;
        instance.configuracaoFooterBar.setSaving(false);
        instance.onSavingQuestion(estadoQuestao);

        expect(instance.configuracaoFooterBar.isSaving()).toBe(estadoQuestao);
    });

    it('Deve iniciar conteúdo', () => {
        instance._iniciarConteudo();
        expect(instance.items).toEqual(items);
        expect(instance.itemAtual).toEqual(instance.items[0]);
    });

    it('#getCorIcone - Deve retornar uma cor em hexadecimal', () => {
        expect(instance.getCorIcone(1)).toBeDefined();
    });

    it('Deve configurar o <nav-bar /> corretamente', () => {
        instance._configurarNavBar();
        expect(instance.configuracaoNavBar.getTextoInicial()).toBe('Qual seu nível de dor?');
    });

    it('Deve configurar o <footer-bar /> corretamente', () => {
        instance._configurarFooterBar();
        expect(instance.configuracaoFooterBar.isModoAtualizar()).toBe(false);
        expect(instance.configuracaoFooterBar.isSaving()).toBe(false);
        expect(instance.configuracaoFooterBar.getIdentificadorGrid()).toBe(TipoQuestao.INTENSIDADE);
    });

    it('#_listenEvents - Deve escutar eventos', () => {
        let spySubscribe: jasmine.Spy = spyOn(instance._events, 'subscribe').and.stub();

        instance._listenEvents();

        expect(spySubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.INTENSIDADE);
        expect(spySubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.INTENSIDADE);
        expect(spySubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.INTENSIDADE);
    });

    it('#_destroyEvents - Deve destruir eventos que estão sendo escutados', () => {
        let spyUnsubscribe: jasmine.Spy = spyOn(instance._events, 'unsubscribe').and.stub();

        instance._destroyEvents();

        expect(spyUnsubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.INTENSIDADE);
        expect(spyUnsubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.INTENSIDADE);
        expect(spyUnsubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.INTENSIDADE);
    });

});
