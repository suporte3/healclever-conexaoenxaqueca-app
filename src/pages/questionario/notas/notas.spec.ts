import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { AppHelper } from './../../../core/helpers/app.helper';
import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { NavParamsMock } from './../../../core/mocks/nav-params.mock';
import { NavParams } from 'ionic-angular';
import { TestUtils } from './../../../app/app.test';
import { Crise } from './../../../core/models/crise.model';
import { NotasPage } from './notas';
import { ComponentFixture, async, fakeAsync, tick } from '@angular/core/testing';

describe('Notas Page:', () => {

    let fixture: ComponentFixture<NotasPage> = null;
    let instance: any = null;
    let _instance: any = NotasPage.prototype;
    let criseObjetoTest: Crise = {
        _id: 'idCrise',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.beforeEachCompiler([NotasPage]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeDefined();
        expect(instance).toBeDefined();
        done();
    });

    it('Deve iniciar o componente ao carregar', (done) => {
        expect(instance._iniciar).toHaveBeenCalled();
        done();
    });

    it('#ngOnDestroy - Deve destruir o componente', () => {
        spyOn(instance, '_destroyEvents').and.stub();

        instance.ngOnDestroy();

        expect(instance._destroyEvents).toHaveBeenCalled();
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        spyOn(instance, '_configurarNavBar').and.stub();
        spyOn(instance, '_configurarFooterBar').and.stub();
        spyOn(instance, '_listenEvents').and.stub();

        instance._iniciar();

        expect(instance._configurarNavBar).toHaveBeenCalled();
        expect(instance._configurarFooterBar).toHaveBeenCalled();
        expect(instance._listenEvents).toHaveBeenCalled();
    });

    it('#onClickDissmissQuestions - Deve ir para o Painel', () => {
        spyOn(instance._navCtrl, 'setRoot').and.stub();
        instance.onClickDissmissQuestions();
        expect(instance._navCtrl.setRoot).toHaveBeenCalledWith('DiarioPage');
    });

    it('#onClickConfirm - Deve registrar e ir para Sumario', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'registrarQuestao').and.stub();
        spyOn(instance.questaoPersonalizada, 'setHouveInteracao').and.stub();
        instance.anotacoes = 'anotação teste';
        instance._anotacoes = 'anotação antiga teste';

        instance.onClickConfirm();
        expect(instance.questaoPersonalizada.registrarQuestao).toHaveBeenCalled();
        expect(instance.questaoPersonalizada.setHouveInteracao).toHaveBeenCalledWith(true);
    });

    it('#onSavingQuestion - Deve alterar o estado da questão', () => {
        let estadoQuestao: boolean = true;
        instance.configuracaoFooterBar.setSaving(false);
        instance.onSavingQuestion(estadoQuestao);

        expect(instance.configuracaoFooterBar.isSaving()).toBe(estadoQuestao);
    });

    it('Deve atualizar opções da questão', () => {
        instance._configurarQuestao();
        instance.onAtualizarQuestao();
        expect(instance.anotacoes).toBeDefined();
    });

    it('Deve configurar o componente com false ao sair na questão', () => {
        instance.ionViewDidLeave();
        expect(instance.componentesConfigurado).toBe(false);
    });

    it('Deve configurar a questão ao entrar na tela', () => {
        spyOn(instance, '_configurarQuestao').and.stub();
        spyOn(instance, '_prepararQuestao').and.stub();
        instance.ionViewWillEnter();
        expect(instance._configurarQuestao).toHaveBeenCalled();
        expect(instance._prepararQuestao).toHaveBeenCalled();
    });

    it('#_configurarQuestao - Deve configurar a questão corretamente', () => {
        spyOn(QuestaoPersonalizadaService.prototype, 'setSumario').and.stub();

        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncDataProvider);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('DiarioPage');
        expect(instance.questaoPersonalizada.getHouveInteracao()).toBe(false);
        expect(QuestaoPersonalizadaService.prototype.setSumario).not.toHaveBeenCalled();
    });

    it('#_configurarQuestao - Deve configurar a questão corretamente - sumário', () => {

        instance._navParams = new NavParams({ 'com.healclever.myMigraine.route': 'sumario' });
        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncDataProvider);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('DiarioPage');
        expect(instance.questaoPersonalizada.getHouveInteracao()).toBe(false);
        expect(instance.questaoPersonalizada.isSumario()).toBe(true);
    });

    it('Deve preparar questão', (done) => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'getCriseId').and.returnValue(Promise.resolve('criseId'));
        instance._docCrise = null;
        instance._prepararQuestao();

        expect(instance.questaoPersonalizada.getCriseId).toHaveBeenCalled();
        fixture.whenStable().then(
            () => {
                expect(instance.questaoPersonalizada.getCrise()._id).toEqual('criseId');
                done();
            },
        );
    });

    it('Deve preparar questão - sumário', () => {
        instance._configurarQuestao();
        instance._configurarFooterBar();

        spyOn(instance.questaoPersonalizada, 'setCrise').and.stub();
        spyOn(instance.questaoPersonalizada, 'triggerAtualizarQuestao').and.stub();
        instance._docCrise = criseObjetoTest;
        instance._prepararQuestao();

        expect(instance.questaoPersonalizada.triggerAtualizarQuestao).toHaveBeenCalled();
        expect(instance.questaoPersonalizada.setCrise).toHaveBeenCalledWith(instance._docCrise);
    });

    it('#onClickLimparNotas - Deve limpar notas', fakeAsync(() => {
        let sweetAlertParams: any = {
            title: 'Tem certeza que deseja resetar suas anotações?',
            text: '',
            type: null,
            showCancelButton: true,
            confirmButtonText: 'Sim',
            cancelButtonText: 'Cancelar',
        };
        spyOn(AppHelper.prototype, 'sweetAlert').and.returnValue(Promise.resolve());
        instance.anotacoes = 'anotações teste';
        instance.onClickLimparNotas();
        expect(AppHelper.prototype.sweetAlert).toHaveBeenCalledWith(sweetAlertParams);
        tick();
        expect(instance.anotacoes).toBeNull();
    }));

    it('#onClickAddNota - Deve adicionar nota técnica', () => {
        instance.anotacoes = null;
        instance.onClickAddNota();
        expect(instance.anotacoes).not.toBeNull();
    });

    it('Deve configurar o <nav-bar /> corretamente', () => {
        instance._configurarNavBar();
        expect(instance.configuracaoNavBar.getTextoInicial()).toBe('Gostaria de adicionar anotações?');
    });

    it('Deve configurar o <footer-bar /> corretamente', () => {
        instance._configurarFooterBar();
        expect(instance.configuracaoFooterBar.isModoAtualizar()).toBe(true);
        expect(instance.configuracaoFooterBar.isSaving()).toBe(false);
        expect(instance.configuracaoFooterBar.getIdentificadorGrid()).toBe(TipoQuestao.NOTAS);
    });

    it('#_listenEvents - Deve escutar eventos', () => {
        let spySubscribe: jasmine.Spy = spyOn(instance._events, 'subscribe').and.stub();

        instance._listenEvents();

        expect(spySubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.NOTAS);
        expect(spySubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.NOTAS);
    });

    it('#_destroyEvents - Deve destruir eventos que estão sendo escutados', () => {
        let spyUnsubscribe: jasmine.Spy = spyOn(instance._events, 'unsubscribe').and.stub();

        instance._destroyEvents();

        expect(spyUnsubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.NOTAS);
        expect(spyUnsubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.NOTAS);
    });

});
