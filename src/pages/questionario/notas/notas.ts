import { AppHelper } from './../../../core/helpers/app.helper';
import { AppParam } from './../../../core/models/pojo/app-param.pojo';
import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { Crise } from './../../../core/models/crise.model';
import { ConfiguracaoFooterBar } from './../../../core/models/pojo/configuracao-footer-bar.pojo';
import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { ConfiguracaoNavBar } from './../../../core/models/pojo/configuracao-nav-bar.pojo';
import { Component, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'ib-page-notas',
  templateUrl: 'notas.html',
})
export class NotasPage implements OnDestroy {

  public anotacoes: string = null;
  public componentesConfigurado: boolean = false;
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public questaoPersonalizada: QuestaoPersonalizadaService;
  public configuracaoFooterBar: ConfiguracaoFooterBar;

  private _anotacoes: string;
  private _docCrise: Crise;

  constructor(private _navCtrl: NavController, private _navParams: NavParams, private _syncDataProvider: SyncDataProvider, private _events: Events) {

    this._iniciar();
  }

  ngOnDestroy() {
    this._destroyEvents();
  }

  ionViewWillEnter() {
    this._configurarQuestao();
    this._prepararQuestao();
  }

  ionViewDidLeave() {
    this.componentesConfigurado = false;
  }

  onClickConfirm() {
    if (this.anotacoes && this.anotacoes !== this._anotacoes) {
      this.questaoPersonalizada.setHouveInteracao(true);
    }
    this.questaoPersonalizada.registrarQuestao();
  }

  onClickNext() { }

  onClickDissmissQuestions() {
    this._navCtrl.setRoot('DiarioPage');
  }

  onErrorSaveQuestion(err: any) {
    console.log('DEU ERRO....', err);
  }

  onSavingQuestion(estadoQuestao: boolean) {
    this.configuracaoFooterBar.setSaving(estadoQuestao);
  }

  onClickHelp() {
    console.log('click help');
  }

  onCallbackPreRegistrarQuestao(): Promise<any> {
    let scope: NotasPage = this;

    return new Promise(function (resolve) {
      scope.questaoPersonalizada.getCrise().anotacoes = scope.anotacoes;
      resolve();
    });
  }

  onCallbackPreAtualizarQuestao(): Promise<any> {
    return Promise.resolve();
  }

  onAtualizarQuestao() {
    this.anotacoes = this.questaoPersonalizada.getCrise().anotacoes;
    this._anotacoes = this.anotacoes;
  }

  onClickAddNota() {
    let tempoAtual = moment().local().format('HH:mm');
    this.anotacoes = (this.anotacoes ? this.anotacoes.trim() + '\n' : '');
    this.anotacoes += tempoAtual + ': ';
  }

  onClickLimparNotas() {
    let appHelper: AppHelper = new AppHelper();
    appHelper.sweetAlert({
      title: 'Tem certeza que deseja resetar suas anotações?',
      text: '',
      type: null,
      showCancelButton: true,
      confirmButtonText: 'Sim',
      cancelButtonText: 'Cancelar',
    }).then(
      () => {
        this.anotacoes = null;
      },
    ).catch(
      (e) => {
        console.log(e);
        // TRACK ERROR
      },
    );
  }

  private _iniciar() {
    this._docCrise = (this._navParams.get(AppParam.CRISE_ID) ? this._navParams.get(AppParam.CRISE_ID) : null);
    this._configurarNavBar();
    this._configurarFooterBar();

    this._listenEvents();
  }

  private _prepararQuestao() {
    if (this._docCrise) {
      this.questaoPersonalizada.setCrise(this._docCrise);
      this.questaoPersonalizada.triggerAtualizarQuestao();
    } else {
      this.questaoPersonalizada.getCriseId()
        .then(
        (criseId: string) => {
          this.questaoPersonalizada.getCrise()._id = criseId;
        },
      );
    }
  }

  private _configurarQuestao() {
    this.questaoPersonalizada = new QuestaoPersonalizadaService();
    this.questaoPersonalizada.setPage(this);
    this.questaoPersonalizada.setSyncDataServiceReference(this._syncDataProvider);
    this.questaoPersonalizada.setProximaPagina('DiarioPage');
    this.questaoPersonalizada.setHouveInteracao(false);

    if (this._navParams.get(AppParam.ROUTE) === 'sumario') {
      this.questaoPersonalizada.setSumario(true);
    }
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('Gostaria de adicionar anotações?');
  }

  private _configurarFooterBar() {
    this.configuracaoFooterBar = new ConfiguracaoFooterBar();
    this.configuracaoFooterBar.setModoAtualizar(true);
    this.configuracaoFooterBar.setSaving(false);
    this.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.NOTAS);
  }

  private _listenEvents() {
    this._events.subscribe('footer:onClickDissmissQuestions' + TipoQuestao.NOTAS,
      () => {
        this.onClickDissmissQuestions();
      },
    );

    this._events.subscribe('footer:onClickConfirm' + TipoQuestao.NOTAS,
      () => {
        this.onClickConfirm();
      },
    );
  }

  private _destroyEvents() {
    this._events.unsubscribe('footer:onClickDissmissQuestions' + TipoQuestao.NOTAS);
    this._events.unsubscribe('footer:onClickConfirm' + TipoQuestao.NOTAS);
  }

}
