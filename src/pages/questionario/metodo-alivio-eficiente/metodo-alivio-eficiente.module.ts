import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MetodoAlivioEficientePage } from './metodo-alivio-eficiente';

@NgModule({
  declarations: [
    MetodoAlivioEficientePage,
  ],
  imports: [
    IonicPageModule.forChild(MetodoAlivioEficientePage),
    ComponentsModule,
  ],
})
export class MetodoAlivioEficientePageModule { }
