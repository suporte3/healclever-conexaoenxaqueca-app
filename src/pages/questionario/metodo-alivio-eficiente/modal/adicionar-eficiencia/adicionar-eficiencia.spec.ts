import { Observable } from 'rxjs';
import { TipoQuestao } from './../../../../../core/models/pojo/tipo-questao.pojo';
import { TipoEficiencia } from './../../../../../core/models/tipo-eficiencia.enum';
import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { CapitalizePipe } from './../../../../../pipes/capitalize/capitalize';
import { NavParamsMock } from './../../../../../core/mocks/nav-params.mock';
import { ViewControllerMock } from './../../../../../core/mocks/app.mock';
import { ViewController, NavParams } from 'ionic-angular';
import { TestUtils } from './../../../../../app/app.test';
import { AdicionarEficienciaPage } from './adicionar-eficiencia';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Adicionar Eficiencia:', () => {

    let fixture: ComponentFixture<AdicionarEficienciaPage> = null;
    let instance: any = null;
    let _instance: any = AdicionarEficienciaPage.prototype;

    TestUtils.addProvider(
        { provide: ViewController, useClass: ViewControllerMock },
    );

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: ViewController, useClass: ViewControllerMock },
        );
        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.beforeEachCompiler([AdicionarEficienciaPage, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o modal ao carregar o componente', () => {
        expect(instance._iniciar).toBeTruthy();
    });

    it('Deve iniciar o modal', () => {
        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setQuantidade(2);
        instance._navParams = new NavParams({ 'modeloOpcao': modeloOpcao });
        let spyModeloOpcaoDefinirIcones: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirIcones');

        instance._iniciar();
        expect(instance.modeloOpcao).toBeDefined();
        expect(instance._goingOut).toBe(false);
        expect(spyModeloOpcaoDefinirIcones).toHaveBeenCalledWith(TipoQuestao.MEDICAMENTO);

        spyModeloOpcaoDefinirIcones.calls.reset();
        modeloOpcao.setQuantidade(undefined);
        instance._iniciar();
        expect(spyModeloOpcaoDefinirIcones).toHaveBeenCalledWith(TipoQuestao.METODO_ALIVIO);
    });

    it('Deve fechar o modal', () => {
        spyOn(instance, '_sairComParamentro').and.stub();
        instance.onClickCancelar();
        expect(instance._sairComParamentro).toHaveBeenCalledWith(false);
    });

    it('#addEficienciaNadaUtil - Deve adicionar eficiência', () => {
        let spyModeloOpcaoSetTipoEficiencia: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setTipoEficiencia');
        spyOn(instance, '_sairComParamentro');

        instance.addEficienciaNadaUtil();
        expect(spyModeloOpcaoSetTipoEficiencia).toHaveBeenCalledWith(TipoEficiencia.NADA_UTIL);
        expect(instance._sairComParamentro).toHaveBeenCalledWith(true);
    });

    it('#addEficienciaUtil - Deve adicionar eficiência', () => {
        let spyModeloOpcaoSetTipoEficiencia: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setTipoEficiencia');
        spyOn(instance, '_sairComParamentro');

        instance.addEficienciaUtil();
        expect(spyModeloOpcaoSetTipoEficiencia).toHaveBeenCalledWith(TipoEficiencia.UTIL);
        expect(instance._sairComParamentro).toHaveBeenCalledWith(true);
    });

    it('#addEficienciaPoucoUtil - Deve adicionar eficiência', () => {
        let spyModeloOpcaoSetTipoEficiencia: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setTipoEficiencia');
        spyOn(instance, '_sairComParamentro');

        instance.addEficienciaPoucoUtil();
        expect(spyModeloOpcaoSetTipoEficiencia).toHaveBeenCalledWith(TipoEficiencia.POUCO_UTIL);
        expect(instance._sairComParamentro).toHaveBeenCalledWith(true);
    });

    it('#_sairComParamentro - Deve sair do modal corretamente com parametro', (done) => {
        let paramentro: boolean = true;
        instance._goingOut = false;
        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('id-opcao');
        instance.modeloOpcao = modeloOpcao;
        spyOn(instance._viewCtrl, 'dismiss');

        instance._sairComParamentro(paramentro);
        expect(instance._goingOut).toBe(true);

        let obs = Observable.of(1).delay(500);
        obs.subscribe(
            () => {
                expect(instance._viewCtrl.dismiss).toHaveBeenCalledWith({ 'modeloOpcao': instance.modeloOpcao });
                done();
            },
        );
    });

    it('#_sairComParamentro - Deve sair do modal corretamente sem parametro', (done) => {
        let paramentro: boolean = false;
        instance._goingOut = false;
        spyOn(instance._viewCtrl, 'dismiss');

        instance._sairComParamentro(paramentro);
        expect(instance._goingOut).toBe(true);

        let obs = Observable.of(1).delay(500);
        obs.subscribe(
            () => {
                expect(instance._viewCtrl.dismiss).toHaveBeenCalled();
                done();
            },
        );
    });

});
