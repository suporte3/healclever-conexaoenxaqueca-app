import { TipoQuestao } from './../../../../../core/models/pojo/tipo-questao.pojo';
import { TipoEficiencia } from './../../../../../core/models/tipo-eficiencia.enum';
import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-eficiencia',
  templateUrl: 'adicionar-eficiencia.html',
})
export class AdicionarEficienciaPage {

  public tipoEficiencia: TipoEficiencia = 0;
  public modeloOpcao: ModeloOpcao = new ModeloOpcao();
  private _goingOut: boolean;

  constructor(private _viewCtrl: ViewController, private _navParams: NavParams) {

    this._iniciar();
  }

  onClickCancelar() {
    this._sairComParamentro(false);
  }

  public addEficienciaNadaUtil() {
    this.modeloOpcao.setTipoEficiencia(TipoEficiencia.NADA_UTIL);
    this._sairComParamentro(true);
  }

  public addEficienciaPoucoUtil() {
    this.modeloOpcao.setTipoEficiencia(TipoEficiencia.POUCO_UTIL);
    this._sairComParamentro(true);
  }

  public addEficienciaUtil() {
    this.modeloOpcao.setTipoEficiencia(TipoEficiencia.UTIL);
    this._sairComParamentro(true);
  }

  private _iniciar() {
    this._goingOut = false;

    if (this._navParams.get('modeloOpcao') !== 'default') {
      this.modeloOpcao = this._navParams.get('modeloOpcao');
      if (this.modeloOpcao.getQuantidade()) {
        this.modeloOpcao.definirIcones(TipoQuestao.MEDICAMENTO);
      } else {
        this.modeloOpcao.definirIcones(TipoQuestao.METODO_ALIVIO);
      }
    }
  }

  private _sairComParamentro(parametro: boolean) {
    if (!this._goingOut) {
      document.getElementById('menu-toggler').remove();
      this._goingOut = true;
      let scope: AdicionarEficienciaPage = this;
      setTimeout(function () {
        if (parametro) {
          scope._viewCtrl.dismiss({ 'modeloOpcao': scope.modeloOpcao });
        } else {
          scope._viewCtrl.dismiss();
        }
      }, 500);
    }
  }

}
