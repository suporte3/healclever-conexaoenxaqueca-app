import { PipesModule } from './../../../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarEficienciaPage } from './adicionar-eficiencia';

@NgModule({
  declarations: [
    AdicionarEficienciaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarEficienciaPage),
    PipesModule,
  ],
})
export class AdicionarEficienciaPageModule { }
