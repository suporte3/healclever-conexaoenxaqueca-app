import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SintomasPage } from './sintomas';

@NgModule({
  declarations: [
    SintomasPage,
  ],
  imports: [
    IonicPageModule.forChild(SintomasPage),
    ComponentsModule,
  ],
})
export class SintomasPageModule { }
