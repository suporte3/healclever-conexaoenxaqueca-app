import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarSintomaPage } from './adicionar-sintoma';

@NgModule({
  declarations: [
    AdicionarSintomaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarSintomaPage),
  ],
})
export class AdicionarSintomaPageModule { }
