import { TipoOpcao } from './../../../../../core/models/tipo-opcao.enum';
import { ValidationHelper } from './../../../../../core/helpers/validation.helper';
import { AppHelper } from './../../../../../core/helpers/app.helper';
import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { SyncDataProvider } from './../../../../../providers/sync-data/sync-data';
import { Opcoes } from './../../../../../core/models/opcoes.model';
import { Opcao } from './../../../../../core/models/opcao.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-sintoma',
  templateUrl: 'adicionar-sintoma.html',
})
export class AdicionarSintomaPage implements OnInit {

  @ViewChild('adicionarSintomaForm') adicionarSintomaForm: FormGroup;

  public opcao: Opcao = {
    _id: null,
    tipo: TipoOpcao.DINAMICO,
    nome: null,
    dataCriacao: null,
    ativo: true,
  };
  private opcaoSintoma: Opcoes = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_SINTOMA_ID,
    opcoes: [],
  };

  constructor(private _viewCtrl: ViewController, private _syncDataProvider: SyncDataProvider,
    private _navParams: NavParams, private _formBuilder: FormBuilder) {

    this._iniciar();
  }

  ngOnInit() {
    this.adicionarSintomaForm = this._formBuilder.group({
      nome: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          ValidationHelper.opcaoJaExiste.bind(this, this.opcaoSintoma.opcoes),
        ]),
      ],
    });
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  onClickRegistrar() {

    if (this.adicionarSintomaForm.valid) {
      let appHelper: AppHelper = new AppHelper();
      let opcaoInativa: any = appHelper.isOpcaoInativa(this.opcao.nome, this.opcaoSintoma.opcoes);
      if (opcaoInativa) {
        this.opcao = appHelper.ativarOpcao(opcaoInativa);
      } else {
        this.opcao = appHelper.criarNovaOpcao(this.opcao.nome);
        this.opcaoSintoma.opcoes.push(this.opcao);
      }

      let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovaOpcaoNormal(this.opcao);

      this._syncDataProvider.addOrUpdate(this.opcaoSintoma)
        .then(
        () => { },
        (err) => {
          console.log('ERRO AO CADASTRAR SINTOMA', err);
          // TRACK
        },
      );

      this._viewCtrl.dismiss({ 'opcao': this.opcaoSintoma, 'modeloOpcao': modeloOpcao });
    }
  }

  private _iniciar() {
    let opcoes: Opcoes = this._navParams.get('opcoes');
    if (opcoes) {
      this.opcaoSintoma = opcoes;
    }
  }

}
