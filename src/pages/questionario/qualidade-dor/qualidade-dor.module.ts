import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QualidadeDorPage } from './qualidade-dor';

@NgModule({
  declarations: [
    QualidadeDorPage,
  ],
  imports: [
    IonicPageModule.forChild(QualidadeDorPage),
    ComponentsModule,
  ],
})
export class QualidadeDorPageModule { }
