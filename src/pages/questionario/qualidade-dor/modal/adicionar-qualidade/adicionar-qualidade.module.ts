import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarQualidadePage } from './adicionar-qualidade';

@NgModule({
  declarations: [
    AdicionarQualidadePage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarQualidadePage),
  ],
})
export class AdicionarQualidadePageModule { }
