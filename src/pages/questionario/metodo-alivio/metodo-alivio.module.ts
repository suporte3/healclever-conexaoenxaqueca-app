import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MetodoAlivioPage } from './metodo-alivio';

@NgModule({
  declarations: [
    MetodoAlivioPage,
  ],
  imports: [
    IonicPageModule.forChild(MetodoAlivioPage),
    ComponentsModule,
  ],
})
export class MetodoAlivioPageModule { }
