import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarMetodoAlivioPage } from './adicionar-metodo-alivio';

@NgModule({
  declarations: [
    AdicionarMetodoAlivioPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarMetodoAlivioPage),
  ],
})
export class AdicionarMetodoAlivioPageModule { }
