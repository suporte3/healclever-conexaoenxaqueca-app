import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { AppHelper } from './../../../../../core/helpers/app.helper';
import { ValidationHelper } from './../../../../../core/helpers/validation.helper';
import { SyncDataProvider } from './../../../../../providers/sync-data/sync-data';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TipoOpcao } from './../../../../../core/models/tipo-opcao.enum';
import { Opcoes } from './../../../../../core/models/opcoes.model';
import { Opcao } from './../../../../../core/models/opcao.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-metodo-alivio',
  templateUrl: 'adicionar-metodo-alivio.html',
})
export class AdicionarMetodoAlivioPage implements OnInit {

  @ViewChild('adicionarMetodoAlivioForm') adicionarMetodoAlivioForm: FormGroup;

  public opcao: Opcao = {
    _id: null,
    tipo: TipoOpcao.DINAMICO,
    nome: null,
    dataCriacao: null,
    ativo: true,
  };
  private opcaoMetodo: Opcoes = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_QUALIDADE_DOR_ID,
    opcoes: [],
  };

  constructor(private _viewCtrl: ViewController, private _syncDataProvider: SyncDataProvider,
    private _navParams: NavParams, private _formBuilder: FormBuilder) {

    this._iniciar();
  }

  ngOnInit() {
    this.adicionarMetodoAlivioForm = this._formBuilder.group({
      nome: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          ValidationHelper.opcaoJaExiste.bind(this, this.opcaoMetodo.opcoes),
        ]),
      ],
    });
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  onClickRegistrar() {

    if (this.adicionarMetodoAlivioForm.valid) {
      let appHelper: AppHelper = new AppHelper();
      let opcaoInativa: any = appHelper.isOpcaoInativa(this.opcao.nome, this.opcaoMetodo.opcoes);
      if (opcaoInativa) {
        this.opcao = appHelper.ativarOpcao(opcaoInativa);
      } else {
        this.opcao = appHelper.criarNovaOpcao(this.opcao.nome);
        this.opcaoMetodo.opcoes.push(this.opcao);
      }

      let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovaOpcaoNormal(this.opcao);

      this._syncDataProvider.addOrUpdate(this.opcaoMetodo)
        .then(
        () => { },
        (err) => {
          console.log('ERRO AO CADASTRAR METODO ALIVIO', err);
          // TRACK
        },
      );

      this._viewCtrl.dismiss({ 'opcao': this.opcaoMetodo, 'modeloOpcao': modeloOpcao });
    }
  }

  private _iniciar() {
    let opcoes: Opcoes = this._navParams.get('opcoes');
    if (opcoes) {
      this.opcaoMetodo = opcoes;
    }
  }

}
