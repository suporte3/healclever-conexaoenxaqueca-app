import { AppParam } from './../../../core/models/pojo/app-param.pojo';
import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { Crise } from './../../../core/models/crise.model';
import { QuestaoGridService } from './../../../services/questao-grid/questao-grid.service';
import { GridQuestionarioComponent } from './../../../components/grid-questionario/grid-questionario';
import { ConfiguracaoFooterBar } from './../../../core/models/pojo/configuracao-footer-bar.pojo';
import { ConfiguracaoNavBar } from './../../../core/models/pojo/configuracao-nav-bar.pojo';
import { ConfiguracaoGrid } from './../../../core/models/pojo/configuracao-grid.pojo';
import { Component, ViewChild, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, ModalController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-metodo-alivio',
  templateUrl: 'metodo-alivio.html',
})
export class MetodoAlivioPage implements OnDestroy {

  public componentesConfigurado: boolean;
  public configuracaoGrid: ConfiguracaoGrid = new ConfiguracaoGrid();
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public configuracaoFooterBar: ConfiguracaoFooterBar = new ConfiguracaoFooterBar();
  @ViewChild('grid') grid: GridQuestionarioComponent;

  private _questaoGrid: QuestaoGridService = new QuestaoGridService();
  private _docCrise: Crise;

  constructor(private _navCtrl: NavController, private _modalCtrl: ModalController, private _navParams: NavParams,
    private _events: Events, private _syncDataProvider: SyncDataProvider) {

    this._iniciar();
  }

  ngOnDestroy() {
    this._destroyEvents();
  }

  ionViewDidEnter() {
    this._questaoGrid.setGrid(this.grid);

    this._questaoGrid.iniciarGrid(this._docCrise).then(
      () => {
        this._questaoGrid.montarGrid();
        this.componentesConfigurado = this._questaoGrid.componentesConfigurado;
      },
      () => {
        // Não foi possível iniciar, devido a não ser possível obter os dados relacionado a este registro
        // Pensar em um tratamento
        console.log('Não foi possível iniciar, devido a não ser possível obter os dados relacionado a este registro');
      },
    );
  }

  onClickConfirm() {
    this._questaoGrid.salvarOpcoes();
  }

  onClickNext() {
    this._questaoGrid.salvarOpcoes();
  }

  onClickDissmissQuestions() {
    this._navCtrl.setRoot('DiarioPage');
  }

  private _iniciar() {
    this.componentesConfigurado = this._questaoGrid.componentesConfigurado;
    this._docCrise = (this._navParams.get(AppParam.CRISE_ID) ? this._navParams.get(AppParam.CRISE_ID) : null);

    this._configurarNavBar();
    this._configurarFooterBar();
    this._configurarGrid();

    this._listenEvents();
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('Foi ultilizado algum método de alívio?');
    this.configuracaoNavBar.setTextoModoRemover('Toque no método de alívio que deseja remover:');
    this.configuracaoNavBar.setIdentificadorGrid(TipoQuestao.METODO_ALIVIO);
  }

  private _configurarFooterBar() {

    this.configuracaoFooterBar.setModoAtualizar(false);
    this.configuracaoFooterBar.setSaving(false);
    this.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.METODO_ALIVIO);
  }

  private _configurarGrid() {
    this._questaoGrid.setNavCtrlReference(this._navCtrl);
    this._questaoGrid.setEventsReference(this._events);
    this._questaoGrid.setSyncDataServiceReference(this._syncDataProvider);
    this._questaoGrid.setConfiguracaoFooterBarReference(this.configuracaoFooterBar);

    this.configuracaoGrid.setIdentificadorGrid(TipoQuestao.METODO_ALIVIO);
    this.configuracaoGrid.setModalAdicionarOpcaoReferencia('AdicionarMetodoAlivioPage');
    this.configuracaoGrid.setPossuiModal(true);
    // this.configuracaoGrid.setQuestao(this);
    this.configuracaoGrid.setModalCtrl(this._modalCtrl);
    this.configuracaoGrid.setDocumentoOptionId(SyncDataProvider.DOCUMENT_OPCOES_METODO_ALIVIO_ID);
    this.configuracaoGrid.setNomeDoObjetoOpcao('metodosAlivio');
    this.configuracaoGrid.setProximaPagina('ConsequenciaPage');

    if (this._navParams.get(AppParam.ROUTE) === 'sumario') {
      this.configuracaoGrid.setRota('sumario');
    }
  }

  private _listenEvents() {
    this._events.subscribe('footer:onClickDissmissQuestions' + TipoQuestao.METODO_ALIVIO,
      () => {
        this.onClickDissmissQuestions();
      },
    );

    this._events.subscribe('footer:onClickNext' + TipoQuestao.METODO_ALIVIO,
      () => {
        this.onClickNext();
      },
    );

    this._events.subscribe('footer:onClickConfirm' + TipoQuestao.METODO_ALIVIO,
      () => {
        this.onClickConfirm();
      },
    );
  }

  private _destroyEvents() {
    this._questaoGrid.destroyListenGridEvents();
    this._events.unsubscribe('footer:onClickDissmissQuestions' + TipoQuestao.METODO_ALIVIO);
    this._events.unsubscribe('footer:onClickNext' + TipoQuestao.METODO_ALIVIO);
    this._events.unsubscribe('footer:onClickConfirm' + TipoQuestao.METODO_ALIVIO);
  }

}
