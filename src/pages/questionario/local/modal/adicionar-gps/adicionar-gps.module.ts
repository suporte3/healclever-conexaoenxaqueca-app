import { PipesModule } from './../../../../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarGpsPage } from './adicionar-gps';

@NgModule({
  declarations: [
    AdicionarGpsPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarGpsPage),
    PipesModule,
  ],
})
export class AdicionarGpsPageModule { }
