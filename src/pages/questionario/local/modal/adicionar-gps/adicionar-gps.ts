import { AppHelper } from './../../../../../core/helpers/app.helper';
import { Localizacao } from './../../../../../core/models/localizacao.model';
import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { Component, ChangeDetectionStrategy, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { IonicPage, NavParams, ViewController, Platform, ToastController } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import clone from 'lodash-es/clone';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-gps',
  templateUrl: 'adicionar-gps.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AdicionarGpsPage {

  public isLocalFixo: boolean;
  public mapIsLoaded: boolean = false;
  public modeloOpcao: ModeloOpcao;
  public map: google.maps.Map;
  @ViewChild('map') mapElement: ElementRef;
  @ViewChild('mapSearchBox') mapSearchBoxElement: ElementRef;

  private _modeloLocalizacao: Localizacao;
  private _marker: google.maps.Marker;
  private _infoWindow: google.maps.InfoWindow;
  private _isLocalAtual: boolean = false;

  constructor(private _viewCtrl: ViewController, private _navParams: NavParams, private _geolocation: Geolocation,
    private _platform: Platform, private _changeDetectorRef: ChangeDetectorRef, private _toastCtrl: ToastController) {

    this._iniciar();
  }

  ionViewDidLoad() {
    this._initLoadMap();
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  onClickRegistrar() {
    this.modeloOpcao.setLocalFixo(this.isLocalFixo);
    this._viewCtrl.dismiss({ 'modeloOpcao': this.modeloOpcao });
  }

  private _iniciar() {
    this.modeloOpcao = clone(this._navParams.get('modeloOpcao'));
    this.isLocalFixo = this._navParams.get('isLocalFixo');
  }

  private _initLoadMap() {
    let latLng: any;

    if (this.modeloOpcao.getValorGPS()) {
      try {
        latLng = new google.maps.LatLng(this.modeloOpcao.getValorGPS().latitude, this.modeloOpcao.getValorGPS().longitude);
        this._isLocalAtual = false;
        this._loadMap(latLng);
      } catch (err) {
        // TRACK ERR
        new AppHelper().toastMensagem(this._toastCtrl, 'Não foi possível iniciar o Google Maps');
        this.onClickCancelar();
      }
    } else {
      this._platform.ready().then(
        () => {
          this._geolocation.getCurrentPosition().then(
            (position: any) => {
              try {
                latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                this._isLocalAtual = true;
                this._loadMap(latLng);
              } catch (err) {
                // TRACK ERR
                new AppHelper().toastMensagem(this._toastCtrl, 'Não foi possível iniciar o Google Maps');
                this.onClickCancelar();
              }
            }, (err) => {
              console.log('AdicionarGPS~_initLoadMap()', err);
              new AppHelper().toastMensagem(this._toastCtrl, 'Não foi possível iniciar o Google Maps');
              this.onClickCancelar();
            },
          );
        },
      );
    }
  }

  private _loadMap(latLng: any) {
    let scope: AdicionarGpsPage = this;

    let mapOptions = {
      center: latLng,
      zoomControl: false,
      streetViewControl: false,
      mapTypeControl: false,
      clickableIcons: false,
      fullscreenControl: false,
      // noClear: true,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    google.maps.event.addListenerOnce(this.map, 'idle', function () {
      scope.mapIsLoaded = true;

      scope._addMarker(null, null);
      scope._addSearchBox();

      scope._changeDetectorRef.detectChanges();
    });
  }

  private _addMarker(position: any, descricao: string) {

    let currentPosition: any = position || this.map.getCenter();

    if (!this._marker) {
      let markerOptions: google.maps.MarkerOptions = {
        position: null,
        map: this.map,
        draggable: true,
        animation: google.maps.Animation.DROP,
      };
      this._marker = new google.maps.Marker(markerOptions);
    }

    this._marker.setPosition(currentPosition);

    if (descricao) {
      this._addInfoWindow(this._marker, descricao);
      this._offsetCenter(currentPosition, 0, -100);
      this._setModeloLocalizacao(descricao, currentPosition.lat(), currentPosition.lng());
    } else {
      this._geocodePosition(currentPosition).then(
        (content: string) => {
          descricao = content;
          this._isLocalAtual = false;
          this._addInfoWindow(this._marker, descricao);
          this._offsetCenter(currentPosition, 0, -100);
          this._setModeloLocalizacao(descricao, currentPosition.lat(), currentPosition.lng());
        },
      );
    }
  }

  private _getTitulo(): string {
    let titulo: string;
    if (this.modeloOpcao.getValorGPS()) {
      titulo = (this.modeloOpcao.getValorGPS().titulo ? this.modeloOpcao.getValorGPS().titulo : this.modeloOpcao.getNome());
    } else {
      titulo = this.modeloOpcao.getNome();
    }

    if (this._isLocalAtual) {
      titulo = titulo + '(local atual)';
    }

    return titulo;
  }

  private _addInfoWindow(marker: any, content: any) {

    if (!this._infoWindow) {
      this._infoWindow = new google.maps.InfoWindow();
    }

    let infoWindowHTML: string;
    infoWindowHTML = '<h5>' + this._getTitulo() + '</h5>';
    infoWindowHTML += '<p>' + content + '</p>';
    this._infoWindow.setContent(infoWindowHTML);
    this._infoWindow.close();
    this._infoWindow.open(this.map, marker);
  }

  private _addSearchBox() {
    let scope: AdicionarGpsPage = this;

    // Create the search box and link it to the UI element.
    let searchBox = new google.maps.places.SearchBox(this.mapSearchBoxElement.nativeElement);

    // Bias the SearchBox results towards current map's viewport.
    this.map.addListener('bounds_changed', function () {
      searchBox.setBounds(scope.map.getBounds());
    });

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
      let places: any = searchBox.getPlaces();

      if (places.length === 0) {
        return;
      }

      // For each place, get the icon, name and location.
      let bounds = new google.maps.LatLngBounds();
      let place: any = places[0];
      if (place) {
        if (!place.geometry) {
          console.log('Returned place contains no geometry');
          return;
        }

        /*
        var icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };*/

        // Create a marker for each place.
        /*markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            title: place.name,
            position: place.geometry.location
        }));*/
        scope._addMarker(place.geometry.location, places.formatted_address);

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      }

      scope.map.fitBounds(bounds);
    });
  }

  private _geocodePosition(position: any): Promise<any> {

    return new Promise(function (resolve) {
      let geocoder: google.maps.Geocoder = new google.maps.Geocoder();
      geocoder.geocode(
        {
          location: position,
        },
        function (results: any, status: any) {
          if (status === google.maps.GeocoderStatus.OK) {
            resolve(results[0].formatted_address);
          } else {
            resolve('Não foi possível identificar o local');
          }
        },
      );
    });
  }

  private _setModeloLocalizacao(desc: string, lat: number, lng: number) {
    this._modeloLocalizacao = {
      titulo: this.modeloOpcao.getNome(),
      descricao: desc,
      latitude: lat,
      longitude: lng,
    };

    this.modeloOpcao.setValorGPS(this._modeloLocalizacao);
    this.modeloOpcao.setLocalFixo(this.isLocalFixo);
  }

  private _offsetCenter(latlng: any, offsetx: number, offsety: number) {
    // latlng is the apparent centre-point
    // offsetx is the distance you want that point to move to the right, in pixels
    // offsety is the distance you want that point to move upwards, in pixels
    // offset can be negative
    // offsetx and offsety are both optional

    let scale = Math.pow(2, this.map.getZoom());

    let worldCoordinateCenter: google.maps.Point = this.map.getProjection().fromLatLngToPoint(latlng);
    let pixelOffset: google.maps.Point = new google.maps.Point((offsetx / scale) || 0, (offsety / scale) || 0);

    let worldCoordinateNewCenter: google.maps.Point = new google.maps.Point(
      worldCoordinateCenter.x - pixelOffset.x,
      worldCoordinateCenter.y + pixelOffset.y,
    );

    let newCenter: google.maps.LatLng = this.map.getProjection().fromPointToLatLng(worldCoordinateNewCenter);

    this.map.setCenter(newCenter);

  }

}
