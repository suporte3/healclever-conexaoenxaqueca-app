import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CicloMenstrualPage } from './ciclo-menstrual';

@NgModule({
  declarations: [
    CicloMenstrualPage,
  ],
  imports: [
    IonicPageModule.forChild(CicloMenstrualPage),
    ComponentsModule,
  ],
})
export class CicloMenstrualPageModule { }
