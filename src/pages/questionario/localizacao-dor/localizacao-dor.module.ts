import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LocalizacaoDorPage } from './localizacao-dor';

@NgModule({
  declarations: [
    LocalizacaoDorPage,
  ],
  imports: [
    IonicPageModule.forChild(LocalizacaoDorPage),
    ComponentsModule,
  ],
})
export class LocalizacaoDorPageModule { }
