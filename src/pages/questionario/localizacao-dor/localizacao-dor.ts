import { LocalizacaoPart } from './../../../core/models/pojo/localizacao-dor-part.pojo';
import { ConfiguracaoLocalizacaoDorTouch } from './../../../core/models/pojo/configuracao-localizacao-dor-touch.pojo';
import { AppHelper } from './../../../core/helpers/app.helper';
import { AppParam } from './../../../core/models/pojo/app-param.pojo';
import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { Crise } from './../../../core/models/crise.model';
import { Opcao } from './../../../core/models/opcao.model';
import { ConfiguracaoFooterBar } from './../../../core/models/pojo/configuracao-footer-bar.pojo';
import { ConfiguracaoNavBar } from './../../../core/models/pojo/configuracao-nav-bar.pojo';
import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { Component, OnDestroy } from '@angular/core';
import { IonicPage, NavController, NavParams, Events } from 'ionic-angular';
import filter from 'lodash-es/filter';
import findIndex from 'lodash-es/findIndex';
import * as moment from 'moment';

@IonicPage()
@Component({
  selector: 'ib-page-localizacao-dor',
  templateUrl: 'localizacao-dor.html',
})
export class LocalizacaoDorPage implements OnDestroy {

  public questaoPersonalizada: QuestaoPersonalizadaService;
  public componenteConfigurado: boolean;
  public configuracaoLocalizacaoDorTouch: ConfiguracaoLocalizacaoDorTouch;
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public configuracaoFooterBar: ConfiguracaoFooterBar;

  private _localizacoesFixas: Array<Opcao>;
  private _docCrise: Crise;

  constructor(public _navCtrl: NavController, private _syncDataProvider: SyncDataProvider, private _navParams: NavParams, private _events: Events) {

    this._iniciar();
  }

  ngOnDestroy() {
    this._destroyEvents();
  }

  ionViewWillEnter() {
    this._configurarQuestao();
    this._prepararQuestao();
  }

  onClickConfirm() {
    this.questaoPersonalizada.registrarQuestao();
  }

  onClickNext() {
    this.questaoPersonalizada.registrarQuestao();
  }

  onClickDissmissQuestions() {
    this._navCtrl.setRoot('DiarioPage');
  }

  onErrorSaveQuestion(err: any) { console.log(err); }
  onSavingQuestion(estadoQuestao: boolean) {
    this.configuracaoFooterBar.setSaving(estadoQuestao);
  }

  onClickHelp() {
    console.log('click tutorial');
  }

  public onClickArea(areaID: number, selecionado: boolean) {

    this.questaoPersonalizada.setHouveInteracao(true);
    let stringAreaId: string = areaID.toString();
    if (selecionado) {

      let localizacao: Opcao = (filter(this._localizacoesFixas, o => o._id === stringAreaId))[0];
      localizacao.dataCriacao = moment().local().format(AppHelper.FORMATO_DATA_ISO_8601);

      this.questaoPersonalizada.getCrise().localizacoesDeDor.push(localizacao);
    } else {
      let index: number = findIndex(
        this.questaoPersonalizada.getCrise().localizacoesDeDor,
        function (o) {
          return o._id === areaID.toString();
        },
      );

      if (index >= 0) {
        this.questaoPersonalizada.getCrise().localizacoesDeDor.splice(index, 1);
      }
    }
  }

  onCallbackPreRegistrarQuestao(): Promise<any> {

    return Promise.resolve();
  }

  onCallbackPreAtualizarQuestao(): Promise<any> {

    return Promise.resolve();
  }

  onAtualizarQuestao() {
    let extensao: string = '.svg';
    let localizacoes: Array<LocalizacaoPart> = [];
    for (let i = 1; i < 20; i++) {
      let caminhoImagem = 'assets/img/localizacao-dor/localizacao_dor_' + i + extensao;
      let stringAreaId: string = i.toString();
      let isSelecionado: boolean = false;

      let localizacao: Opcao[] = (filter(this.questaoPersonalizada.getCrise().localizacoesDeDor, o => o._id === stringAreaId));
      if (localizacao.length > 0) {
        isSelecionado = true;
      }
      localizacoes[i] = new LocalizacaoPart(caminhoImagem, isSelecionado);
    }

    this.configuracaoLocalizacaoDorTouch.setImagemInicial('assets/img/localizacao-dor/localizacao_dor_0' + extensao);
    this.configuracaoLocalizacaoDorTouch.setImagemMapa('assets/img/localizacao-dor/localizacao_dor_map' + extensao);
    this.configuracaoLocalizacaoDorTouch.setLocalizacoes(localizacoes);
    this.configuracaoLocalizacaoDorTouch.setPage(this);
    this.configuracaoLocalizacaoDorTouch.setHasTouch(true);
    this.componenteConfigurado = true;
  }

  private _iniciar() {
    this.componenteConfigurado = false;
    this.configuracaoLocalizacaoDorTouch = new ConfiguracaoLocalizacaoDorTouch();
    this._docCrise = (this._navParams.get(AppParam.CRISE_ID) ? this._navParams.get(AppParam.CRISE_ID) : null);

    this._inicializarLocalizacoesDor();
    this._configurarNavBar();
    this._configurarFooterBar();

    this._listenEvents();
  }

  private _prepararQuestao() {
    if (this._docCrise) {
      this.questaoPersonalizada.setCrise(this._docCrise);
      this.questaoPersonalizada.triggerAtualizarQuestao();
      this.configuracaoFooterBar.setModoAtualizar(true);
    } else {
      this.questaoPersonalizada.getCriseId()
        .then(
        (criseId: string) => {
          this.questaoPersonalizada.getCrise()._id = criseId;
        },
      );
    }
  }

  private _configurarQuestao() {
    this.questaoPersonalizada = new QuestaoPersonalizadaService();
    this.questaoPersonalizada.setPage(this);
    this.questaoPersonalizada.setSyncDataServiceReference(this._syncDataProvider);
    this.questaoPersonalizada.setProximaPagina('QualidadeDorPage');
    this.questaoPersonalizada.setHouveInteracao(false);

    if (this._navParams.get(AppParam.ROUTE) === 'sumario') {
      this.questaoPersonalizada.setSumario(true);
    }
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('Selecione o local da dor:');
  }

  private _configurarFooterBar() {
    this.configuracaoFooterBar = new ConfiguracaoFooterBar();
    this.configuracaoFooterBar.setModoAtualizar(false);
    this.configuracaoFooterBar.setSaving(false);
    this.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.LOCALIZACAO_DOR);
  }

  private _inicializarLocalizacoesDor() {
    this._syncDataProvider.getDocumentById(SyncDataProvider.DOCUMENT_OPCOES_LOCALIZACAO_DOR_ID).then(
      (result: any) => {
        if (result.error && result.status === 404) {
          // TRACK
          // Não encontrei as opções pré cadastradas
        } else if (result.error === undefined) {
          this._localizacoesFixas = result.opcoes;
        } else {
          // TRACK
        }
      },
    );
  }

  private _listenEvents() {
    this._events.subscribe('footer:onClickDissmissQuestions' + TipoQuestao.LOCALIZACAO_DOR,
      () => {
        this.onClickDissmissQuestions();
      },
    );

    this._events.subscribe('footer:onClickNext' + TipoQuestao.LOCALIZACAO_DOR,
      () => {
        this.onClickNext();
      },
    );

    this._events.subscribe('footer:onClickConfirm' + TipoQuestao.LOCALIZACAO_DOR,
      () => {
        this.onClickConfirm();
      },
    );
  }

  private _destroyEvents() {
    this._events.unsubscribe('footer:onClickDissmissQuestions' + TipoQuestao.LOCALIZACAO_DOR);
    this._events.unsubscribe('footer:onClickNext' + TipoQuestao.LOCALIZACAO_DOR);
    this._events.unsubscribe('footer:onClickConfirm' + TipoQuestao.LOCALIZACAO_DOR);
  }

}
