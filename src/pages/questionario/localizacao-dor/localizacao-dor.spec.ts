import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { QuestaoPersonalizadaService } from './../../../services/questao-personalizada/questao-personalizada.service';
import { NavParamsMock } from './../../../core/mocks/nav-params.mock';
import { NavParams } from 'ionic-angular';
import { TestUtils } from './../../../app/app.test';
import { Opcao } from './../../../core/models/opcao.model';
import { Crise } from './../../../core/models/crise.model';
import { LocalizacaoDorPage } from './localizacao-dor';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Localização Dor Page:', () => {

    let fixture: ComponentFixture<LocalizacaoDorPage> = null;
    let instance: any = null;
    let _instance: any = LocalizacaoDorPage.prototype;
    let criseObjetoTest: Crise = {
        _id: 'idCrise',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };

    let localizacoes: Array<Opcao> = [
        {
            _id: '12',
            nome: 'occipital direita',
            dataCriacao: null,
            ativo: true,
            tipo: 0,
        },
    ];

    let localizacoesFixas: Array<Opcao> = [
        {
            _id: '1',
            nome: 'cervical ou nucal esquerda',
            dataCriacao: null,
            ativo: true,
            tipo: 0,
        },
        {
            _id: '2',
            nome: 'occipital esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '3',
            nome: 'frontal esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '4',
            nome: 'parietal esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '5',
            nome: 'orbitária esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '6',
            nome: 'artéria temporal profunda posterior esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '7',
            nome: 'temporal esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '8',
            nome: 'maxilar esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '9',
            nome: 'auricular  e ATM esquerda',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '10',
            nome: 'vértex',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '11',
            nome: 'cervical ou nucal direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '12',
            nome: 'occipital direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '13',
            nome: 'frontal direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '14',
            nome: 'parietal direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '15',
            nome: 'orbitária direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '16',
            nome: 'artéria temporal profunda posterior direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '17',
            nome: 'temporal direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '18',
            nome: 'maxilar direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
        {
            _id: '19',
            nome: 'maxilar direita',
            ativo: true,
            dataCriacao: null,
            tipo: 0,
        },
    ];

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.beforeEachCompiler([LocalizacaoDorPage]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();

            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeDefined();
        expect(instance).toBeDefined();
        done();
    });

    it('Deve iniciar o componente ao carregar', (done) => {
        expect(instance._iniciar).toHaveBeenCalled();
        done();
    });

    it('#ngOnDestroy - Deve destruir o componente', () => {
        spyOn(instance, '_destroyEvents').and.stub();

        instance.ngOnDestroy();

        expect(instance._destroyEvents).toHaveBeenCalled();
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        spyOn(instance, '_inicializarLocalizacoesDor').and.stub();
        spyOn(instance, '_configurarNavBar').and.stub();
        spyOn(instance, '_configurarFooterBar').and.stub();
        spyOn(instance, '_listenEvents').and.stub();

        instance._iniciar();

        expect(instance._inicializarLocalizacoesDor).toHaveBeenCalled();
        expect(instance._configurarNavBar).toHaveBeenCalled();
        expect(instance._configurarFooterBar).toHaveBeenCalled();
        expect(instance._listenEvents).toHaveBeenCalled();
    });

    it('Deve configurar a questão ao entrar na tela', () => {
        spyOn(instance, '_configurarQuestao');
        spyOn(instance, '_prepararQuestao').and.stub();

        instance.ionViewWillEnter();

        expect(instance._configurarQuestao).toHaveBeenCalled();
        expect(instance._prepararQuestao).toHaveBeenCalled();
    });

    it('#_configurarQuestao - Deve configurar a questão corretamente', () => {
        spyOn(QuestaoPersonalizadaService.prototype, 'setSumario').and.stub();

        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncDataProvider);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('QualidadeDorPage');
        expect(instance.questaoPersonalizada.getHouveInteracao()).toBe(false);
        expect(QuestaoPersonalizadaService.prototype.setSumario).not.toHaveBeenCalled();
    });

    it('#_configurarQuestao - Deve configurar a questão corretamente - sumário', () => {
        instance._navParams = new NavParams({ 'com.healclever.myMigraine.route': 'sumario' });
        instance._configurarQuestao();

        expect(instance.questaoPersonalizada.getPage()).toBe(instance);
        expect(instance.questaoPersonalizada.getSyncDataServiceReference()).toBe(instance._syncDataProvider);
        expect(instance.questaoPersonalizada.getProximaPagina()).toBe('QualidadeDorPage');
        expect(instance.questaoPersonalizada.getHouveInteracao()).toBe(false);
        expect(instance.questaoPersonalizada.isSumario()).toBe(true);
    });

    it('#onClickDissmissQuestions - Deve ir para o Painel', () => {
        spyOn(instance._navCtrl, 'setRoot').and.stub();
        instance.onClickDissmissQuestions();
        expect(instance._navCtrl.setRoot).toHaveBeenCalledWith('DiarioPage');
    });

    it('#onClickNext - Deve registrar a questão', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'registrarQuestao');

        instance.onClickNext();
        expect(instance.questaoPersonalizada.registrarQuestao).toHaveBeenCalled();
    });

    it('#onClickConfirm - Deve registrar a questão', () => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'registrarQuestao');

        instance.onClickConfirm();
        expect(instance.questaoPersonalizada.registrarQuestao).toHaveBeenCalled();
    });

    it('#onSavingQuestion - Deve alterar o estado da questão', () => {
        let estadoQuestao: boolean = true;
        instance.configuracaoFooterBar.setSaving(false);
        instance.onSavingQuestion(estadoQuestao);

        expect(instance.configuracaoFooterBar.isSaving()).toBe(estadoQuestao);
    });

    it('Deve preparar questão', (done) => {
        instance._configurarQuestao();

        spyOn(instance.questaoPersonalizada, 'getCriseId').and.returnValue(Promise.resolve('criseId'));
        instance._docCrise = null;
        instance._prepararQuestao();

        expect(instance.questaoPersonalizada.getCriseId).toHaveBeenCalled();
        fixture.whenStable().then(
            () => {
                expect(instance.questaoPersonalizada.getCrise()._id).toEqual('criseId');
                done();
            },
        );
    });

    it('Deve preparar questão - sumário', () => {
        instance._configurarQuestao();
        instance._configurarFooterBar();

        spyOn(instance.configuracaoFooterBar, 'setModoAtualizar').and.stub();
        spyOn(instance.questaoPersonalizada, 'setCrise').and.stub();
        spyOn(instance.questaoPersonalizada, 'triggerAtualizarQuestao').and.stub();
        instance._docCrise = criseObjetoTest;
        instance._prepararQuestao();

        expect(instance.questaoPersonalizada.triggerAtualizarQuestao).toHaveBeenCalled();
        expect(instance.questaoPersonalizada.setCrise).toHaveBeenCalledWith(instance._docCrise);
        expect(instance.configuracaoFooterBar.setModoAtualizar).toHaveBeenCalledWith(true);
    });

    it('#onAtualizarQuestao - Deve atualizar a questão', () => {
        instance._configurarQuestao();
        instance.onAtualizarQuestao();

        expect(instance.configuracaoLocalizacaoDorTouch.getImagemInicial()).toBe('assets/img/localizacao-dor/localizacao_dor_0.svg');
        expect(instance.configuracaoLocalizacaoDorTouch.getImagemMapa()).toBe('assets/img/localizacao-dor/localizacao_dor_map.svg');
        expect(instance.configuracaoLocalizacaoDorTouch.getLocalizacoes()).toBeDefined();
        expect(instance.configuracaoLocalizacaoDorTouch.getPage()).toBe(instance);
        expect(instance.componenteConfigurado).toBe(true);
    });

    it('#onClickArea - Deve add area', () => {
        instance._configurarQuestao();

        instance.questaoPersonalizada.getCrise().localizacoesDeDor = localizacoes;
        instance._localizacoesFixas = localizacoesFixas;
        let indiceNovaLocalizacao: number = 3;
        let novaLocalizacao: Opcao = localizacoesFixas[indiceNovaLocalizacao - 1];

        instance.onClickArea(indiceNovaLocalizacao, true);
        expect(instance.questaoPersonalizada.getCrise().localizacoesDeDor).toContain(novaLocalizacao);
    });

    it('#onClickArea - Deve remover area', () => {
        instance._configurarQuestao();

        instance.questaoPersonalizada.getCrise().localizacoesDeDor = localizacoes;
        instance._localizacoesFixas = localizacoesFixas;
        let indiceAntigaLocalizacao: number = 12;
        let antigaLocalizacao: Opcao = localizacoesFixas[indiceAntigaLocalizacao - 1];

        instance.onClickArea(indiceAntigaLocalizacao, false);
        expect(instance.questaoPersonalizada.getCrise().localizacoesDeDor).not.toContain(antigaLocalizacao);
    });

    it('Deve configurar o <nav-bar /> corretamente', () => {
        instance._configurarNavBar();
        expect(instance.configuracaoNavBar.getTextoInicial()).toBe('Selecione o local da dor:');
    });

    it('Deve configurar o <footer-bar /> corretamente', () => {
        instance._configurarFooterBar();
        expect(instance.configuracaoFooterBar.isModoAtualizar()).toBe(false);
        expect(instance.configuracaoFooterBar.isSaving()).toBe(false);
        expect(instance.configuracaoFooterBar.getIdentificadorGrid()).toBe(TipoQuestao.LOCALIZACAO_DOR);
    });

    it('#_listenEvents - Deve escutar eventos', () => {
        let spySubscribe: jasmine.Spy = spyOn(instance._events, 'subscribe').and.stub();

        instance._listenEvents();

        expect(spySubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.LOCALIZACAO_DOR);
        expect(spySubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.LOCALIZACAO_DOR);
        expect(spySubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.LOCALIZACAO_DOR);
    });

    it('#_destroyEvents - Deve destruir eventos que estão sendo escutados', () => {
        let spyUnsubscribe: jasmine.Spy = spyOn(instance._events, 'unsubscribe').and.stub();

        instance._destroyEvents();

        expect(spyUnsubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.LOCALIZACAO_DOR);
        expect(spyUnsubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.LOCALIZACAO_DOR);
        expect(spyUnsubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.LOCALIZACAO_DOR);
    });

});
