import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarConsequenciaPage } from './adicionar-consequencia';

@NgModule({
  declarations: [
    AdicionarConsequenciaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarConsequenciaPage),
  ],
})
export class AdicionarConsequenciaPageModule { }
