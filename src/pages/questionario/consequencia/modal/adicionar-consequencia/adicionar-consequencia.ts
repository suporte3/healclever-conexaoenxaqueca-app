import { ValidationHelper } from './../../../../../core/helpers/validation.helper';
import { AppHelper } from './../../../../../core/helpers/app.helper';
import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { SyncDataProvider } from './../../../../../providers/sync-data/sync-data';
import { Opcoes } from './../../../../../core/models/opcoes.model';
import { TipoOpcao } from './../../../../../core/models/tipo-opcao.enum';
import { Opcao } from './../../../../../core/models/opcao.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-consequencia',
  templateUrl: 'adicionar-consequencia.html',
})
export class AdicionarConsequenciaPage implements OnInit {

  @ViewChild('adicionarConsequenciaForm') adicionarConsequenciaForm: FormGroup;

  public opcao: Opcao = {
    _id: null,
    tipo: TipoOpcao.DINAMICO,
    nome: null,
    dataCriacao: null,
    ativo: true,
  };

  private opcaoConsequencia: Opcoes = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_CONSEQUENCIA_ID,
    opcoes: [],
  };

  constructor(private _viewCtrl: ViewController, private _syncDataProvider: SyncDataProvider,
    private _navParams: NavParams, private _formBuilder: FormBuilder) {

    this._iniciar();
  }

  ngOnInit() {
    this.adicionarConsequenciaForm = this._formBuilder.group({
      nome: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          ValidationHelper.opcaoJaExiste.bind(this, this.opcaoConsequencia.opcoes),
        ]),
      ],
    });
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  onClickRegistrar() {
    if (this.adicionarConsequenciaForm.valid) {

      let appHelper: AppHelper = new AppHelper();
      let opcaoInativa: any = appHelper.isOpcaoInativa(this.opcao.nome, this.opcaoConsequencia.opcoes);
      if (opcaoInativa) {
        this.opcao = appHelper.ativarOpcao(opcaoInativa);
      } else {
        this.opcao = appHelper.criarNovaOpcao(this.opcao.nome);
        this.opcaoConsequencia.opcoes.push(this.opcao);
      }

      let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovaOpcaoNormal(this.opcao);

      this._syncDataProvider.addOrUpdate(this.opcaoConsequencia)
        .then(
        () => { },
        (err) => {
          console.log('ERRO AO CADASTRAR CONSEQUENCIA', err);
          // TRACK
        },
      );

      this._viewCtrl.dismiss({ 'opcao': this.opcaoConsequencia, 'modeloOpcao': modeloOpcao });
    }
  }

  private _iniciar() {
    let opcoes: Opcoes = this._navParams.get('opcoes');
    if (opcoes) {
      this.opcaoConsequencia = opcoes;
    }
  }

}
