import { ComponentsModule } from './../../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComidaPage } from './comida';

@NgModule({
  declarations: [
    ComidaPage,
  ],
  imports: [
    IonicPageModule.forChild(ComidaPage),
    ComponentsModule,
  ],
})
export class ComidaPageModule { }
