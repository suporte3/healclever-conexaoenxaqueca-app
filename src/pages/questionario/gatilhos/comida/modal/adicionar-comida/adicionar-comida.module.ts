import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarComidaPage } from './adicionar-comida';

@NgModule({
  declarations: [
    AdicionarComidaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarComidaPage),
  ],
})
export class AdicionarComidaPageModule { }
