import { ComponentsModule } from './../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GatilhosPage } from './gatilhos';

@NgModule({
  declarations: [
    GatilhosPage,
  ],
  imports: [
    IonicPageModule.forChild(GatilhosPage),
    ComponentsModule,
  ],
})
export class GatilhosPageModule { }
