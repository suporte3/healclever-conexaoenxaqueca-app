import { ConfiguracaoGrid } from './../../../core/models/pojo/configuracao-grid.pojo';
import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { TipoQuestao } from './../../../core/models/pojo/tipo-questao.pojo';
import { NavParamsMock } from './../../../core/mocks/nav-params.mock';
import { NavParams, ModalController } from 'ionic-angular';
import { TestUtils } from './../../../app/app.test';
import { Crise } from './../../../core/models/crise.model';
import { GatilhosPage } from './gatilhos';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Gatilhos Page:', () => {

    let fixture: ComponentFixture<GatilhosPage> = null;
    let instance: any = null;
    let _instance: any = GatilhosPage.prototype;
    let criseObjetoTest: Crise = {
        _id: 'idCrise',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.addProvider(ModalController);
        TestUtils.beforeEachCompiler([GatilhosPage]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();

            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o componente ao carregar', () => {
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#ngOnDestroy - Deve destruir o componente', () => {
        spyOn(instance, '_destroyEvents').and.stub();

        instance.ngOnDestroy();

        expect(instance._destroyEvents).toHaveBeenCalled();
    });

    it('#ionViewDidEnter - Deve iniciar grid', (done) => {
        spyOn(instance._questaoGrid, 'setGrid').and.stub();
        spyOn(instance._questaoGrid, 'iniciarGrid').and.returnValue(Promise.resolve());
        spyOn(instance._questaoGrid, 'montarGrid').and.stub();

        instance._docCrise = criseObjetoTest;
        instance._questaoGrid.componentesConfigurado = true;
        instance.ionViewDidEnter();
        expect(instance._questaoGrid.setGrid).toHaveBeenCalledWith(instance.grid);
        expect(instance._questaoGrid.iniciarGrid).toHaveBeenCalledWith(instance._docCrise);

        fixture.whenStable().then(
            () => {
                expect(instance._questaoGrid.montarGrid).toHaveBeenCalled();
                expect(instance.componentesConfigurado).toBe(true);
                done();
            },
        );
    });

    it('#onClickDissmissQuestions - Deve ir para o Painel', () => {
        spyOn(instance._navCtrl, 'setRoot').and.stub();
        instance.onClickDissmissQuestions();
        expect(instance._navCtrl.setRoot).toHaveBeenCalledWith('DiarioPage');
    });

    it('#onClickNext - Deve disparar eventos para proxima página', () => {
        spyOn(instance._questaoGrid, 'salvarOpcoes').and.stub();
        instance.onClickConfirm();
        expect(instance._questaoGrid.salvarOpcoes).toHaveBeenCalled();
    });

    it('#onClickConfirm - Deve disparar eventos para proxima página', () => {
        spyOn(instance._questaoGrid, 'salvarOpcoes').and.stub();
        instance.onClickConfirm();
        expect(instance._questaoGrid.salvarOpcoes).toHaveBeenCalled();
    });

    it('Deve configurar <grid />, <nav-bar /> e <footer-bar /> ao iniciar', () => {
        spyOn(instance, '_configurarGrid').and.stub();
        spyOn(instance, '_configurarFooterBar').and.stub();
        spyOn(instance, '_configurarNavBar').and.stub();
        spyOn(instance, '_listenEvents').and.stub();
        instance._iniciar();
        expect(instance._configurarGrid).toHaveBeenCalled();
        expect(instance._configurarFooterBar).toHaveBeenCalled();
        expect(instance._configurarNavBar).toHaveBeenCalled();
        expect(instance._listenEvents).toHaveBeenCalled();
    });

    it('Deve configurar o <grid /> corretamente', () => {
        spyOn(instance._questaoGrid, 'setNavCtrlReference').and.stub();
        spyOn(instance._questaoGrid, 'setEventsReference').and.stub();
        spyOn(instance._questaoGrid, 'setSyncDataServiceReference').and.stub();
        spyOn(instance._questaoGrid, 'setConfiguracaoFooterBarReference').and.stub();
        instance._configurarGrid();
        expect(instance._questaoGrid.setNavCtrlReference).toHaveBeenCalled();
        expect(instance._questaoGrid.setEventsReference).toHaveBeenCalled();
        expect(instance._questaoGrid.setSyncDataServiceReference).toHaveBeenCalled();
        expect(instance._questaoGrid.setConfiguracaoFooterBarReference).toHaveBeenCalled();
        expect(instance.configuracaoGrid.getPossuiModal()).toBe(true);
        expect(instance.configuracaoGrid.getIdentificadorGrid()).toEqual(TipoQuestao.GATILHO);
        // expect(instance.configuracaoGrid.getQuestao()).toEqual(instance);
        expect(instance.configuracaoGrid.getModalCtrl()).toEqual(instance._modalCtrl);
        expect(instance.configuracaoGrid.getModalAdicionarOpcaoReferencia()).toEqual('AdicionarGatilhoPage');
        expect(instance.configuracaoGrid.getDocumentoOptionId()).toEqual(SyncDataProvider.DOCUMENT_OPCOES_GATILHO_ID);
        expect(instance.configuracaoGrid.getNomeDoObjetoOpcao()).toEqual('gatilhos');
        expect(instance.configuracaoGrid.getPossuiOpcoesGatilhos()).toBe(false);

        criseObjetoTest._id = null;
        instance._docCrise = criseObjetoTest;
        instance.configuracaoFooterBar = null;
        instance.configuracaoGrid = new ConfiguracaoGrid();
        instance._navParams = new NavParams({ 'com.healclever.myMigraine.route': 'sumario' });
        instance._configurarGrid();
        expect(instance.configuracaoGrid.getRota()).toBe('sumario');
    });

    it('Deve configurar o <nav-bar /> corretamente', () => {
        instance._configurarNavBar();
        expect(instance.configuracaoNavBar.getIdentificadorGrid()).toEqual(TipoQuestao.GATILHO);
        expect(instance.configuracaoNavBar.getTextoModoRemover()).toEqual('Toque no gatilho que deseja remover:');
        expect(instance.configuracaoNavBar.getTextoInicial()).toEqual('O que você acredita que contribui para a crise?');
    });

    it('Deve configurar o <footer-bar /> corretamente', () => {
        instance._configurarFooterBar();
        expect(instance.configuracaoFooterBar.isModoAtualizar()).toBe(false);
        expect(instance.configuracaoFooterBar.isSaving()).toBe(false);
        expect(instance.configuracaoFooterBar.getIdentificadorGrid()).toBe(TipoQuestao.GATILHO);
    });

    it('#_listenEvents - Deve escutar eventos', () => {
        let spySubscribe: jasmine.Spy = spyOn(instance._events, 'subscribe').and.stub();

        instance._listenEvents();

        expect(spySubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.GATILHO);
        expect(spySubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.GATILHO);
        expect(spySubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.GATILHO);
    });

    it('#_destroyEvents - Deve destruir eventos que estão sendo escutados', () => {
        let spyUnsubscribe: jasmine.Spy = spyOn(instance._events, 'unsubscribe').and.stub();
        spyOn(instance._questaoGrid, 'destroyListenGridEvents').and.stub();

        instance._destroyEvents();

        expect(instance._questaoGrid.destroyListenGridEvents).toHaveBeenCalled();
        expect(spyUnsubscribe.calls.argsFor(0)[0]).toEqual('footer:onClickDissmissQuestions' + TipoQuestao.GATILHO);
        expect(spyUnsubscribe.calls.argsFor(1)[0]).toEqual('footer:onClickNext' + TipoQuestao.GATILHO);
        expect(spyUnsubscribe.calls.argsFor(2)[0]).toEqual('footer:onClickConfirm' + TipoQuestao.GATILHO);
    });

});
