import { ModeloOpcao } from './../../../../../../core/models/pojo/opcao.pojo';
import { AppHelper } from './../../../../../../core/helpers/app.helper';
import { NavParamsMock } from './../../../../../../core/mocks/nav-params.mock';
import { ViewControllerMock } from './../../../../../../core/mocks/app.mock';
import { ViewController, NavParams } from 'ionic-angular';
import { FormBuilder } from '@angular/forms';
import { TestUtils } from './../../../../../../app/app.test';
import { AdicionarBebidaPage } from './adicionar-bebida';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Adicionar Bebida:', () => {

    let fixture: ComponentFixture<AdicionarBebidaPage> = null;
    let instance: any = null;
    let _instance: any = AdicionarBebidaPage.prototype;

    beforeEach(async(() => {

        TestUtils.addProvider(FormBuilder);
        TestUtils.addProvider(
            { provide: ViewController, useClass: ViewControllerMock },
        );
        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.beforeEachCompiler([AdicionarBebidaPage]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            });

        spyOn(_instance, '_iniciar').and.callThrough();
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('Deve iniciar o modal ao carregar o componente', () => {
        expect(instance._iniciar).toBeTruthy();
    });

    it('Deve iniciar o modal', () => {
        instance._iniciar();
        expect(instance.opcaoBebida).toBeDefined();
    });

    it('Deve fechar o modal', () => {
        spyOn(instance._viewCtrl, 'dismiss');
        instance.onClickCancelar();
        expect(instance._viewCtrl.dismiss).toHaveBeenCalled();
    });

    it('#onClickRegistrar - Deve registrar', () => {
        let spyAppUtilsAtivarOpcao: jasmine.Spy = spyOn(AppHelper.prototype, 'ativarOpcao');
        let spyAppUtilsCriarNovaOpcao: jasmine.Spy = spyOn(AppHelper.prototype, 'criarNovaOpcao');
        let spyAppUtilsIsOpcaoNativa: jasmine.Spy = spyOn(AppHelper.prototype, 'isOpcaoInativa');
        let spyModeloOpcaoCriarModeloDeNovaOpcaoNormal: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'criarModeloDeNovaOpcaoNormal');
        spyOn(instance._viewCtrl, 'dismiss').and.stub();
        spyOn(instance._syncDataProvider, 'addOrUpdate').and.callThrough();
        instance.opcaoBebida = {};
        instance.opcaoBebida.opcoes = [];

        spyModeloOpcaoCriarModeloDeNovaOpcaoNormal.and.callThrough();
        spyAppUtilsAtivarOpcao.and.callThrough();
        instance.adicionarBebidaGatilhoForm.get('nome').clearValidators();
        instance.adicionarBebidaGatilhoForm.get('nome').setValue('opção bebida teste');
        instance.opcao.ativo = false;
        spyAppUtilsIsOpcaoNativa.and.returnValue(instance.opcao);
        instance.onClickRegistrar();
        let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovaOpcaoNormal(instance.opcao);
        expect(spyAppUtilsAtivarOpcao).toHaveBeenCalled();
        expect(spyAppUtilsCriarNovaOpcao).not.toHaveBeenCalled();
        expect(instance._viewCtrl.dismiss).toHaveBeenCalledWith({ 'opcao': instance.opcaoBebida, 'modeloOpcao': modeloOpcao });
        expect(instance._syncDataProvider.addOrUpdate).toHaveBeenCalled();
        expect(spyModeloOpcaoCriarModeloDeNovaOpcaoNormal).toHaveBeenCalled();

        spyModeloOpcaoCriarModeloDeNovaOpcaoNormal.calls.reset();
        spyAppUtilsAtivarOpcao.calls.reset();
        spyAppUtilsCriarNovaOpcao.calls.reset();
        spyAppUtilsCriarNovaOpcao.and.callThrough();
        spyOn(instance.opcaoBebida.opcoes, 'push').and.stub();
        spyAppUtilsIsOpcaoNativa.and.returnValue(false);
        instance.opcao.nome = 'opção bebida teste';
        instance.onClickRegistrar();
        expect(spyAppUtilsAtivarOpcao).not.toHaveBeenCalled();
        expect(spyAppUtilsCriarNovaOpcao).toHaveBeenCalled();
        expect(instance.opcaoBebida.opcoes.push).toHaveBeenCalled();
    });

});
