import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarBebidaPage } from './adicionar-bebida';

@NgModule({
  declarations: [
    AdicionarBebidaPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarBebidaPage),
  ],
})
export class AdicionarBebidaPageModule { }
