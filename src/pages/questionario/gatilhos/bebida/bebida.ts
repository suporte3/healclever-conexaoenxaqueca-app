import { AppParam } from './../../../../core/models/pojo/app-param.pojo';
import { TipoQuestao } from './../../../../core/models/pojo/tipo-questao.pojo';
import { SyncDataProvider } from './../../../../providers/sync-data/sync-data';
import { Crise } from './../../../../core/models/crise.model';
import { QuestaoGridService } from './../../../../services/questao-grid/questao-grid.service';
import { GridQuestionarioComponent } from './../../../../components/grid-questionario/grid-questionario';
import { ConfiguracaoFooterBar } from './../../../../core/models/pojo/configuracao-footer-bar.pojo';
import { ConfiguracaoNavBar } from './../../../../core/models/pojo/configuracao-nav-bar.pojo';
import { ConfiguracaoGrid } from './../../../../core/models/pojo/configuracao-grid.pojo';
import { Component, OnDestroy, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-bebida',
  templateUrl: 'bebida.html',
})
export class BebidaPage implements OnDestroy {

  public componentesConfigurado: boolean;
  public configuracaoGrid: ConfiguracaoGrid = new ConfiguracaoGrid();
  public configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
  public configuracaoFooterBar: ConfiguracaoFooterBar = new ConfiguracaoFooterBar();
  @ViewChild('grid') grid: GridQuestionarioComponent;

  private _questaoGrid: QuestaoGridService = new QuestaoGridService();
  private _docCrise: Crise;

  constructor(private _navCtrl: NavController, private _modalCtrl: ModalController, private _navParams: NavParams,
    private _events: Events, private _syncDataProvider: SyncDataProvider) {

    this._iniciar();
  }

  ngOnDestroy() {
    this._destroyEvents();
  }

  ionViewDidEnter() {
    this._questaoGrid.setGrid(this.grid);

    this._questaoGrid.iniciarGrid(this._docCrise).then(
      () => {
        this._questaoGrid.montarGrid();
        this.componentesConfigurado = this._questaoGrid.componentesConfigurado;
      },
      () => {
        // Não foi possível iniciar, devido a não ser possível obter os dados relacionado a este registro
        // Pensar em um tratamento
        console.log('Não foi possível iniciar, devido a não ser possível obter os dados relacionado a este registro');
      },
    );
  }

  onClickConfirm() {
    this._questaoGrid.salvarOpcoes();
  }

  onClickDissmissQuestions() {
    this._navCtrl.setRoot('DiarioPage');
  }

  onClickHelp() {
    console.log('click help');
  }

  private _iniciar() {
    this.componentesConfigurado = this._questaoGrid.componentesConfigurado;
    this._docCrise = (this._navParams.get(AppParam.CRISE_ID) ? this._navParams.get(AppParam.CRISE_ID) : null);

    this._configurarNavBar();
    this._configurarFooterBar();
    this._configurarGrid();

    this._listenEvents();
  }

  private _configurarNavBar() {
    this.configuracaoNavBar.setTextoInicial('Selecione a bebida:');
    this.configuracaoNavBar.setTextoModoRemover('Toque na bebida que deseja remover:');
    this.configuracaoNavBar.setIdentificadorGrid(TipoQuestao.GATILHO_BEBIDA);
  }

  private _configurarFooterBar() {
    this.configuracaoFooterBar.setModoAtualizar(true);
    this.configuracaoFooterBar.setSaving(false);
    this.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.GATILHO_BEBIDA);
  }

  private _configurarGrid() {
    this._questaoGrid.setNavCtrlReference(this._navCtrl);
    this._questaoGrid.setEventsReference(this._events);
    this._questaoGrid.setSyncDataServiceReference(this._syncDataProvider);
    this._questaoGrid.setConfiguracaoFooterBarReference(this.configuracaoFooterBar);

    this.configuracaoGrid.setIdentificadorGrid(TipoQuestao.GATILHO_BEBIDA);
    this.configuracaoGrid.setModalAdicionarOpcaoReferencia('AdicionarBebidaPage');
    this.configuracaoGrid.setPossuiModal(true);
    // this.configuracaoGrid.setQuestao(this);
    this.configuracaoGrid.setModalCtrl(this._modalCtrl);
    this.configuracaoGrid.setDocumentoOptionId(SyncDataProvider.DOCUMENT_OPCOES_GATILHO_BEBIDA_ID);
    this.configuracaoGrid.setNomeDoObjetoOpcao('bebidas');
    this.configuracaoGrid.setPossuiOpcoesGatilhos(true);

    if (this._navParams.get(AppParam.ROUTE) === 'sumario') {
      this.configuracaoGrid.setRota('sumario');
    }
  }

  private _listenEvents() {
    this._events.subscribe('footer:onClickDissmissQuestions' + TipoQuestao.GATILHO_BEBIDA,
      () => {
        this.onClickDissmissQuestions();
      },
    );

    this._events.subscribe('footer:onClickConfirm' + TipoQuestao.GATILHO_BEBIDA,
      () => {
        this.onClickConfirm();
      },
    );
  }

  private _destroyEvents() {
    this._questaoGrid.destroyListenGridEvents();
    this._events.unsubscribe('footer:onClickDissmissQuestions' + TipoQuestao.GATILHO_BEBIDA);
    this._events.unsubscribe('footer:onClickConfirm' + TipoQuestao.GATILHO_BEBIDA);
  }

}
