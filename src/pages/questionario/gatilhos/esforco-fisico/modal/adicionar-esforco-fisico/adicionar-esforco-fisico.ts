import { ModeloOpcao } from './../../../../../../core/models/pojo/opcao.pojo';
import { AppHelper } from './../../../../../../core/helpers/app.helper';
import { ValidationHelper } from './../../../../../../core/helpers/validation.helper';
import { TipoOpcao } from './../../../../../../core/models/tipo-opcao.enum';
import { Opcoes } from './../../../../../../core/models/opcoes.model';
import { Opcao } from './../../../../../../core/models/opcao.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SyncDataProvider } from './../../../../../../providers/sync-data/sync-data';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-esforco-fisico',
  templateUrl: 'adicionar-esforco-fisico.html',
})
export class AdicionarEsforcoFisicoPage implements OnInit {

  @ViewChild('adicionarEsforcoFisicoGatilhoForm') adicionarEsforcoFisicoGatilhoForm: FormGroup;

  public opcao: Opcao = {
    _id: null,
    tipo: TipoOpcao.DINAMICO,
    nome: null,
    dataCriacao: null,
    ativo: true,
  };
  private opcaoEsforcoFisico: Opcoes = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_GATILHO_ID,
    opcoes: [],
  };

  constructor(private _viewCtrl: ViewController, private _syncDataProvider: SyncDataProvider,
    private _navParams: NavParams, private _formBuilder: FormBuilder) {

    this._iniciar();
  }

  ngOnInit() {
    this.adicionarEsforcoFisicoGatilhoForm = this._formBuilder.group({
      nome: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          ValidationHelper.opcaoJaExiste.bind(this, this.opcaoEsforcoFisico.opcoes),
        ]),
      ],
    });
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  onClickRegistrar() {

    if (this.adicionarEsforcoFisicoGatilhoForm.valid) {
      let appHelper: AppHelper = new AppHelper();
      let opcaoInativa: any = appHelper.isOpcaoInativa(this.opcao.nome, this.opcaoEsforcoFisico.opcoes);
      if (opcaoInativa) {
        this.opcao = appHelper.ativarOpcao(opcaoInativa);
      } else {
        this.opcao = appHelper.criarNovaOpcao(this.opcao.nome);
        this.opcaoEsforcoFisico.opcoes.push(this.opcao);
      }

      let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovaOpcaoNormal(this.opcao);

      this._syncDataProvider.addOrUpdate(this.opcaoEsforcoFisico)
        .then(
        () => { },
        (err) => {
          console.log('ERRO AO CADASTRAR ESFORCO FISICO', err);
          // TRACK
        },
      );

      this._viewCtrl.dismiss({ 'opcao': this.opcaoEsforcoFisico, 'modeloOpcao': modeloOpcao });
    }
  }

  private _iniciar() {
    let opcoes: Opcoes = this._navParams.get('opcoes');
    if (opcoes) {
      this.opcaoEsforcoFisico = opcoes;
    }
  }

}
