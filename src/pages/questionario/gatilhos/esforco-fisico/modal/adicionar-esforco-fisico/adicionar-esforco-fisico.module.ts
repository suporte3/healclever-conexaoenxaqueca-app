import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarEsforcoFisicoPage } from './adicionar-esforco-fisico';

@NgModule({
  declarations: [
    AdicionarEsforcoFisicoPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarEsforcoFisicoPage),
  ],
})
export class AdicionarEsforcoFisicoPageModule { }
