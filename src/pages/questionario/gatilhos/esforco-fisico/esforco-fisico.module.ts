import { ComponentsModule } from './../../../../components/components.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EsforcoFisicoPage } from './esforco-fisico';

@NgModule({
  declarations: [
    EsforcoFisicoPage,
  ],
  imports: [
    IonicPageModule.forChild(EsforcoFisicoPage),
    ComponentsModule,
  ],
})
export class EsforcoFisicoPageModule { }
