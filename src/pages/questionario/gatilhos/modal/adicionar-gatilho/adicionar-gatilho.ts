import { ValidationHelper } from './../../../../../core/helpers/validation.helper';
import { AppHelper } from './../../../../../core/helpers/app.helper';
import { ModeloOpcao } from './../../../../../core/models/pojo/opcao.pojo';
import { TipoOpcao } from './../../../../../core/models/tipo-opcao.enum';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Opcoes } from './../../../../../core/models/opcoes.model';
import { SyncDataProvider } from './../../../../../providers/sync-data/sync-data';
import { Opcao } from './../../../../../core/models/opcao.model';
import { Component, OnInit, ViewChild } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'ib-page-adicionar-gatilho',
  templateUrl: 'adicionar-gatilho.html',
})
export class AdicionarGatilhoPage implements OnInit {

  @ViewChild('adicionarGatilhoForm') adicionarGatilhoForm: FormGroup;

  public opcao: Opcao = {
    _id: null,
    tipo: TipoOpcao.DINAMICO,
    nome: null,
    dataCriacao: null,
    ativo: true,
  };
  private opcaoGatilho: Opcoes = {
    _id: SyncDataProvider.DOCUMENT_OPCOES_GATILHO_ID,
    opcoes: [],
  };

  constructor(private _viewCtrl: ViewController, private _syncDataProvider: SyncDataProvider,
    private _navParams: NavParams, private _formBuilder: FormBuilder) {

    this._iniciar();
  }

  ngOnInit() {
    this.adicionarGatilhoForm = this._formBuilder.group({
      nome: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(3),
          ValidationHelper.opcaoJaExiste.bind(this, this.opcaoGatilho.opcoes),
        ]),
      ],
    });
  }

  onClickCancelar() {
    this._viewCtrl.dismiss();
  }

  onClickRegistrar() {

    if (this.adicionarGatilhoForm.valid) {
      let appHelper: AppHelper = new AppHelper();
      let opcaoInativa: any = appHelper.isOpcaoInativa(this.opcao.nome, this.opcaoGatilho.opcoes);
      if (opcaoInativa) {
        this.opcao = appHelper.ativarOpcao(opcaoInativa);
      } else {
        this.opcao = appHelper.criarNovaOpcao(this.opcao.nome);
        this.opcaoGatilho.opcoes.push(this.opcao);
      }

      let modeloOpcao: ModeloOpcao = new ModeloOpcao().criarModeloDeNovaOpcaoNormal(this.opcao);

      this._syncDataProvider.addOrUpdate(this.opcaoGatilho)
        .then(
        () => { },
        (err) => {
          console.log('ERRO AO CADASTRAR GATILHO', err);
          // TRACK
        },
      );

      this._viewCtrl.dismiss({ 'opcao': this.opcaoGatilho, 'modeloOpcao': modeloOpcao });
    }
  }

  private _iniciar() {
    let opcoes: Opcoes = this._navParams.get('opcoes');
    if (opcoes) {
      this.opcaoGatilho = opcoes;
    }
  }

}
