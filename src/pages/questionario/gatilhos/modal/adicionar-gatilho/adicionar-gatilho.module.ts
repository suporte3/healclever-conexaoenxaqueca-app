import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdicionarGatilhoPage } from './adicionar-gatilho';

@NgModule({
  declarations: [
    AdicionarGatilhoPage,
  ],
  imports: [
    IonicPageModule.forChild(AdicionarGatilhoPage),
  ],
})
export class AdicionarGatilhoPageModule { }
