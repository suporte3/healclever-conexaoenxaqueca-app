import { AuthProvider } from './../../providers/auth/auth';
import { ProtectedPageService } from './../../services/protected-page/protected-page.service';
import { ApiProvider } from './../../providers/api/api';
import { GoogleFitProvider } from './../../providers/google-fit/google-fit';
import { Alimento } from './../../core/models/alimento.model';
import { Sono } from './../../core/models/sono.model';
import { AppHelper } from './../../core/helpers/app.helper';
import { Crise } from './../../core/models/crise.model';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Component, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import { IonicPage, NavController, Platform, Slides, Content, FabContainer, DateTime } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import 'moment/min/locales';

@IonicPage()
@Component({
  selector: 'ib-page-diario',
  templateUrl: 'diario.html',
  providers: [GoogleFitProvider],
})
export class DiarioPage extends ProtectedPageService implements AfterViewInit, OnDestroy {

  public googleFitIsEnabled: boolean = false;
  public loadingSlides: boolean = false;
  public slides: Array<any> = [];
  public dateTimeValor: string;
  public nomeMes: Array<string>;
  public minDateTime: any;
  public maxDateTime: any;
  public dataAtual: any = {
    valor: null,
    descricao: null,
  };

  public criseEmProgresso: Crise = null;
  public shouldShowFabWrapper: boolean = false;
  @ViewChild(Slides) slideDiario: Slides;
  @ViewChild('content') content: Content;
  @ViewChild('fab') fab: FabContainer;
  @ViewChild('datePicker') datePicker: DateTime;

  private _periodoMax: any;
  private _periodoMin: any;
  private _RANGE_MINIMO_SLIDE: number = 0;
  private _NR_ADD_SLIDES: number = 7;
  private _isDateSelected: boolean = false;

  constructor(public navCtrl: NavController, public authService: AuthProvider, private _api: ApiProvider,
    private _syncData: SyncDataProvider, private _platform: Platform, private storage: Storage,
    private _googleFitService: GoogleFitProvider) {

    super(navCtrl, authService);
    this._iniciar();
  }

  public onClickLogout() {
    this.authService.logout().then(
      () => {
        this.navCtrl.setRoot('TutorialPage', null, { animate: true });
      },
    );
  }

  ngOnDestroy() {
    if (this.slideDiario) {
      this.slideDiario.ngOnDestroy();
    }
  }

  ngAfterViewInit() {
    this.content.resize();
  }

  ionViewDidLoad() {
    // Carregar recursos globais
  }

  ionViewWillEnter() {
    // Executar algoritmo antes da página estar ativa
  }

  ionViewDidEnter() {
    // página ativa
    this._montarSlides().then(
      () => {
        this.slideDiario.update();
        this.slideDiario.slideTo(1);
      },
    );

    this._verificarCrisesEmAberto().then(
      (result: any) => {
        if (result.docs.length > 0) {
          this.criseEmProgresso = result.docs[0];
        } else {
          this.criseEmProgresso = null;
        }
      },
    );
  }

  ionViewDidLeave() {
    // A página entra em estado pause, mantendo-se em cache
    // Manter recursos
  }

  ionViewWillUnload() {
    // Liberar recursos, quando a página for excluída
  }

  public onClickAtualizarDadosFitness() {
    this._platform.ready().then(
      () => {
        let indiceAtual = this.slideDiario.getActiveIndex();
        this._googleFitService.atualizarDadosFitness(this.slides[indiceAtual].date);
      },
    );
  }

  public onChangeIntegrarGoogleFit() {

    this._platform.ready().then(
      () => {
        this._googleFitService.integrar();
      },
    );
  }

  public onClickAlimentacao() {
    let slideMoment: any = this._getAtualSlideMoment();
    this.navCtrl.push('RegistroAlimentoPage', { diarioSaudeMoment: slideMoment });
  }

  public onClickSono() {
    let slideMoment: any = this._getAtualSlideMoment();
    this.navCtrl.push('RegistroSonoPage', { diarioSaudeMoment: slideMoment });
  }

  public onClickRegistrarCrise() {
    if (this.criseEmProgresso) {
      this.navCtrl.push('SumarioPage', { crise: this.criseEmProgresso });
    } else {
      this.navCtrl.push('DuracaoDorPage');
    }
  }

  public onClickFab(acao: string) {

    if (acao !== 'openClose') {
      this.fab.close();

      if (acao === 'alimentacao') {
        this.onClickAlimentacao();
      } else if (acao === 'crise') {
        this.onClickRegistrarCrise();
      } else if (acao === 'sono') {
        this.onClickSono();
      }
    }

    this.shouldShowFabWrapper = !this.shouldShowFabWrapper;
  }

  public onClickRelatorioMelhoria() {
    // this._navCtrl.push(RelatorioMelhoriaPage);
  }

  public onClickVerAlimentos() { }

  public onClickFalarComElo() {
    // this._navCtrl.push(ChatPage);
  }

  public onClickObterClima() {
    let payload: any = {
      latitude: -26.2685463,
      longitude: -48.8898509,
      language: 'pt-BR',
      timePeriod: '2017-09-09T20:59:43.678Z',
    };

    this._api.get('/clima/previsao', payload)
      .map(response => response.json())
      .subscribe(
      (response: any) => {
        console.log('Resposta', response);
      },
      (error: any) => {
        console.log('Error', error);
      },
    );
  }

  public onClickSincronizar() {
    /*this._syncData.getDocumentById(SyncDataProvider.DOCUMENT_PERFIL_ID)
      .then(
      (result: any) => {
        if (result.error && result.status === 404) {

        } else if (result.error === undefined) {
          result.sincronizado = true;
          this._authenticationProvider.setHttpReference(this._http);
          this._authenticationProvider.refreshTokenUsuario()
            .subscribe(
            (responseToken: any) => {
              let sessaoUsuario = JSON.parse(responseToken['_body']);
              this._syncData.addOrUpdate(result)
                .then(
                () => {
                  let appHelper: AppHelper = new AppHelper();
                  appHelper.atualizarPerfilUsuario(null,
                    {
                      sincronizado: true,
                      sessao: { token: sessaoUsuario.token },
                    },
                  )
                    .then(
                    (usuario: Usuario) => {
                      this._syncData.setDatabaseContinuous(true);
                      this._syncData.sincronizarDatabase(usuario.sessao)
                        .then(
                        () => { console.log('SUCESSO AO SINCRONIZAR'); },
                        () => {
                          // RETIRAR SINCRONIZAÇÃO
                          console.log('ERRO NA SINCRONIZAÇÃO');
                        },
                      );
                    },
                    () => {
                      // VOLTAR DOCUMENTO e ARQUIVO PARA sincroniza = false;
                      console.log('ERRO AO ATUALIZAR PERFIL DE USUARIO');
                    },
                  );
                },
                (err) => {
                  console.log('ERRO addOrUpdate()', err);
                },
              );
            },
            (error) => {
              // TRACK
              console.log('ERRO refreshTokenUsuario()', error);
            },
          );
        } else {
          // erro desconhecido
        }
      },
      () => { },
      );*/
  }

  onClickMinhasCrises() {
    this.navCtrl.push('MinhasCrisesPage', { dataAtual: this.dataAtual.valor });
  }

  public onSlideChanged() {

    let indiceAtual = this.slideDiario.getActiveIndex();
    if (this.slides[indiceAtual]) {
      let momentDate: any = this.slides[indiceAtual].date;

      if (!this._isDateSelected) {
        this.dateTimeValor = momentDate.format(AppHelper.FORMATO_DATA_ISO_8601);
      }
    }
  }

  public onClickNextSlide() {
    this.slideDiario.slideNext();
  }

  public onClickPrevSlide() {
    this.slideDiario.slidePrev();
  }

  public onClickChangeDateSlide() {
    this._isDateSelected = true;
    this.datePicker.open();
  }

  public onCancelDateTime() {
    this._isDateSelected = false;
  }

  public onChangeDateTime() {

    let dateTimeMoment: any = moment().date(this.datePicker.value.day).month(this.datePicker.value.month - 1).year(this.datePicker.value.year).hours(0).minutes(0).seconds(0).milliseconds(0);
    this._atualizarDataAtual(dateTimeMoment);

    if (this._isDateSelected) {

      this._addMoreSlidesByDateTime();
      this._isDateSelected = false;
    } else {
      this._addMoreSlidesBySlided();
    }
  }

  public trackSlideById(index: number, slide: any) {
    index = index;
    return slide.id;
  }

  private _getAtualSlideMoment(): any {
    let slideMoment: any = null;
    let indiceAtual = this.slideDiario.getActiveIndex();
    if (this.slides[indiceAtual]) {
      slideMoment = this.slides[indiceAtual].date;
    }

    return slideMoment;
  }

  private _isEnabledGoogleFit() {

    if (this._platform.is('android')) {
      this.storage.get('gFit').then(
        (googleFitEnabled: any) => {
          if (googleFitEnabled !== undefined) {
            this.googleFitIsEnabled = googleFitEnabled;
          } else {
            this.googleFitIsEnabled = false;
          }
        },
      );
    } else {
      this.googleFitIsEnabled = false;
    }
  }

  private _addMoreSlidesByDateTime() {
    if (this._canMoveSlideLeft() || this._canMoveSlideRight() || this._canMoveSlideByDatetime()) {
      let scope: DiarioPage = this;

      this.loadingSlides = true;
      setTimeout(() => {
        scope._atualizarPeriodoConsultaSlides();
        scope._montarSlides().then(
          () => {
            scope._moveToCurrentSlide(500);
            scope.loadingSlides = false;
          },
        );
      }, 1000);
    } else {

      this._moveToCurrentSlide(500);
      this.loadingSlides = false;
    }
  }

  private _addMoreSlidesBySlided() {
    if (this._canMoveSlideLeft() || this._canMoveSlideRight()) {
      let scope: DiarioPage = this;

      this.loadingSlides = true;
      setTimeout(
        () => {

          scope._atualizarPeriodoConsultaSlides();
          scope._montarSlides().then(
            () => {

              scope._moveToCurrentSlide(0);
              scope.loadingSlides = false;
            },
          );
        }, 1000);
    }
  }

  private _canMoveSlideByDatetime() {
    let canMove: boolean = false;

    let diffPeriodoMin: number = this._periodoMin.diff(this.dataAtual.valor, 'days');
    let diffPeriodoMax: number = this._periodoMax.diff(this.dataAtual.valor, 'days');

    if (diffPeriodoMin > 0) {
      canMove = true;
    }

    if (diffPeriodoMax < 0) {
      canMove = true;
    }

    return canMove;
  }

  private _canMoveSlideRight() {
    let indiceAtual = this.slideDiario.getActiveIndex();
    let dateFirstSlide: string = this.slides[0].date.format('DD-MM-YYYY');
    let dateAtual: string = this._getLoadDateMax().format('DD-MM-YYYY');

    return (indiceAtual === this._RANGE_MINIMO_SLIDE && dateFirstSlide !== dateAtual);
  }

  private _canMoveSlideLeft() {
    let indiceAtual = this.slideDiario.getActiveIndex();
    let indiceAncora: number = this.slides.length - this._RANGE_MINIMO_SLIDE - 1;
    let dateAtual: string = this._getLoadDateMin().format('DD-MM-YYYY');
    let dateLastSlide: string = this.slides[this.slides.length - 1].date.format('DD-MM-YYYY');

    return (indiceAtual === indiceAncora && dateLastSlide !== dateAtual);
  }

  private _moveToCurrentSlide(delayTime: number) {
    let indice: number = -1;
    this.slides.every(
      (itemSlide: any, i: number) => {
        let dateSlide: string = itemSlide.date.format('DD-MM-YYYY');
        let dateAtual: string = this.dataAtual.valor.format('DD-MM-YYYY');

        if (dateSlide === dateAtual) {
          indice = i;
          return false;
        } else {
          return true;
        }
      },
    );

    if (indice >= 0) {
      Observable.of(null).delay(delayTime).subscribe(
        () => {
          this.slideDiario.slideTo(indice, delayTime, false);
        },
      );
    }
  }

  private _atualizarPeriodoConsultaSlides() {
    this._periodoMin = moment(this.dataAtual.valor).subtract(this._NR_ADD_SLIDES, 'days');
    this._periodoMax = moment(this.dataAtual.valor).add(this._NR_ADD_SLIDES, 'days');

    let dataMin: any = this._getLoadDateMin();
    let dataMax: any = this._getLoadDateMax();

    let diffPeriodoMin: number = this._periodoMin.diff(dataMin, 'days');
    let diffPeriodoMax: number = this._periodoMax.diff(dataMax, 'days');

    if (diffPeriodoMin < 0) {
      this._periodoMin = moment(dataMin);
    }

    if (diffPeriodoMax > 0) {
      this._periodoMax = moment(dataMax);
    }
  }

  private _iniciar() {
    moment.locale('pt-BR');
    this.nomeMes = moment.monthsShort();
    this._periodoMax = moment().local().add(1, 'days');
    this._periodoMin = moment(this._periodoMax).subtract(2, 'weeks');

    this._atualizarDataAtual(moment(this._periodoMax));
    this._isEnabledGoogleFit();

    this.dateTimeValor = this.dataAtual.valor.format(AppHelper.FORMATO_DATA_ISO_8601);
    this._definirPeriodoMinConsulta();
    this._definirPeriodoMaxConsulta();

    // this._montarSlides();
  }

  private _atualizarDataAtual(dataAtual: any) {
    this.dataAtual.valor = dataAtual;
    this.dataAtual.descricao = this.dataAtual.valor.calendar(null, {
      lastDay: '[Ontem]',
      sameDay: '[Hoje]',
      nextDay: '[Amanhã]',
      lastWeek: '[última] dddd',
      nextWeek: 'dddd',
      sameElse: 'L',
    });
  }

  private _definirPeriodoMinConsulta() {
    let dataMin: any = this._getLoadDateMin();
    this.minDateTime = moment(dataMin).format(AppHelper.FORMATO_DATA_ISO_8601);
  }

  private _definirPeriodoMaxConsulta() {
    let dataMax: any = this._getLoadDateMax();
    this.maxDateTime = moment(dataMax).format(AppHelper.FORMATO_DATA_ISO_8601);
  }

  private _getLoadDateMin() {
    return moment().local().date(1).month(0).year(2000).startOf('day');
  }

  private _getLoadDateMax() {
    return moment().local().add(1, 'days').endOf('day');
  }

  private _montarSlides(): Promise<any> {
    let scope: DiarioPage = this;

    return new Promise(
      function (resolve) {
        let periodoDeConsulta: number = scope._periodoMax.diff(scope._periodoMin, 'days');
        scope._buildSlides(periodoDeConsulta);

        resolve();
      },
    );
  }

  private _buildSlides(periodoDeConsulta: number) {
    this.slides = [];

    let dataAtual: any = moment(this._periodoMax);
    for (let indice = 0; indice <= periodoDeConsulta; indice++) {

      let diaAtual: number = parseInt(dataAtual.format('D'), 10);
      let mesAtual: number = parseInt(dataAtual.format('M'), 10) - 1;
      let anoAtual: number = parseInt(dataAtual.format('YYYY'), 10);

      let momentMin: any = moment().year(anoAtual).month(mesAtual).date(diaAtual).startOf('day');
      let momentMax: any = moment().year(anoAtual).month(mesAtual).date(diaAtual).endOf('day');

      let idDiarioSaude: string = SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID + diaAtual + '-' + mesAtual + '-' + anoAtual;

      let slide: any = {
        id: idDiarioSaude,
        date: momentMin,
        crise: {},
        sono: {},
        alimentos: [],
        nrPassos: 0,
        googleFitSync: false,
      };

      this._consultarDiarioSaude(idDiarioSaude).then(
        (result: any) => {
          this._atualizarSonoView(result.sono, slide);
          this._atualizarAlimentosView(result.alimentos, slide);
          slide.nrPassos = result.passos;
          slide.googleFitSync = result.googleFitSync;
        },
        () => {
          slide.sono = null;
          slide.alimentos = [];
          slide.nrPassos = 0;
        },
      );

      this._consultarCrises(momentMin.format(AppHelper.FORMATO_DATA_ISO_8601), momentMax.format(AppHelper.FORMATO_DATA_ISO_8601)).then(
        (result: any) => {
          slide.crise.intensidadeMedia = this._getDocMediaIntensidade(result.docs);
          slide.crise.nrCrise = result.docs.length;
        },
        () => {
          slide.crise = null;
        },
      );

      this.slides.push(slide);
      slide = null;

      /*if (this.slideDiario) {
        this.slideDiario.update();
      }*/

      dataAtual = moment(dataAtual).subtract(1, 'days');
    }
  }

  private _atualizarSonoView(sono: Sono, slide: any) {
    if (sono) {
      let sonoIni: any = moment(sono.inicio).utc();
      let sonoFim: any = moment(sono.fim).utc();
      let duracaoSono: any = sonoIni.diff(sonoFim);
      let duracaoSonoHumanizada: string = moment.duration(duracaoSono).humanize();

      slide.sono.duracaoSono = duracaoSonoHumanizada;
      slide.sono.qualidadeSubjetiva = sono.qualidadeSubjetiva;
    }
  }

  private _atualizarAlimentosView(alimentos: Alimento[], slide: any) {
    let limit: number = 3;

    for (let i = alimentos.length - 1; i >= 0; i -= 1) {
      let alimento: any = alimentos[i];
      if (limit > 0) {
        slide.alimentos.push({
          nome: alimento.nome,
        });
        limit--;
      }
    }
  }

  private _consultarDiarioSaude(idDiarioSaude: string): Promise<any> {
    let scope: DiarioPage = this;

    return new Promise(function (resolve, reject) {
      scope._syncData.getDocumentById(idDiarioSaude).then(
        (result: any) => {
          if (result.error && result.status === 404) {
            reject();
          } else if (result.error === undefined) {
            resolve(result);
          } else {
            console.log('ERRO DESCONHECIMENTO', result);
            // Tratar este erro de maneira que o usuário entenda e compreenda
            // Para isso, tratar junta com a Rosie no "chatbox"
            reject();
          }
        },
      );
    });
  }

  private _verificarCrisesEmAberto() {
    let scope: DiarioPage = this;

    return new Promise(function (resolve, reject) {
      scope._syncData.find(
        {
          selector: {
            type: 'crise',
            dataFinal: {
              $eq: null,
            },
          },
        },
      ).then(
        (result: any) => {

          resolve(result);
        },
      ).catch(
        (err: any) => {
          console.log('ERRO FIND', err);
          reject();
        },
      );
    });
  }

  private _consultarCrises(dataIni: string, dataFim: string): Promise<any> {
    let scope: DiarioPage = this;

    return new Promise(function (resolve, reject) {
      scope._syncData.find(
        {
          selector: {
            type: 'crise',
            dataInicial: {
              $gte: dataIni,
              $lte: dataFim,
            },
          },
        },
      ).then(
        (result: any) => {
          if (result.docs.length === 0) {
            reject();
          } else {
            resolve(result);
          }
        },
      ).catch(
        (err: any) => {
          console.log('ERRO FIND', err);
          reject();
        },
      );
    });
  }

  private _getDocMediaIntensidade(documentos: Array<any>) {
    let intensidadeMedia: number = 0;
    let somaMedia: number = 0;

    documentos.forEach(
      (doc: any) => {
        somaMedia += this._getIntensidadeMedia(doc.intensidade);
      },
    );

    if (somaMedia > 0) {
      intensidadeMedia = (somaMedia / documentos.length);
    }

    return intensidadeMedia;
  }

  private _getIntensidadeMedia(intensidades: Array<any>) {
    let intensidadeMedia: number = 0;

    let somaIntensidade: number = 0;
    intensidades.forEach(
      (intensidade: any) => {
        somaIntensidade += intensidade.valor;
      },
    );

    if (somaIntensidade > 0) {
      intensidadeMedia = (somaIntensidade / intensidades.length);
    }

    return intensidadeMedia;
  }

}
