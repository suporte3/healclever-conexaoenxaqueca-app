import { TranslateModule } from '@ngx-translate/core';
import { ComponentsModule } from './../../components/components.module';
import { PipesModule } from './../../pipes/pipes.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DiarioPage } from './diario';

@NgModule({
  declarations: [
    DiarioPage,
  ],
  imports: [
    IonicPageModule.forChild(DiarioPage),
    PipesModule,
    ComponentsModule,
    TranslateModule,
  ],
})
export class DiarioPageModule { }
