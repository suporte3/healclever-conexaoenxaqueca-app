import { Sono } from './sono.model';
import { Alimento } from './alimento.model';
export interface Diario {
    _id: string;
    alimentos: Alimento[];
    sono: Sono;
    passos: number;
    googleFitSync: boolean;
    type: string;
}
