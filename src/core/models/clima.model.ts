export interface Clima {
    codigo: number;
   descricao: string;
   temperatura: number;
   pressaoAtmosferica: number;
   pressaoAtmosfericaUltimaHora: number;
   diaOuNoite: string;
}
