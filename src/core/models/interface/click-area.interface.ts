export interface ClickAreaInterface {
  onClickArea(areaID: number, action: boolean): void;
}
