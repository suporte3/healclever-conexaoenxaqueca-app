export interface AuthenticationEventHandlerInterface {
  onLoginBySmartphoneUserNotFound(): void;
  onSuccessLogin(tipoAutenticacao: number): void;
  onErrorDatabaseInit(tipoAutenticacao: number): void;
  onErrorRequest(error: any, tipoRequisicao: number): void;
  onErrorSincronizarDatabase(): void;
  onErrorAtualizarPerfilUsuario(error: any): void;
}
