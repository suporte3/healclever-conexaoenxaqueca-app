export interface QuestaoPersonalizadaInterface {
    onCallbackPreRegistrarQuestao(): Promise<any>;
    onCallbackPreAtualizarQuestao(): Promise<any>;
    onAtualizarQuestao(): void;
}
