export interface Localizacao {
    titulo: string;
    descricao: string;
    latitude: number;
    longitude: number;
}
