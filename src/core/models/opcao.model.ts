import { TipoOpcao } from './tipo-opcao.enum';

export interface Opcao {
    _id: string;
    nome: string;
    dataCriacao: string;
    ativo: boolean;
    tipo: TipoOpcao;
}
