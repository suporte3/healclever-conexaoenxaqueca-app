import { Clima } from './clima.model';
import { Localizacao } from './localizacao.model';
import { Intensidade } from './intensidade.model';
import { Medicamento } from './medicamento.model';
import { Opcao } from './opcao.model';

export interface Crise {

    _id: string;
    dataInicial: string;
    dataFinal: string;
    intensidade: Array<Intensidade>;
    medicamentos: Array<Medicamento>;
    qualidadesDor: Array<Opcao>;
    sintomas: Array<Opcao>;
    gatilhos: Array<Opcao>;
    esforcosFisicos: Array<Opcao>;
    bebidas: Array<Opcao>;
    comidas: Array<Opcao>;
    cicloMestrual: Array<Opcao>;
    metodosAlivio: Array<Opcao>;
    metodosAlivioEficiente: Array<Opcao>;
    consequencias: Array<Opcao>;
    locais: Array<Opcao>;
    localizacoesDeDor: Array<Opcao>;
    anotacoes: string;
    clima: Clima;
    gps: Localizacao;
    type: string;
}
