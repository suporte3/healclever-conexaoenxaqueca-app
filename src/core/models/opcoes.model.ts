import { Opcao } from './opcao.model';

export interface Opcoes {
    _id: string;
    opcoes: Array<Opcao>;
}
