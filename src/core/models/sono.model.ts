export interface Sono {
    inicio: string;
    fim: string;
    qualidadeSubjetiva: string;
    type: string;
}
