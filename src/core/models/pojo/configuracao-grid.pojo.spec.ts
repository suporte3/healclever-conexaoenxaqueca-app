import { ConfiguracaoGrid } from './configuracao-grid.pojo';
import { TestUtils } from './../../../app/app.test';
import { Crise } from './../crise.model';
import { async, inject } from '@angular/core/testing';

describe('Configuração Grid Questão:', () => {

    let criseObjTest: Crise = {

        _id: 'criseId',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: null,
    };

    beforeEach(async(() => {

        TestUtils.addProvider(ConfiguracaoGrid);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve obter e definir variável _modalEventoClique',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let modalEventoClique: string = 'AdicionarGPS';
            expect(configuracaoGrid.getModalEventoClique()).toBeUndefined();
            configuracaoGrid.setModalEventoClique(modalEventoClique);
            expect(configuracaoGrid.getModalEventoClique()).toBe(modalEventoClique);
        }),
    );

    it('Deve obter e definir variável _possuiEventoClique',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let possuiEventoClique: boolean = true;
            expect(configuracaoGrid.getPossuiEventoClique()).toBe(false);
            configuracaoGrid.setPossuiEventoClique(possuiEventoClique);
            expect(configuracaoGrid.getPossuiEventoClique()).toBe(possuiEventoClique);
        }),
    );

    it('Deve obter e definir variável _possuiModal',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let possuiModal: boolean = true;
            expect(configuracaoGrid.getPossuiModal()).toBe(false);
            configuracaoGrid.setPossuiModal(possuiModal);
            expect(configuracaoGrid.getPossuiModal()).toBe(possuiModal);
        }),
    );

    it('Deve obter e definir variável _possuiEficiencia',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let possuiEficiencia: boolean = true;
            expect(configuracaoGrid.getPossuiEficiencia()).toBe(false);
            configuracaoGrid.setPossuiEficiencia(possuiEficiencia);
            expect(configuracaoGrid.getPossuiEficiencia()).toBe(possuiEficiencia);
        }),
    );

    it('Deve obter e definir variável _identificadorGrid',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let identificadorGrid: string = 'identificadorGridQuestao';
            expect(configuracaoGrid.getIdentificadorGrid()).toBeUndefined();
            configuracaoGrid.setIdentificadorGrid(identificadorGrid);
            expect(configuracaoGrid.getIdentificadorGrid()).toBe(identificadorGrid);
        }),
    );
/*
    it('Deve obter e definir variável _questao',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let questao: any = QualidadeDorPage;
            expect(configuracaoGrid.getQuestao()).toBeUndefined();
            configuracaoGrid.setQuestao(questao);
            expect(configuracaoGrid.getQuestao()).toBe(questao);
        }),
    );

    it('Deve obter e definir variável _modalCtrl',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let modalCtrl: any = QualidadeDorPage;
            expect(configuracaoGrid.getModalCtrl()).toBeUndefined();
            configuracaoGrid.setModalCtrl(modalCtrl);
            expect(configuracaoGrid.getModalCtrl()).toBe(modalCtrl);
        }),
    );*/

    it('Deve obter e definir variável _modalAdicionarOpcaoReferencia',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let modalAdicionarOpcaoReferencia: any = 'QualidadeDorPage';
            expect(configuracaoGrid.getModalAdicionarOpcaoReferencia()).toBeUndefined();
            configuracaoGrid.setModalAdicionarOpcaoReferencia(modalAdicionarOpcaoReferencia);
            expect(configuracaoGrid.getModalAdicionarOpcaoReferencia()).toBe(modalAdicionarOpcaoReferencia);
        }),
    );

    it('Deve obter e definir variável _documentoOptionId',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let documentoOptionId: any = 'aS12Dz-asasz-ajb1sC2-asin1d-pin19';
            expect(configuracaoGrid.getDocumentoOptionId()).toBeUndefined();
            configuracaoGrid.setDocumentoOptionId(documentoOptionId);
            expect(configuracaoGrid.getDocumentoOptionId()).toBe(documentoOptionId);
        }),
    );

    it('Deve obter e definir variável _nomeDoObjetoOpcao',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let nomeDoObjetoOpcao: string = 'nomeDoObjetoDoBancoDeDados';
            expect(configuracaoGrid.getNomeDoObjetoOpcao()).toBeUndefined();
            configuracaoGrid.setNomeDoObjetoOpcao(nomeDoObjetoOpcao);
            expect(configuracaoGrid.getNomeDoObjetoOpcao()).toBe(nomeDoObjetoOpcao);
        }),
    );

    it('Deve obter e definir variável _syncDataServiceReference',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let syncDataService: any = {};
            expect(configuracaoGrid.getSyncDataServiceReference()).toBeUndefined();
            configuracaoGrid.setSyncDataServiceReference(syncDataService);
            expect(configuracaoGrid.getSyncDataServiceReference()).toBe(syncDataService);
        }),
    );

    it('Deve obter e definir variável _possuiOpcoesGatilhos',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let possuiOpcoesGatilho: boolean = true;
            expect(configuracaoGrid.getPossuiOpcoesGatilhos()).toBe(false);
            configuracaoGrid.setPossuiOpcoesGatilhos(possuiOpcoesGatilho);
            expect(configuracaoGrid.getPossuiOpcoesGatilhos()).toBe(possuiOpcoesGatilho);
        }),
    );

    it('Deve obter e definir variável _proximaPagina',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let proximaPagina: any = 'QualidadeDorPage';
            expect(configuracaoGrid.getProximaPagina()).toBeUndefined();
            configuracaoGrid.setProximaPagina(proximaPagina);
            expect(configuracaoGrid.getProximaPagina()).toBe(proximaPagina);
        }),
    );

    it('Deve obter e definir variável _crise',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            expect(configuracaoGrid.getCrise()).toBeDefined();
            configuracaoGrid.setCrise(criseObjTest);
            expect(configuracaoGrid.getCrise()).toBe(criseObjTest);
        }),
    );

    it('Deve obter e definir variável _rota',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let rota: string = 'sumario';
            expect(configuracaoGrid.getRota()).toBeUndefined();
            configuracaoGrid.setRota(rota);
            expect(configuracaoGrid.getRota()).toBe(rota);
        }),
    );

    it('Deve obter e definir variável _marcarSomenteUmaOpcaoNoGrid',
        inject([ConfiguracaoGrid], (configuracaoGrid: ConfiguracaoGrid) => {

            let marcarSomenteUmaOpcaoNoGrid: boolean = true;
            expect(configuracaoGrid.isMarcarSomenteUmaOpcaoNoGrid()).toBe(false);
            configuracaoGrid.setMarcarSomenteUmaOpcaoNoGrid(marcarSomenteUmaOpcaoNoGrid);
            expect(configuracaoGrid.isMarcarSomenteUmaOpcaoNoGrid()).toBe(marcarSomenteUmaOpcaoNoGrid);
        }),
    );
});
