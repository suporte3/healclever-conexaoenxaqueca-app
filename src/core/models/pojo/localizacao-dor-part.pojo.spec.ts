import { TestUtils } from './../../../app/app.test';
import { LocalizacaoPart } from './localizacao-dor-part.pojo';
import { async } from '@angular/core/testing';

describe('Localização Part:', () => {

    beforeEach(async(() => {

        TestUtils.addProvider(LocalizacaoPart);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('#getCaminhoImagem - Deve obter caminho da imagem', () => {
        let caminhoImagem: string = 'caminho/caminho/imagem';
        let localizacaoPart: LocalizacaoPart = new LocalizacaoPart(caminhoImagem, true);
        expect(localizacaoPart.getCaminhoImagem()).toBe(caminhoImagem);
    });

    it('#setSelecionado - Deve definir status', () => {
        let caminhoImagem: string = 'caminho/caminho/imagem';
        let localizacaoPart: LocalizacaoPart = new LocalizacaoPart(caminhoImagem, true);
        localizacaoPart.setSelecionado(false);
        expect(localizacaoPart.isSelecionado()).toBe(false);
    });

    it('#isSelecionado - Deve obter status', () => {
        let caminhoImagem: string = 'caminho/caminho/imagem';
        let localizacaoPart: LocalizacaoPart = new LocalizacaoPart(caminhoImagem, true);
        expect(localizacaoPart.isSelecionado()).toBe(true);
    });
});
