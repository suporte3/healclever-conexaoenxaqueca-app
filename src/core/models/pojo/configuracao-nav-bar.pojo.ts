export class ConfiguracaoNavBar {

    private _textoInicial: string;
    private _textoModoRemover: string;
    private _textoModoEdicao: string;
    private _identificadorGrid: string;

    public getTextoInicial() {
        return this._textoInicial;
    }

    public setTextoInicial(textoInicial: string) {
        this._textoInicial = textoInicial;
    }

    public getTextoModoRemover() {
        return this._textoModoRemover;
    }

    public setTextoModoRemover(textoModoRemover: string) {
        this._textoModoRemover = textoModoRemover;
    }

    public getTextoModoEdicao() {
        return this._textoModoEdicao;
    }

    public setTextoModoEdicao(textoModoEdicao: string) {
        this._textoModoEdicao = textoModoEdicao;
    }

    public getIdentificadorGrid() {
        return this._identificadorGrid;
    }

    public setIdentificadorGrid(identificadorGrid: string) {
        this._identificadorGrid = identificadorGrid;
    }

}
