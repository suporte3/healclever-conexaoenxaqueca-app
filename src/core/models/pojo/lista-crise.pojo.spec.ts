import { AppHelper } from './../../helpers/app.helper';
import { Cor } from './cor.pojo';
import { Crise } from './../crise.model';
import { Intensidade } from './../intensidade.model';
import { TestUtils } from './../../../app/app.test';
import { ModeloListaCrise } from './lista-crise.pojo';
import { async, inject } from '@angular/core/testing';

describe('Modelo Lista Crise:', () => {

    beforeEach(async(() => {

        TestUtils.addProvider(ModeloListaCrise);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('#definirDetalhesIntensidade - sem intensidade',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let intensidade: Intensidade = {
                valor: 0,
                dataCriacao: null,
            };

            let criseObjTest: Crise = {

                _id: 'criseId',
                dataInicial: null,
                dataFinal: null,
                intensidade: [],
                medicamentos: [],
                qualidadesDor: [],
                sintomas: [],
                gatilhos: [],
                esforcosFisicos: [],
                bebidas: [],
                comidas: [],
                cicloMestrual: [],
                metodosAlivio: [],
                metodosAlivioEficiente: [],
                consequencias: [],
                locais: [],
                localizacoesDeDor: [],
                anotacoes: null,
                clima: null,
                gps: null,
                type: null,
            };

            modeloListaCrise.setCrise(criseObjTest);
            modeloListaCrise.definirDetalhesIntensidade();
            expect(modeloListaCrise.getCorIntensidade()).toBe(Cor.CINZA);
            expect(modeloListaCrise.getClassificacaoIntensidade()).toBe('indefinida');

            intensidade.valor = 2;
            criseObjTest.intensidade = [intensidade];
            modeloListaCrise.setCrise(criseObjTest);
            modeloListaCrise.definirDetalhesIntensidade();
            expect(modeloListaCrise.getCorIntensidade()).toBe(Cor.CRISE_FRACA);
            expect(modeloListaCrise.getClassificacaoIntensidade()).toBe('fraca');

            intensidade.valor = 4;
            criseObjTest.intensidade = [intensidade];
            modeloListaCrise.setCrise(criseObjTest);
            modeloListaCrise.definirDetalhesIntensidade();
            expect(modeloListaCrise.getCorIntensidade()).toBe(Cor.CRISE_MEDIA);
            expect(modeloListaCrise.getClassificacaoIntensidade()).toBe('média');

            intensidade.valor = 7;
            criseObjTest.intensidade = [intensidade];
            modeloListaCrise.setCrise(criseObjTest);
            modeloListaCrise.definirDetalhesIntensidade();
            expect(modeloListaCrise.getCorIntensidade()).toBe(Cor.CRISE_FORTE);
            expect(modeloListaCrise.getClassificacaoIntensidade()).toBe('forte');
        }),
    );

    it('Deve obter e definir variável _crise',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let criseObjTest: Crise = {

                _id: 'criseId',
                dataInicial: null,
                dataFinal: null,
                intensidade: [],
                medicamentos: [],
                qualidadesDor: [],
                sintomas: [],
                gatilhos: [],
                esforcosFisicos: [],
                bebidas: [],
                comidas: [],
                cicloMestrual: [],
                metodosAlivio: [],
                metodosAlivioEficiente: [],
                consequencias: [],
                locais: [],
                localizacoesDeDor: [],
                anotacoes: null,
                clima: null,
                gps: null,
                type: null,
            };

            expect(modeloListaCrise.getCrise()).toBeUndefined();
            modeloListaCrise.setCrise(criseObjTest);
            expect(modeloListaCrise.getCrise()).toBe(criseObjTest);
        }),
    );

    it('Deve obter e definir variável _textoData',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let textoData: string = 'há duas semanas';

            expect(modeloListaCrise.getTextoData()).toBeUndefined();
            modeloListaCrise.setTextoData(textoData);
            expect(modeloListaCrise.getTextoData()).toBe(textoData);
        }),
    );

    it('Deve obter e definir variável _duracao',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let duracao: string = '3h 25min';

            expect(modeloListaCrise.getDuracao()).toBeUndefined();
            modeloListaCrise.setDuracao(duracao);
            expect(modeloListaCrise.getDuracao()).toBe(duracao);
        }),
    );

    it('Deve obter e definir variável _ocorrendo',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let ocorrendo: boolean = false;

            expect(modeloListaCrise.isOcorrendo()).toBe(false);
            modeloListaCrise.setOcorrendo(ocorrendo);
            expect(modeloListaCrise.isOcorrendo()).toBe(ocorrendo);
        }),
    );

    it('Deve obter e definir variável _porcentagem',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let porcentagem: number = 87.5;

            expect(modeloListaCrise.getPorcentagem()).toBeUndefined();
            modeloListaCrise.setPorcentagem(porcentagem);
            expect(modeloListaCrise.getPorcentagem()).toBe(porcentagem);
        }),
    );

    it('Deve obter e definir variável _progressoCor com cor fixa',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let progressoCor = '#CCCCCC';

            expect(modeloListaCrise.getProgressoCor()).toBeUndefined();
            modeloListaCrise.setProgressoCorFixo(progressoCor);
            expect(modeloListaCrise.getProgressoCor()).toBe(progressoCor);
        }),
    );

    it('Deve obter e definir variável _progressoCor de acordo com o valor de _porcentagem',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let porcentagem: number = 87.5;
            let progressoCor = new AppHelper().getIndiceRespostaCriseCor(porcentagem);

            expect(modeloListaCrise.getProgressoCor()).toBeUndefined();
            modeloListaCrise.setPorcentagem(porcentagem);
            modeloListaCrise.setProgressoCor();
            expect(modeloListaCrise.getProgressoCor()).toBe(progressoCor);
        }),
    );

    it('Deve obter e definir variável _progressoCor com #000000 quando _porcentagem indefinido',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let progressoCor: string = '#000000';
            expect(modeloListaCrise.getProgressoCor()).toBeUndefined();
            modeloListaCrise.setProgressoCor();
            expect(modeloListaCrise.getProgressoCor()).toBe(progressoCor);
        }),
    );

    it('Deve obter e definir variável _classificacaoIntensidade',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let classificacaoIntensidade: string = 'média';
            expect(modeloListaCrise.getClassificacaoIntensidade()).toBeUndefined();
            modeloListaCrise.setClassificacaoIntensidade(classificacaoIntensidade);
            expect(modeloListaCrise.getClassificacaoIntensidade()).toBe(classificacaoIntensidade);
        }),
    );

    it('Deve obter e definir variável _corIntensidade',
        inject([ModeloListaCrise], (modeloListaCrise: ModeloListaCrise) => {

            let corIntensidade: string = '#000000';
            expect(modeloListaCrise.getCorIntensidade()).toBeUndefined();
            modeloListaCrise.setCorIntensidade(corIntensidade);
            expect(modeloListaCrise.getCorIntensidade()).toBe(corIntensidade);
        }),
    );
});
