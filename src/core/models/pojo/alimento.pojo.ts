export class ModeloAlimento {

    private _CAFE_DA_MANHA: number = 0;
    private _ALMOCO: number = 1;
    private _JANTAR: number = 2;
    private _LANCHES: number = 3;

    private _id: string;
    private _nome: string;
    private _dataCriacao: string;
    private _cafeDaManha: boolean;
    private _almoco: boolean;
    private _jantar: boolean;
    private _lanches: boolean;
    private _ativo: boolean;
    private _icone: string;
    private _selecionado: boolean;

    public setId(id: string) {
        this._id = id;
    }

    public getId(): string {
        return this._id;
    }

    public setNome(nome: string) {
        this._nome = nome;
    }

    public getNome(): string {
        return this._nome;
    }

    public setDataCriacao(dataCriacao: string) {
        this._dataCriacao = dataCriacao;
    }

    public getDataCriacao(): string {
        return this._dataCriacao;
    }

    public setCafeDaManha(isCafeDaManha: boolean) {
        this._cafeDaManha = isCafeDaManha;
    }

    public isCafeDaManha(): boolean {
        return this._cafeDaManha;
    }

    public setAlmoco(isAlmoco: boolean) {
        this._almoco = isAlmoco;
    }

    public isAlmoco(): boolean {
        return this._almoco;
    }

    public setJantar(isJantar: boolean) {
        this._jantar = isJantar;
    }

    public isJantar(): boolean {
        return this._jantar;
    }

    public setLanches(isLanches: boolean) {
        this._lanches = isLanches;
    }

    public isLanches(): boolean {
        return this._lanches;
    }

    public setAtivo(isAtivo: boolean) {
        this._ativo = isAtivo;
    }

    public isAtivo() {
        return this._ativo;
    }

    public setIcone(icone: string) {
        this._icone = icone;
    }

    public getIcone() {
        return this._icone;
    }

    public isSelecionado(): boolean {
        return this._selecionado;
    }

    public setSelecionado(selecionado: boolean) {
        this._selecionado = selecionado;
    }

    public criarNovoAlimento(alimento: any) {
        this.setId(alimento._id);
        this.setNome(alimento.nome);
        this.setDataCriacao(alimento.dataCriacao);
        this.setAtivo(alimento.ativo);
        this.setCafeDaManha(alimento.cafeDaManha);
        this.setAlmoco(alimento.almoco);
        this.setJantar(alimento.jantar);
        this.setLanches(alimento.lanches);

        return this;
    }

    public definirIcone(categoria: number) {
        let icone: string = null;
        switch (categoria) {
            case this._CAFE_DA_MANHA:
                icone = 'rosie-alimentos-cafe';
                break;
            case this._ALMOCO:
                icone = 'rosie-alimentos-almoco';
                break;
            case this._JANTAR:
                icone = 'rosie-alimentos-jantar';
                break;
            case this._LANCHES:
                icone = 'rosie-alimentos-lanches';
                break;
            default:
                icone = 'rosie-alimentos-cafe';
                break;
        }

        this.setIcone(icone);
    }

    public getValueCategoryStatusByType(type: number) {
        let status: boolean = false;

        switch (type) {
            case this._CAFE_DA_MANHA:
                status = this.isCafeDaManha();
                break;
            case this._ALMOCO:
                status = this.isAlmoco();
                break;
            case this._JANTAR:
                status = this.isJantar();
                break;
            case this._LANCHES:
                status = this.isLanches();
                break;
            default:
                status = this.isCafeDaManha();
                break;
        }

        return status;
    }
}
