import { LocalizacaoDorPage } from './../../../pages/questionario/localizacao-dor/localizacao-dor';
import { LocalizacaoPart } from './localizacao-dor-part.pojo';
import { TestUtils } from './../../../app/app.test';
import { async, inject } from '@angular/core/testing';
import { ConfiguracaoLocalizacaoDorTouch } from './configuracao-localizacao-dor-touch.pojo';

describe('Coniguração Localização Canvas Touch:', () => {

    beforeEach(async(() => {

        TestUtils.addProvider(ConfiguracaoLocalizacaoDorTouch);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve obter e definir variável _imagemInicial',
        inject([ConfiguracaoLocalizacaoDorTouch], (configuracaoLocalizacaoCanvasTouch: ConfiguracaoLocalizacaoDorTouch) => {

            let imagemInicial: string = 'caminho/imagem/inicial/jpg';
            expect(configuracaoLocalizacaoCanvasTouch.getImagemInicial()).toBeUndefined();
            configuracaoLocalizacaoCanvasTouch.setImagemInicial(imagemInicial);
            expect(configuracaoLocalizacaoCanvasTouch.getImagemInicial()).toBe(imagemInicial);
        }),
    );

    it('Deve obter e definir variável _imagemMapa',
        inject([ConfiguracaoLocalizacaoDorTouch], (configuracaoLocalizacaoCanvasTouch: ConfiguracaoLocalizacaoDorTouch) => {

            let imagemMapa: string = 'caminho/imagem/mapa/jpg';
            expect(configuracaoLocalizacaoCanvasTouch.getImagemMapa()).toBeUndefined();
            configuracaoLocalizacaoCanvasTouch.setImagemMapa(imagemMapa);
            expect(configuracaoLocalizacaoCanvasTouch.getImagemMapa()).toBe(imagemMapa);
        }),
    );

    it('Deve obter e definir variável _localizacoes',
        inject([ConfiguracaoLocalizacaoDorTouch], (configuracaoLocalizacaoCanvasTouch: ConfiguracaoLocalizacaoDorTouch) => {

            let localizacoesPartObjTest: Array<LocalizacaoPart> = [
                new LocalizacaoPart('caminho/da/imagem/jpg', false),
                new LocalizacaoPart('caminho/da/imagem/png', false),
                new LocalizacaoPart('caminho/da/imagem/jpg', true),
            ];
            expect(configuracaoLocalizacaoCanvasTouch.getLocalizacoes()).toBeUndefined();
            configuracaoLocalizacaoCanvasTouch.setLocalizacoes(localizacoesPartObjTest);
            expect(configuracaoLocalizacaoCanvasTouch.getLocalizacoes()).toBe(localizacoesPartObjTest);
        }),
    );

    it('Deve obter e definir variável _page',
        inject([ConfiguracaoLocalizacaoDorTouch], (configuracaoLocalizacaoCanvasTouch: ConfiguracaoLocalizacaoDorTouch) => {

            let page: any = LocalizacaoDorPage;
            expect(configuracaoLocalizacaoCanvasTouch.getPage()).toBeUndefined();
            configuracaoLocalizacaoCanvasTouch.setPage(page);
            expect(configuracaoLocalizacaoCanvasTouch.getPage()).toBe(page);
        }),
    );

    it('Deve obter e definir variável _temTouch',
        inject([ConfiguracaoLocalizacaoDorTouch], (configuracaoLocalizacaoCanvasTouch: ConfiguracaoLocalizacaoDorTouch) => {

            let temTouch: boolean = true;
            expect(configuracaoLocalizacaoCanvasTouch.hasTouch()).toBe(false);
            configuracaoLocalizacaoCanvasTouch.setHasTouch(temTouch);
            expect(configuracaoLocalizacaoCanvasTouch.hasTouch()).toBe(temTouch);
        }),
    );
});
