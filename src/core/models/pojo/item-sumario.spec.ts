import { QualidadeDorPage } from './../../../pages/questionario/qualidade-dor/qualidade-dor';
import { ItemSumarioCrise } from './item-sumario.pojo';
import { TestUtils } from './../../../app/app.test';
import { Crise } from './../crise.model';
import { async, inject } from '@angular/core/testing';

describe('Modelo Item Sumário:', () => {

    let criseObjTest: Crise = {

        _id: 'criseId',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: null,
    };

    beforeEach(async(() => {

        TestUtils.addProvider(ItemSumarioCrise);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve obter e definir variável _crise', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        expect(modeloItemSumarioCrise.getCrise()).toBeUndefined();
        modeloItemSumarioCrise.setCrise(criseObjTest);
        expect(modeloItemSumarioCrise.getCrise()).toBe(criseObjTest);
    }));

    it('Deve obter e definir variável _tipo', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let tipo: string = ItemSumarioCrise.ANOTACOES;
        expect(modeloItemSumarioCrise.getTipo()).toBeUndefined();
        modeloItemSumarioCrise.setTipo(tipo);
        expect(modeloItemSumarioCrise.getTipo()).toBe(tipo);
    }));

    it('Deve obter e definir variável _titulo', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let titulo: string = 'titulo';
        expect(modeloItemSumarioCrise.getTitulo()).toBeUndefined();
        modeloItemSumarioCrise.setTitulo(titulo);
        expect(modeloItemSumarioCrise.getTitulo()).toBe(titulo);
    }));

    it('Deve obter e definir variável _conteudo', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let conteudo: any = 'conteudo';
        expect(modeloItemSumarioCrise.getConteudo()).toBeUndefined();
        modeloItemSumarioCrise.setConteudo(conteudo);
        expect(modeloItemSumarioCrise.getConteudo()).toBe(conteudo);
    }));

    it('Deve obter e definir variável _icone', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let icone: string = 'icone';
        expect(modeloItemSumarioCrise.getIcone()).toBeUndefined();
        modeloItemSumarioCrise.setIcone(icone);
        expect(modeloItemSumarioCrise.getIcone()).toBe(icone);
    }));

    it('Deve obter e definir variável _page', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let page: any = QualidadeDorPage;
        expect(modeloItemSumarioCrise.getPage()).toBeUndefined();
        modeloItemSumarioCrise.setPage(page);
        expect(modeloItemSumarioCrise.getPage()).toEqual(page);
    }));

    it('Deve obter e definir variável _complete', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let isComplete: boolean = true;
        expect(modeloItemSumarioCrise.isComplete()).toBe(false);
        modeloItemSumarioCrise.setComplete(isComplete);
        expect(modeloItemSumarioCrise.isComplete()).toBe(isComplete);
    }));

    it('Deve obter e definir variável _dataInicial', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let dataInicio: string = '2017-05-22';
        expect(modeloItemSumarioCrise.getDataInicial()).toBeUndefined();
        modeloItemSumarioCrise.setDataInicial(dataInicio);
        expect(modeloItemSumarioCrise.getDataInicial()).toBe(dataInicio);
    }));

    it('Deve obter e definir variável _tempoInicial', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let tempoInicio: string = '14:30';
        expect(modeloItemSumarioCrise.getTempoInicial()).toBeUndefined();
        modeloItemSumarioCrise.setTempoInicial(tempoInicio);
        expect(modeloItemSumarioCrise.getTempoInicial()).toBe(tempoInicio);
    }));

    it('Deve obter e definir variável _dataFinal', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let dataFinal: string = '2017-03-22';
        expect(modeloItemSumarioCrise.getDataFinal()).toBeUndefined();
        modeloItemSumarioCrise.setDataFinal(dataFinal);
        expect(modeloItemSumarioCrise.getDataFinal()).toBe(dataFinal);
    }));

    it('Deve obter e definir variável _tempoFinal', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let tempoFinal: string = '15:30';
        expect(modeloItemSumarioCrise.getTempoFinal()).toBeUndefined();
        modeloItemSumarioCrise.setTempoFinal(tempoFinal);
        expect(modeloItemSumarioCrise.getTempoFinal()).toBe(tempoFinal);
    }));

    it('Deve obter e definir variável _duracao', inject([ItemSumarioCrise], (modeloItemSumarioCrise: ItemSumarioCrise) => {

        let duracao: string = '1h e 32 min';
        expect(modeloItemSumarioCrise.getDuracao()).toBeUndefined();
        modeloItemSumarioCrise.setDuracao(duracao);
        expect(modeloItemSumarioCrise.getDuracao()).toBe(duracao);
    }));
});
