import { SyncDataProvider } from './../../../providers/sync-data/sync-data';
import { Crise } from './../crise.model';
import { Opcoes } from './../opcoes.model';

export class ConfiguracaoGrid {

    private _opcoesDocumento: Opcoes = {
        _id: null,
        opcoes: [],
    };
    private _crise: Crise = {
        _id: null,
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    private _identificadorGrid: string;
    private _nomeDoObjetoOpcao: string;
    private _rota: string;
    private _proximaPagina: string;
    private _marcarSomenteUmaOpcaoNoGrid: boolean = false;
    // private _questao: any;
    private _possuiModal: boolean = false;
    private _documentoOptionId: string;
    private _modalCtrl: any;
    private _modalAdicionarOpcaoReferencia: string;
    private _possuiEficiencia: boolean = false;
    private _syncDataServiceReference: any;
    private _modalEventoClique: string;
    private _possuiEventoClique: boolean = false;
    private _possuiOpcoesGatilhos: boolean = false;

    public getProximaPagina(): string {
        return this._proximaPagina;
    }

    public setProximaPagina(value: string) {
        this._proximaPagina = value;
    }

    public getRota() {
        return this._rota;
    }

    public setRota(rota: string) {
        this._rota = rota;
    }

    public getNomeDoObjetoOpcao(): string {
        return this._nomeDoObjetoOpcao;
    }

    public setNomeDoObjetoOpcao(value: string) {
        this._nomeDoObjetoOpcao = value;
    }

    public getIdentificadorGrid(): string {
        return this._identificadorGrid;
    }

    public setIdentificadorGrid(value: string) {
        this._identificadorGrid = value;
    }

    public setOpcoesDocumento(opcoesDaQuestao: any) {
        this._opcoesDocumento = opcoesDaQuestao;
    }

    public getOpcoesDocumento() {
        return this._opcoesDocumento;
    }

    public getCrise(): Crise {
        return this._crise;
    }

    public setCrise(crise: Crise) {
        this._crise = crise;
    }

    public isMarcarSomenteUmaOpcaoNoGrid(): boolean {
        return this._marcarSomenteUmaOpcaoNoGrid;
    }

    public setMarcarSomenteUmaOpcaoNoGrid(value: boolean) {
        this._marcarSomenteUmaOpcaoNoGrid = value;
    }
/*
    public getQuestao(): any {
        return this._questao;
    }

    public setQuestao(value: any) {
        this._questao = value;
    }*/

    public getPossuiModal(): boolean {
        return this._possuiModal;
    }

    public setPossuiModal(value: boolean) {
        this._possuiModal = value;
    }

    public getDocumentoOptionId(): any {
        return this._documentoOptionId;
    }

    public setDocumentoOptionId(value: any) {
        this._documentoOptionId = value;
    }

    public getModalCtrl(): any {
        return this._modalCtrl;
    }

    public setModalCtrl(value: any) {
        this._modalCtrl = value;
    }

    public getModalAdicionarOpcaoReferencia(): string {
        return this._modalAdicionarOpcaoReferencia;
    }

    public setModalAdicionarOpcaoReferencia(value: string) {
        this._modalAdicionarOpcaoReferencia = value;
    }

    public getPossuiEficiencia(): boolean {
        return this._possuiEficiencia;
    }

    public setPossuiEficiencia(value: boolean) {
        this._possuiEficiencia = value;
    }

    public getSyncDataServiceReference(): SyncDataProvider {
        return this._syncDataServiceReference;
    }

    public setSyncDataServiceReference(value: any) {
        this._syncDataServiceReference = value;
    }

    public getModalEventoClique(): string {
        return this._modalEventoClique;
    }

    public setModalEventoClique(value: string) {
        this._modalEventoClique = value;
    }

    public getPossuiEventoClique(): boolean {
        return this._possuiEventoClique;
    }

    public setPossuiEventoClique(value: boolean) {
        this._possuiEventoClique = value;
    }

    public getPossuiOpcoesGatilhos(): any {
        return this._possuiOpcoesGatilhos;
    }

    public setPossuiOpcoesGatilhos(value: boolean) {
        this._possuiOpcoesGatilhos = value;
    }

}
