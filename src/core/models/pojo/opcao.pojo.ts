import { Crise } from './../crise.model';
import { Medicamento } from './../medicamento.model';
import { TipoQuestao } from './tipo-questao.pojo';
import { Cor } from './cor.pojo';
import { TipoOpcao } from './../tipo-opcao.enum';
import { TipoEficiencia } from './../tipo-eficiencia.enum';
import { Localizacao } from './../localizacao.model';

export class ModeloOpcao {
    private _id: string;
    private nome: string;
    private dataCriacao: string;
    private ativo: boolean;
    private tipo: TipoOpcao;
    private quantidade: number;
    private icone: string;
    private iconeEficiencia: string;
    private corIconeEficiencia: string;
    private cor: string;
    private selecionado: boolean;
    private tipoEficiencia: TipoEficiencia;
    private tipoQuestao: string;
    private localFixo: boolean;
    private valorGPS: Localizacao;

    public definirIconeEficiencia() {
        switch (this.tipoEficiencia) {
            case TipoEficiencia.NADA_UTIL:
                this.setIconeEficiencia('mm-intensidade-forte');
                this.setCorIconeEficiencia(Cor.LARANJA);
                break;
            case TipoEficiencia.POUCO_UTIL:
                this.setIconeEficiencia('mm-intensidade-media');
                this.setCorIconeEficiencia(Cor.CRISE_FRACA);
                break;
            case TipoEficiencia.UTIL:
                this.setIconeEficiencia('mm-intensidade-fraca');
                this.setCorIconeEficiencia(Cor.VERDE_CLARO);
                break;
            default:
                console.log('TRACK ERROR');
        }
    }

    public definirIcones(identificador: string) {
        let iconesFixos: Array<string>;
        switch (identificador) {
            case TipoQuestao.MEDICAMENTO:
                this.icone = 'mm-medicamento';
                break;
            case TipoQuestao.QUALIDADE_DOR:
                iconesFixos = [
                    'mm-qualidade-dor-peso',
                    'mm-qualidade-dor-pontada',
                    'mm-qualidade-dor-explosao',
                    'mm-qualidade-dor-pressao',
                    'mm-qualidade-dor-pulsatil',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.SINTOMA:
                iconesFixos = [
                    'mm-sintomas-vomito',
                    'mm-sintomas-tontura',
                    'mm-sintomas-congestao-nasal',
                    'mm-sintomas-intolerancia-luz',
                    'mm-sintomas-intolerancia-barulho',
                    'mm-sintomas-dor-pescoco',
                    'mm-sintomas-alteracoes-visao',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.GATILHO:
                iconesFixos = [
                    'mm-gatilho-esforco-fisico',
                    'mm-gatilho-menstruacao',
                    'mm-gatilho-ansiedade',
                    'mm-gatilho-estresse',
                    'mm-gatilho-falta-sono',
                    'mm-gatilho-jejum',
                    'mm-gatilho-bebida',
                    'mm-gatilho-comida',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.GATILHO_ESFORCO_FISICO:
                iconesFixos = [
                    'mm-gatilho-esforco-fisico-corrida',
                    'mm-gatilho-esforco-fisico-academia',
                    'mm-gatilho-esforco-fisico-esportes',
                    'mm-gatilho-esforco-fisico-tosse',
                    'mm-gatilho-esforco-fisico-sexo',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.GATILHO_BEBIDA:
                iconesFixos = [
                    'mm-gatilho-bebida-cafe',
                    'mm-gatilho-bebida-cerveja',
                    'mm-gatilho-bebida-destilado',
                    'mm-gatilho-bebida-vinho',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.GATILHO_COMIDA:
                iconesFixos = [
                    'mm-gatilho-comida-chocolate',
                    'mm-gatilho-comida-chinesa',
                    'mm-gatilho-comida-frituras',
                    'mm-gatilho-comida-queijo',
                    'mm-gatilho-comida-salsicha-embutidos',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.CICLO_MENSTRUAL:
                iconesFixos = [
                    'mm-ciclo-menstrual-antes',
                    'mm-ciclo-menstrual-sim',
                    'mm-ciclo-menstrual-nao',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.METODO_ALIVIO:
                iconesFixos = [
                    'mm-metodo-alivio-banho',
                    'mm-metodo-alivio-massagem',
                    'mm-metodo-alivio-quarto-escuro',
                    'mm-metodo-alivio-relaxamento',
                    'mm-metodo-alivio-compressa',
                    'mm-metodo-alivio-repouso',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.CONSEQUENCIA:
                iconesFixos = [
                    'mm-consequencia-falta-escola',
                    'mm-consequencia-falta-trabalho',
                    'mm-consequencia-atividades-sociais',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            case TipoQuestao.LOCAL:
                iconesFixos = [
                    'mm-local-trabalho',
                    'mm-local-transito',
                    'mm-local-casa',
                    'mm-local-escola',
                ];
                this._definirIconeFixoOuDinamico(iconesFixos);
                break;
            default:
                this.icone = 'mm-opcao-dinamica';
        }
    }

    public definirQuantidadeMedicamento(medicamentosDaCrise: Array<Medicamento>, medicamentosSelecionadosDaQuestao: Array<ModeloOpcao>) {

        medicamentosDaCrise.forEach((opcaoCrise: Medicamento) => {
            if (this.nome === opcaoCrise.nome) {
                this.quantidade = opcaoCrise.quantidade;
            }
        }, this);

        medicamentosSelecionadosDaQuestao.forEach((opcaoSelecionada: ModeloOpcao) => {
            if (this.nome === opcaoSelecionada.nome) {
                this.quantidade = opcaoSelecionada.quantidade;
            }
        }, this);

    }

    public definirOpcaoGPS(crise: Crise, valorGPSOpcaoDocumento: Localizacao) {
        let valorGPSModeloOpcao: Localizacao = valorGPSOpcaoDocumento;
        let isLocalFixo: boolean = (valorGPSOpcaoDocumento ? true : false);
        let valorGPSDaCrise: any = null;
        if (crise.locais.length > 0) {
            crise.locais.every((local: any) => {
                if (local._id === this.getId()) {
                    valorGPSDaCrise = local.gps;

                    if (valorGPSDaCrise && isLocalFixo) {
                        if (valorGPSDaCrise.latitude !== valorGPSOpcaoDocumento.latitude ||
                            valorGPSDaCrise.longitude !== valorGPSOpcaoDocumento.longitude) {
                            isLocalFixo = false;
                            valorGPSModeloOpcao = valorGPSDaCrise;
                        }
                    } else if (valorGPSDaCrise && !isLocalFixo) {
                        valorGPSModeloOpcao = valorGPSDaCrise;
                    }

                    return false;
                } else {
                    return true;
                }
            });
        }

        this.setLocalFixo(isLocalFixo);
        this.setValorGPS(valorGPSModeloOpcao);
    }

    public criarModeloDeNovaOpcaoNormal(opcao: any): ModeloOpcao {
        this.setId(opcao._id);
        this.setNome(opcao.nome);
        this.setAtivo(opcao.ativo);
        this.setTipo(opcao.tipo);

        return this;
    }

    public criarModeloDeNovoMedicamento(opcao: any, quantidade: number): ModeloOpcao {
        this.setId(opcao._id);
        this.setNome(opcao.nome);
        this.setAtivo(opcao.ativo);
        this.setTipo(opcao.tipo);
        this.setQuantidade(quantidade);

        return this;
    }

    public getId(): string {
        return this._id;
    }

    public setId(value: string) {
        this._id = value;
    }

    public getNome(): string {
        return this.nome;
    }

    public setNome(value: string) {
        this.nome = value;
    }

    public getDataCriacao(): string {
        return this.dataCriacao;
    }

    public setDataCriacao(value: string) {
        this.dataCriacao = value;
    }

    public getAtivo(): boolean {
        return this.ativo;
    }

    public setAtivo(value: boolean) {
        this.ativo = value;
    }

    public getTipo(): TipoOpcao {
        return this.tipo;
    }

    public setTipo(value: TipoOpcao) {
        this.tipo = value;
    }

    public getQuantidade(): number {
        return this.quantidade;
    }

    public setQuantidade(value: number) {
        this.quantidade = value;
    }

    public getIcone(): string {
        return this.icone;
    }

    public setIcone(value: string) {
        this.icone = value;
    }

    public getCor(): string {
        return this.cor;
    }

    public setCor(value: string) {
        this.cor = value;
    }

    public setCorIconeEficiencia(corIconeEficiencia: string) {
        this.corIconeEficiencia = corIconeEficiencia;
    }

    public getCorIconeEficiencia() {
        return this.corIconeEficiencia;
    }

    public isSelecionado(): boolean {
        return this.selecionado;
    }

    public setSelecionado(value: boolean) {
        this.selecionado = value;
    }

    public getTipoEficiencia(): TipoEficiencia {
        return this.tipoEficiencia;
    }

    public setTipoEficiencia(value: TipoEficiencia) {
        this.tipoEficiencia = value;
    }

    public getIconeEficiencia() {
        return this.iconeEficiencia;
    }

    public setIconeEficiencia(iconeEficiencia: string) {
        this.iconeEficiencia = iconeEficiencia;
    }

    public getTipoQuestao(): string {
        return this.tipoQuestao;
    }

    public setTipoQuestao(value: string) {
        this.tipoQuestao = value;
    }

    public setLocalFixo(isLocalFixo: boolean) {
        this.localFixo = isLocalFixo;
    }

    public isLocalFixo() {
        return this.localFixo;
    }

    public setValorGPS(valorGPS: Localizacao) {
        this.valorGPS = valorGPS;
    }

    public getValorGPS() {
        return this.valorGPS;
    }

    private _definirIconeFixoOuDinamico(iconesFixos: Array<string>) {
        if (this.tipo === undefined) {
            this.icone = 'mm-opcao-dinamica';
            iconesFixos.forEach(
                (iconeFixo) => {
                    if (this._id === iconeFixo) {
                        this.icone = iconeFixo;
                        return false;
                    }
                },
            );
        } else {
            if (this.tipo === TipoOpcao.FIXO) {
                iconesFixos.forEach(
                    (iconeFixo) => {
                        if (this._id === iconeFixo) {
                            this.icone = iconeFixo;
                            return false;
                        }
                    },
                );
            } else {
                this.icone = 'mm-opcao-dinamica';
            }
        }
    }
}
