import { TestUtils } from './../../../app/app.test';
import { ConfiguracaoFooterBar } from './configuracao-footer-bar.pojo';
import { async, inject } from '@angular/core/testing';

describe('Configuração FooterBar:', () => {

    beforeEach(async(() => {

        TestUtils.addProvider(ConfiguracaoFooterBar);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve obter e definir variável _modoAtualizar',
        inject([ConfiguracaoFooterBar], (configuracaoFooterBar: ConfiguracaoFooterBar) => {

            let modoAtualizar: boolean = true;
            expect(configuracaoFooterBar.isModoAtualizar()).toBe(false);
            configuracaoFooterBar.setModoAtualizar(modoAtualizar);
            expect(configuracaoFooterBar.isModoAtualizar()).toBe(modoAtualizar);
        }),
    );

    it('Deve obter e definir variável _salvandoQuestao',
        inject([ConfiguracaoFooterBar], (configuracaoFooterBar: ConfiguracaoFooterBar) => {

            let salvandoQuestao: boolean = true;
            expect(configuracaoFooterBar.isSaving()).toBe(false);
            configuracaoFooterBar.setSaving(salvandoQuestao);
            expect(configuracaoFooterBar.isSaving()).toBe(salvandoQuestao);
        }),
    );
});
