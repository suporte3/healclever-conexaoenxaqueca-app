import { LocalizacaoPart } from './localizacao-dor-part.pojo';
import { ClickAreaInterface } from './../interface/click-area.interface';

export class ConfiguracaoLocalizacaoDorTouch {

    private _imagemInicial: string;
    private _imagemMapa: string;
    private _localizacoes: Array<LocalizacaoPart>;
    private _page: ClickAreaInterface;
    private _temTouch: boolean = false;

    public setHasTouch(habilitado: boolean) {
        this._temTouch = habilitado;
    }

    public hasTouch() {
        return this._temTouch;
    }

    public getImagemInicial(): string {
        return this._imagemInicial;
    }

    public setImagemInicial(imagemInicial: string) {
        this._imagemInicial = imagemInicial;
    }

    public getImagemMapa(): string {
        return this._imagemMapa;
    }

    public setImagemMapa(imagemMapa: string) {
        this._imagemMapa = imagemMapa;
    }

    public getLocalizacoes(): Array<LocalizacaoPart> {
        return this._localizacoes;
    }

    public setLocalizacoes(localizacoes: Array<LocalizacaoPart>) {
        this._localizacoes = localizacoes;
    }

    public getPage(): ClickAreaInterface {
        return this._page;
    }

    public setPage(page: ClickAreaInterface) {
        this._page = page;
    }

}
