import { AppHelper } from './../../helpers/app.helper';
import { Cor } from './cor.pojo';
import { Crise } from './../crise.model';

export class ModeloListaCrise {

    private _crise: Crise;
    private _textoData: string;
    private _duracao: string;
    private _ocorrendo: boolean = false;
    private _porcentagem: number;
    private _progressoCor: string;
    private _classificacaoIntensidade: string;
    private _corIntensidade: string;

    public getCrise(): Crise {
        return this._crise;
    }

    public setCrise(crise: Crise) {
        this._crise = crise;
    }

    public setOcorrendo(isOcorrendo: boolean) {
        this._ocorrendo = isOcorrendo;
    }

    public isOcorrendo() {
        return this._ocorrendo;
    }

    public setTextoData(textoData: string) {
        this._textoData = textoData;
    }

    public getTextoData() {
        return this._textoData;
    }

    public setDuracao(textoDuracao: string) {
        this._duracao = textoDuracao;
    }

    public getDuracao() {
        return this._duracao;
    }

    public setCorIntensidade(corIntensidade: string) {
        this._corIntensidade = corIntensidade;
    }

    public getCorIntensidade(): string {
        return this._corIntensidade;
    }

    public setClassificacaoIntensidade(classificacaoIntensidade: string) {
        this._classificacaoIntensidade = classificacaoIntensidade;
    }

    public getClassificacaoIntensidade(): string {
        return this._classificacaoIntensidade;
    }

    public getProgressoCor(): string {
        return this._progressoCor;
    }

    public setProgressoCor(): void {

        if (this._porcentagem) {
            this._progressoCor = new AppHelper().getIndiceRespostaCriseCor(this._porcentagem);
        } else {
            this._progressoCor = '#000000';
        }
    }

    public setProgressoCorFixo(color: string): void {
        this._progressoCor = color;
    }

    public getPorcentagem(): number {
        return this._porcentagem;
    }

    public setPorcentagem(value: number) {
        this._porcentagem = value;
    }

    public definirDetalhesIntensidade(): void {
        if (this.getCrise()) {
            let hasIntensidade: boolean = (this.getCrise().intensidade[0] ? (this.getCrise().intensidade[0].valor ? true : false) : false);
            if (hasIntensidade) {
                let valorIntensidade: number = this.getCrise().intensidade[0].valor;
                if (this.getCrise().intensidade[0].valor <= 3) {
                    this.setCorIntensidade(Cor.CRISE_FRACA);
                    this.setClassificacaoIntensidade('fraca');
                } else if (valorIntensidade > 3 && valorIntensidade <= 6) {
                    this.setCorIntensidade(Cor.CRISE_MEDIA);
                    this.setClassificacaoIntensidade('média');
                } else if (valorIntensidade > 6 && valorIntensidade <= 10) {
                    this.setCorIntensidade(Cor.CRISE_FORTE);
                    this.setClassificacaoIntensidade('forte');
                }
            } else {
                this.setCorIntensidade(Cor.CINZA);
                this.setClassificacaoIntensidade('indefinida');
            }
        }
    }
}
