export class TipoQuestao {

    public static readonly DURACAO_DOR: string = 'idDuracaoDor';
    public static readonly INTENSIDADE: string = 'idIntensidade';
    public static readonly LOCALIZACAO_DOR: string = 'idLocalizacaoDor';
    public static readonly MEDICAMENTO: string = 'idMedicamento';
    public static readonly QUALIDADE_DOR: string = 'idQualidade';
    public static readonly SINTOMA: string = 'idSintoma';
    public static readonly CONSEQUENCIA: string = 'idConsequencia';
    public static readonly LOCAL: string = 'idLocal';
    public static readonly GATILHO: string = 'idGatilho';
    public static readonly GATILHO_ESFORCO_FISICO: string = 'idEsforcoFisico';
    public static readonly GATILHO_COMIDA: string = 'idComida';
    public static readonly GATILHO_BEBIDA: string = 'idBebida';
    public static readonly CICLO_MENSTRUAL: string = 'idCicloMestrual';
    public static readonly METODO_ALIVIO: string = 'idMetodoAlivio';
    public static readonly METODO_ALIVIO_EFICIENTE: string = 'idMetodoAlivioEficiente';
    public static readonly NOTAS: string = 'idNotas';
}
