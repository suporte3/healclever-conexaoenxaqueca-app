import { TestUtils } from './../../../app/app.test';
import { ConfiguracaoNavBar } from './configuracao-nav-bar.pojo';
import { async, inject } from '@angular/core/testing';

describe('Configuração NavBar:', () => {

    beforeEach(async(() => {

        TestUtils.addProvider(ConfiguracaoNavBar);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve obter e definir variável _textoInicial',
        inject([ConfiguracaoNavBar], (configuracaoNavBar: ConfiguracaoNavBar) => {

            let textoInicial: string = 'Selecione algo da questão abaixo para responder (texto exemplo)';
            expect(configuracaoNavBar.getTextoInicial()).toBeUndefined();
            configuracaoNavBar.setTextoInicial(textoInicial);
            expect(configuracaoNavBar.getTextoInicial()).toBe(textoInicial);
        }),
    );

    it('Deve obter e definir variável _textoModoRemover',
        inject([ConfiguracaoNavBar], (configuracaoNavBar: ConfiguracaoNavBar) => {

            let textoModoRemover: string = 'Selecione para remover (texto exemplo)';
            expect(configuracaoNavBar.getTextoModoRemover()).toBeUndefined();
            configuracaoNavBar.setTextoModoRemover(textoModoRemover);
            expect(configuracaoNavBar.getTextoModoRemover()).toBe(textoModoRemover);
        }),
    );

    it('Deve obter e definir variável _identificadorGrid',
        inject([ConfiguracaoNavBar], (configuracaoNavBar: ConfiguracaoNavBar) => {

            let identificadorGrid: string = 'identificadorDoGrid';
            expect(configuracaoNavBar.getIdentificadorGrid()).toBeUndefined();
            configuracaoNavBar.setIdentificadorGrid(identificadorGrid);
            expect(configuracaoNavBar.getIdentificadorGrid()).toBe(identificadorGrid);
        }),
    );
});
