import { Clima } from './../clima.model';
import { Crise } from './../crise.model';

export class ItemSumarioCrise {

    public static readonly INTENSIDADE: string = 'intensidade';
    public static readonly MEDICAMENTO: string = 'medicamentos';
    public static readonly QUALIDADE_DOR: string = 'qualidadesDor';
    public static readonly SINTOMA: string = 'sintomas';
    public static readonly GATILHO: string = 'gatilhos';
    public static readonly ESFORCO_FISICO: string = 'esforcosFisicos';
    public static readonly BEBIDA: string = 'bebidas';
    public static readonly COMIDA: string = 'comidas';
    public static readonly CICLO_MENSTRUAL: string = 'cicloMestrual';
    public static readonly METODO_ALIVIO: string = 'metodosAlivio';
    public static readonly CONSEQUENCIAS: string = 'consequencias';
    public static readonly LOCAIS: string = 'locais';
    public static readonly LOCALIZACAO_DOR: string = 'localizacoesDeDor';
    public static readonly METODO_EFICIENTE: string = 'metodosAlivioEficiente';
    public static readonly ANOTACOES: string = 'anotacoes';
    public static readonly DURACAO: string = 'duracao';
    public static readonly CLIMA: string = 'clima';

    private _crise: Crise;
    private _tipo: string;
    private _titulo: string;
    private _conteudo: any;
    private _icone: string;
    private _page: any;
    private _complete: boolean = false; // manter false como pré inicializado
    private _dataInicial: string;
    private _tempoInicial: string;
    private _dataFinal: string;
    private _tempoFinal: string;
    private _duracao: string;
    private _clima: Clima;

    public setCrise(crise: Crise) {
        this._crise = crise;
    }

    public getCrise() {
        return this._crise;
    }

    public setTipo(tipo: string) {
        this._tipo = tipo;
    }

    public getTipo() {
        return this._tipo;
    }

    public setTitulo(titulo: string) {
        this._titulo = titulo;
    }

    public getTitulo() {
        return this._titulo;
    }

    public setConteudo(conteudo: any) {
        this._conteudo = conteudo;
    }

    public getConteudo() {
        return this._conteudo;
    }

    public setIcone(icone: string) {
        this._icone = icone;
    }

    public getIcone() {
        return this._icone;
    }

    public setPage(page: any) {
        this._page = page;
    }

    public getPage() {
        return this._page;
    }

    public setComplete(complete: boolean) {
        this._complete = complete;
    }

    public isComplete() {
        return this._complete;
    }

    public setDataInicial(dataInicial: string) {
        this._dataInicial = dataInicial;
    }

    public getDataInicial() {
        return this._dataInicial;
    }

    public setTempoInicial(tempoInicial: string) {
        this._tempoInicial = tempoInicial;
    }

    public getTempoInicial() {
        return this._tempoInicial;
    }

    public setDataFinal(dataFinal: string) {
        this._dataFinal = dataFinal;
    }

    public getDataFinal() {
        return this._dataFinal;
    }

    public setTempoFinal(tempoFinal: string) {
        this._tempoFinal = tempoFinal;
    }

    public getTempoFinal() {
        return this._tempoFinal;
    }

    public setDuracao(duracao: string) {
        this._duracao = duracao;
    }

    public getDuracao() {
        return this._duracao;
    }

    public getClima(): Clima {
        return this._clima;
    }

    public setClima(clima: Clima) {
        this._clima = clima;
    }
}
