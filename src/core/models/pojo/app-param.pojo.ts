export class AppParam {
    static readonly CRISE_ID: string = 'com.healclever.myMigraine.crise_param';
    static readonly ROUTE: string = 'com.healclever.myMigraine.route';
    static readonly ATIVAR_MODO_GPS: string = 'ativarModoGPS:LocalPage';
}
