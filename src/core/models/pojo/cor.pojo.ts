export class Cor {
    static readonly VERDE_ESCURO = '#01402e';
    static readonly VERDE_CLARO = '#279379';
    static readonly LARANJA = '#c9402e';
    static readonly CINZA = '#c6c6c6';
    static readonly CRISE_FRACA = '#FFDF00';
    static readonly CRISE_MEDIA = '#F4960E';
    static readonly CRISE_FORTE = '#c9402e'; // '#E52B1F';
}
