export class ConfiguracaoFooterBar {

    private _modoAtualizar: boolean = false;
    private _salvandoQuestao: boolean = false;
    private _identificadorGrid: string;

    public setModoAtualizar(modo: boolean) {
        this._modoAtualizar = modo;
    }

    public isModoAtualizar() {
        return this._modoAtualizar;
    }

    public setSaving(isSaving: boolean) {
        this._salvandoQuestao = isSaving;
    }

    public isSaving() {
        return this._salvandoQuestao;
    }

    public getIdentificadorGrid() {
        return this._identificadorGrid;
    }

    public setIdentificadorGrid(identificadorGrid: string) {
        this._identificadorGrid = identificadorGrid;
    }

}
