import { Injectable } from '@angular/core';

@Injectable()
export class LocalizacaoPart {

    constructor(private _caminhoImagem: string, private _selecionado: boolean) {

    }

    getCaminhoImagem() {
        return this._caminhoImagem;
    }

    setSelecionado(status: boolean) {
        this._selecionado = status;
    }

    isSelecionado(): boolean {
        return this._selecionado;
    }
}
