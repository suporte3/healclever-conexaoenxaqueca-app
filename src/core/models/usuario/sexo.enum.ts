export enum SexoUsuario {
    UKNOW           = -1,
    FEMININO        = 0,
    MASCULINO       = 1,
}
