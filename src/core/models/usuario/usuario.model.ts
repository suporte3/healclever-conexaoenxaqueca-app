export interface Usuario {
    _id: string;
    dataAdmissao: string;
    email: string;
    first_name: string;
    last_name: string;
    birthday: string;
    genero: number;
    sessao: any;
    sincronizado: boolean;
    logado: boolean;
    facebook: boolean;
}
