import { TipoEficiencia } from './tipo-eficiencia.enum';

export interface MetodoAlivio {
    _id: string;
    nome: string;
    tipoEficiencia: TipoEficiencia;
    dataCriacao: string;
}
