export interface Alimento {
    _id: string;
    nome: string;
    dataCriacao: string;
    cafeDaManha: boolean;
    almoco: boolean;
    jantar: boolean;
    lanches: boolean;
    ativo: boolean;
}
