export interface HealthGoal {
    titulo: string;
    descricao: string;
    type: string;
}
