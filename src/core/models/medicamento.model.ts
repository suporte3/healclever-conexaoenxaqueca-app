export interface Medicamento {
    _id: string;
    nome: string;
    quantidade: number;
    dataCriacao: string;
}
