import { Crise } from './../models/crise.model';
import { Cor } from './../models/pojo/cor.pojo';
import { Opcao } from './../models/opcao.model';
import { AppParam } from './../models/pojo/app-param.pojo';
import { TipoOpcao } from './../models/tipo-opcao.enum';
import { TipoEficiencia } from './../models/tipo-eficiencia.enum';
import { Usuario } from './../models/usuario/usuario.model';
import { SexoUsuario } from './../models/usuario/sexo.enum';
import { ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import * as blobUtil from 'blob-util';
import * as moment from 'moment';
import { default as swal } from 'sweetalert2';

export class AppHelper {

    public static readonly FORMATO_DATA_ISO_8601: string = 'YYYY-MM-DD[T]HH:mm:ss.SSS[Z]';

    static generateUniqueID(): string {
        return AppHelper.gerarCharacteres() + AppHelper.gerarCharacteres() + '-' +
        AppHelper.gerarCharacteres() + '-' + AppHelper.gerarCharacteres() + '-' +
        AppHelper.gerarCharacteres() + '-' + AppHelper.gerarCharacteres() +
        AppHelper.gerarCharacteres() + AppHelper.gerarCharacteres();
    }

    static gerarCharacteres(): string {
        let caracteres: string = Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);

        return caracteres;
    }
    /*
        public getGPSAtual(): Promise<any> {
            return new Promise(
                function (resolve, reject) {
                    new Geolocation().getCurrentPosition().then((resp) => {
                        let gps: Localizacao = {
                            titulo: null,
                            descricao: null,
                            latitude: resp.coords.latitude,
                            longitude: resp.coords.longitude,
                        };

                        resolve(gps);
                    }).catch((error) => {
                        reject(error);
                    });
                },
            );
        }

        public atualizarGPSCrise(crise: Crise): Promise<any> {
            let scope: AppUtils = this;

            return new Promise(
                function (resolve) {
                    if (!crise.gps) {
                        scope.getGPSAtual().then(
                            (gps: Localizacao) => {
                                crise.gps = gps;
                                resolve(crise);
                            },
                            () => {
                                resolve(crise);
                            },
                        );
                    } else {
                        resolve(crise);
                    }
                },
            );
        }*/

    public getFacebookGenero(genero: string): number {
        let typeGenero: number = SexoUsuario.UKNOW;
        if (genero === 'male') {
            typeGenero = SexoUsuario.MASCULINO;
        }
        if (genero === 'female') {
            typeGenero = SexoUsuario.FEMININO;
        }

        return typeGenero;
    }

    public atualizarPerfilUsuario(usuario: Usuario, properties: any): Promise<any> {
        let scope = this;
        let usuarios: Array<Usuario> = [];
        return new Promise(
            function (resolve, reject) {
                let storage: Storage = new Storage({});
                storage.get('usuarios')
                    .then(
                    (usuariosArmazenados) => {
                        try {
                            usuarios = usuariosArmazenados;
                            usuarios.forEach(
                                (usuarioEncontrado) => {
                                    usuario = usuarioEncontrado;
                                    if (usuario && usuarioEncontrado.email === usuario.email
                                        || usuarioEncontrado.logado === true) {
                                        usuario = scope.atualizarObjeto(properties, usuario);
                                    } else {
                                        usuarioEncontrado = usuario;
                                    }
                                    return false;
                                });
                        } catch (e) {
                            // track error forEach usuarios
                        }
                    },
                )
                    .then(
                    () => {
                        if (usuarios && usuarios.length > 0) {
                            storage.set('usuarios', usuarios);
                            resolve(usuario);
                        } else {
                            reject();
                        }
                    },
                );
            },
        );
    }

    public getUsuarioLogado(): Promise<any> {
        return new Promise(
            function (resolve, reject) {
                try {

                    let storage: Storage = new Storage({});

                    storage.get('usuarios')
                        .then(
                        (usuariosArmazenados) => {
                            if (usuariosArmazenados) {
                                let usuarioEncontrado: boolean = false;
                                usuariosArmazenados.forEach(
                                    (usuario: Usuario) => {
                                        if (usuario.logado) {
                                            usuarioEncontrado = true;
                                            resolve(usuario);
                                            return false;
                                        }
                                    });

                                if (!usuarioEncontrado) {
                                    reject();
                                }
                            } else {
                                reject();
                            }
                        },
                    );
                } catch (e) {
                    reject();
                }
            },
        );
    }

    public getUsuarioByEmail(email: string): Promise<any> {
        let usuarios: Array<any> = [];
        let usuarioEncontrado: boolean = false;
        let promise: Promise<any> = new Promise(
            function (resolve, reject) {

                let storage: Storage = new Storage({});
                storage.get('usuarios')
                    .then(
                    (usuariosArmazenados) => {
                        if (usuariosArmazenados) {
                            usuarios = usuariosArmazenados;
                            usuariosArmazenados.forEach(
                                (usuario: Usuario) => {
                                    if (email === usuario.email) {
                                        usuarioEncontrado = true;
                                        resolve(usuario);
                                        return false;
                                    }
                                });

                            if (!usuarioEncontrado) {
                                reject();
                            }
                        } else {
                            reject();
                        }
                    },
                );
            },
        );

        return new Promise(
            function (resolve, reject) {
                promise.then(
                    (usuario) => {
                        if (usuarios.length <= 0 && !usuarioEncontrado) {
                            reject();
                        } else {
                            resolve(usuario);
                        }
                    },
                    () => {
                        reject();
                    },
                );
            },
        );
    }

    public getBlobImage(pictureURL: string, mimeType: string): Promise<any> {

        return new Promise(
            function (resolve, reject) {
                blobUtil.imgSrcToDataURL(pictureURL, mimeType, 'Anonymous').then(function (dataURL: any) {
                    blobUtil.dataURLToBlob(dataURL).then(function (blob: Blob) {
                        resolve(blob);
                    }).catch(function (err: any) {
                        reject(err);
                        // track
                    });
                }).catch(function (err: any) {
                    reject(err);
                    // track
                });
            },
        );
    }

    public getMimetype(uri: string): string {
        let extensaoRegexpArray: RegExpExecArray = /jpg|png(?=.)/.exec(uri);
        let extensaoImagem = ((extensaoRegexpArray) ? extensaoRegexpArray[0] : undefined);

        let mimeType: string = null;
        if (extensaoImagem) {
            if (extensaoImagem === 'jpg') {
                mimeType = 'image/jpeg';
            }
            if (extensaoImagem === 'png') {
                mimeType = 'image/png';
            }
        }

        return mimeType;
    }
/*
    public getFacebookProfilePicture(facebookId: string): Promise<any> {
        return new Promise(
            function (resolve, reject) {
                new Facebook().api('/' + facebookId + '/picture?type=large&redirect=false', [])
                    .then(
                    (response) => {
                        resolve(response.data.url);
                    },
                    (err) => {
                        reject(err);
                    },
                );
            },
        );
    }
*/
    public atualizarObjeto(properties: any, objeto: any) {
        for (let key in properties) {
            if (objeto.hasOwnProperty(key)) {
                if (typeof objeto[key] === 'object') {
                    objeto[key] = this.atualizarObjeto(properties[key], objeto[key]);
                } else {
                    objeto[key] = properties[key];
                }
            }
        }

        return objeto;
    }

    public linearInterpolationColor(corInicial: string, corFinal: string, proporcao: number): string {
        /* tslint:disable:no-bitwise */
        let ah: number = parseInt(corInicial.replace(/#/g, ''), 16);
        let ar: number = ah >> 16;
        let ag: number = ah >> 8 & 0xff;
        let ab: number = ah & 0xff;

        let bh: number = parseInt(corFinal.replace(/#/g, ''), 16);
        let br: number = bh >> 16;
        let bg: number = bh >> 8 & 0xff;
        let bb: number = bh & 0xff;

        let rr: number = ar + proporcao * (br - ar);
        let rg: number = ag + proporcao * (bg - ag);
        let rb: number = ab + proporcao * (bb - ab);

        let hexadecimal: string = '#' + ((1 << 24) + (rr << 16) + (rg << 8) + rb | 0).toString(16).slice(1);

        return hexadecimal;
        /* tslint:enable:no-bitwise */
    }

    public getCriseId(): Promise<any> {
        let storage: Storage = new Storage({});
        return storage.get(AppParam.CRISE_ID);
    }

    public setCriseId(criseId: string): Promise<any> {
        let storage: Storage = new Storage({});
        return storage.set(AppParam.CRISE_ID, criseId);
    }

    public isOpcaoInativa(nomeOpcao: string, opcoes: Array<any>): any {
        let opcaoInativa: any = null;
        opcoes.every(
            (opcao) => {
                if (nomeOpcao === opcao.nome && !opcao.ativo) {
                    opcaoInativa = opcao;
                    return false;
                } else {
                    return true;
                }
            },
        );

        return opcaoInativa;
    }

    public ativarOpcao(opcao: any): any {
        opcao.ativo = true;

        return opcao;
    }

    public criarNovaOpcao(nome: string): any {
        let novaOpcao: Opcao = {
            _id: AppHelper.generateUniqueID(),
            tipo: TipoOpcao.DINAMICO,
            nome: null,
            dataCriacao: moment().local().format(AppHelper.FORMATO_DATA_ISO_8601),
            ativo: true,
        };

        novaOpcao.nome = nome.toLowerCase();

        return novaOpcao;
    }

    public sweetAlert(opcoes: any): Promise<any> {
        let settings: any = {
            confirmButtonColor: Cor.VERDE_CLARO,
            cancelButtonColor: Cor.LARANJA,
        };
        swal.setDefaults(settings);

        return swal(opcoes);
    }

    public getIndiceRespostaCriseCor(porcentagem: number): string {
        let corRetorno: string = null;
        if (porcentagem) {
            let corInicial: string;
            let corFinal: string;
            let proporcao: number;

            if (porcentagem <= 50) {
                corInicial = Cor.LARANJA;
                corFinal = Cor.CRISE_FRACA;
                proporcao = (porcentagem * 2) / 100;
            } else {
                corInicial = Cor.CRISE_FRACA;
                corFinal = Cor.VERDE_CLARO;
                proporcao = ((porcentagem - 50) * 2) / 100;
            }

            corRetorno = this.linearInterpolationColor(corInicial, corFinal, proporcao);
        }

        return corRetorno;
    }

    public textToCapitalize(text: string) {
        return (text.charAt(0).toUpperCase() + text.slice(1));
    }

    public getClassificacaoEficiencia(tipoEficiencia: number) {
        let classificacao: string = '';

        switch (tipoEficiencia) {
            case TipoEficiencia.NADA_UTIL:
                classificacao = 'nada útil';
                break;
            case TipoEficiencia.POUCO_UTIL:
                classificacao = 'pouco útil';
                break;
            case TipoEficiencia.UTIL:
                classificacao = 'útil';
                break;
            default:
                console.log('TRACK ERR');
        }

        return classificacao;
    }

    public getPorcentagemCriseCompleta(crise: Crise, usuario: Usuario): number {
        let pesoTotal: number = 0;
        let valorDeparaQuestao: number = 0;

        this._getClassificacaoPesoQuestoes(crise, usuario).forEach(
            (classificacao) => {
                let respondido: number = classificacao.respondido;
                let pesoQuestao: number = classificacao.peso;
                pesoTotal += pesoQuestao;
                valorDeparaQuestao += (pesoQuestao * respondido);
            },
        );

        return (valorDeparaQuestao / pesoTotal) * 100;
    }

    public retirarOpcaoDoArray(array: Array<any>, opcao: Opcao): Array<any> {
        array.every(
            (element: any, indice: number) => {
                if (element.nome === opcao.nome) {
                    array.splice(indice, 1);
                    return false;
                } else {
                    return true;
                }
            },
        );

        return array;
    }

    public toastMensagem(toastCtrl: ToastController, mensagem: string) {
        let toast = toastCtrl.create({
            message: mensagem,
            duration: 3000,
            position: 'bottom',
            showCloseButton: true,
        });

        toast.present();
    }

    private _getClassificacaoPesoQuestoes(crise: Crise, usuario: Usuario): Array<any> {
        let arrayClassificacaoPesoQuestoes: Array<any> = [];

        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.dataInicial ? 0 : 1, peso: 10 },
        );
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.dataFinal ? 0 : 1, peso: 10 },
        );
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.intensidade[0] ? 0 : 1, peso: 10 },
        );
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.medicamentos[0] ? 0 : 1, peso: 10 },
        );
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.sintomas[0] ? 0 : 1, peso: 9 },
        );
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.gatilhos[0] ? 0 : 1, peso: 9 },
        );
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.localizacoesDeDor[0] ? 0 : 1, peso: 8 },
        );

        if (!usuario || usuario.genero === SexoUsuario.UKNOW || usuario.genero === SexoUsuario.FEMININO) {
            arrayClassificacaoPesoQuestoes.push(
                { respondido: !crise.cicloMestrual[0] ? 0 : 1, peso: 7 },
            );
        }

        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.qualidadesDor[0] ? 0 : 1, peso: 6 },
        );
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.metodosAlivio[0] ? 0 : 1, peso: 6 },
        );

        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.consequencias[0] ? 0 : 1, peso: 5 },
        );

        /*
        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.clima[0] ? 0 : 1, peso: 5 },
        );*/

        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.metodosAlivioEficiente[0] ? 0 : 1, peso: 4 },
        );

        arrayClassificacaoPesoQuestoes.push(
            { respondido: !crise.anotacoes ? 0 : 1, peso: 3 },
        );

        return arrayClassificacaoPesoQuestoes;
    }

}
