import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class ValidationHelper {

    static getValidatorErrorMessage(validatorName: string, validatorValue?: any) {
        let config: any = {
            'required': `Informe o campo ${validatorName}`,
            'invalidEmailAddress': 'Informe um e-mail válido',
            'minlength': `Este campo deve ter tamnho mínimo de ${validatorValue.requiredLength} caracteres`,
            'invalidInputNumber': 'Informe somente numeros',
        };

        return config[validatorName];
    }

    static validarFormatoEmail(control: FormControl) {
        // tslint:disable-next-line
        if (control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }

    static validarFormatoEmailNullTrue(control: FormControl) {
        // tslint:disable-next-line
        if (!control.value ||
            control.value.match(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)) {
            return null;
        } else {
            return { 'invalidEmailAddress': true };
        }
    }

    static validarInputNumber(control: FormControl) {
        if (!control.value || control.value.match(/^[0-9]+$/)) {
            return null;
        } else {
            return { 'invalidInputNumber': true };
        }
    }

    static opcaoJaExiste(opcoes: Array<any>, control: FormControl) {

        if (control.value) {
            let error: any = null;
            opcoes.every(
                (opcao: any) => {
                    let nomeSemAcento = ValidationHelper.removerAcentos(control.value.toLowerCase());
                    let opcaoSemAcento = ValidationHelper.removerAcentos(opcao.nome.toLowerCase());
                    if (nomeSemAcento === opcaoSemAcento && opcao.ativo) {
                        error = { 'opcaoJaExiste': true };
                        control.setErrors(error);
                        return false;
                    } else {
                        return true;
                    }
                },
            );

            return error;
        }

        return null;
    }

    static removerAcentos(texto: String) {
        let map: any = {
            'â': 'a', 'Â': 'A', 'à': 'a', 'À': 'A', 'á': 'a', 'Á': 'A', 'ã': 'a', 'Ã': 'A', 'ê': 'e', 'Ê': 'E', 'è': 'e', 'È': 'E', 'é':
            'e', 'É': 'E', 'î': 'i', 'Î': 'I', 'ì': 'i', 'Ì': 'I', 'í': 'i', 'Í': 'I', 'õ': 'o', 'Õ': 'O', 'ô': 'o', 'Ô': 'O', 'ò': 'o', 'Ò':
            'O', 'ó': 'o', 'Ó': 'O', 'ü': 'u', 'Ü': 'U', 'û': 'u', 'Û': 'U', 'ú': 'u', 'Ú': 'U', 'ù': 'u', 'Ù': 'U', 'ç': 'c', 'Ç': 'C',
        };

        return texto.replace(/[\W\[\] ]/g, function (a: any) { return map[a] || a; });
    }

    constructor() { }
}

export interface ValidationResult {
    [key: string]: boolean;
}
