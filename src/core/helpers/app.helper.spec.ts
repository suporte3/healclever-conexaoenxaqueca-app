import { TipoOpcao } from './../models/tipo-opcao.enum';
import { Opcao } from './../models/opcao.model';
import { Usuario } from './../models/usuario/usuario.model';
import { AppHelper } from './app.helper';
import { TestUtils } from './../../app/app.test';
import { async, inject } from '@angular/core/testing';

describe('App Helper:', () => {

    beforeEach(async(() => {

        TestUtils.addProvider(AppHelper);

        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve atulizar dados do usuário', () => {
        let usuarioDados: Usuario = {
            _id: 'idTets',
            dataAdmissao: '',
            email: 'pedro@mymigraine.com',
            first_name: 'Pedro',
            last_name: 'Silva',
            birthday: null,
            genero: 0,
            sessao: {
                expires: '13/10/2018',
                ip: '1923221',
                issued: '01',
                password: 'teste001',
                provider: 'test',
                roles: 'test',
                token: 'test',
                userDBs: 'test',
            },
            sincronizado: false,
            logado: false,
            facebook: false,
        };
        let dadosParaSerAtualizados: any = { logado: true };
        let appHelper: AppHelper = new AppHelper();
        let usuario: Usuario = appHelper.atualizarObjeto(dadosParaSerAtualizados, usuarioDados);
        expect(usuario._id).toEqual(usuarioDados._id);
        expect(usuario.email).toEqual(usuarioDados.email);
        expect(usuario.first_name).toEqual(usuarioDados.first_name);
        expect(usuario.last_name).toEqual(usuarioDados.last_name);
        expect(usuario.birthday).toEqual(usuarioDados.birthday);
        expect(usuario.genero).toEqual(usuarioDados.genero);
        expect(usuario.sincronizado).toEqual(usuarioDados.sincronizado);
        expect(usuario.logado).toEqual(usuarioDados.logado);
        expect(usuario.facebook).toEqual(usuarioDados.facebook);
        expect(usuario.sessao.expires).toEqual(usuarioDados.sessao.expires);
        expect(usuario.sessao.ip).toEqual(usuarioDados.sessao.ip);
        expect(usuario.sessao.issued).toEqual(usuarioDados.sessao.issued);
        expect(usuario.sessao.password).toEqual(usuarioDados.sessao.password);
        expect(usuario.sessao.provider).toEqual(usuarioDados.sessao.provider);
        expect(usuario.sessao.roles).toEqual(usuarioDados.sessao.roles);
        expect(usuario.sessao.token).toEqual(usuarioDados.sessao.token);
        expect(usuario.sessao.userDBs).toEqual(usuarioDados.sessao.userDBs);
    });

    it('#retirarOpcaoDoArray - Deve retirar uma opcao de um array qualquer', inject([AppHelper], (appUtils: AppHelper) => {

        let arrayOpcoes = [{
            nome: 'opcao1',
            ativo: true,
        },
        {
            nome: 'opcao2',
            ativo: true,
        }];

        let opcao: Opcao = {
            _id: 'idOpcao2',
            nome: 'opcao2',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };

        let arraySemOpcao: Array<any> = appUtils.retirarOpcaoDoArray(arrayOpcoes, opcao);
        expect(arraySemOpcao).not.toContain(opcao);
    }));

});
