import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class SyncDataMock {

    static readonly DOCUMENT_OPCOES_MEDICAMENTO_ID = 'ce-1234-ajshs-12n2m-ssjk-skms1';
    static readonly DOCUMENT_OPCOES_QUALIDADE_DOR_ID = 'ce-1234-qualidade-dor-skms1';

    data: any;
    db: any;
    remote: any;
    username: string;
    password: string;
    private _documento: any = null;

    static generateUniqueID(): string {
        return SyncDataMock.gerarCharacteres() + SyncDataMock.gerarCharacteres() + '-' +
            SyncDataMock.gerarCharacteres() + '-' + SyncDataMock.gerarCharacteres() + '-' +
            SyncDataMock.gerarCharacteres() + '-' + SyncDataMock.gerarCharacteres() +
            SyncDataMock.gerarCharacteres() + SyncDataMock.gerarCharacteres();
    }

    static gerarCharacteres(): string {
        let caracteres: string = Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);

        return caracteres;
    }

    constructor(public http: Http) {

    }

    public setDocumento(documento: any) {
        this._documento = documento;
    }

    public getDocumento() {
        return this._documento;
    }

    init() {
        return true;
    }

    addOrUpdate(object: any): Promise<any> {
        let scope = this;
        object = null;
        return new Promise(
            function (resolve, reject) {
                if (!scope.getDocumento()) {
                    resolve();
                } else {
                    reject();
                }
            },
        );
    }

    public createIndex(index: any): Promise<any> {
        index = null;
        return Promise.resolve();
    }

    public find(query: any): Promise<any> {
        query = null;
        return Promise.resolve();
    }

    public deleteDocById(docId: string): Promise<any> {
        let scope = this;
        docId = null;

        return new Promise(
            function (resolve, reject) {
                if (!scope.getDocumento()) {
                    resolve();
                } else {
                    reject();
                }
            },
        );
    }

    getDocumentById(id: string): Promise<any> {
        return new Promise(
            resolve => {
                resolve(id);
            });
    }

    addOrUpdateManyDocuments(manyObjects: any): Promise<any> {

        return new Promise(
            resolve => {
                resolve(manyObjects);
            });
    }
}
