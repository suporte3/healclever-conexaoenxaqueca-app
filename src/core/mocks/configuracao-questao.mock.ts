export class ConfiguracaoQuestaoMock {

    private _modalEventoClique: any = 'TESTE';
    private _possuiEventoClique: boolean = false;
    private _possuiQuantidade: boolean = false;
    private _possuiModal: boolean = false;
    private _identificadorGrid: string = 'TESTE';
    private _questao: any = null;
    private _modalCtrl: any = null;
    private _modalAdicionarOpcaoReferencia: any = null;
    private _documentoOptionId: any = null;
    private _nomeDoObjetoOpcao: string = null;
    private _syncDataServiceReference: any = null;

    public getModalEventoClique(): any {
        return this._modalEventoClique;
    }

    public setModalEventoClique(value: any) {
        this._modalEventoClique = value;
    }

    public getPossuiQuantidade(): boolean {
        return this._possuiQuantidade;
    }

    public setPossuiQuantidade(value: boolean) {
        this._possuiQuantidade = value;
    }

    public getIdentificadorGrid(): string {
        return this._identificadorGrid;
    }

    public setIdentificadorGrid(value: string) {
        this._identificadorGrid = value;
    }

    public getPossuiEventoClique(): boolean {
        return this._possuiEventoClique;
    }

    public setPossuiEventoClique(value: boolean) {
        this._possuiEventoClique = value;
    }

    public getPossuiModal(): boolean {
        return this._possuiModal;
    }

    public setPossuiModal(value: boolean) {
        this._possuiModal = value;
    }

    public getQuestao(): any {
        return this._questao;
    }

    public setQuestao(value: any) {
        this._questao = value;
    }

    public getModalCtrl(): any {
        return this._modalCtrl;
    }

    public setModalCtrl(value: any) {
        this._modalCtrl = value;
    }

    public getModalAdicionarOpcaoReferencia(): any {
        return this._modalAdicionarOpcaoReferencia;
    }

    public setModalAdicionarOpcaoReferencia(value: any) {
        this._modalAdicionarOpcaoReferencia = value;
    }

    public getDocumentoOptionId(): any {
        return this._documentoOptionId;
    }

    public setDocumentoOptionId(value: any) {
        this._documentoOptionId = value;
    }

    public getNomeDoObjetoOpcao(): string {
        return this._nomeDoObjetoOpcao;
    }

    public setNomeDoObjetoOpcao(value: string) {
        this._nomeDoObjetoOpcao = value;
    }

    public getSyncDataServiceReference(): any {
        return this._syncDataServiceReference;
    }

    public setSyncDataServiceReference(value: any) {
        this._syncDataServiceReference = value;
    }
}
