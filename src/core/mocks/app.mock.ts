export class PlatformMock {
    public ready(): any {
        return new Promise((resolve: Function) => {
            resolve();
        });
    }
}

export class FabContainerMock {
    public close(): any {
        return {};
    }

    public toggleList(): any {
        return {};
    }
}

export class ContentMock {
    public resize(): any {
        return {};
    }
}

export class ViewControllerMock {
    public _setHeader(): any {
        return {};
    }

    public _setNavbar(): any {
        return {};
    }

    public _setFooter(): any {
        return {};
    }

    public _setIONContent(): any {
        return {};
    }

    public _setIONContentRef(): any {
        return {};
    }

    public getContent(): any {
        return new ContentMock();
    }

    public dismiss(): any {
        return {};
    }
}

export class EventsMock {
    subscribe(topic: string, ...handlers: Function[]): void {
        topic = '';
        handlers = null;
    }

    unsubscribe(topic: string, handler?: Function): any {
        topic = '';
        handler = null;
        return {};
    }

    publish(): any {
        return {};
    }
}
