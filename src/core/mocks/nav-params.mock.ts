export class NavParamsMock {

  static returnParams: any = null;

  static setParams(value: any) {

    NavParamsMock.returnParams = value;
  }

  public get(key: any): any {
    key = 'default';

    if (NavParamsMock.returnParams) {
      return NavParamsMock.returnParams;
    }

    return key;
  }
}
