import { Cor } from './../../core/models/pojo/cor.pojo';
import { Medicamento } from './../../core/models/medicamento.model';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Opcao } from './../../core/models/opcao.model';
import { Localizacao } from './../../core/models/localizacao.model';
import { ModeloOpcao } from './../../core/models/pojo/opcao.pojo';
import { Opcoes } from './../../core/models/opcoes.model';
import { TipoQuestao } from './../../core/models/pojo/tipo-questao.pojo';
import { TipoOpcao } from './../../core/models/tipo-opcao.enum';
import { ConfiguracaoGrid } from './../../core/models/pojo/configuracao-grid.pojo';
import { Crise } from './../../core/models/crise.model';
import { GridManagerService } from './grid-manager.service';
import { NavParamsMock } from './../../core/mocks/nav-params.mock';
import { NavParams, ModalController, Events } from 'ionic-angular';
import { async, inject } from '@angular/core/testing';
import { TestUtils } from './../../app/app.test';

describe('Grid Manager:', () => {

    beforeEach(async(() => {

        TestUtils.addProvider(
            { provide: NavParams, useClass: NavParamsMock },
        );
        TestUtils.addProvider(GridManagerService);
        TestUtils.addProvider(ModalController);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve setar e buscar a crise', inject([GridManagerService], (gridManager: GridManagerService) => {
        let crise: Crise = {
            _id: 'idCrise',
            dataInicial: null,
            dataFinal: null,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: null,
            clima: null,
            gps: null,
            type: 'crise',
        };

        gridManager.setCrise(crise);
        expect(gridManager.getCrise()).toEqual(crise);
    }));

    it('Deve setar e buscar configuração do grid', inject([GridManagerService], (gridManager: GridManagerService) => {
        let configuracaoDaQuestao: ConfiguracaoGrid = new ConfiguracaoGrid();
        configuracaoDaQuestao.setIdentificadorGrid(TipoQuestao.CONSEQUENCIA);

        gridManager.setConfiguracao(configuracaoDaQuestao);

        expect(gridManager.getConfiguracao()).toEqual(configuracaoDaQuestao);
    }));

    it('Deve setar e buscar a opção do documento', inject([GridManagerService], (gridManager: GridManagerService) => {
        let opcoes: any = {
            _id: '',
            opcoes: [],
        };
        gridManager.setDocumentOptions(opcoes);
        let documentOptions: Opcoes = gridManager.getDocumentOptions();
        expect(gridManager.getDocumentOptions()).toEqual(documentOptions);
    }));

    it('Deve setar e buscar opções selecionadas', inject([GridManagerService], (gridManager: GridManagerService) => {
        let opcaoSelecionada: ModeloOpcao = new ModeloOpcao();
        opcaoSelecionada.setId('opcaoId');
        opcaoSelecionada.setNome('nome opção');

        let arrayModeloOpcaoSelecionadas: Array<ModeloOpcao> = [];
        arrayModeloOpcaoSelecionadas.push(opcaoSelecionada);

        gridManager.setOpcoesSelecionadas(arrayModeloOpcaoSelecionadas);

        expect(gridManager.getOpcoesSelecionadas()).toEqual(arrayModeloOpcaoSelecionadas);
    }));

    it('Deve setar e buscar opções de locais fixo', inject([GridManagerService], (gridManager: GridManagerService) => {
        let valorGPS: Localizacao = {
            titulo: 'Casa',
            descricao: 'Rua x, Bairro y ...',
            latitude: 0,
            longitude: 0,
        };

        let localFixo: ModeloOpcao = new ModeloOpcao();
        localFixo.setId('opcaoId');
        localFixo.setNome('nome opção');
        localFixo.setValorGPS(valorGPS);

        let arrayOpcoesLocaisFixo: Array<ModeloOpcao> = [];
        arrayOpcoesLocaisFixo.push(localFixo);

        gridManager.setOpcoesLocalFixo(arrayOpcoesLocaisFixo);

        expect(gridManager.getOpcoesLocalFixo()).toEqual(arrayOpcoesLocaisFixo);
    }));

    it('Deve setar e buscar opções de locais fixo do banco de dados', inject([GridManagerService], (gridManager: GridManagerService) => {
        let valorGPS: Localizacao = {
            titulo: 'Casa',
            descricao: 'Rua x, Bairro y ...',
            latitude: 0,
            longitude: 0,
        };

        let localFixo: ModeloOpcao = new ModeloOpcao();
        localFixo.setId('opcaoId');
        localFixo.setNome('nome opção');
        localFixo.setValorGPS(valorGPS);

        let arrayOpcoesLocaisFixo: Array<ModeloOpcao> = [];
        arrayOpcoesLocaisFixo.push(localFixo);

        gridManager.setOpcoesLocalFixoDatabase(arrayOpcoesLocaisFixo);

        expect(gridManager.getOpcoesLocalFixoDatabase()).toEqual(arrayOpcoesLocaisFixo);
    }));

    it('Deve duplicar opções ativas', inject([GridManagerService], (gridManager: GridManagerService) => {
        let documentOptions: Opcoes = {
            _id: null,
            opcoes: [],
        };
        let opcao1: Opcao = {
            _id: 'idOpcao1',
            nome: 'nomeOpcao1',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };
        let opcao2: Opcao = {
            _id: 'idOpcao2',
            nome: 'nomeOpcao2',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        let opcao3: Opcao = {
            _id: 'idOpcao3',
            nome: 'nomeOpcao3',
            dataCriacao: '14/02/2017',
            ativo: false,
            tipo: TipoOpcao.FIXO,
        };
        documentOptions.opcoes.push(opcao1);
        documentOptions.opcoes.push(opcao2);
        documentOptions.opcoes.push(opcao3);

        gridManager.setDocumentOptions(documentOptions);

        let opcoesAtivas = gridManager.duplicarOpcoesAtivas();
        expect(opcoesAtivas).not.toContain(opcao3);
        expect(opcoesAtivas.length).toEqual(2);
    }));

    it('Deve verificar se a opção está selecionada nos dados de crise ao iniciar questão', inject([GridManagerService], (gridManager: any) => {
        let configuracao: ConfiguracaoGrid = new ConfiguracaoGrid();
        configuracao.setNomeDoObjetoOpcao('sintomas');
        gridManager.setConfiguracao(configuracao);

        let crise: Crise = {
            _id: null,
            dataInicial: null,
            dataFinal: null,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: null,
            clima: null,
            gps: null,
            type: 'crise',
        };

        let opcao: Opcao = {
            _id: 'idSintoma1',
            nome: 'sintoma',
            dataCriacao: '13-05-2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };
        crise.sintomas.push(opcao);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idSintoma1');
        modeloOpcao.setNome('sintoma');
        modeloOpcao.setDataCriacao('13-05-2017');
        modeloOpcao.setAtivo(true);

        gridManager.setCrise(crise);
        gridManager._iniciouQuestao = false;
        spyOn(gridManager, '_verificarSelecaoViaCrise').and.stub();
        gridManager.seOpcaoEstaSelecionada(modeloOpcao);
        expect(gridManager._verificarSelecaoViaCrise).toHaveBeenCalledWith(modeloOpcao);
    }));

    it('Deve verificar se a opção está selecionada nos dados das opções selecionadas ao atualizar questão', inject([GridManagerService], (gridManager: any) => {
        gridManager._iniciouQuestao = true;
        gridManager._needRefresh = false;

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idSintoma1');
        modeloOpcao.setNome('sintoma');
        modeloOpcao.setDataCriacao('13-05-2017');
        modeloOpcao.setAtivo(true);

        spyOn(gridManager, '_verificarSelecaoViaArraySelecionadas').and.stub();
        gridManager.seOpcaoEstaSelecionada(modeloOpcao);
        expect(gridManager._verificarSelecaoViaArraySelecionadas).toHaveBeenCalledWith(modeloOpcao);
    }));

    it('Deve verificar se a opção está selecionada nos dados de crise ao atualizar opção do tipo subgatilho na questão gatilho', inject([GridManagerService], (gridManager: any) => {
        let configuracao: ConfiguracaoGrid = new ConfiguracaoGrid();
        configuracao.setIdentificadorGrid(TipoQuestao.GATILHO);
        configuracao.setNomeDoObjetoOpcao('sintomas');
        gridManager.setConfiguracao(configuracao);

        gridManager._iniciouQuestao = true;
        gridManager._needRefresh = true;

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('mm-gatilho-esforco-fisico');
        modeloOpcao.setNome('esforço físico');
        modeloOpcao.setDataCriacao('13-05-2017');
        modeloOpcao.setAtivo(true);

        spyOn(gridManager, '_verificarSelecaoViaCrise').and.stub();
        gridManager.seOpcaoEstaSelecionada(modeloOpcao);
        expect(gridManager._verificarSelecaoViaCrise).toHaveBeenCalledWith(modeloOpcao);
    }));

    it('#_verificarSelecaoViaCrise - Deve verificar nos dados de crise e array selecionadas ao atualizar questão', inject([GridManagerService], (gridManager: any) => {
        let isSelecionado: boolean = false;

        let configuracao: ConfiguracaoGrid = new ConfiguracaoGrid();
        configuracao.setNomeDoObjetoOpcao('gatilhos');
        gridManager.setConfiguracao(configuracao);

        let crise: Crise = {
            _id: null,
            dataInicial: null,
            dataFinal: null,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: null,
            clima: null,
            gps: null,
            type: 'crise',
        };
        let opcao: Opcao = {
            _id: 'idGatilho1',
            nome: 'gatilho',
            dataCriacao: '13-05-2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };
        crise.gatilhos.push(opcao);
        gridManager.setCrise(crise);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idGatilho1');

        gridManager._needRefresh = true;
        let spyOnSelecionadasPush: jasmine.Spy = spyOn(gridManager.getOpcoesSelecionadas(), 'push').and.stub();
        isSelecionado = gridManager._verificarSelecaoViaCrise(modeloOpcao);
        expect(isSelecionado).toBe(true);
        expect(spyOnSelecionadasPush).toHaveBeenCalledWith(modeloOpcao);

        //  Teste 2 - com array selecionadas
        let opcao1: ModeloOpcao = new ModeloOpcao();
        opcao1.setId('idTeste');
        opcao1.setNome('opcao1');
        opcao1.setDataCriacao('13/10/2017');
        opcao1.setAtivo(true);

        let opcao2: ModeloOpcao = new ModeloOpcao();
        opcao2.setId('idGatilho');
        opcao2.setNome('gatilho');
        opcao2.setDataCriacao('20/10/2017');
        opcao2.setAtivo(true);

        let arrayOpcoesSelecionadas: Array<ModeloOpcao> = [];
        arrayOpcoesSelecionadas.push(opcao1);
        arrayOpcoesSelecionadas.push(opcao2);
        gridManager.setOpcoesSelecionadas(arrayOpcoesSelecionadas);

        spyOnSelecionadasPush.calls.reset();
        isSelecionado = false;
        isSelecionado = gridManager._verificarSelecaoViaCrise(modeloOpcao);
        expect(isSelecionado).toBe(true);
        expect(spyOnSelecionadasPush).not.toHaveBeenCalledWith(modeloOpcao);
    }));

    it('#_verificarSelecaoViaCrise - Deve verificar apenas nos dados de crise quando não precisar atualizar questão', inject([GridManagerService], (gridManager: any) => {
        let isSelecionado: boolean = false;

        let configuracao: ConfiguracaoGrid = new ConfiguracaoGrid();
        configuracao.setNomeDoObjetoOpcao('gatilhos');
        gridManager.setConfiguracao(configuracao);

        let crise: Crise = {
            _id: null,
            dataInicial: null,
            dataFinal: null,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: null,
            clima: null,
            gps: null,
            type: 'crise',
        };
        let opcao: Opcao = {
            _id: 'idGatilho1',
            nome: 'gatilho',
            dataCriacao: '13-05-2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };
        crise.gatilhos.push(opcao);
        gridManager.setCrise(crise);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idGatilho1');

        gridManager._needRefresh = true;
        spyOn(gridManager.getOpcoesSelecionadas(), 'push').and.stub();
        isSelecionado = gridManager._verificarSelecaoViaCrise(modeloOpcao);
        expect(isSelecionado).toBe(true);
        expect(gridManager.getOpcoesSelecionadas().push).toHaveBeenCalledWith(modeloOpcao);
    }));

    it('Deve verificar se opção está selecionada no array selecionadas', inject([GridManagerService], (gridManager: any) => {
        let isSelecionado: boolean = false;
        let arrayOpcoesSelecionadas: Array<ModeloOpcao> = [];

        let opcao1: ModeloOpcao = new ModeloOpcao();
        opcao1.setId('idTeste');
        opcao1.setNome('opcao1');
        opcao1.setDataCriacao('13/10/2017');
        opcao1.setAtivo(true);

        let opcao2: ModeloOpcao = new ModeloOpcao();
        opcao2.setId('idTeste2');
        opcao2.setNome('opcao2');
        opcao2.setDataCriacao('20/10/2017');
        opcao2.setAtivo(true);

        arrayOpcoesSelecionadas.push(opcao1);
        arrayOpcoesSelecionadas.push(opcao2);
        gridManager.setOpcoesSelecionadas(arrayOpcoesSelecionadas);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idTeste2');

        isSelecionado = gridManager._verificarSelecaoViaArraySelecionadas(modeloOpcao);
        expect(isSelecionado).toBe(true);
    }));

    it('Deve atualizar todas as opções do grid', inject([GridManagerService], (gridManager: GridManagerService) => {
        spyOn(gridManager, 'montarOpcoesDoGrid').and.callThrough();
        spyOn(gridManager, 'montarGrid').and.stub();

        let configuracao: ConfiguracaoGrid = new ConfiguracaoGrid();
        configuracao.setNomeDoObjetoOpcao('gatilhos');
        gridManager.setConfiguracao(configuracao);

        let documentOptions: Opcoes = {
            _id: null,
            opcoes: [],
        };
        let opcao1: Opcao = {
            _id: 'idOpcao1',
            nome: 'nomeOpcao1',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };
        let opcao2: Opcao = {
            _id: 'idOpcao2',
            nome: 'nomeOpcao2',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        let opcao3: Opcao = {
            _id: 'idOpcao3',
            nome: 'nomeOpcao3',
            dataCriacao: '14/02/2017',
            ativo: false,
            tipo: TipoOpcao.FIXO,
        };
        documentOptions.opcoes.push(opcao1);
        documentOptions.opcoes.push(opcao2);
        documentOptions.opcoes.push(opcao3);

        gridManager.setDocumentOptions(documentOptions);

        let arrayOpcoesDoGrid: Array<ModeloOpcao> = [];

        let modeloOpcao1: ModeloOpcao = new ModeloOpcao();
        modeloOpcao1.setId(opcao1._id);
        modeloOpcao1.setNome('nomeOpcao1a');
        modeloOpcao1.setDataCriacao(opcao1.dataCriacao);
        modeloOpcao1.setAtivo(true);

        let modeloOpcao2: ModeloOpcao = new ModeloOpcao();
        modeloOpcao2.setId(opcao2._id);
        modeloOpcao2.setNome('nomeOpcao2a');
        modeloOpcao2.setDataCriacao(opcao1.dataCriacao);
        modeloOpcao2.setAtivo(true);

        arrayOpcoesDoGrid.push(modeloOpcao1);
        arrayOpcoesDoGrid.push(modeloOpcao2);
        gridManager.arrayOpcoesDoGrid = arrayOpcoesDoGrid;

        gridManager.atualizarOpcoes();
        expect(gridManager.montarOpcoesDoGrid).toHaveBeenCalled();
        expect(gridManager.montarGrid).toHaveBeenCalled();
        expect(gridManager.arrayOpcoesDoGrid[0].getNome()).toEqual('nomeOpcao1');
        expect(gridManager.arrayOpcoesDoGrid[1].getNome()).toEqual('nomeOpcao2');
    }));

    it('#atualizarOpcaoDoGrid - Deve atualizar apenas uma opção do grid', inject([GridManagerService], (gridManager: GridManagerService) => {
        let arrayOpcoesDoGrid: Array<ModeloOpcao> = [];

        let modeloOpcao1: ModeloOpcao = new ModeloOpcao();
        modeloOpcao1.setId('opcao1');
        modeloOpcao1.setNome('nomeOpcao1');
        modeloOpcao1.setDataCriacao('13-03-2017');
        modeloOpcao1.setAtivo(true);

        let modeloOpcao2: ModeloOpcao = new ModeloOpcao();
        modeloOpcao2.setId('opcao2');
        modeloOpcao2.setNome('nomeOpcao2a');
        modeloOpcao2.setDataCriacao('13-03-2017');
        modeloOpcao2.setAtivo(true);

        arrayOpcoesDoGrid.push(modeloOpcao1);
        arrayOpcoesDoGrid.push(modeloOpcao2);
        gridManager.arrayOpcoesDoGrid = arrayOpcoesDoGrid;

        let modeloOpcaoParaAtualizar: ModeloOpcao = new ModeloOpcao();
        modeloOpcaoParaAtualizar.setId('opcao2');
        modeloOpcaoParaAtualizar.setNome('nomeOpcao2');
        modeloOpcaoParaAtualizar.setAtivo(false);

        spyOn(gridManager, 'montarGrid').and.stub();
        gridManager.atualizarOpcaoDoGrid(modeloOpcaoParaAtualizar);
        expect(gridManager.arrayOpcoesDoGrid[1].getNome()).toBe(modeloOpcaoParaAtualizar.getNome());
        expect(gridManager.arrayOpcoesDoGrid[1].getAtivo()).toBe(false);
        expect(gridManager.montarGrid).toHaveBeenCalled();
    }));

    it('Deve remover a opção', inject([GridManagerService], (gridManager: GridManagerService) => {
        spyOn(gridManager, 'atualizarOpcoes').and.stub();
        spyOn(SyncDataProvider.prototype, 'addOrUpdateManyDocuments').and.stub();
        gridManager.setConfiguracao(new ConfiguracaoGrid());
        let documentOptions: Opcoes = {
            _id: null,
            opcoes: [],
        };

        let opcao1: Opcao = {
            _id: 'idOpcao1',
            nome: 'nomeOpcao1',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };

        let opcao2: Opcao = {
            _id: 'idOpcao2',
            nome: 'nomeOpcao2',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };

        let opcao3: Opcao = {
            _id: 'idOpcao3',
            nome: 'nomeOpcao3',
            dataCriacao: '14/02/2017',
            ativo: false,
            tipo: TipoOpcao.FIXO,
        };

        documentOptions.opcoes.push(opcao1);
        documentOptions.opcoes.push(opcao2);
        documentOptions.opcoes.push(opcao3);

        gridManager.getCrise().medicamentos = documentOptions.opcoes;
        gridManager.setDocumentOptions(documentOptions);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idOpcao1');
        modeloOpcao.setNome('opcao1');
        modeloOpcao.setDataCriacao('13/10/2017');
        modeloOpcao.setAtivo(true);
        modeloOpcao.setTipo(TipoOpcao.DINAMICO);
        let opcao: Opcao = {
            _id: modeloOpcao.getId(),
            nome: modeloOpcao.getNome(),
            dataCriacao: modeloOpcao.getDataCriacao(),
            ativo: modeloOpcao.getAtivo(),
            tipo: modeloOpcao.getTipo(),
        };

        gridManager.getConfiguracao().setNomeDoObjetoOpcao('medicamentos');
        gridManager.getConfiguracao().setSyncDataServiceReference(new SyncDataProvider(null, null, null));
        gridManager.removerOpcao(modeloOpcao);

        expect(gridManager.getDocumentOptions().opcoes).not.toContain(opcao);
        expect(gridManager.getCrise()[gridManager.getConfiguracao().getNomeDoObjetoOpcao()]).not.toContain(opcao);
    }));

    it('Deve retirar uma opcao do array de opções selecionadas', inject([GridManagerService], (gridManager: GridManagerService) => {
        let arrayOpcoesSelecionadas: Array<ModeloOpcao> = [];

        let opcao1: ModeloOpcao = new ModeloOpcao();
        opcao1.setId('idOpcao1');
        opcao1.setNome('opcao1');
        opcao1.setDataCriacao('13/10/2017');
        opcao1.setAtivo(true);

        let opcao2: ModeloOpcao = new ModeloOpcao();
        opcao2.setId('idOpcao2');
        opcao2.setNome('opcao2');
        opcao2.setDataCriacao('20/10/2017');
        opcao2.setAtivo(true);

        arrayOpcoesSelecionadas.push(opcao1);
        arrayOpcoesSelecionadas.push(opcao2);

        gridManager.setOpcoesSelecionadas(arrayOpcoesSelecionadas);

        let opcao: ModeloOpcao = new ModeloOpcao();
        opcao.setId('idOpcao2');
        opcao.setNome('opcao2');
        opcao.setDataCriacao('20/10/2017');
        opcao.setAtivo(true);
        opcao.setTipo(TipoOpcao.DINAMICO);

        gridManager.retirarModeloOpcaoDoArraySelecionado(opcao);

        expect(gridManager.getOpcoesSelecionadas()).not.toContain(opcao);
    }));

    it('Deve atualizar status da opção', inject([GridManagerService], (gridManager: GridManagerService) => {

        let arrayOpcoes = [{
            nome: 'opcao1',
            ativo: true,
        },
        {
            nome: 'opcao2',
            ativo: true,
        }];

        let opcao: ModeloOpcao = new ModeloOpcao();
        opcao.setId('idOpcao1');
        opcao.setNome('opcao1');
        opcao.setDataCriacao('13/10/2017');
        opcao.setAtivo(true);
        opcao.setTipo(TipoOpcao.DINAMICO);

        let opcoes = gridManager.atualizarStatusOpcao(arrayOpcoes, opcao);
        expect(opcoes[0].ativo).toBeFalsy();
    }));

    it('Deve montar opções do grid - CicloMenstrualPage', inject(
        [GridManagerService], (gridManager: any) => {

            let configuracaoQuestao: ConfiguracaoGrid = new ConfiguracaoGrid();
            configuracaoQuestao.setIdentificadorGrid(TipoQuestao.CICLO_MENSTRUAL);
            configuracaoQuestao.setPossuiModal(false);
            // configuracaoQuestao.setQuestao(cicloMenstrual);
            configuracaoQuestao.setPossuiEficiencia(false);

            gridManager.setConfiguracao(configuracaoQuestao);

            let opcao1: Opcao = {
                _id: 'idOpcao1',
                nome: 'opcao1',
                dataCriacao: '11/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let opcao2: Opcao = {
                _id: 'idOpcao2',
                nome: 'opcao2',
                dataCriacao: '12/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let opcao3: Opcao = {
                _id: 'idOpcao3',
                nome: 'opcao3',
                dataCriacao: '13/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let arrayOpcoes: Array<Opcao> = [];

            arrayOpcoes.push(opcao1);
            arrayOpcoes.push(opcao2);
            arrayOpcoes.push(opcao3);

            spyOn(gridManager, 'definirCorOpcao');
            let spyDefinirIcones: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirIcones');
            let spyModeloSetId: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setId');
            let spyModeloSetNome: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setNome');
            let spyModeloSetAtivo: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setAtivo');
            let spyModeloSetTipo: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setTipo');
            let spyModeloDefinirQuantidadeMedicamento: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirQuantidadeMedicamento');
            let spyModeloDefinirGPS: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirOpcaoGPS');
            spyOn(gridManager, '_setViewMetodoEficiencia').and.stub();
            spyOn(gridManager, '_addBotoesModal').and.stub();

            gridManager.montarOpcoesDoGrid(arrayOpcoes);
            expect(spyModeloDefinirQuantidadeMedicamento).not.toHaveBeenCalled();
            expect(gridManager.arrayOpcoesDoGrid.length).toEqual(arrayOpcoes.length);
            expect(spyDefinirIcones).toHaveBeenCalledWith(TipoQuestao.CICLO_MENSTRUAL);
            expect(spyModeloSetId).toHaveBeenCalled();
            expect(spyModeloSetNome).toHaveBeenCalled();
            expect(spyModeloSetAtivo).toHaveBeenCalled();
            expect(spyModeloSetTipo).toHaveBeenCalled();
            expect(gridManager.definirCorOpcao).toHaveBeenCalled();
            expect(gridManager._setViewMetodoEficiencia).not.toHaveBeenCalled();
            expect(gridManager._addBotoesModal).not.toHaveBeenCalled();
            expect(spyModeloDefinirQuantidadeMedicamento).not.toHaveBeenCalled();
            expect(spyModeloDefinirGPS).not.toHaveBeenCalled();
        }),
    );

    it('Deve montar opções do grid com modal', inject(
        [GridManagerService], (gridManager: any) => {

            let configuracaoQuestao: ConfiguracaoGrid = new ConfiguracaoGrid();
            configuracaoQuestao.setIdentificadorGrid(TipoQuestao.CICLO_MENSTRUAL);
            configuracaoQuestao.setPossuiModal(true);
            gridManager.setConfiguracao(configuracaoQuestao);

            let opcao1: Opcao = {
                _id: 'idOpcao1',
                nome: 'opcao1',
                dataCriacao: '11/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let opcao2: Opcao = {
                _id: 'idOpcao2',
                nome: 'opcao2',
                dataCriacao: '12/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let opcao3: Opcao = {
                _id: 'idOpcao3',
                nome: 'opcao3',
                dataCriacao: '13/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let arrayOpcoes: Array<Opcao> = [];

            arrayOpcoes.push(opcao1);
            arrayOpcoes.push(opcao2);
            arrayOpcoes.push(opcao3);

            spyOn(gridManager, '_addBotoesModal').and.stub();
            gridManager.montarOpcoesDoGrid(arrayOpcoes);
            expect(gridManager._addBotoesModal).toHaveBeenCalled();
        }),
    );

    it('Deve montar opções do grid para Método Alívio Eficiente', inject(
        [GridManagerService], (gridManager: any) => {

            let configuracaoQuestao: ConfiguracaoGrid = new ConfiguracaoGrid();
            configuracaoQuestao.setIdentificadorGrid(TipoQuestao.METODO_ALIVIO_EFICIENTE);
            configuracaoQuestao.setPossuiModal(true);
            configuracaoQuestao.setPossuiEficiencia(true);
            gridManager.setConfiguracao(configuracaoQuestao);

            let metodo1: Opcao = {
                _id: 'idOpcao1',
                nome: 'opcao1',
                dataCriacao: '11/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };
            let metodo2: Opcao = {
                _id: 'idOpcao2',
                nome: 'opcao2',
                dataCriacao: '12/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };
            let metodo3: Opcao = {
                _id: 'idOpcao3',
                nome: 'opcao3',
                dataCriacao: '13/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let metodosAlivio: Array<Opcao> = [];

            metodosAlivio.push(metodo1);
            metodosAlivio.push(metodo2);
            metodosAlivio.push(metodo3);

            let medicamento1: Medicamento = {
                _id: 'med1',
                nome: 'tylenol',
                quantidade: 1,
                dataCriacao: '13/07/2017',
            };
            let medicamento2: Medicamento = {
                _id: 'med2',
                nome: 'propanolol',
                quantidade: 1,
                dataCriacao: '14/07/2017',
            };

            let medicamentos: Array<Medicamento> = [];
            medicamentos.push(medicamento1);
            medicamentos.push(medicamento2);

            gridManager.getCrise().metodosAlivio = metodosAlivio;
            gridManager.getCrise().medicamentos = medicamentos;

            spyOn(gridManager, '_montarMedicamentosOuMetodosAlivio').and.returnValue([]);
            spyOn(gridManager, 'montarGrid').and.stub();
            gridManager.montarGridMetodoAlivio();
            expect(gridManager._montarMedicamentosOuMetodosAlivio).toHaveBeenCalled();
            expect(gridManager.getConfiguracao().getIdentificadorGrid()).toEqual(TipoQuestao.METODO_ALIVIO_EFICIENTE);
            expect(gridManager.montarGrid).toHaveBeenCalled();
        }),
    );

    it('Deve montar opções do grid para Medicamento', inject(
        [GridManagerService], (gridManager: GridManagerService) => {

            let _gridManager: any = gridManager;

            let configuracaoQuestao: ConfiguracaoGrid = new ConfiguracaoGrid();
            configuracaoQuestao.setIdentificadorGrid(TipoQuestao.MEDICAMENTO);
            configuracaoQuestao.setDocumentoOptionId(SyncDataProvider.DOCUMENT_OPCOES_MEDICAMENTO_ID);
            configuracaoQuestao.setPossuiModal(true);
            // configuracaoQuestao.setQuestao(medicamentoPage);

            gridManager.setEvents(new Events());
            gridManager.setConfiguracao(configuracaoQuestao);

            let medicamento: Medicamento = {
                _id: 'med1',
                nome: 'tylenol',
                quantidade: 1,
                dataCriacao: '13-07-2017',
            };
            let medicamento1: Medicamento = {
                _id: 'med2',
                nome: 'propanolol',
                quantidade: 2,
                dataCriacao: '14-07-2017',
            };

            gridManager.getCrise().medicamentos = [medicamento, medicamento1];

            let opcao1: Opcao = {
                _id: 'med1',
                nome: 'tylenol',
                dataCriacao: '13-07-2017',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };
            let opcao2: Opcao = {
                _id: 'idOpcao2',
                nome: 'opcao2',
                dataCriacao: '12-12-2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };
            let opcao3: Opcao = {
                _id: 'med2',
                nome: 'propanolol',
                dataCriacao: '14-07-2017',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };
            let arrayOpcoes: Array<Opcao> = [];
            arrayOpcoes.push(opcao1);
            arrayOpcoes.push(opcao2);
            arrayOpcoes.push(opcao3);

            let spyDefinirCorOpcao: jasmine.Spy = spyOn(gridManager, 'definirCorOpcao');
            let spyDefinirIcones: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirIcones');
            let spyModeloSetId: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setId');
            let spyModeloSetNome: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setNome');
            let spyModeloSetAtivo: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setAtivo');
            let spyModeloSetTipo: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setTipo');
            let spyModeloDefinirQuantidadeMedicamento: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirQuantidadeMedicamento');
            let spyModeloDefinirGPS: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirOpcaoGPS');
            let spySetViewMetodoEficiencia: jasmine.Spy = spyOn(_gridManager, '_setViewMetodoEficiencia').and.stub();
            let spyAddBtnModal: jasmine.Spy = spyOn(_gridManager, '_addBotoesModal').and.stub();
            let spyEventsPublish: jasmine.Spy = spyOn(gridManager.getEvents(), 'publish');

            gridManager.montarOpcoesDoGrid(arrayOpcoes);
            expect(spyModeloDefinirQuantidadeMedicamento).toHaveBeenCalled();
            expect(spyDefinirIcones).toHaveBeenCalledWith(TipoQuestao.MEDICAMENTO);
            expect(spyModeloSetId).toHaveBeenCalled();
            expect(spyModeloSetNome).toHaveBeenCalled();
            expect(spyModeloSetAtivo).toHaveBeenCalled();
            expect(spyModeloSetTipo).toHaveBeenCalled();
            expect(spyDefinirCorOpcao).toHaveBeenCalled();
            expect(spySetViewMetodoEficiencia).not.toHaveBeenCalled();
            expect(spyAddBtnModal).toHaveBeenCalled();
            expect(spyModeloDefinirGPS).not.toHaveBeenCalled();
            expect(spyEventsPublish).not.toHaveBeenCalled();

            spyModeloDefinirQuantidadeMedicamento.calls.reset();
            spyDefinirIcones.calls.reset();
            spyModeloSetId.calls.reset();
            spyModeloSetNome.calls.reset();
            spyModeloSetAtivo.calls.reset();
            spyModeloSetTipo.calls.reset();
            spyDefinirCorOpcao.calls.reset();
            spySetViewMetodoEficiencia.calls.reset();
            spyAddBtnModal.calls.reset();
            spyModeloDefinirGPS.calls.reset();
            spyEventsPublish.calls.reset();
            gridManager.montarOpcoesDoGrid([]);
            expect(spyModeloDefinirQuantidadeMedicamento).not.toHaveBeenCalled();
            expect(spyDefinirIcones).not.toHaveBeenCalledWith(TipoQuestao.MEDICAMENTO);
            expect(spyModeloSetId).not.toHaveBeenCalled();
            expect(spyModeloSetNome).not.toHaveBeenCalled();
            expect(spyModeloSetAtivo).not.toHaveBeenCalled();
            expect(spyModeloSetTipo).not.toHaveBeenCalled();
            expect(spyDefinirCorOpcao).not.toHaveBeenCalled();
            expect(spySetViewMetodoEficiencia).not.toHaveBeenCalled();
            expect(spyAddBtnModal).toHaveBeenCalled();
            expect(spyModeloDefinirGPS).not.toHaveBeenCalled();
            expect(spyEventsPublish).toHaveBeenCalledWith('medicamento:onPrimeiroCadastroMedicamento', true);
        }),
    );

    it('Deve montar opções do grid - LocalPage', inject(
        [GridManagerService], (gridManager: any) => {

            let configuracaoQuestao: ConfiguracaoGrid = new ConfiguracaoGrid();
            configuracaoQuestao.setIdentificadorGrid(TipoQuestao.LOCAL);
            configuracaoQuestao.setPossuiModal(true);

            gridManager.setConfiguracao(configuracaoQuestao);

            let opcao1: Opcao = {
                _id: 'idOpcao1',
                nome: 'opcao1',
                dataCriacao: '11/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let opcao2: Opcao = {
                _id: 'idOpcao2',
                nome: 'opcao2',
                dataCriacao: '12/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let opcao3: Opcao = {
                _id: 'idOpcao3',
                nome: 'opcao3',
                dataCriacao: '13/12/2012',
                ativo: true,
                tipo: TipoOpcao.FIXO,
            };

            let arrayOpcoes: Array<Opcao> = [];

            arrayOpcoes.push(opcao1);
            arrayOpcoes.push(opcao2);
            arrayOpcoes.push(opcao3);

            spyOn(gridManager, 'definirCorOpcao');
            let spyDefinirIcones: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirIcones');
            let spyModeloSetId: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setId');
            let spyModeloSetNome: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setNome');
            let spyModeloSetAtivo: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setAtivo');
            let spyModeloSetTipo: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setTipo');
            let spyModeloDefinirQuantidadeMedicamento: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirQuantidadeMedicamento');
            let spyModeloDefinirGPS: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirOpcaoGPS');
            spyOn(gridManager, '_setViewMetodoEficiencia').and.stub();
            spyOn(gridManager, '_addBotoesModal').and.stub();

            gridManager.montarOpcoesDoGrid(arrayOpcoes);
            expect(gridManager.arrayOpcoesDoGrid.length).toEqual(arrayOpcoes.length);
            expect(spyModeloDefinirQuantidadeMedicamento).not.toHaveBeenCalled();
            expect(spyDefinirIcones).toHaveBeenCalledWith(TipoQuestao.LOCAL);
            expect(spyModeloSetId).toHaveBeenCalled();
            expect(spyModeloSetNome).toHaveBeenCalled();
            expect(spyModeloSetAtivo).toHaveBeenCalled();
            expect(spyModeloSetTipo).toHaveBeenCalled();
            expect(gridManager.definirCorOpcao).toHaveBeenCalled();
            expect(gridManager._setViewMetodoEficiencia).not.toHaveBeenCalled();
            expect(gridManager._addBotoesModal).toHaveBeenCalled();
            expect(spyModeloDefinirQuantidadeMedicamento).not.toHaveBeenCalled();
            expect(spyModeloDefinirGPS).toHaveBeenCalled();
        }),
    );

    it('#_addBotoesModal - Deve adicionar botões modal, add e excluir', inject([GridManagerService], (gridManager: any) => {

        gridManager._addBotoesModal();

        let opcaoAdicionar: ModeloOpcao = new ModeloOpcao();
        opcaoAdicionar.setId('id-adicionar-opcao');
        opcaoAdicionar.setIcone('mm-opcao-adicionar');
        opcaoAdicionar.setNome('adicionar');
        gridManager.definirCorOpcao(opcaoAdicionar);

        let opcaoRemover: ModeloOpcao = new ModeloOpcao();
        opcaoRemover.setId('id-remover-opcao');
        opcaoRemover.setIcone('mm-opcao-excluir');
        opcaoRemover.setCor(Cor.LARANJA);
        opcaoRemover.setNome('excluir');

        expect(gridManager.arrayOpcoesDoGrid).toContain(opcaoAdicionar);
        expect(gridManager.arrayOpcoesDoGrid).toContain(opcaoRemover);
    }));

    it('Deve montar o grid', inject([GridManagerService], (gridManager: GridManagerService) => {

        let modeloOpcao1: ModeloOpcao = new ModeloOpcao();
        modeloOpcao1.setId('idModeloOpcao1');
        modeloOpcao1.setNome('modeloOpcao1');
        modeloOpcao1.setDataCriacao('13/10/2017');
        modeloOpcao1.setAtivo(true);
        modeloOpcao1.setTipo(TipoOpcao.FIXO);
        modeloOpcao1.setQuantidade(2);

        let modeloOpcao2: ModeloOpcao = new ModeloOpcao();
        modeloOpcao2.setId('idModeloOpcao2');
        modeloOpcao2.setNome('modeloOpcao2');
        modeloOpcao2.setDataCriacao('14/10/2017');
        modeloOpcao2.setAtivo(true);
        modeloOpcao2.setTipo(TipoOpcao.DINAMICO);
        modeloOpcao2.setQuantidade(1);

        let modeloOpcao3: ModeloOpcao = new ModeloOpcao();
        modeloOpcao3.setId('idModeloOpcao3');
        modeloOpcao3.setNome('modeloOpcao3');
        modeloOpcao3.setDataCriacao('15/10/2017');
        modeloOpcao3.setAtivo(true);
        modeloOpcao3.setTipo(TipoOpcao.DINAMICO);
        modeloOpcao3.setQuantidade(1);

        let modeloOpcao4: ModeloOpcao = new ModeloOpcao();
        modeloOpcao4.setId('idModeloOpcao4');
        modeloOpcao4.setNome('modeloOpcao4');
        modeloOpcao4.setDataCriacao('20/10/2017');
        modeloOpcao4.setAtivo(true);
        modeloOpcao4.setTipo(TipoOpcao.DINAMICO);
        modeloOpcao4.setQuantidade(1);

        gridManager.arrayOpcoesDoGrid.push(modeloOpcao1);
        gridManager.arrayOpcoesDoGrid.push(modeloOpcao2);
        gridManager.arrayOpcoesDoGrid.push(modeloOpcao3);
        gridManager.arrayOpcoesDoGrid.push(modeloOpcao4);
        gridManager.montarGrid();
        expect(gridManager.bindOpcoesDoGrid.length).toBe(Math.ceil(gridManager.arrayOpcoesDoGrid.length / 3));
    }));

    it('Deve definir cor de uma opção adicionar em modo remover', inject([GridManagerService], (gridManager: GridManagerService) => {

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('id-adicionar-opcao');
        modeloOpcao.setNome('adicionar');
        modeloOpcao.setIcone('mm-opcao-adicionar');

        gridManager.bindIsModoRemover = true;
        gridManager.definirCorOpcao(modeloOpcao);
        expect(modeloOpcao.getCor()).toBe(Cor.CINZA);
    }));

    it('Deve definir cor de uma opção adicionar em modo cadastro', inject([GridManagerService], (gridManager: GridManagerService) => {

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('id-adicionar-opcao');
        modeloOpcao.setNome('adicionar');
        modeloOpcao.setIcone('mm-opcao-adicionar');

        gridManager.bindIsModoRemover = false;
        gridManager.definirCorOpcao(modeloOpcao);
        expect(modeloOpcao.getCor()).toBe(Cor.VERDE_ESCURO);
    }));

    it('Deve definir cor para uma opção selecionada', inject([GridManagerService], (gridManager: GridManagerService) => {

        spyOn(gridManager, 'seOpcaoEstaSelecionada').and.returnValue(true);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idDinamico');
        modeloOpcao.setNome('opcão dinâmica');

        gridManager.bindIsModoRemover = false;
        gridManager.definirCorOpcao(modeloOpcao);
        expect(modeloOpcao.getCor()).toBe(Cor.LARANJA);
    }));

    it('Deve definir cor para uma opção normal', inject([GridManagerService], (gridManager: GridManagerService) => {

        spyOn(gridManager, 'seOpcaoEstaSelecionada').and.returnValue(false);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idDinamico');
        modeloOpcao.setNome('opcão dinâmica');
        modeloOpcao.setTipo(TipoOpcao.DINAMICO);

        gridManager.bindIsModoRemover = false;
        gridManager.definirCorOpcao(modeloOpcao);
        expect(modeloOpcao.getCor()).toBe(Cor.VERDE_CLARO);

        gridManager.bindIsModoRemover = true;
        gridManager.definirCorOpcao(modeloOpcao);
        expect(modeloOpcao.getCor()).toBe(Cor.LARANJA);
    }));

    it('Deve definir cor para uma opção fixa', inject([GridManagerService], (gridManager: GridManagerService) => {

        spyOn(gridManager, 'seOpcaoEstaSelecionada').and.returnValue(false);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idFixo');
        modeloOpcao.setNome('opcão fixa');
        modeloOpcao.setTipo(TipoOpcao.FIXO);

        gridManager.bindIsModoRemover = false;
        gridManager.definirCorOpcao(modeloOpcao);
        expect(modeloOpcao.getCor()).toBe(Cor.VERDE_CLARO);

        gridManager.bindIsModoRemover = true;
        gridManager.definirCorOpcao(modeloOpcao);
        expect(modeloOpcao.getCor()).toBe(Cor.CINZA);
    }));

    it('Deve configurar o modal', inject([GridManagerService], (gridManager: GridManagerService) => {
        spyOn(gridManager, 'criarModalAdicionarOpcoes').and.stub();

        let configuracaoQuestao: ConfiguracaoGrid = new ConfiguracaoGrid();
        configuracaoQuestao.setPossuiModal(true);

        gridManager.setConfiguracao(configuracaoQuestao);

        gridManager.configurarModal();
        expect(gridManager.criarModalAdicionarOpcoes).toHaveBeenCalled();
    }));

    it('Deve definir os atributos de view de método eficiência', inject([GridManagerService], (gridManager: any) => {
        let crise: Crise = {
            _id: 'idCrise',
            dataInicial: null,
            dataFinal: null,
            intensidade: [],
            medicamentos: [],
            qualidadesDor: [],
            sintomas: [],
            gatilhos: [],
            esforcosFisicos: [],
            bebidas: [],
            comidas: [],
            cicloMestrual: [],
            metodosAlivio: [],
            metodosAlivioEficiente: [],
            consequencias: [],
            locais: [],
            localizacoesDeDor: [],
            anotacoes: null,
            clima: null,
            gps: null,
            type: 'crise',
        };
        let opcao: Opcao = {
            _id: 'idMetodoEficiente',
            nome: 'metodo eficiente 1',
            dataCriacao: '13-05-2017',
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        crise.metodosAlivioEficiente.push(opcao);
        gridManager.setCrise(crise);

        let modeloOpcao: ModeloOpcao = new ModeloOpcao();
        modeloOpcao.setId('idMetodoEficiente');
        modeloOpcao.setNome('metodo eficiente 1');
        modeloOpcao.setDataCriacao('13-05-2017');
        modeloOpcao.setAtivo(true);

        let spySetTipoEficiencia: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'setTipoEficiencia');
        let spyDefinirIconeEficiencia: jasmine.Spy = spyOn(ModeloOpcao.prototype, 'definirIconeEficiencia');
        gridManager._setViewMetodoEficiencia(opcao, modeloOpcao);
        expect(spySetTipoEficiencia).toHaveBeenCalled();
        expect(spyDefinirIconeEficiencia).toHaveBeenCalled();
    }));

    it('Deve montar dados de medicamentos ou método alívio na questão métodos eficientes', inject([GridManagerService], (gridManager: any) => {
        gridManager.setConfiguracao(new ConfiguracaoGrid());
        let spyOnDuplicarOpcoesAtivas: jasmine.Spy = spyOn(gridManager, 'duplicarOpcoesAtivas').and.stub();
        let spyOnMontarOpcoesDoGrid: jasmine.Spy = spyOn(gridManager, 'montarOpcoesDoGrid').and.stub();

        let documentOptions: Opcoes = {
            _id: null,
            opcoes: [],
        };
        let opcao1: Opcao = {
            _id: 'idOpcao1',
            nome: 'nomeOpcao1',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };
        let opcao2: Opcao = {
            _id: 'idOpcao2',
            nome: 'nomeOpcao2',
            dataCriacao: '14/02/2017',
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        let opcao3: Opcao = {
            _id: 'idOpcao3',
            nome: 'nomeOpcao3',
            dataCriacao: '14/02/2017',
            ativo: false,
            tipo: TipoOpcao.FIXO,
        };
        documentOptions.opcoes.push(opcao1);
        documentOptions.opcoes.push(opcao2);
        documentOptions.opcoes.push(opcao3);
        let arrayOpcoes: Array<any> = documentOptions.opcoes;

        gridManager._montarMedicamentosOuMetodosAlivio(arrayOpcoes, TipoQuestao.MEDICAMENTO);
        expect(gridManager.getConfiguracao().getIdentificadorGrid()).toBe(TipoQuestao.MEDICAMENTO);
        expect(spyOnDuplicarOpcoesAtivas).toHaveBeenCalled();
        expect(spyOnMontarOpcoesDoGrid).toHaveBeenCalled();

        spyOnDuplicarOpcoesAtivas.calls.reset();
        spyOnMontarOpcoesDoGrid.calls.reset();
        gridManager._montarMedicamentosOuMetodosAlivio(arrayOpcoes, TipoQuestao.METODO_ALIVIO);
        expect(gridManager.getConfiguracao().getIdentificadorGrid()).toBe(TipoQuestao.METODO_ALIVIO);
        expect(spyOnDuplicarOpcoesAtivas).toHaveBeenCalled();
        expect(spyOnMontarOpcoesDoGrid).toHaveBeenCalled();
    }));

    it('Deve verificar se opcao é subgatilho', inject([GridManagerService], (gridManager: any) => {
        let modeloOpcaoEsforcoFisico: ModeloOpcao = new ModeloOpcao();
        modeloOpcaoEsforcoFisico.setId('mm-gatilho-esforco-fisico');
        expect(gridManager._isOpcaoSubGatilho(modeloOpcaoEsforcoFisico)).toBe(true);

        let modeloOpcaoBebida: ModeloOpcao = new ModeloOpcao();
        modeloOpcaoBebida.setId('mm-gatilho-bebida');
        expect(gridManager._isOpcaoSubGatilho(modeloOpcaoEsforcoFisico)).toBe(true);

        let modeloOpcaoComida: ModeloOpcao = new ModeloOpcao();
        modeloOpcaoComida.setId('mm-gatilho-comida');
        expect(gridManager._isOpcaoSubGatilho(modeloOpcaoEsforcoFisico)).toBe(true);

        let modeloQualquer: ModeloOpcao = new ModeloOpcao();
        modeloQualquer.setId('idQualquer');
        expect(gridManager._isOpcaoSubGatilho(modeloQualquer)).toBe(false);

    }));
});
