import { Modal, ModalController } from 'ionic-angular';

export class ModalQuestaoService {

    private _modalAdicionarOpcoes: Modal;
    private _modalAdicionarEvento: Modal;
    private _modalAdicionarOpcaoReferencia: any;
    private _modalCtrl: ModalController;

    public getModalAdicionarOpcoes() {
        return this._modalAdicionarOpcoes;
    }

    public setModalAdicionarOpcoes(modalAdicionarOpcoes: any) {
        this._modalAdicionarOpcoes = modalAdicionarOpcoes;
    }

    public getModalAdicionarEvento() {
        return this._modalAdicionarEvento;
    }

    public setModalAdicionarEvento(modalDetalhe: any) {
        this._modalAdicionarEvento = modalDetalhe;
    }

    public setModalCtrl(modalCtrl: ModalController) {
        this._modalCtrl = modalCtrl;
    }

    public getModalCtrl() {
        return this._modalCtrl;
    }

    public setModalAdicionarOpcaoReferencia(modalOpcaoReferencia: any) {
        this._modalAdicionarOpcaoReferencia = modalOpcaoReferencia;
    }

    public getModalAdicionarOpcaoReferencia() {
        return this._modalAdicionarOpcaoReferencia;
    }

    public criarModalAdicionarOpcoes(parametros: any) {
        this._modalAdicionarOpcoes = this._modalCtrl.create(this._modalAdicionarOpcaoReferencia, parametros);
    }

    public criarModalEventoClique(modalReferencia: any, parametros: any) {
        this._modalAdicionarEvento = this._modalCtrl.create(modalReferencia, parametros, { cssClass: 'transparent-modal' });
    }
}
