import { async, inject } from '@angular/core/testing';
import { ModalQuestaoService } from './modal-questao.service';
import { TestUtils } from './../../../app/app.test';
import { ModalController, Modal } from 'ionic-angular';

describe('Modal Questão:', () => {

    let modalCtrl: any = new ModalController(null, null, null);

    beforeEach(async(() => {

        TestUtils.addProvider(ModalQuestaoService);
        TestUtils.addProvider(ModalController);
        TestUtils.beforeEachCompiler([]);
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('#getModalAdicionarOpcoes - Deve obter referência do modal adicionar opção', inject(
        [ModalQuestaoService], (modalQuestao: ModalQuestaoService) => {

            modalQuestao.setModalAdicionarOpcaoReferencia('AdicionarMedicamento');
            modalQuestao.setModalCtrl(modalCtrl);
            modalQuestao.criarModalAdicionarOpcoes({});
            let modal: Modal = modalQuestao.getModalAdicionarOpcoes();

            expect(modal).toBeDefined();
        }),
    );

    it('#getModalAdicionarEvento - Deve obter referência do modal detalhe', inject(
        [ModalQuestaoService], (modalQuestao: ModalQuestaoService) => {
            modalQuestao.setModalCtrl(modalCtrl);
            modalQuestao.criarModalEventoClique('AdicionarQuantidade', {});
            let modal: Modal = modalQuestao.getModalAdicionarEvento();

            expect(modal).toBeDefined();
        }),
    );

    it('#setModalCtrl - Deve definir modal controller', inject(
        [ModalQuestaoService], (modalQuestao: ModalQuestaoService) => {
            modalQuestao.setModalCtrl(modalCtrl);

            expect(modalQuestao.getModalCtrl()).toEqual(modalCtrl);
        }),
    );

    it('#setModalAdicionarOpcaoReferencia - Deve definir referência do modal adicionar', inject(
        [ModalQuestaoService], (modalQuestao: ModalQuestaoService) => {
            modalQuestao.setModalAdicionarOpcaoReferencia('AdicionarMedicamento');

            expect(modalQuestao.getModalAdicionarOpcaoReferencia()).toEqual('AdicionarMedicamento');
        }),
    );

    it('Deve criar modal adicionar opção', inject(
        [ModalQuestaoService], (modalQuestao: ModalQuestaoService) => {
            modalQuestao.setModalAdicionarOpcoes(undefined);
            modalQuestao.setModalCtrl(modalCtrl);
            modalQuestao.criarModalAdicionarOpcoes({});

            expect(modalQuestao.getModalAdicionarOpcoes()).toBeDefined();
        }),
    );

    it('Deve criar modal adicionar detalhe/evento clique', inject(
        [ModalQuestaoService], (modalQuestao: ModalQuestaoService) => {
            modalQuestao.setModalAdicionarEvento(undefined);
            modalQuestao.setModalCtrl(modalCtrl);
            modalQuestao.criarModalEventoClique('AdicionarQuantidade', {});

            expect(modalQuestao.getModalAdicionarEvento()).toBeDefined();
        }),
    );
});
