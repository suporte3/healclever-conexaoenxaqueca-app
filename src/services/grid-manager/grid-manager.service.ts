import { Cor } from './../../core/models/pojo/cor.pojo';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Opcao } from './../../core/models/opcao.model';
import { TipoQuestao } from './../../core/models/pojo/tipo-questao.pojo';
import { TipoOpcao } from './../../core/models/tipo-opcao.enum';
import { Opcoes } from './../../core/models/opcoes.model';
import { Crise } from './../../core/models/crise.model';
import { Events } from 'ionic-angular';
import { ConfiguracaoGrid } from './../../core/models/pojo/configuracao-grid.pojo';
import { ModeloOpcao } from './../../core/models/pojo/opcao.pojo';
import { ModalQuestaoService } from './modal/modal-questao.service';
import findIndex from 'lodash-es/findIndex';
import clone from 'lodash-es/clone';

export class GridManagerService extends ModalQuestaoService {

    public readonly ADICIONAR_OPCAO: string = 'id-adicionar-opcao';
    public readonly REMOVER_OPCAO: string = 'id-remover-opcao';

    public readonly CRISE_PARAM: string = 'com.healclever.myMigraine.crise_param';
    public bindOpcoesDoGrid: Array<Array<ModeloOpcao>>;
    public arrayOpcoesDoGrid: Array<ModeloOpcao> = [];
    public bindIsModoRemover: boolean;
    public bindIsModoGPS: boolean = false;
    public _iniciouQuestao: boolean = false;
    public _needRefresh: boolean = false;

    private _arrayModeloOpcaoSelecionadas: Array<ModeloOpcao> = [];
    private _arrayOpcoesLocaisFixoDatabase: Array<ModeloOpcao> = [];
    private _arrayOpcoesAtualizadasLocaisFixo: Array<ModeloOpcao> = [];
    private readonly NUM_COLUNS: number = 3;
    private _configuracao: ConfiguracaoGrid;
    private _events: Events;

    private _crise: Crise = {
        _id: null,
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };

    private documentOptions: Opcoes = {
        _id: null,
        opcoes: [],
    };

    constructor() {
        super();
    }

    public getEvents() {
        return this._events;
    }

    public setEvents(events: Events) {
        this._events = events;
    }

    public getCrise(): any {
        return this._crise;
    }

    public setCrise(crise: Crise) {
        this._crise = crise;
    }

    public setConfiguracao(configuracao: ConfiguracaoGrid) {
        this._configuracao = configuracao;
    }

    public getConfiguracao(): ConfiguracaoGrid {
        return this._configuracao;
    }

    public setDocumentOptions(opcoesDaQuestao: any) {
        this.documentOptions = opcoesDaQuestao;
    }

    public getDocumentOptions() {
        return this.documentOptions;
    }

    public getOpcoesSelecionadas(): Array<ModeloOpcao> {
        return this._arrayModeloOpcaoSelecionadas;
    }

    public setOpcoesSelecionadas(arrayModeloOpcaoSelecionadas: Array<ModeloOpcao>) {
        this._arrayModeloOpcaoSelecionadas = arrayModeloOpcaoSelecionadas;
    }

    public getOpcoesLocalFixo() {
        return this._arrayOpcoesAtualizadasLocaisFixo;
    }

    public setOpcoesLocalFixo(arrayOpcoesLocaisFixo: Array<ModeloOpcao>) {
        this._arrayOpcoesAtualizadasLocaisFixo = arrayOpcoesLocaisFixo;
    }

    public getOpcoesLocalFixoDatabase() {
        return this._arrayOpcoesLocaisFixoDatabase;
    }

    public setOpcoesLocalFixoDatabase(arrayOpcoesLocaisFixoDatabase: Array<ModeloOpcao>) {
        this._arrayOpcoesLocaisFixoDatabase = arrayOpcoesLocaisFixoDatabase;
    }

    public montarGridMetodoAlivio() {
        let modeloOpcoesMedicamento: Array<ModeloOpcao> = [];
        let modeloOpcoesMetodoAlivio: Array<ModeloOpcao> = [];

        if (this.getCrise().medicamentos.length > 0) {
            modeloOpcoesMedicamento = this._montarMedicamentosOuMetodosAlivio(this.getCrise().medicamentos, TipoQuestao.MEDICAMENTO);
        }

        if (this.getCrise().metodosAlivio.length > 0) {
            modeloOpcoesMetodoAlivio = this._montarMedicamentosOuMetodosAlivio(this.getCrise().metodosAlivio, TipoQuestao.METODO_ALIVIO);
        }

        this.setModalCtrl(this.getConfiguracao().getModalCtrl());

        this.arrayOpcoesDoGrid = [];

        let matrizMedicametoMetodosAlivio: Array<Array<ModeloOpcao>> = [modeloOpcoesMedicamento, modeloOpcoesMetodoAlivio];
        matrizMedicametoMetodosAlivio.forEach(
            (arrayModeloOpcao) => {
                if (arrayModeloOpcao.length > 0) {
                    arrayModeloOpcao.forEach(
                        (modeloOpcao) => {
                            this.arrayOpcoesDoGrid.push(modeloOpcao);
                        },
                    );
                }
            },
        );

        this.getConfiguracao().setIdentificadorGrid(TipoQuestao.METODO_ALIVIO_EFICIENTE);
        this.montarGrid();
    }

    public duplicarOpcoesAtivas(): Array<Opcao> {
        let arrayCopy: Array<any> = [];
        this.documentOptions.opcoes.forEach((data) => {
            if (data.ativo) {
                arrayCopy.push(Object.assign({}, data));
            }
        });
        return arrayCopy;
    }

    public seOpcaoEstaSelecionada(modeloOpcao: ModeloOpcao): boolean {

        let selecionada: boolean = false;

        if (!this._iniciouQuestao) {

            if (this.getCrise()[this.getConfiguracao().getNomeDoObjetoOpcao()] &&
                this.getCrise()[this.getConfiguracao().getNomeDoObjetoOpcao()].length > 0) {
                selecionada = this._verificarSelecaoViaCrise(modeloOpcao);
            }
        } else {
            // if (this.getOpcoesSelecionadas().length > 0) {
            if (this._needRefresh &&
                this.getConfiguracao().getIdentificadorGrid() === TipoQuestao.GATILHO &&
                this._isOpcaoSubGatilho(modeloOpcao)) {
                selecionada = this._verificarSelecaoViaCrise(modeloOpcao);
            } else {
                selecionada = this._verificarSelecaoViaArraySelecionadas(modeloOpcao);
            }
            // }
        }

        return selecionada;
    }

    public atualizarOpcoes() {
        let opcoesDoGrid: Array<Opcao> = this.duplicarOpcoesAtivas();
        this.montarOpcoesDoGrid(opcoesDoGrid);
        this.montarGrid();
    }

    public atualizarOpcaoDoGrid(modeloOpcaoParaAtualizar: ModeloOpcao) {
        let indiceParaAtualizar: number = -1;
        this.arrayOpcoesDoGrid.every(
            (modeloOpcaoGrid: ModeloOpcao, i: number) => {
                if (modeloOpcaoGrid.getId() === modeloOpcaoParaAtualizar.getId()) {
                    indiceParaAtualizar = i;
                    return false;
                } else {
                    return true;
                }
            },
        );

        if (indiceParaAtualizar > -1) {
            this.arrayOpcoesDoGrid[indiceParaAtualizar] = modeloOpcaoParaAtualizar;
        }

        this.montarGrid();
    }

    public removerOpcao(opcaoParaRemover: ModeloOpcao) {
        let cloneModeloOpcao: ModeloOpcao = clone(opcaoParaRemover);

        this.getCrise()[this.getConfiguracao().getNomeDoObjetoOpcao()] = this.retirarModeloOpcaoDoArray(
            this.getCrise()[this.getConfiguracao().getNomeDoObjetoOpcao()],
            cloneModeloOpcao,
        );

        this.retirarModeloOpcaoDoArraySelecionado(cloneModeloOpcao);
        this.documentOptions.opcoes = this.atualizarStatusOpcao(this.documentOptions.opcoes, cloneModeloOpcao);

        let manyDocuments: any = [this.documentOptions, this.getCrise()];

        this.getConfiguracao().getSyncDataServiceReference().addOrUpdateManyDocuments(manyDocuments);
        this.atualizarOpcoes();
    }

    public retirarModeloOpcaoDoArray(array: Array<any>, opcao: ModeloOpcao): Array<any> {
        array.every(
            (element: any, indice: number) => {
                if (element.nome === opcao.getNome()) {
                    array.splice(indice, 1);
                    return false;
                } else {
                    return true;
                }
            },
        );

        return array;
    }

    public retirarModeloOpcaoDoArraySelecionado(modeloOpcao: ModeloOpcao): Array<ModeloOpcao> {

        this.getOpcoesSelecionadas().every(
            (opcao: ModeloOpcao, indice: number) => {
                if (opcao.getId() === modeloOpcao.getId()) {
                    this.getOpcoesSelecionadas().splice(indice, 1);
                    return false;
                } else {
                    return true;
                }
            },
        );

        return this.getOpcoesSelecionadas();
    }

    public atualizarStatusOpcao(array: Array<any>, opcao: ModeloOpcao): Array<any> {
        array.every(
            (element: any) => {
                if (element.nome === opcao.getNome()) {
                    element.ativo = false;
                    return false;
                } else {
                    return true;
                }
            },
        );

        return array;
    }

    public montarOpcoesDoGrid(opcoesDoGrid: Array<Opcao>) {
        this.arrayOpcoesDoGrid = [];

        opcoesDoGrid.forEach((opcao: any) => {
            let modeloOpcao: ModeloOpcao = new ModeloOpcao();
            modeloOpcao.setId(opcao._id);
            modeloOpcao.setNome(opcao.nome);
            modeloOpcao.setAtivo(opcao.ativo);
            modeloOpcao.setTipo(opcao.tipo);
            modeloOpcao.setDataCriacao(opcao.dataCriacao);

            if (this.getConfiguracao().getPossuiEficiencia()) {
                this._setViewMetodoEficiencia(opcao, modeloOpcao);
            }

            modeloOpcao.definirIcones(this.getConfiguracao().getIdentificadorGrid());

            if (this.getConfiguracao().getIdentificadorGrid() === TipoQuestao.MEDICAMENTO) {

                modeloOpcao.definirQuantidadeMedicamento(
                    this.getCrise().medicamentos,
                    this.getOpcoesSelecionadas(),
                );
            }

            if (this.getConfiguracao().getIdentificadorGrid() === TipoQuestao.LOCAL) {

                modeloOpcao.definirOpcaoGPS(
                    this.getCrise(),
                    opcao.gps,
                );

                if (modeloOpcao.isLocalFixo()) {
                    // this.getOpcoesLocalFixo().push(modeloOpcao);
                    this.getOpcoesLocalFixoDatabase().push(modeloOpcao);
                }
            }

            this.definirCorOpcao(modeloOpcao);
            this.arrayOpcoesDoGrid.push(modeloOpcao);
        });

        this.arrayOpcoesDoGrid.sort(function (a, b) {
            return a.getTipo() - b.getTipo();
        });

        if (this.getConfiguracao().getPossuiModal()) {
            this._addBotoesModal();
        }

        if (this.getConfiguracao().getIdentificadorGrid() === TipoQuestao.MEDICAMENTO &&
            this.getConfiguracao().getDocumentoOptionId() === SyncDataProvider.DOCUMENT_OPCOES_MEDICAMENTO_ID &&
            opcoesDoGrid.length === 0) {
            this.getEvents().publish('medicamento:onPrimeiroCadastroMedicamento', true);
        }
    }

    public montarGrid() {

        let indiceColunaRemovida: number = -1;
        let numeroDeLinhas = Math.ceil(this.arrayOpcoesDoGrid.length / this.NUM_COLUNS);
        this.bindOpcoesDoGrid = Array(numeroDeLinhas);
        let dataIndex = 0;
        for (let linha = 0; linha < numeroDeLinhas; linha++) {
            this.bindOpcoesDoGrid[linha] = Array(this.NUM_COLUNS);
            for (let coluna: number = 0; coluna < this.NUM_COLUNS; coluna++) {
                if (this.arrayOpcoesDoGrid[dataIndex]) {
                    this.bindOpcoesDoGrid[linha][coluna] = this.arrayOpcoesDoGrid[dataIndex];
                    dataIndex++;
                } else {
                    if (!(indiceColunaRemovida > -1)) {
                        indiceColunaRemovida = coluna;
                    }
                    this.bindOpcoesDoGrid[linha].splice(indiceColunaRemovida, 1);
                }
            }
        }
    }

    public definirCorOpcao(modeloOpcao: ModeloOpcao) {
        let corOpcao: string;
        if (modeloOpcao.getId() === this.ADICIONAR_OPCAO) {
            corOpcao = (this.bindIsModoRemover || this.bindIsModoGPS ? Cor.CINZA : Cor.VERDE_ESCURO);
        } else {
            if (!this.bindIsModoRemover && this.seOpcaoEstaSelecionada(modeloOpcao)) {
                corOpcao = Cor.LARANJA;
                modeloOpcao.setSelecionado(true);
            } else {

                if (modeloOpcao.getTipo() === TipoOpcao.FIXO) {
                    corOpcao = (this.bindIsModoRemover ? Cor.CINZA : Cor.VERDE_CLARO);
                } else {
                    corOpcao = (this.bindIsModoRemover ? Cor.LARANJA : Cor.VERDE_CLARO);
                }
            }
        }

        modeloOpcao.setCor(corOpcao);
    }

    public configurarModal() {
        if (this.getConfiguracao().getPossuiModal()) {
            this.setModalCtrl(this.getConfiguracao().getModalCtrl());
            this.setModalAdicionarOpcaoReferencia(
                this.getConfiguracao().getModalAdicionarOpcaoReferencia());
            let parametro = { 'crise': this.getCrise(), 'opcoes': this.getDocumentOptions() };
            this.criarModalAdicionarOpcoes(parametro);
        }
    }

    private _addBotoesModal() {
        let opcaoAdicionar: ModeloOpcao = new ModeloOpcao();
        opcaoAdicionar.setId(this.ADICIONAR_OPCAO);
        opcaoAdicionar.setIcone('mm-opcao-adicionar');
        opcaoAdicionar.setNome('adicionar');
        this.definirCorOpcao(opcaoAdicionar);

        let opcaoRemover: ModeloOpcao = new ModeloOpcao();
        opcaoRemover.setId(this.REMOVER_OPCAO);
        opcaoRemover.setIcone('mm-opcao-excluir');
        opcaoRemover.setCor(Cor.LARANJA);
        opcaoRemover.setNome('excluir');

        this.arrayOpcoesDoGrid.push(opcaoAdicionar);
        this.arrayOpcoesDoGrid.push(opcaoRemover);
    }

    private _setViewMetodoEficiencia(opcao: Opcao, modeloOpcao: ModeloOpcao) {
        this.getCrise().metodosAlivioEficiente.every(
            (metodo: any) => {
                if (metodo.nome === opcao.nome) {
                    modeloOpcao.setTipoEficiencia(metodo.tipoEficiencia);
                    modeloOpcao.definirIconeEficiencia();
                    return false;
                } else {
                    return true;
                }
            },
        );
    }

    private _montarMedicamentosOuMetodosAlivio(arrayOpcoes: Array<any>, tipoQuestao: string): Array<ModeloOpcao> {
        this.documentOptions.opcoes = [];
        arrayOpcoes.forEach((opcaoCrise: any) => {
            let opcaoDocumento: any = {
                _id: opcaoCrise._id,
                nome: opcaoCrise.nome,
                dataCriacao: opcaoCrise.dataCriacao,
                ativo: true,
                tipo: opcaoCrise.tipo,
            };
            this.documentOptions.opcoes.push(opcaoDocumento);
        });

        this.getConfiguracao().setIdentificadorGrid(tipoQuestao);
        let opcoesAtivas: Array<Opcao> = this.duplicarOpcoesAtivas();
        this.montarOpcoesDoGrid(opcoesAtivas);

        return clone(this.arrayOpcoesDoGrid);
    }

    private _verificarSelecaoViaCrise(modeloOpcao: ModeloOpcao): boolean {
        let selecionada: boolean = false;
        this.getCrise()[this.getConfiguracao().getNomeDoObjetoOpcao()].every((opcaoCrise: any) => {
            if (opcaoCrise._id === modeloOpcao.getId()) {
                selecionada = true;

                if (this._needRefresh) {
                    let index: number = findIndex(
                        this.getOpcoesSelecionadas(),
                        function (opcao: ModeloOpcao) {
                            return opcao.getId() === opcaoCrise._id;
                        },
                    );

                    if (index < 0) {
                        this.getOpcoesSelecionadas().push(modeloOpcao);
                    }
                } else {
                    this.getOpcoesSelecionadas().push(modeloOpcao);
                }

                return false;
            } else {
                return true;
            }
        });

        return selecionada;
    }

    private _verificarSelecaoViaArraySelecionadas(modeloOpcao: ModeloOpcao): boolean {
        let selecionada: boolean = false;

        this.getOpcoesSelecionadas().every((opcao: ModeloOpcao) => {
            if (opcao.getId() === modeloOpcao.getId()) {
                selecionada = true;
                return false;
            } else {
                return true;
            }
        });

        return selecionada;
    }

    private _isOpcaoSubGatilho(modeloOpcao: ModeloOpcao): boolean {
        if (modeloOpcao.getId() === 'mm-gatilho-esforco-fisico' ||
            modeloOpcao.getId() === 'mm-gatilho-bebida' ||
            modeloOpcao.getId() === 'mm-gatilho-comida') {
            return true;
        } else {
            return false;
        }
    }
}
