import { Opcoes } from './../../core/models/opcoes.model';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Http, Response } from '@angular/http';

export class SeedService {

    private _arrayOpcoesArquivo: Array<any> = [];
    private arrayIdsOpcoes = [
        SyncDataProvider.DOCUMENT_OPCOES_QUALIDADE_DOR_ID,
        SyncDataProvider.DOCUMENT_OPCOES_SINTOMA_ID,
        SyncDataProvider.DOCUMENT_OPCOES_GATILHO_ID,
        SyncDataProvider.DOCUMENT_OPCOES_GATILHO_ESFORCO_FISICO_ID,
        SyncDataProvider.DOCUMENT_OPCOES_GATILHO_BEBIDA_ID,
        SyncDataProvider.DOCUMENT_OPCOES_GATILHO_COMIDA_ID,
        SyncDataProvider.DOCUMENT_OPCOES_CICLO_MENSTRUAL_ID,
        SyncDataProvider.DOCUMENT_OPCOES_METODO_ALIVIO_ID,
        SyncDataProvider.DOCUMENT_OPCOES_CONSEQUENCIA_ID,
        SyncDataProvider.DOCUMENT_OPCOES_LOCAL_ID,
        SyncDataProvider.DOCUMENT_OPCOES_LOCALIZACAO_DOR_ID,
    ];

    constructor(private _syncDataService: SyncDataProvider, private http: Http) { }

    public adicionarOpces() {
        this._lerArquivos();
    }

    private _lerArquivos() {
        console.log('LENDO ARQUIVOS....');
        this.http.get('assets/seed/seed.json')
            .map((res: Response) => res.json())
            .subscribe((res) => {
                this._arrayOpcoesArquivo = res;
                this._buscarTodasOpcoes();
            }, (err) => {
                console.log('Track error:', err);
            });
    }

    private _buscarTodasOpcoes() {
        for (let i = 0; i < this.arrayIdsOpcoes.length; i++) {
            this._syncDataService.getDocumentById(this.arrayIdsOpcoes[i]).then(
                (result: any) => {
                    if (result.error && result.status === 404) {
                        this._registrarTodasOpcoes(this._arrayOpcoesArquivo[i].opcoes, i);
                    } else if (result.error === undefined) {
                        this._verificarSeOpcaoJaExisteNoBanco(result, i);
                    } else {
                        console.log('Tratar outros tipos de Erros!!');
                    }
                });
        }
    }

    private _verificarSeOpcaoJaExisteNoBanco(opcaoBanco: any, index: number) {
        this._arrayOpcoesArquivo[index].opcoes.forEach((opcaoArquivo: any) => {
            let opcaoEncontrada = opcaoBanco.opcoes.filter(function (opcaoFiltrada: any) {
                return opcaoFiltrada._id === opcaoArquivo._id;
            })[0];
            if (!opcaoEncontrada) {
                opcaoBanco.opcoes.push(opcaoArquivo);
                this._registrarOpcaoNoBanco(opcaoBanco);
            }
        });
    }

    private _registrarOpcaoNoBanco(opcao: any) {
        this._syncDataService.addOrUpdate(opcao)
        .then(
            () => {

            },
            () => {
                console.log('Seed não registrou');
            },
        );
    }

    private _registrarTodasOpcoes(opcoes: any, index: any) {
        let id = this.arrayIdsOpcoes[index];
        let documentOpcoes: Opcoes = {
            _id : id,
            opcoes: opcoes,
        };

        this._registrarOpcaoNoBanco(documentOpcoes);
    }
}
