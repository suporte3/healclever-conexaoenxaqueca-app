import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Diario } from './../../core/models/diario.model';
import { Medicamento } from './../../core/models/medicamento.model';
import { Opcao } from './../../core/models/opcao.model';
import { Crise } from './../../core/models/crise.model';
import findIndex from 'lodash-es/findIndex';
import * as moment from 'moment';

export class CefaleiaEstatisticasService {
    private _dadosCrise: Array<any> = [];
    private _dadosSono: Array<any> = [];

    public setDadosCrise(dadosEstatisticos: Array<any>) {
        this._dadosCrise = dadosEstatisticos;
    }

    public getDadosCrise() {
        return this._dadosCrise;
    }

    public setDadosSono(dadosEstatisticos: Array<any>) {
        this._dadosSono = dadosEstatisticos;
    }

    public getDadosSono() {
        return this._dadosSono;
    }

    public buildDuracaoDor() {
        let textoDuracao: string = null;

        let somaDuracao: number = 0;
        this.getDadosCrise().forEach(
            (crise: Crise) => {
                somaDuracao += moment.utc(crise.dataInicial).diff(moment.utc(crise.dataFinal));
            },
        );

        if (somaDuracao > 0) {
            let mediaDuracao: number = somaDuracao / this.getDadosCrise().length;
            textoDuracao = moment.duration(mediaDuracao).humanize();
        } else {
            textoDuracao = '-';
        }

        return textoDuracao;
    }

    public buildQuandoAcontece() {
        let quandoAcontece: Array<any> = [];
        let dadosEstatisticos: Array<any> = [
            {
                label: 'dias de semana',
                percentual: '0',
            },
            {
                label: 'fins de semana',
                percentual: '0',
            },
        ];
        let diaDeSemana: number = 0;
        let fimDeSemana: number = 0;
        let percentualDiaSemana: number = 0;
        let percentualFimSemana: number = 0;
        let sizeCrises: number = this.getDadosCrise().length;

        this.getDadosCrise().forEach(
            (crise: Crise) => {
                let dia: number = moment.utc(crise.dataInicial).weekday();
                if (dia === 6 || dia === 0) {
                    fimDeSemana++;
                } else {
                    diaDeSemana++;
                }
            },
        );

        if (diaDeSemana > 0) {
            percentualDiaSemana = Math.round((diaDeSemana * 100) / sizeCrises);
            dadosEstatisticos[0].percentual = percentualDiaSemana;
            quandoAcontece = dadosEstatisticos;
        }

        if (fimDeSemana > 0) {
            percentualFimSemana = Math.round((fimDeSemana * 100) / sizeCrises);
            dadosEstatisticos[1].percentual = percentualFimSemana;
            quandoAcontece = dadosEstatisticos;
        }

        return quandoAcontece;
    }

    public buildItensBarGraph(tipoDeDados: string) {

        let dataListItens: Array<any> = [];
        this.getDadosCrise().forEach(
            (crise: any) => {
                crise[tipoDeDados].forEach(
                    (opcao: Opcao) => {

                        let index: number = findIndex(
                            dataListItens,
                            function (opcaoDataList: any) {
                                return opcaoDataList.label === opcao.nome;
                            },
                        );

                        if (index >= 0) {

                            dataListItens[index].count = dataListItens[index].count + 1;
                        } else {

                            let item: any = {
                                label: null,
                                percentual: 0,
                                count: 0,
                            };

                            item.label = opcao.nome;
                            item.count = 1;
                            dataListItens.push(item);
                        }
                    },
                );
            },
        );

        dataListItens.sort(function (a, b) {
            return b.count - a.count;
        });

        let somaItens: number = 0;

        dataListItens.forEach(
            (item: any) => {
                somaItens += item.count;
            },
        );

        let itensList: Array<any> = [];
        for (let i: number = 0; i < 3; i++) {
            let item: any = dataListItens[i];

            if (item) {
                let percentual: number = (item.count * 100) / this.getDadosCrise().length;
                item.percentual = Math.floor(percentual);

                itensList.push(item);
            }
        }

        return itensList;
    }

    public buildMenstruacao() {
        let dadosMenstruacao: Array<any> = [];

        let menstruacaoAssociada: any = {
            label: 'associada',
            percentual: 0,
        };

        let menstruacaoNaoAssociada: any = {
            label: 'não associada',
            percentual: 0,
        };

        let menstruacaoPreMenstrual: any = {
            label: 'pré menstrual',
            percentual: 0,
        };

        let somaTotal: number = 0;

        this.getDadosCrise().forEach(
            (crise: Crise) => {
                let menstruacaoId: string = (crise.cicloMestrual[0] ? crise.cicloMestrual[0]._id : undefined);

                if (menstruacaoId) {
                    if (menstruacaoId === 'mm-ciclo-menstrual-antes') {
                        menstruacaoPreMenstrual.percentual++;
                    } else if (menstruacaoId === 'mm-ciclo-menstrual-sim') {
                        menstruacaoAssociada.percentual++;
                    } else {
                        menstruacaoNaoAssociada.percentual++;
                    }

                    somaTotal++;
                }
            },
        );

        if (somaTotal > 0) {
            menstruacaoAssociada.percentual = Math.round((menstruacaoAssociada.percentual * 100) / somaTotal);
            menstruacaoNaoAssociada.percentual = Math.round((menstruacaoNaoAssociada.percentual * 100) / somaTotal);
            menstruacaoPreMenstrual.percentual = Math.round((menstruacaoPreMenstrual.percentual * 100) / somaTotal);

            dadosMenstruacao[0] = menstruacaoAssociada;
            dadosMenstruacao[1] = menstruacaoNaoAssociada;
            dadosMenstruacao[2] = menstruacaoPreMenstrual;
        }

        return dadosMenstruacao;
    }

    public buildMedicamentos() {

        let dataListItens: Array<any> = [];
        this.getDadosCrise().forEach(
            (crise: Crise) => {
                crise.medicamentos.forEach(
                    (medicamento: Medicamento) => {
                        let index: number = findIndex(
                            dataListItens,
                            function (opcaoDataList: any) {
                                return opcaoDataList.label === medicamento.nome;
                            },
                        );

                        if (index >= 0) {

                            dataListItens[index].quantidade = dataListItens[index].quantidade + medicamento.quantidade;
                        } else {

                            let item: any = {
                                label: null,
                                quantidade: 0,
                            };

                            item.label = medicamento.nome;
                            item.quantidade = medicamento.quantidade;
                            dataListItens.push(item);
                        }
                    },
                );
            },
        );

        dataListItens.sort(function (a, b) {
            return b.quantidade - a.quantidade;
        });

        let itensList: Array<any> = [];
        for (let i: number = 0; i < 3; i++) {
            let item: any = dataListItens[i];

            if (item) {
                itensList.push(item);
            }
        }

        return itensList;
    }

    public buildMetodoAlivio() {
        let dataListItens: Array<any> = [];

        this.getDadosCrise().forEach(
            (crise: Crise) => {
                crise.metodosAlivioEficiente.forEach(
                    (metodoAlivioEficiente: any) => {

                        let index: number = findIndex(
                            dataListItens,
                            function (opcaoDataList: any) {
                                return opcaoDataList.label === metodoAlivioEficiente.nome;
                            },
                        );

                        if (index >= 0) {

                            dataListItens[index].utilidade[metodoAlivioEficiente.tipoEficiencia].valor = dataListItens[index].utilidade[metodoAlivioEficiente.tipoEficiencia].valor + 1;
                            dataListItens[index].count = dataListItens[index].count + 1;
                        } else {

                            let item: any = {
                                label: null,
                                utilidade: [
                                    {
                                        nome: 'nada útil',
                                        icone: 'mm-intensidade-forte',
                                        valor: 0,
                                    },
                                    {
                                        nome: 'pouco útil',
                                        icone: 'mm-intensidade-media',
                                        valor: 0,
                                    },
                                    {
                                        nome: 'útil',
                                        icone: 'mm-intensidade-fraca',
                                        valor: 0,
                                    },
                                ],
                                count: 0,
                            };

                            item.label = metodoAlivioEficiente.nome;
                            item.utilidade[metodoAlivioEficiente.tipoEficiencia].valor = 1;
                            item.count = 1;
                            dataListItens.push(item);
                        }
                    },
                );
            },
        );

        dataListItens.sort(function (a, b) {
            return b.count - a.count;
        });

        let itensList: Array<any> = [];
        for (let i: number = 0; i < 3; i++) {
            let item: any = dataListItens[i];

            if (item) {

                item.utilidade.sort(function (a: any, b: any) {
                    return b.valor - a.valor;
                });

                itensList.push(item);
            }
        }

        return itensList;
    }

    public buildSono() {

        let dadosSono: Array<any> = [];
        let dadosEstatisticos: Array<any> = [
            {
                label: 'antes de uma crise',
                duracao: '-',
            },
            {
                label: 'noite normal',
                duracao: '-',
            },
        ];

        let somaSonoAntesCrise: number = 0;
        let somaSonoNoiteNormal: number = 0;

        let diasComCrise: number = 0;
        let diasSemCrise: number = 0;
        this.getDadosSono().forEach(
            (diarioSaude: Diario) => {

                let sonoIni: string = diarioSaude.sono.inicio;
                let sonoFim: string = diarioSaude.sono.fim;

                if (this._sofreuCrise(diarioSaude._id)) {
                    somaSonoAntesCrise += moment.utc(sonoIni).diff(moment.utc(sonoFim));
                    diasComCrise++;
                } else {
                    somaSonoNoiteNormal += moment.utc(sonoIni).diff(moment.utc(sonoFim));
                    diasSemCrise++;
                }
            },
        );

        if (diasComCrise > 0) {
            let mediaDuracaoAntesCrise: number = somaSonoAntesCrise / diasComCrise;
            let descricaoDuracaoAntesCrise: string = moment.duration(mediaDuracaoAntesCrise).humanize();
            dadosEstatisticos[0].duracao = descricaoDuracaoAntesCrise;
            dadosSono = dadosEstatisticos;
        }

        if (diasSemCrise > 0) {
            let mediaDuracaoNoiteNormal: number = somaSonoNoiteNormal / diasSemCrise;
            let descricaoDuracaoNoiteNormal: string = moment.duration(mediaDuracaoNoiteNormal).humanize();
            dadosEstatisticos[1].duracao = descricaoDuracaoNoiteNormal;
            dadosSono = dadosEstatisticos;
        }

        return dadosSono;
    }

    public buildLocalizacaoDor() {
        let dataListItens: Array<any> = [];

        this.getDadosCrise().forEach(
            (crise: Crise) => {
                crise.localizacoesDeDor.forEach(
                    (localizacaoDor: Opcao) => {
                        let index: number = findIndex(
                            dataListItens,
                            function (opcaoDataList: any) {
                                return opcaoDataList._id === localizacaoDor._id;
                            },
                        );

                        if (index >= 0) {

                            dataListItens[index].count = dataListItens[index].count + 1;
                        } else {

                            let item: any = {
                                _id: 0,
                                count: 0,
                            };

                            item._id = localizacaoDor._id;
                            item.count = 1;
                            dataListItens.push(item);
                        }
                    },
                );
            },
        );

        dataListItens.sort(function (a, b) {
            return b.count - a.count;
        });

        let itensList: Array<any> = [];
        for (let i: number = 0; i < 5; i++) {
            let item: any = dataListItens[i];

            if (item) {
                itensList.push(item._id);
            }
        }

        return itensList;
    }

    private _sofreuCrise(diarioSaudeId: string): boolean {
        let sofreuCrise: boolean = false;

        this.getDadosCrise().every(
            (crise: Crise) => {
                let criseMoment: any = moment.utc(crise.dataInicial);
                let criseDiarioId: string = SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID + criseMoment.date() + '-' + criseMoment.month() + '-' + criseMoment.year();
                if (criseDiarioId === diarioSaudeId) {
                    sofreuCrise = true;
                    return false;
                } else {
                    return true;
                }
            },
        );

        return sofreuCrise;
    }
}
