import { GridQuestionarioComponent } from './../../components/grid-questionario/grid-questionario';
import { ClimaService } from './../clima/clima.service';
import { Opcao } from './../../core/models/opcao.model';
import { AppHelper } from './../../core/helpers/app.helper';
import { TipoQuestao } from './../../core/models/pojo/tipo-questao.pojo';
import { TipoOpcao } from './../../core/models/tipo-opcao.enum';
import { ModeloOpcao } from './../../core/models/pojo/opcao.pojo';
import { Opcoes } from './../../core/models/opcoes.model';
import { ConfiguracaoFooterBar } from './../../core/models/pojo/configuracao-footer-bar.pojo';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Events, NavController } from 'ionic-angular';
import { Crise } from './../../core/models/crise.model';
import findIndex from 'lodash-es/findIndex';
import * as moment from 'moment';
import clone from 'lodash-es/clone';

export class QuestaoGridService {

    public componentesConfigurado: boolean = false;
    private _grid: GridQuestionarioComponent = null;
    private _navCtrlReference: NavController;
    private _configuracaoFooterBarReference: ConfiguracaoFooterBar;
    private _syncDataServiceReference: SyncDataProvider;
    private _eventsReference: Events;
    private _crise: Crise = {
        _id: null,
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    private _opcoesDocumento: Opcoes = {
        _id: null,
        opcoes: [],
    };

    public getGrid() {
        return this._grid;
    }

    public setGrid(grid: GridQuestionarioComponent) {
        this._grid = grid;
    }

    public getNavCtrlReference() {
        return this._navCtrlReference;
    }

    public setNavCtrlReference(navCtrlReference: NavController) {
        this._navCtrlReference = navCtrlReference;
    }

    public getEventsReference() {
        return this._eventsReference;
    }

    public setEventsReference(events: Events) {
        this._eventsReference = events;
    }

    public getSyncDataServiceReference() {
        return this._syncDataServiceReference;
    }

    public setSyncDataServiceReference(syncDataServiceReference: SyncDataProvider) {
        this._syncDataServiceReference = syncDataServiceReference;
    }

    public setOpcoesDocumento(opcoesDaQuestao: any) {
        this._opcoesDocumento = opcoesDaQuestao;
    }

    public getOpcoesDocumento() {
        return this._opcoesDocumento;
    }

    public getCrise(): any {
        return this._crise;
    }

    public setCrise(crise: Crise) {
        this._crise = crise;
    }

    public getConfiguracaoGridReference() {
        if (this.getGrid()) {
            return this.getGrid().configuracaoQuestao;
        } else {
            return null;
        }
    }

    public getConfiguracaoFooterBarReference() {
        return this._configuracaoFooterBarReference;
    }

    public setConfiguracaoFooterBarReference(configuracaoFooterBarReference: ConfiguracaoFooterBar) {
        this._configuracaoFooterBarReference = configuracaoFooterBarReference;
    }

    public montarGrid() {
        if (this.componentesConfigurado) {
            this.getGrid().refresh();
        } else {
            this.listenGridEvents();
            this.getGrid().iniciar();
            this.componentesConfigurado = true;
        }

        this._getClima();
    }

    public iniciarGrid(docCrise: Crise): Promise<any> {
        let scope: QuestaoGridService = this;

        return new Promise(
            function (resolve, reject) {
                if (docCrise && docCrise._id) {
                    scope.setCrise(clone(docCrise));
                    scope.getConfiguracaoGridReference().setCrise(clone(docCrise));
                    scope.getConfiguracaoFooterBarReference().setModoAtualizar(true);

                    if (scope.getConfiguracaoGridReference().getIdentificadorGrid() === TipoQuestao.METODO_ALIVIO_EFICIENTE) {
                        resolve();
                    } else {
                        scope._requisitarOpcoesDocumento().then(
                            () => {
                                scope.getConfiguracaoGridReference().setOpcoesDocumento(scope.getOpcoesDocumento());
                                resolve();
                            },
                            () => {
                                console.log('_requisitarOpcoesDocumento()~1');
                                reject();
                            },
                        );
                    }
                } else {

                    let appHelper: AppHelper = new AppHelper();
                    appHelper.getCriseId()
                        .then(
                        (_docCriseId) => {
                            scope.getCrise()._id = _docCriseId;
                            scope._requisitarDados().then(
                                () => {
                                    scope.getConfiguracaoGridReference().setCrise(scope.getCrise());
                                    scope.getConfiguracaoGridReference().setOpcoesDocumento(scope.getOpcoesDocumento());
                                    resolve();
                                },
                                () => {
                                    reject();
                                },
                            );
                        },
                    );
                }
            },
        );
    }

    public listenGridEvents() {
        /*this.getEventsReference().subscribe(
            'grid:removerOpcao' + this.getConfiguracaoGridReference().getIdentificadorGrid(),
            (manyDocuments: Array<any>) => {
                this.getSyncDataServiceReference().addOrUpdateManyDocuments(manyDocuments);
            },
        );*/
    }

    public destroyListenGridEvents() {
        // this.getEventsReference().unsubscribe('grid:removerOpcao' + this.getConfiguracaoGridReference().getIdentificadorGrid());
    }

    public salvarOpcoes() {

        if (!this.getGrid().isHouveInteracaoUsuario()) {
            this._verificarEContinuar();
        } else {
            this._onSavingQuestion(true);

            switch (this.getConfiguracaoGridReference().getIdentificadorGrid()) {
                case TipoQuestao.GATILHO_ESFORCO_FISICO:
                    this._addOuRetirarOpcaoGatilhoDaCrise('mm-gatilho-esforco-fisico', 'esforço físico');
                    break;
                case TipoQuestao.GATILHO_BEBIDA:
                    this._addOuRetirarOpcaoGatilhoDaCrise('mm-gatilho-bebida', 'bebida');
                    break;
                case TipoQuestao.GATILHO_COMIDA:
                    this._addOuRetirarOpcaoGatilhoDaCrise('mm-gatilho-comida', 'comida');
                    break;
                default:
                    this._atualizarCrise()
                        .then(
                        () => {
                            this._verificarEContinuar();
                        },
                        (err) => {
                            this._onErrorSaveQuestion(err);
                        },
                    );
            }
        }
    }

    private _getClima() {
        let scope: QuestaoGridService = this;
        let climaService: ClimaService = new ClimaService();
        climaService.setHttpReference(this.getSyncDataServiceReference().http);
        climaService.setSyncDataServiceReference(this.getSyncDataServiceReference());
        climaService.setCrise(this.getCrise());
        climaService.atualizarDadosClimaticos().then(
            () => {
                scope.setCrise(scope.getCrise());
            },
        );
    }

    private _addOpcaoParaCrise(opcaoSelecionada: ModeloOpcao): any {
        if (!this.getCrise()[this.getConfiguracaoGridReference().getNomeDoObjetoOpcao()]) {
            this.getCrise()[this.getConfiguracaoGridReference().getNomeDoObjetoOpcao()] = [];
        }

        let opcao: any = {};
        opcao._id = opcaoSelecionada.getId();
        opcao.nome = opcaoSelecionada.getNome();
        opcao.dataCriacao = (opcaoSelecionada.getDataCriacao() ? opcaoSelecionada.getDataCriacao() : moment().local().format(AppHelper.FORMATO_DATA_ISO_8601));
        opcao.ativo = opcaoSelecionada.getAtivo();
        opcao.tipo = opcaoSelecionada.getTipo();

        switch (this.getConfiguracaoGridReference().getIdentificadorGrid()) {
            case TipoQuestao.MEDICAMENTO:
                opcao.quantidade = opcaoSelecionada.getQuantidade();
                break;
            case TipoQuestao.METODO_ALIVIO_EFICIENTE:
                opcao.tipoEficiencia = opcaoSelecionada.getTipoEficiencia();
                break;
            case TipoQuestao.LOCAL:
                opcao.gps = opcaoSelecionada.getValorGPS();
                break;
            default:
                console.log('_addOpcaoParaCrise()~TRACK ERROR');
        }

        return opcao;
    }

    private _addOuRetirarOpcaoGatilhoDaCrise(idOpcao: string, nomeOpcao: string) {
        let opcaoSubGatilho: Opcao = {
            _id: idOpcao,
            nome: nomeOpcao,
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.FIXO,
        };

        let qtdSubGatilhos = this.getCrise()[this.getConfiguracaoGridReference().getNomeDoObjetoOpcao()].length;
        if (qtdSubGatilhos === 0 && this.getGrid().getOpcoesSelecionadas().length > 0) {
            this._addSubGatilho(opcaoSubGatilho);
        } else if (qtdSubGatilhos > 0 && this.getGrid().getOpcoesSelecionadas().length === 0) {
            this._excluirSubGatilho(opcaoSubGatilho);
        }

        this._atualizarCrise().then(
            () => {
                this.getConfiguracaoGridReference().setCrise(this.getCrise());
                this.getNavCtrlReference().pop();
            },
            (err: any) => {
                this._onErrorSaveQuestion(err);
            },
        );
    }

    private _addSubGatilho(opcaoSubGatilho: Opcao) {
        this.getCrise().gatilhos.push(opcaoSubGatilho);
    }

    private _excluirSubGatilho(opcaoSubGatilho: Opcao) {
        this.getCrise().gatilhos = new AppHelper().retirarOpcaoDoArray(this.getCrise().gatilhos, opcaoSubGatilho);
    }

    private _atualizarCrise(): Promise<any> {
        let scope: QuestaoGridService = this;

        return new Promise(
            function (resolve, reject) {

                scope._prepararDados().then(
                    () => {
                        if (scope.getConfiguracaoGridReference().getIdentificadorGrid() === TipoQuestao.LOCAL &&
                            scope.getGrid().getOpcoesLocalFixo().length > 0) { // ATUALIZAR LOCAL FIXO
                            scope._cadastrarOpcoesLocaisFixo().then(
                                () => {
                                    scope.getSyncDataServiceReference().addOrUpdate(scope.getCrise()).then(
                                        () => {
                                            // SUCESSO
                                            resolve();
                                        },
                                        () => {
                                            reject();
                                        },
                                    );
                                },
                            );
                        } else { // ATUALIZAR CRISE
                            scope.getSyncDataServiceReference().addOrUpdate(scope.getCrise()).then(
                                () => {
                                    // SUCESSO
                                    resolve();
                                },
                                () => {
                                    reject();
                                },
                            );
                        }
                    },
                );
            },
        );
    }

    private _verificarEContinuar() {
        if (this.getConfiguracaoGridReference().getRota() === 'sumario') {
            this.getNavCtrlReference().pop();
        } else {
            this._proximaQuestao();
        }
    }

    private _proximaQuestao() {
        switch (this.getConfiguracaoGridReference().getIdentificadorGrid()) {
            case TipoQuestao.METODO_ALIVIO:
                let medicamentos = this.getCrise().medicamentos;
                let metodosAlivio = this.getCrise().metodosAlivio;

                if (medicamentos.length > 0 || metodosAlivio.length > 0) {
                    this.getNavCtrlReference().push('MetodoAlivioEficientePage');
                } else {
                    this.getNavCtrlReference().push(this.getConfiguracaoGridReference().getProximaPagina());
                }
                break;
            case TipoQuestao.GATILHO_ESFORCO_FISICO:
                this.getNavCtrlReference().pop();
                break;
            case TipoQuestao.GATILHO_BEBIDA:
                this.getNavCtrlReference().pop();
                break;
            case TipoQuestao.GATILHO_COMIDA:
                this.getNavCtrlReference().pop();
                break;
            default:
                this.getNavCtrlReference().push(this.getConfiguracaoGridReference().getProximaPagina());
                this._onSavingQuestion(false);
        }
    }

    private _requisitarDados() {
        let scope: QuestaoGridService = this;

        return new Promise(
            function (resolve, reject) {
                scope._requisitarDocumentoCrise().then(
                    () => {
                        if (scope.getConfiguracaoGridReference().getIdentificadorGrid() === TipoQuestao.METODO_ALIVIO_EFICIENTE) {
                            resolve();
                        } else {
                            scope._requisitarOpcoesDocumento().then(
                                () => {
                                    resolve();
                                },
                                () => {
                                    reject();
                                },
                            );
                        }
                    },
                    () => {
                        reject();
                    },
                );
            },
        );
    }

    private _requisitarDocumentoCrise() {
        let scope: QuestaoGridService = this;

        return new Promise(
            function (resolve, reject) {
                scope.getSyncDataServiceReference().getDocumentById(scope.getCrise()._id).then(
                    (result: any) => {
                        if (result.error && result.status === 404) {
                            console.log('Não foi encontrado o documento com o id passado');
                            // Tratamento
                            reject();
                        } else if (result.error === undefined) {
                            scope.setCrise(result);
                            // this.getOpcoes();
                            resolve();
                        } else {
                            console.log('ERRO DESCONHECIMENTO', result);
                            reject();
                        }
                    },
                );
            },
        );
    }

    private _requisitarOpcoesDocumento(): Promise<any> {
        let scope: QuestaoGridService = this;

        return new Promise(
            function (resolve, reject) {
                scope.getSyncDataServiceReference().getDocumentById(
                    scope.getConfiguracaoGridReference().getDocumentoOptionId()).then(
                    (result: any) => {
                        if (result.error && result.status === 404) {

                            if (scope.getConfiguracaoGridReference().getIdentificadorGrid() === TipoQuestao.MEDICAMENTO) {

                                let documentoOpcoesMedicamento: Opcoes = {
                                    _id: SyncDataProvider.DOCUMENT_OPCOES_MEDICAMENTO_ID,
                                    opcoes: [],
                                };
                                scope.getSyncDataServiceReference().addOrUpdate(documentoOpcoesMedicamento).then(
                                    () => {
                                        scope.setOpcoesDocumento(documentoOpcoesMedicamento);
                                        resolve();
                                    },
                                    (err: any) => {
                                        console.log('Medicamento:_requisitarOpcoesDocumento()~TRACK ERROR', err);
                                        // TRACK
                                        scope.setOpcoesDocumento(documentoOpcoesMedicamento);
                                        reject();
                                    },
                                );
                            } else {
                                reject();
                            }
                        } else if (result.error === undefined) {

                            scope.setOpcoesDocumento(result);
                            resolve();
                        } else {
                            console.log('Tratar outros tipos de Erros!!');
                            reject();
                        }
                    },
                );
            },
        );
    }

    private _cadastrarOpcoesLocaisFixo(): Promise<any> {
        let scope: QuestaoGridService = this;

        return new Promise(function (resolve) {
            let isAtualizado: boolean = scope._atualizarOpcoesLocaisFixo();

            if (isAtualizado) {
                scope.getSyncDataServiceReference().addOrUpdate(scope.getOpcoesDocumento()).then(
                    () => {
                        // SUCESSO
                        resolve();
                    },
                    () => {
                        resolve();
                    },
                );
            }
        });
    }

    private _atualizarOpcoesLocaisFixo(): boolean {
        let isAtualizado: boolean = false;
        let indice: number = -1;
        let opcaoAtualiazar: ModeloOpcao;

        this.getOpcoesDocumento().opcoes.forEach(
            (opcaoCadastrada: any, i: number) => {
                this.getGrid().getOpcoesLocalFixo().every(
                    (opcaoParaAtualiazar) => {
                        if (opcaoCadastrada._id === opcaoParaAtualiazar.getId()) {
                            indice = i;
                            opcaoAtualiazar = opcaoParaAtualiazar;

                            return false;
                        } else {
                            return true;
                        }
                    },
                );
            },
        );

        if (indice > -1) {
            let opcaoCadastrada: any = this.getOpcoesDocumento().opcoes[indice];
            if (opcaoCadastrada.gps) {
                if (!opcaoAtualiazar.isLocalFixo()) {
                    delete opcaoCadastrada.gps;
                } else {
                    opcaoCadastrada.gps = opcaoAtualiazar.getValorGPS();
                }
            } else {
                opcaoCadastrada.gps = opcaoAtualiazar.getValorGPS();
            }

            isAtualizado = true;
        }

        return isAtualizado;
    }

    private _onSavingQuestion(estadoQuestao: boolean) {
        this.getConfiguracaoFooterBarReference().setSaving(estadoQuestao);
    }

    private _onErrorSaveQuestion(err: any) {
        console.log('DEU ERRO....', err);
    }

    private _prepararDados(): Promise<any> {
        let scope: QuestaoGridService = this;

        return new Promise(
            function (resolve) {
                scope._addOpcao();

                scope._retirarOpcao();

                // Medicamento ou eficiencia
                /*let opcoes1: Array<any> = scope.getCrise()[scope.getConfiguracaoGridReference().getNomeDoObjetoOpcao()];
                let opcoesSelecionadas: Array<ModeloOpcao> = scope.getGrid().getOpcoesSelecionadas();
                opcoesSelecionadas.forEach(
                    (opcaoSelecionada: any) => {
                        let index: number = -1;
                        index = findIndex(
                            opcoes1,
                            function (opcao: any) {
                                return opcao._id === opcaoSelecionada.getId();
                            },
                        );

                        if (index >= 0) {
                            let novaOpcao: Opcao = scope._addOpcaoParaCrise(opcaoSelecionada);
                            opcoes1[index] = novaOpcao;
                        }
                    },
                );*/

                resolve();
            },
        );
    }

    private _addOpcao() {
        for (let i = 0; i < this.getGrid().getOpcoesSelecionadas().length; i++) {
            let opcaoSelecionada = this.getGrid().getOpcoesSelecionadas()[i];
            let opcoesCrise: any = this.getCrise()[this.getConfiguracaoGridReference().getNomeDoObjetoOpcao()];
            let index: number = -1;

            if (opcoesCrise) {
                index = findIndex(
                    opcoesCrise,
                    function (opcao: any) {
                        return opcao._id === opcaoSelecionada.getId();
                    },
                );
            }

            if (index < 0) {
                let novaOpcao: any = this._addOpcaoParaCrise(opcaoSelecionada);
                this.getCrise()[this.getConfiguracaoGridReference().getNomeDoObjetoOpcao()].push(novaOpcao);
            }
        }
    }

    private _retirarOpcao() {
        let opcoes: any = this.getCrise()[this.getConfiguracaoGridReference().getNomeDoObjetoOpcao()];
        for (let i = opcoes.length - 1; i >= 0; i -= 1) {
            let indiceOpcaoSelecionada: number = -1;
            indiceOpcaoSelecionada = findIndex(
                this.getGrid().getOpcoesSelecionadas(),
                function (o: ModeloOpcao) {
                    return o.getId() === opcoes[i]._id;
                },
            );

            if (indiceOpcaoSelecionada === -1) {
                opcoes.splice(i, 1);
            }
        }
    }
}
