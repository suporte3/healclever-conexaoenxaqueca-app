import { AuthProvider } from './../../providers/auth/auth';
import { NavController } from 'ionic-angular';

export class ProtectedPageService {

    constructor(
        public _navCtrl: NavController,
        public _authService: AuthProvider) {
    }

    ionViewCanEnter() {

        /*this._authService.isAuthenticated()
            .then(
            (isAuthenticated: boolean) => {
                if (!isAuthenticated) {
                    this._authService.logout().then(
                        () => {
                            // Enviar mensagem para usuário:
                            // Sua sessão expirou. Faça o login Mantenha-se ativo para a sessão não expirar em 30 dias.
                            this._navCtrl.setRoot('TutorialPage');
                        },
                    );
                    return false;
                }
            },
            );*/

        return true;
    }
}
