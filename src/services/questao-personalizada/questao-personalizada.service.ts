import { ClimaService } from './../clima/clima.service';
import { NotasPage } from './../../pages/questionario/notas/notas';
import { NavController } from 'ionic-angular';
import { NotificationService } from './../notification/notification.service';
import { AppHelper } from './../../core/helpers/app.helper';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Crise } from './../../core/models/crise.model';

export class QuestaoPersonalizadaService {

    private _page: any;
    private _crise: Crise = {
        _id: null,
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    private _syncDataProviderReference: SyncDataProvider;
    private _sumario: boolean = false;
    private _proximaPagina: string;
    private _houveInteracao: boolean;

    constructor() { }

    public getPage(): any {
        return this._page;
    }

    public setPage(page: any) {
        this._page = page;
    }

    public getCrise(): Crise {
        return this._crise;
    }

    public setCrise(crise: Crise) {
        this._crise = crise;
    }

    public getSyncDataServiceReference(): SyncDataProvider {
        return this._syncDataProviderReference;
    }

    public setSyncDataServiceReference(syncDataProvider: SyncDataProvider) {
        this._syncDataProviderReference = syncDataProvider;
    }

    public getProximaPagina(): string {
        return this._proximaPagina;
    }

    public setProximaPagina(proximaPagina: string) {
        this._proximaPagina = proximaPagina;
    }

    public isSumario(): boolean {
        return this._sumario;
    }

    public setSumario(sumario: boolean) {
        this._sumario = sumario;
    }

    public setHouveInteracao(houveInteracao: boolean) {
        this._houveInteracao = houveInteracao;
    }

    public getHouveInteracao() {
        return this._houveInteracao;
    }

    public registrarQuestao(): Promise<any> {
        let scope: QuestaoPersonalizadaService = this;

        let promise: Promise<any>;
        if (this.getHouveInteracao()) {

            this.getPage().onSavingQuestion(true);

            promise = new Promise(
                function (resolve, reject) {
                    if (scope.getSyncDataServiceReference() && scope.getPage()) {
                        scope.getPage().onCallbackPreRegistrarQuestao()
                            .then(
                            () => {
                                scope._commitQuestao().then(
                                    () => {
                                        resolve();
                                    },
                                    () => {
                                        reject();
                                    },
                                );
                            },
                        );
                    } else {
                        reject();
                    }
                },
            );
        } else {
            promise = new Promise(
                function (resolve) {
                    scope._nextPage();
                    resolve();
                },
            );
        }

        return promise;
    }

    public getCriseId(): Promise<any> {
        let appHelper: AppHelper = new AppHelper();
        let scope = this;

        return new Promise(
            function (resolve, reject) {
                try {
                    appHelper.getCriseId().then(
                        (docCriseId) => {
                            scope.getCrise()._id = docCriseId;
                            scope._consultarDocumento()
                                .then(
                                () => {
                                    resolve(docCriseId);
                                },
                                (err) => {
                                    reject(err);
                                },
                            );
                        },
                    );
                } catch (e) {
                    reject(e);
                }
            },
        );
    }

    public triggerAtualizarQuestao() {
        this._atualizarQuestao();
    }

    private _commitQuestao(): Promise<any> {
        let scope: QuestaoPersonalizadaService = this;

        return new Promise(
            function (resolve, reject) {
                scope.getSyncDataServiceReference().addOrUpdate(scope.getCrise())
                    .then(
                    () => {

                        scope._checkAndLaunchNotification();
                        scope.getPage().onSavingQuestion(false);
                        scope._nextPage();
                        resolve();
                    },
                    (err) => {
                        scope.getPage().onSavingQuestion(false);
                        scope.getPage().onErrorSaveQuestion(err);
                        reject();
                    },
                );
            },
        );
    }

    private _checkAndLaunchNotification() {
        if (!this.getCrise().dataFinal) {
            NotificationService.launchCriseEmProgresso();
        }
    }

    private _nextPage() {
        let navCtrl: NavController = this.getPage()._navCtrl; // ACESSO AO navCtrl SEM CONTRATO E PERIGOSO
        if (this.isSumario()) {
            navCtrl.pop();
        } else {
            if (this.getPage() instanceof NotasPage) {
                navCtrl.setRoot(this.getProximaPagina());
            } else {
                navCtrl.push(this.getProximaPagina());
            }
        }
    }

    private _consultarDocumento(): Promise<any> {
        let scope: QuestaoPersonalizadaService = this;
        return new Promise(
            function (resolve, reject) {
                scope.getSyncDataServiceReference().getDocumentById(scope.getCrise()._id).then(
                    (result: any) => {
                        if (result.error && result.status === 404) {
                            // TRACK
                            reject(result.error);
                        } else if (result.error === undefined) {
                            scope.setCrise(result);
                            scope._atualizarQuestao().then(
                                () => {
                                    resolve();
                                },
                            );
                        } else {
                            // TRACK
                            reject(result);
                        }
                    });
            },
        );
    }

    private _atualizarQuestao(): Promise<any> {
        let scope: QuestaoPersonalizadaService = this;

        return new Promise(
            function (resolve) {
                scope.getPage().onCallbackPreAtualizarQuestao()
                    .then(
                    () => {
                        // Thread
                        let climaService = new ClimaService();
                        climaService.setHttpReference(scope.getSyncDataServiceReference().http);
                        climaService.setSyncDataServiceReference(scope.getSyncDataServiceReference());
                        climaService.setCrise(scope.getCrise());
                        climaService.atualizarDadosClimaticos().then(
                            () => {
                                scope.setCrise(scope.getCrise());
                            },
                        );

                        scope.getPage().onAtualizarQuestao();
                        resolve();
                    },
                );
            },
        );
    }
}
