import { Clima } from './../../core/models/clima.model';
import { Network } from '@ionic-native/network';
import { Crise } from './../../core/models/crise.model';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class ClimaService {

  private _httpReference: Http;
  private _syncDataServiceReference: SyncDataProvider;
  private _crise: Crise;

  public setSyncDataServiceReference(syncDataService: SyncDataProvider) {
    this._syncDataServiceReference = syncDataService;
  }

  public getSyncDataServiceReference() {
    return this._syncDataServiceReference;
  }

  public setHttpReference(httpReference: Http) {
    this._httpReference = httpReference;
  }

  public getHttpReference(): Http {
    return this._httpReference;
  }

  public setCrise(crise: Crise) {
    this._crise = crise;
  }

  public getCrise() {
    return this._crise;
  }

  public getClimaAtual(latitude: number, longitude: number): Promise<any> {
    let scope: ClimaService = this;

    return new Promise(function (resolve, reject) {
      let network = new Network();
      if (network.type !== 'none') {

        let queryString: string = '?latitude=' + latitude + '&longitude=' + longitude;

        scope.getHttpReference()
          .get(SyncDataProvider.BASE_API_URL + '/api/clima/atual' + queryString)
          .subscribe(
          (res: any) => {
            resolve(res);
          },
          (err: any) => {
            console.log('Clima service ERROR:', err);
            reject();
          },
        );
      } else {
        reject();
      }
    });
  }

  public atualizarDadosClimaticos(): Promise<any> {
    let scope: ClimaService = this;
    return new Promise(
      function (resolve) {

        let locais: Array<any> = scope.getCrise().locais;
        if (!scope.getCrise().clima && locais.length > 0 && locais[0].gps) {

          scope.getClimaAtual(locais[0].gps.latitude, locais[0].gps.longitude).then(
            (climaResponse: any) => {
              climaResponse = JSON.parse(climaResponse._body);

              if (!scope.getCrise().clima) {
                scope.getCrise().clima = <Clima>{};
              }

              if (climaResponse.observation.wx_phrase) {
                scope.getCrise().clima.descricao = climaResponse.observation.wx_phrase;
              }
              if (climaResponse.observation.wx_icon) {
                scope.getCrise().clima.codigo = climaResponse.observation.wx_icon;
              }
              if (climaResponse.observation.temp) {
                scope.getCrise().clima.temperatura = climaResponse.observation.temp;
              }
              if (climaResponse.observation.pressure) {
                scope.getCrise().clima.pressaoAtmosferica = climaResponse.observation.pressure;
              }
              if (climaResponse.observation.pressure_tend) {
                scope.getCrise().clima.pressaoAtmosfericaUltimaHora = climaResponse.observation.pressure_tend;
              }
              if (climaResponse.observation.day_ind) {
                scope.getCrise().clima.diaOuNoite = climaResponse.observation.day_ind;
              }

              scope.getSyncDataServiceReference().addOrUpdate(scope.getCrise()).then(
                () => {
                  resolve();
                },
                () => {
                  resolve();
                },
              );
            },
            () => {
              console.log('Erro ao obter clima');
              resolve();
            },
          );
        } else {
          resolve();
        }
      },
    );
  }

}
