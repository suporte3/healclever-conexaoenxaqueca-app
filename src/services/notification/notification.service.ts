import { LocalNotifications } from '@ionic-native/local-notifications';

export class NotificationService {

    public static readonly CRISE_EM_PROGRESSO_NOTIFICATION_ID: number = 123456;

    public static launchCriseEmProgresso() {

        new LocalNotifications().schedule({
            id: NotificationService.CRISE_EM_PROGRESSO_NOTIFICATION_ID,
            title: 'Crise em andamento',
            text: 'Toque aqui para completar sua entrada',
            ongoing: true,
        });
    }

    public static cancelarNotificaticao(id: number) {
        new LocalNotifications().cancel(id);
    }

    public static launchTestNotiication(titulo: string) {
        new LocalNotifications().schedule({
            id: Math.floor(Math.random() * 65536),
            title: titulo,
            text: 'Slide to dismiss',
        });
    }
}
