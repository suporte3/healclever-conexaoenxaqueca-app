import { JwtHelper } from 'angular2-jwt';
import { Storage } from '@ionic/storage';
import { ApiProvider } from './../api/api';
import { Injectable } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { asap } from 'rxjs/scheduler/asap';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import * as moment from 'moment';

@Injectable()
export class AuthProvider {

  private static readonly FIRST_TIME_SCHEDULED_REFRESH: number = 3000;
  private static readonly REPEAT_TIME_SCHEDULED_REFRESH: number = (15 * 60) * 1000;
  private static readonly AUTH_BASE_URL: string = 'https://service.us.apiconnect.ibmcloud.com/gws/apigateway/api/321c788a37912aeded69af2b85a98a3abc4dc268e1cfc3e3e512848d903e0d31/authentication';

  private _jwtToken: string;
  private _refreshTokenSubscription: Subscription;

  constructor(private _api: ApiProvider, private _storage: Storage) {

  }

  public signup(fbAuthCode: string) {
    let url: string = AuthProvider.AUTH_BASE_URL + '/auth/signup';
    let payload: any = {
      authCode: fbAuthCode,
    };

    return this._api.get(url, payload)
      .map(response => response.json())
      .toPromise()
      .then(
      (response: any) => {
        if (response.statusCode === 200) {
          this._saveToken(response.body);
          return response.body;
        }
      },
    ).catch(
      (err: any) => { console.log('Error signup', err); },
    );
  }

  public refreshToken(token: string) {
    let url: string = AuthProvider.AUTH_BASE_URL + '/auth/refresh-token';
    let payload: any = {
      token: token,
    };

    return this._api.get(url, payload)
      .map(response => response.json())
      .toPromise()
      .then(
      (response: any) => {
        if (response.statusCode === 200) {
          this._saveToken(response.body);

          return response.body;
        }
      },
    ).catch(
      (err: any) => { console.log('Error refreshToken', err); },
    );
  }

  public isValidToken(): Promise<boolean> {
    return this._storage.get('jwt_token').then(
      (token) => {
        if (!token) { return false; }
        let jwtHelper = new JwtHelper();
        return token != null && !jwtHelper.isTokenExpired(token);
      },
    );
  }

  public isAuthenticated() {
    return this._storage.get('jwt_token').then(
      (token) => {
        if (!token) { return false; }
        return true;
      },
    );
  }

  public getCloudantCredentials(token: any) {
    let scope: AuthProvider = this;
    let url: string = AuthProvider.AUTH_BASE_URL + '/auth/credentials';
    let payload: any = {
      token: token,
    };

    return new Promise(
      (resolve, reject) => {
        scope._api.get(url, payload)
          .map((response) => response.json())
          .subscribe(
          (response: any) => {
            if (response.statusCode === 200) {
              let tokenCredentials: string = response.body;
              scope._storage.set('cloudant_credentials', tokenCredentials);
              resolve(tokenCredentials);
            }
          },
          () => {
            scope._storage.get('cloudant_credentials').then(
              (tokenCredentials: string) => {
                if (tokenCredentials) {

                  resolve(tokenCredentials);
                } else {
                  reject();
                }
              },
            );
          },
        );
      },
    );
  }

  public logout(): Promise<any> {
    return new Promise(
      (resolve) => {
        this.unscheduleRefreshToken();
        this._storage.remove('jwt_token');
        this._storage.remove('cloudant_credentials');

        resolve();
      },
    );
  }

  public unscheduleRefreshToken() {
    if (this._refreshTokenSubscription) {
      this._refreshTokenSubscription.unsubscribe();
    }
  }

  public startupTokenRefresh() {

    this._storage.get('jwt_token').then(
      (jwt: any) => {

        if (jwt) {
          let jwtToken: any = new JwtHelper().decodeToken(jwt);

          let momentExp: any = moment.unix(jwtToken.exp);
          let momentIat: any = moment.unix(jwtToken.iat);
          let ms: number = momentExp.diff(momentIat);
          let duration: number = (moment.duration(ms).days() / 2);
          let metadeDuration: any = momentExp.subtract(duration, 'days');

          if (moment().isSameOrAfter(metadeDuration)) {
            this._jwtToken = jwt;
            this._scheduleNewRefreshToken(AuthProvider.FIRST_TIME_SCHEDULED_REFRESH);
          }
        }
      },
    );
  }

  private _getNewToken() {
    this.refreshToken(this._jwtToken).then(
      (token: any) => {
        if (token) {
          this.unscheduleRefreshToken();
        } else {
          this._scheduleNewRefreshToken(AuthProvider.REPEAT_TIME_SCHEDULED_REFRESH);
        }
      },
    );
  }

  private _scheduleNewRefreshToken(timeMillis: number) {
    this._refreshTokenSubscription = asap.schedule(
      () => {
        this._getNewToken();
      },
      timeMillis,
    );
  }

  private _saveToken(token: string) {
    this._storage.set('jwt_token', token);
  }

}
