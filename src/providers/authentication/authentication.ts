import { AppHelper } from './../../core/helpers/app.helper';
import { Usuario } from './../../core/models/usuario/usuario.model';
import { Observable } from 'rxjs/Observable';
import { SyncDataProvider } from './../sync-data/sync-data';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import { Http, Response, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/**
 * @deprecated Serviço substituído por AuthProvider
 */
@Injectable()
export class AuthenticationProvider {

  static readonly ONLINE: number = 1;
  static readonly OFFLINE: number = 2;

  static readonly LOGIN: number = 1;
  static readonly CADASTRO: number = 2;
  static readonly TOKEN: number = 3;
  static readonly FACEBOOK: number = 4;

  private _page: any;
  private _usuarioDados: Usuario;
  private _syncDataServiceReference: SyncDataProvider;
  private _httpReference: Http;
  private _credenciaisAutenticacao: any;
  private _appHelper: AppHelper;
  private _facebookProfileID: string;

  constructor(public http: Http, private _storage: Storage) {

  }

  public setPage(page: any) {
    this._page = page;
  }

  public getFacebookProfileID() {
    return this._facebookProfileID;
  }

  public setFacebookProfileID(facebookProfileID: string) {
    this._facebookProfileID = facebookProfileID;
  }

  public setCredenciaisAutenticacao(credenciaisAutenticacao: any) {
    this._credenciaisAutenticacao = credenciaisAutenticacao;
  }

  public getCredenciaisAutenticacao() {
    return this._credenciaisAutenticacao;
  }

  public setUsuarioDados(usuarioDados: any) {
    this._usuarioDados = usuarioDados;
  }

  public getUsuarioDados(): Usuario {
    return this._usuarioDados;
  }

  public setSyncDataServiceReference(syncDataServiceReference: SyncDataProvider) {
    this._syncDataServiceReference = syncDataServiceReference;
  }

  public getSyncDataServiceReference() {
    return this._syncDataServiceReference;
  }

  public setHttpReference(httpReference: Http) {
    this._httpReference = httpReference;
  }

  public getHttpReference(): Http {
    return this._httpReference;
  }
  /*
    public facebookLogin(): Promise<any> {
      let permissoesDoUsuario: string[] = ['email', 'public_profile', 'user_friends', 'user_birthday'];
      return new Facebook().login(permissoesDoUsuario);
    }*/

  public verificarSeUsuarioExiste(email: string): Observable<Response> {
    return this.getHttpReference().get(SyncDataProvider.BASE_API_URL + '/auth/validate-email/' + email);
  }

  public buscarAtualizarPerfilUsuario(): Promise<any> {
    let scope = this;

    return new Promise(
      function (resolve, reject) {
        scope.getSyncDataServiceReference().getDocumentById(SyncDataProvider.DOCUMENT_PERFIL_ID)
          .then(
          (result) => {
            if (result.error && result.status === 404) {
              scope._criarAtualizarDocumentoPerfilUsuario()
                .then(
                () => {
                  resolve(scope.getUsuarioDados());
                },
                () => {
                  reject();
                },
              );

              // cadastrar imagem
              // scope.getSyncDataServiceReference().addOrUpdateProfileImageByFacebook(scope.getFacebookProfileID(), null);
            } else if (result.error === undefined) {
              scope._criarAtualizarDocumentoPerfilUsuario()
                .then(
                () => {
                  resolve(scope.getUsuarioDados());
                },
                () => {
                  reject();
                },
              );
              // resolve(result);
            } else {
              // MONITORAR ERRO DESCONHECIDO
              reject(result);
            }
          },
          (err) => {
            reject(err);
          },
        );
      },
    );
  }

  public requisitarLoginBySmarthone(emailUsuarioFormulario: string) {
    this._getAppUtils().getUsuarioByEmail(emailUsuarioFormulario)
      .then(
      (usuario) => {
        this.getSyncDataServiceReference().iniciarDatabase(usuario)
          .then(
          () => {
            this.getSyncDataServiceReference().setDatabaseContinuous(usuario.sincronizado);
            this.prepararUsuario(AuthenticationProvider.OFFLINE);
          },
          () => {

            this._page.onErrorDatabaseInit(AuthenticationProvider.OFFLINE);
          },
        );
      },
      () => {

        this._page.onLoginBySmartphoneUserNotFound();
      },
    );
  }

  public requisitarLoginComServidor() {

    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.getHttpReference().post(SyncDataProvider.BASE_API_URL + '/auth/login',
      JSON.stringify(this.getCredenciaisAutenticacao()), { headers: headers })
      .subscribe((responseLogin: any) => {
        let sessaoUsuario = JSON.parse(responseLogin['_body']);

        this.getSyncDataServiceReference().iniciarDatabase(sessaoUsuario)
          .then(
          (dadosPerfilUsuario) => {

            this.setUsuarioDados(dadosPerfilUsuario);
            this.prepararUsuario(AuthenticationProvider.ONLINE);

          },
          () => {
            this._page.onErrorDatabaseInit(AuthenticationProvider.OFFLINE);
          },
        );
      }, (error) => {
        // TRACK ERROR
        this._page.onErrorRequest(error, AuthenticationProvider.LOGIN);
      });
  }

  public requisitarCadastroComFacebook(facebookDados: any) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.getHttpReference().post(SyncDataProvider.BASE_API_URL + '/auth/facebook/token',
      JSON.stringify(this.getCredenciaisAutenticacao()), { headers: headers })
      .subscribe(
      (responseFacebook: any) => {
        let sessaoUsuario: any = JSON.parse(responseFacebook['_body']);
        sessaoUsuario.email = facebookDados.email;
        sessaoUsuario.first_name = facebookDados.first_name;
        sessaoUsuario.last_name = facebookDados.last_name;
        sessaoUsuario.birthday = facebookDados.birthday;
        sessaoUsuario.genero = this._getAppUtils().getFacebookGenero(facebookDados.gender);
        this.getSyncDataServiceReference().iniciarDatabase(sessaoUsuario)
          .then(
          (usuarioDados) => {
            this.setFacebookProfileID(facebookDados.id);

            usuarioDados.dataAdmissao = moment().local().format(AppHelper.FORMATO_DATA_ISO_8601);
            this.setUsuarioDados(usuarioDados);
            this.prepararUsuario(AuthenticationProvider.ONLINE);
          },
          (error) => {
            this._page.onErrorDatabaseInit(error);
          },
        );
      },
      (error) => {
        this._page.onErrorRequest(error, AuthenticationProvider.FACEBOOK);
      },
    );
  }

  public requisitarCadastroUsuario() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');

    this.getHttpReference().post(SyncDataProvider.BASE_API_URL + '/auth/register',
      JSON.stringify(this.getCredenciaisAutenticacao()), { headers: headers })
      .subscribe((responseRegister: any) => {
        let sessaoUsuario = JSON.parse(responseRegister['_body']);
        this.getSyncDataServiceReference().iniciarDatabase(sessaoUsuario).then(
          (dadosPerfilUsuario: Usuario) => {
            dadosPerfilUsuario.dataAdmissao = moment().local().format(AppHelper.FORMATO_DATA_ISO_8601);
            this.setUsuarioDados(dadosPerfilUsuario);
            this.prepararUsuario(AuthenticationProvider.ONLINE);
          },
          () => {
            this._page.onErrorDatabaseInit(AuthenticationProvider.ONLINE);
          },
        );
      }, (error: any) => {
        this._page.onErrorRequest(error, AuthenticationProvider.CADASTRO);
        // TRACK ERROR
      });
  }

  public recuperarSessao() {
    this.refreshTokenUsuario()
      .subscribe(
      (responseToken: any) => {
        let sessaoUsuario = JSON.parse(responseToken['_body']);
        this.getSyncDataServiceReference().iniciarDatabase(sessaoUsuario)
          .then(
          (usuarioDadosAtualizado) => {
            this.setUsuarioDados(usuarioDadosAtualizado);
            this.getSyncDataServiceReference().setDatabaseContinuous(usuarioDadosAtualizado.sincronizado);
            this.prepararUsuario(AuthenticationProvider.ONLINE);
          },
          () => {
            this._page.onErrorDatabaseInit(AuthenticationProvider.ONLINE);
          },
        );
      },
      (error) => {
        // TRACK
        this._page.onErrorRequest(error, AuthenticationProvider.TOKEN);
      },
    );
  }

  public prepararUsuario(tipoAutenticacao: number) {
    tipoAutenticacao = 0;
    /*let seedService = new SeedService(this.getSyncDataServiceReference(), this.getHttpReference());
    seedService.adicionarOpces();
    if (tipoAutenticacao === AuthenticationProvider.ONLINE) {
      this.buscarAtualizarPerfilUsuario()
        .then(
        (documentoPerfilUsuario) => {
          this.getSyncDataServiceReference().setDatabaseContinuous(documentoPerfilUsuario.sincronizado);
          this.getSyncDataServiceReference().sincronizarDatabase(documentoPerfilUsuario.sessao)
            .then(
            () => {
              this._page.onSuccessLogin(AuthenticationProvider.ONLINE);
            },
            () => {
              this._page.onErrorSincronizarDatabase();
            },
          );
        },
        (error) => {
          this._page.onErrorAtualizarPerfilUsuario(error);
        },
      );
    } else {
      // verificar se pode sincronizar
      this._page.onSuccessLogin(AuthenticationProvider.OFFLINE);
    }*/
  }

  public refreshTokenUsuario(): Observable<Response> {
    let headers = new Headers();
    headers.append(
      'Content-Type',
      'application/json',
    );
    headers.append(
      'Authorization',
      'Bearer ' + this.getUsuarioDados().sessao.token + ':' + this.getUsuarioDados().sessao.password,
    );

    return this.getHttpReference().post(SyncDataProvider.BASE_API_URL + '/auth/refresh', null, { headers: headers });
  }

  public requestLogout(): Observable<Response> {
    let headers = new Headers();
    headers.append(
      'Content-Type',
      'application/json',
    );
    headers.append(
      'Authorization',
      'Bearer ' + this.getUsuarioDados().sessao.token + ':' + this.getUsuarioDados().sessao.password,
    );

    return this.getHttpReference()
      .post(
      SyncDataProvider.BASE_API_URL + '/auth/logout',
      null,
      { headers: headers },
    );
  }

  private _gravarDadosUsuarioEmCelular(): void {

    try {

      this._getAppUtils().atualizarPerfilUsuario(this.getUsuarioDados(), { sessao: this.getUsuarioDados().sessao, logado: true })
        .then(
        () => { },
        () => {
          this._storage.set('usuarios', [this.getUsuarioDados()]);
        },
      );
    } catch (e) {
      // TRACK
    }
  }

  private _criarAtualizarDocumentoPerfilUsuario() {

    let scope = this;

    return new Promise(
      function (resolve, reject) {

        scope.getSyncDataServiceReference().addOrUpdate(scope.getUsuarioDados()).then(
          () => {
            scope._gravarDadosUsuarioEmCelular();
            resolve(scope.getUsuarioDados());
          },
          () => {
            reject();
            // TRACK ERROR
          },
        );
      },
    );
  }

  private _getAppUtils() {
    if (!this._appHelper) {
      return this._appHelper = new AppHelper();
    }
  }

}
