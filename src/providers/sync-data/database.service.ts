import * as PouchDB from 'pouchdb';
import * as PouchDBFind from 'pouchdb-find';
PouchDB.plugin(PouchDBFind);

export abstract class DatabaseService {

    static readonly DOCUMENT_PERFIL_ID = 'perfil_id';
    static readonly DOCUMENT_PERFIL_IMAGEM_ID = 'perfil_imagem_id';

    static readonly DOCUMENT_OPCOES_MEDICAMENTO_ID = 'ce-1234-ajshs-medicamento-skms1';
    static readonly DOCUMENT_OPCOES_QUALIDADE_DOR_ID = 'ce-1234-qualidade-dor-skms1';
    static readonly DOCUMENT_OPCOES_SINTOMA_ID = 'ce-1234-sintoma-skms1';
    static readonly DOCUMENT_OPCOES_GATILHO_ID = 'ce-1234-gatilho-skms1';
    static readonly DOCUMENT_OPCOES_GATILHO_ESFORCO_FISICO_ID = 'ce-1234-esforco-fisico-skms1';
    static readonly DOCUMENT_OPCOES_GATILHO_COMIDA_ID = 'ce-1234-comida-skms1';
    static readonly DOCUMENT_OPCOES_GATILHO_BEBIDA_ID = 'ce-1234-bebida-skms1';
    static readonly DOCUMENT_OPCOES_CICLO_MENSTRUAL_ID = 'ce-1234-ciclo-menstrual-skms1';
    static readonly DOCUMENT_OPCOES_METODO_ALIVIO_ID = 'ce-1234-metodo-alivio-skms1';
    static readonly DOCUMENT_OPCOES_CONSEQUENCIA_ID = 'ce-1234-consequencia-skms1';
    static readonly DOCUMENT_OPCOES_LOCAL_ID = 'ce-1234-local-skms1';
    static readonly DOCUMENT_OPCOES_LOCALIZACAO_DOR_ID = 'ce-1234-localizacao-dor-skms1';
    static readonly DOCUMENT_OPCOES_ALIMENTOS_ID = 'ce-1234-alimentos-skms1';
    static readonly DOCUMENT_DIARIO_SAUDE_ID = 'diario-saude-';
    // static readonly BASE_API_URL = 'http://conexao-enxaqueca-api.mybluemix.net';
    static readonly BASE_API_URL = 'http://platform-elo.mybluemix.net';

    private _db: PouchDB.Database<{}>;
    private _databaseContinuous: boolean;

    public isDatabaseContinuous(): boolean {
        return this._databaseContinuous;
    }

    public setDatabaseContinuous(databaseContinuous: boolean) {
        this._databaseContinuous = databaseContinuous;
    }

    public iniciarDatabase(dbName: any): Promise<any> {

        return this._configurarBancoDeDados(dbName);
    }

    public sincronizarDatabase(dbName: string, credentials: any): Promise<any> {

        let scope: DatabaseService = this;

        return new Promise(
            function (resolve, reject) {
                try {

                    // let username: string = 'e7779e88-0e3f-46b7-bf37-5e61bce6ccbe-bluemix';
                    // let password: string = 'd9aa6e1b4efa5223d60e573bfb88baf23ab2e3729ffae0536e06eba83411484b';
                    let remote: string = 'https://' + credentials.username + '.cloudant.com/' + dbName;

                    console.log('REMOTE', remote);

                    let options: any = {};
                    options.live = true;
                    options.retry = true;
                    options.continuous = true;
                    options.auth = {
                        username: credentials.username,
                        password: credentials.password,
                    };
                    // options.ajax = {timeout: 10000}; // PERF
                    // options.ajax.cache = true;

                    if (!scope.isDatabaseContinuous()) {
                        options.doc_ids = [
                            DatabaseService.DOCUMENT_PERFIL_ID,
                            DatabaseService.DOCUMENT_PERFIL_IMAGEM_ID,
                        ];
                    }

                    let syncDatabase: any = scope.getDatabase().sync(remote, options);
                    syncDatabase.on('change', function (info: any) {
                        if (!scope.isDatabaseContinuous()) {
                            if (info.change.docs.length > 0) {

                                if (info.change.docs[0]._id === DatabaseService.DOCUMENT_PERFIL_ID) {
                                    syncDatabase.cancel();
                                }
                            }
                        }
                    });

                    syncDatabase.on('denied', function (err: any) {
                        if (!scope.isDatabaseContinuous()) {
                            syncDatabase.cancel();
                            // Fazer logout
                            // Emitir evento para apresentar mensagem de erro

                            console.log('sync on denied', err);
                            // Track err
                        }
                    });
                    syncDatabase.on('error', function (err: any) {
                        if (!scope.isDatabaseContinuous()) {
                            syncDatabase.cancel();
                            // Fazer logout
                            // Emitir evento para apresentar mensagem de erro

                            console.log('sync on error', err);
                            // Track err
                        }
                    });
                    resolve();
                } catch (e) {
                    reject();
                    // track exception e
                }
            },
        );
    }

    public getDatabase(): PouchDB.Database<{}> {
        return this._db;
    }

    public closePouchDB(): void {

        if (!this._db) {
            return;
        }

        this._db.close();
        this._db = null;
    }

    private _configurarBancoDeDados(dbName: any): Promise<any> {

        let scope = this;

        return new Promise(
            function (resolve, reject) {
                try {
                    scope.closePouchDB();

                    let nomeDaBaseDeDados: string = 'rosie.' + dbName;
                    scope._db = new PouchDB(nomeDaBaseDeDados);
                    // console.log('ADAPTADOR POUCHDB', scope._db['adapter']);
                    // scope._db = new PouchDB(nomeDaBaseDeDados, { adapter: 'websql' });
                    if (scope._db) {
                        resolve();
                    } else {
                        reject();
                    }
                } catch (e) {
                    reject();
                }
            },
        );
    }
}
