import { AuthProvider } from './../auth/auth';
import { JwtHelper } from 'angular2-jwt';
import { SeedService } from './../../services/seed/seed';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { DatabaseService } from './database.service';
import { Storage } from '@ionic/storage';

@Injectable()
export class SyncDataProvider extends DatabaseService {

  private _conflictCount: number = 0;
  private readonly MAX_CONFLICTS: number = 3;

  constructor(public http: Http, private _jwtHelper: JwtHelper, private _authService: AuthProvider) {

    super();
  }

  public createIndex(index: any): Promise<any> {
    return this.getDatabase().createIndex(index);
  }

  public find(query: any): Promise<any> {
    return this.getDatabase().find(query);
  }

  public prepararUsuario(): Promise<any> {
    let scope: SyncDataProvider = this;

    return new Promise(
      (resolve, reject) => {
        new Storage({}).get('jwt_token').then(
          (token: string) => {
            if (token) {
              let userData: any = this._jwtHelper.decodeToken(token);
              scope.iniciarDatabase(userData._id).then(
                () => {
                  // INI SEED - ADD SUPPORT PARA I18n
                  let seedService = new SeedService(this, this.http);
                  seedService.adicionarOpces();
                  // FIM SEED

                  scope.setDatabaseContinuous(true); // DEFINIR
                  scope._authService.getCloudantCredentials(token).then(
                    (tokenCredentials: string) => {

                      let credentials: any = scope._jwtHelper.decodeToken(tokenCredentials);
                      scope.sincronizarDatabase(userData.dbname, credentials).then(
                        () => {
                          resolve();
                        },
                        () => {
                          // log err
                          reject();
                        },
                      );
                    },
                    (oldTokenCredentials: any) => {
                      if (oldTokenCredentials) {

                        let credentials: any = scope._jwtHelper.decodeToken(oldTokenCredentials);
                        scope.sincronizarDatabase(userData.dbname, credentials).then(
                          () => {
                            resolve();
                          },
                          () => {
                            // log err
                            reject();
                          },
                        );
                      } else {
                        reject();
                      }
                    },
                  );
                },
                () => {
                  // log erro
                  reject();
                },
              );
            } else {
              reject();
            }
          },
        );
      },
    );
  }

  /*
    public addOrUpdateProfileImageByFacebook(facebookId: string, _rev: string): Promise<any> {
      let appHelper: AppHelper = new AppHelper();
      let scope = this;

      return new Promise(
        function (resolve, reject) {
          appHelper.getFacebookProfilePicture(facebookId)
            .then(
            (facebookPictureURL) => {
              let mimeType: string = appHelper.getMimetype(facebookPictureURL);
              if (mimeType) {
                appHelper.getBlobImage(facebookPictureURL, mimeType)
                  .then(
                  (blobData) => {
                    scope.addOrUpdateProfileImage(blobData, mimeType, _rev)
                      .then(
                      (blob) => {
                        resolve(blob);
                      },
                      (err) => {
                        reject(err);
                      },
                    );
                  },
                  (err) => {
                    reject(err);
                  },
                );
              }
            },
            (err) => {
              reject(err);
            },
          );
        },
      );
    }
  */
  public addOrUpdateProfileImage(attachment: Blob, mimeType: string, _rev: string): Promise<any> {
    let scope = this;
    let promise: Promise<any>;

    return new Promise(
      function (resolve, reject) {
        if (_rev) {
          promise = scope.getDatabase()
            .putAttachment(SyncDataProvider.DOCUMENT_PERFIL_IMAGEM_ID, 'perfil', _rev, attachment, mimeType);
        } else {
          promise = scope.getDatabase()
            .putAttachment(SyncDataProvider.DOCUMENT_PERFIL_IMAGEM_ID, 'perfil', attachment, mimeType);
        }

        promise.then(
          function () {
            scope._conflictCount = 0;
            let blob = scope.getDatabase().getAttachment(SyncDataProvider.DOCUMENT_PERFIL_IMAGEM_ID, 'perfil');
            resolve(blob);
          }).catch(
          function (err) {
            if (err.status === 409 && scope._conflictCount <= scope.MAX_CONFLICTS) {

              scope._atualizarRevisaoAttachment(attachment, mimeType)
                .then(
                (blob) => {
                  resolve(blob);
                },
                () => {
                  reject();
                },
              );
              scope._conflictCount++;
            } else {
              // Erro desconhecido
              reject(err);
            }
          },
        );
      },
    );
  }

  public addOrUpdate(object: any): Promise<any> {

    let scope = this;
    return new Promise(
      function (resolve, reject) {
        try {
          scope.getDatabase().put(object).then(
            () => {
              scope._conflictCount = 0;
              resolve();
            },
          ).catch(function (err: any) {
            if (err.status === 409 && scope._conflictCount <= scope.MAX_CONFLICTS) {

              scope._atualizarRevisao(object)
                .then(
                () => {
                  resolve();
                },
                () => {
                  reject(err);
                },
              );
              scope._conflictCount++;
            } else {
              // Erro desconhecido
              reject(err);
            }
          });
        } catch (e) {
          reject(e);
        }
      },
    );
  }

  public getDocumentById(id: string): Promise<any> {
    return new Promise(
      resolve => {
        this.getDatabase().get(id).then(
          (result: any) => {
            resolve(result);
          }).catch(
          (error: any) => {
            resolve(error);
          });
      });
  }

  public getAttachmentsById(id: string): Promise<any> {
    return new Promise(
      resolve => {
        this.getDatabase().get(id, { attachments: true }).then(
          (result: any) => {
            resolve(result);
          }).catch(
          (error: any) => {
            resolve(error);
          });
      });
  }

  public getAllDocuments(): Promise<any> {
    return new Promise(
      resolve => {
        this.getDatabase().allDocs({
          include_docs: true,
          attachments: true,
        }).then(function (result: any) {
          resolve(result);
        }).catch(function (err: any) {
          resolve(err);
        });
      });
  }

  public deleteDocById(docId: string): Promise<any> {
    let scope: SyncDataProvider = this;

    return new Promise(
      function (resolve, reject) {
        scope.getDatabase().get(docId)
          .then(
          (doc: any) => {
            return scope.getDatabase().remove(doc);
          },
        ).then(
          (result: any) => {
            if (result.ok) {
              resolve();
            } else {
              reject(result);
            }
          },
        ).catch(function (err: any) {
          console.log(err);
          reject(err);
        });
      },
    );
  }

  public addOrUpdateManyDocuments(manyObjects: any): Promise<any> {
    let scope: SyncDataProvider = this;

    return new Promise(
      (resolve) => {
        scope.getDatabase().bulkDocs(manyObjects, null, (err: any, response: any) => {

          if (err) {
            console.log('addOrUpdateManyDocuments()~TRACK ERROR', err);
            resolve(err);
          } else if (response.length > 0) {

            response.forEach((erro: any, indice: number) => {
              if (erro.status === 409 && scope._conflictCount <= scope.MAX_CONFLICTS) {

                scope._atualizarRevisao(manyObjects[indice]);
                scope._conflictCount++;
              } else {
                resolve(response);
              }
            });
          } else {

            scope._conflictCount = 0;
            resolve(response);
          }
        });
      });
  }

  private _atualizarRevisao(doc: any): Promise<any> {

    let scope = this;

    return new Promise(
      function (resolve, reject) {
        scope.getDatabase().get(doc._id).then(
          function (origDoc: any) {
            doc._rev = origDoc._rev;
            scope.addOrUpdate(doc)
              .then(
              () => {
                resolve();
              },
              () => {
                reject();
              },
            );
          },
        ).catch(function (error: any) {
          // TRACK
          reject(error);
        });
      },
    );
  }

  private _atualizarRevisaoAttachment(attachment: Blob, mimeType: string): Promise<any> {

    let scope = this;

    return new Promise(
      function (resolve, reject) {
        scope.getDatabase().get(SyncDataProvider.DOCUMENT_PERFIL_IMAGEM_ID).then(
          function (origDoc: any) {
            scope.addOrUpdateProfileImage(attachment, mimeType, origDoc._rev)
              .then(
              (blob) => {
                resolve(blob);
              },
              () => {
                reject();
              },
            );
          },
        ).catch(function (error: any) {
          // TRACK
          reject(error);
        });
      },
    );
  }

}
