import { Storage } from '@ionic/storage';
import { Diario } from './../../core/models/diario.model';
import { Health, HealthQueryOptionsAggregated, HealthData } from '@ionic-native/health';
import { Loading, LoadingController } from 'ionic-angular';
import { SyncDataProvider } from './../sync-data/sync-data';
import { Injectable } from '@angular/core';
import * as moment from 'moment';

@Injectable()
export class GoogleFitProvider {

  private _loadingPopup: Loading;
  private _dataMin: Date;
  private _dataMax: Date;
  private _diario: Diario = {
    _id: null,
    alimentos: [],
    sono: null,
    passos: 0,
    googleFitSync: false,
    type: 'diario-saude',
  };

  constructor(private _storage: Storage, private _health: Health, private _loadingCtrl: LoadingController, private _syncData: SyncDataProvider) {

  }

  public integrar() {
    this._dataMax = moment().local().endOf('day').subtract(1, 'day').toDate();
    this._dataMin = moment(this._dataMax).subtract(1, 'month').startOf('day').toDate();

    this._health.isAvailable()
      .then((available: boolean) => {
        if (available) {
          this._storage.set('gFit', true);
          this._cadastrarPassos();
        } else {
          this._health.requestAuthorization([
            // 'distance', 'nutrition',  // read and write permissions
            {
              read: ['steps'],       // read only permission
              // write: ['height', 'weight'],  // write only permission
            },
          ])
            .then((res: any) => {
              if (res === true) {
                this._storage.set('gFit', true);
                this._cadastrarPassos();
              }
            })
            .catch((e: any) => console.log('Erro ao requisitar autorização', e));
        }
      })
      .catch((e: any) => console.log('Google Fit não está disponível', e));
  }

  public atualizarDadosFitness(data: any) {
    this._dataMin = moment(data).startOf('day').toDate();
    this._dataMax = moment(data).endOf('day').toDate();
    this._cadastrarPassos();
  }

  public checkUserStatus(): Promise<any> {
    let _this: GoogleFitProvider = this;

    return new Promise(function (resolve) {
      _this._storage.get('gFit').then(
        (googleFitEnabled: any) => {
          if (typeof googleFitEnabled === 'boolean') {
            resolve(googleFitEnabled);
          } else {
            resolve(false);
          }
        },
      );
    });
  }

  private _cadastrarPassos() {
    this._loadingPopup = this._loadingCtrl.create({
      content: '<strong>Atualizando dados...</strong><p>Aguarde alguns instantes.</p>',
      spinner: 'dots',
    });
    this._loadingPopup.present();

    this._requisitarPassos().then(
      (healthData: HealthData[]) => {
        healthData.forEach(
          (health: HealthData) => {

            let healthDate: any = moment(health.startDate);
            let diaAtual: number = parseInt(healthDate.format('D'), 10);
            let mesAtual: number = parseInt(healthDate.format('M'), 10) - 1;
            let anoAtual: number = parseInt(healthDate.format('YYYY'), 10);

            let idDocumentoSaude: string = SyncDataProvider.DOCUMENT_DIARIO_SAUDE_ID + diaAtual + '-' + mesAtual + '-' + anoAtual;
            let nrPassos: number = parseInt(health.value, 10);

            this._syncData.getDocumentById(idDocumentoSaude).then(
              (result: any) => {
                if (result.error && result.status === 404) {
                  this._diario._id = idDocumentoSaude;
                  this._diario.passos = nrPassos;
                  this._diario.googleFitSync = true;
                  this._atualizarDadosPasso();
                } else if (result.error === undefined && !result.googleFitSync) {

                  if (result.passos !== nrPassos && nrPassos > 0) {
                    result.passos = nrPassos;
                  }

                  result.googleFitSync = true;
                  this._atualizarDadosPasso();
                } else {
                  // erro desconhecido
                }
              },
            );
          },
        );

        this._loadingPopup.dismiss();
      },
      (err: any) => {
        console.log('Erro ao requisitar meus dados de saúde', err);
      },
    );
  }

  private _atualizarDadosPasso() {
    this._syncData.addOrUpdate(this._diario);
  }

  private _requisitarPassos(): Promise<any> {
    let _this: GoogleFitProvider = this;

    return new Promise(
      function (resolve, reject) {
        let queryOptions: HealthQueryOptionsAggregated = {
          startDate: _this._dataMin,
          endDate: _this._dataMax,
          dataType: 'steps',
          bucket: 'day',
        };
        _this._health.queryAggregated(queryOptions).then(
          (healthData: HealthData[]) => {
            resolve(healthData);
          },
        ).catch(
          (err: any) => {
            reject(err);
          },
        );
      },
    );
  }

}
