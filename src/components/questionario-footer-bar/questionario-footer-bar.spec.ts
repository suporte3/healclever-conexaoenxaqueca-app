import { TipoQuestao } from './../../core/models/pojo/tipo-questao.pojo';
import { ConfiguracaoFooterBar } from './../../core/models/pojo/configuracao-footer-bar.pojo';
import { TestUtils } from './../../app/app.test';
import { QuestionarioFooterBarComponent } from './questionario-footer-bar';
import { ComponentFixture, async } from '@angular/core/testing';

describe('QuestionarioFooterBar Componente:', () => {

    let fixture: ComponentFixture<QuestionarioFooterBarComponent> = null;
    let instance: any = null;

    beforeEach(async(() => {

        TestUtils.beforeEachCompiler([QuestionarioFooterBarComponent]).then(compiled => {
            fixture = compiled.fixture;
            instance = compiled.instance;
            fixture.detectChanges();
        });
    }));

    afterEach(() => {
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('#onClickDissmissQuestions - Não deve sair da questão em quanto estiver salvando', () => {
        spyOn(instance._events, 'publish').and.stub();

        instance.configuracaoFooterBar = new ConfiguracaoFooterBar();
        instance.configuracaoFooterBar.setSaving(true);
        instance.onClickDissmissQuestions();
        expect(instance._events.publish).not.toHaveBeenCalled();
    });

    it('#onClickDissmissQuestions - Deve sair da questão se não estiver salvando', () => {
        spyOn(instance._events, 'publish').and.stub();

        instance.configuracaoFooterBar = new ConfiguracaoFooterBar();
        instance.configuracaoFooterBar.setSaving(false);
        instance.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.DURACAO_DOR);
        instance.onClickDissmissQuestions();
        expect(instance._events.publish).toHaveBeenCalledWith('footer:onClickDissmissQuestions' + instance.configuracaoFooterBar.getIdentificadorGrid());
    });

    it('#onClickNext - Não deve ir para próxima questão em quanto estiver salvando', () => {
        spyOn(instance._events, 'publish').and.stub();

        instance.configuracaoFooterBar = new ConfiguracaoFooterBar();
        instance.configuracaoFooterBar.setSaving(true);
        instance.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.DURACAO_DOR);
        instance.onClickNext();
        expect(instance._events.publish).not.toHaveBeenCalled();
    });

    it('#onClickNext - Deve ir para próxima questão se não estiver salvando', () => {
        spyOn(instance._events, 'publish').and.stub();

        instance.configuracaoFooterBar = new ConfiguracaoFooterBar();
        instance.configuracaoFooterBar.setSaving(false);
        instance.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.DURACAO_DOR);
        instance.onClickNext();
        expect(instance._events.publish).toHaveBeenCalledWith('footer:onClickNext' + instance.configuracaoFooterBar.getIdentificadorGrid());
    });

    it('#onClickConfirm - Não deve ir para próxima questão em quanto estiver salvando', () => {
        spyOn(instance._events, 'publish').and.stub();

        instance.configuracaoFooterBar = new ConfiguracaoFooterBar();
        instance.configuracaoFooterBar.setSaving(true);
        instance.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.DURACAO_DOR);
        instance.onClickConfirm();
        expect(instance._events.publish).not.toHaveBeenCalled();
    });

    it('#onClickConfirm - Deve ir para próxima questão se não estiver salvando', () => {
        spyOn(instance._events, 'publish').and.stub();

        instance.configuracaoFooterBar = new ConfiguracaoFooterBar();
        instance.configuracaoFooterBar.setSaving(false);
        instance.configuracaoFooterBar.setIdentificadorGrid(TipoQuestao.DURACAO_DOR);
        instance.onClickConfirm();
        expect(instance._events.publish).toHaveBeenCalledWith('footer:onClickConfirm' + instance.configuracaoFooterBar.getIdentificadorGrid());
    });
});
