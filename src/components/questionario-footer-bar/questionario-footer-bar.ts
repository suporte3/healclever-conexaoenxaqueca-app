import { ConfiguracaoFooterBar } from './../../core/models/pojo/configuracao-footer-bar.pojo';
import { Events } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'ib-questionario-footer-bar',
  templateUrl: 'questionario-footer-bar.html',
})
export class QuestionarioFooterBarComponent {

  @Input() configuracaoFooterBar: ConfiguracaoFooterBar;

  constructor(private _events: Events) {

  }

  public onClickDissmissQuestions() {
    if (!this.configuracaoFooterBar.isSaving()) {
      this._events.publish('footer:onClickDissmissQuestions' + this.configuracaoFooterBar.getIdentificadorGrid());
    }
  }

  public onClickNext() {
    if (!this.configuracaoFooterBar.isSaving()) {
      this._events.publish('footer:onClickNext' + this.configuracaoFooterBar.getIdentificadorGrid());
    }
  }

  public onClickConfirm() {
    if (!this.configuracaoFooterBar.isSaving()) {
      this._events.publish('footer:onClickConfirm' + this.configuracaoFooterBar.getIdentificadorGrid());
    }
  }

}
