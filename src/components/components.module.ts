import { TranslateModule } from '@ngx-translate/core';
import { DirectivesModule } from './../directives/directives.module';
import { PipesModule } from './../pipes/pipes.module';
import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { MainMenuFooterComponent } from './main-menu-footer/main-menu-footer';
import { QuestionarioFooterBarComponent } from './questionario-footer-bar/questionario-footer-bar';
import { QuestionarioNavBarComponent } from './questionario-nav-bar/questionario-nav-bar';
import { GridQuestionarioComponent } from './grid-questionario/grid-questionario';
import { LocalizacaoDorTouchComponent } from './localizacao-dor-touch/localizacao-dor-touch';
import { ItemSumarioComponent } from './item-sumario/item-sumario';
import { ItemSumarioComumComponent } from './item-sumario-comum/item-sumario-comum';
import { ItemSumarioDuracaoComponent } from './item-sumario-duracao/item-sumario-duracao';
import { ItemSumarioIntensidadeComponent } from './item-sumario-intensidade/item-sumario-intensidade';
import { ItemSumarioLocalizacaoComponent } from './item-sumario-localizacao/item-sumario-localizacao';
import { ItemSumarioClimaComponent } from './item-sumario-clima/item-sumario-clima';

@NgModule({
 declarations: [
 MainMenuFooterComponent,
 QuestionarioFooterBarComponent,
 QuestionarioNavBarComponent,
 GridQuestionarioComponent,
 LocalizacaoDorTouchComponent,
 ItemSumarioComponent,
 ItemSumarioComumComponent,
 ItemSumarioDuracaoComponent,
 ItemSumarioIntensidadeComponent,
 ItemSumarioLocalizacaoComponent,
 ItemSumarioClimaComponent,
 ],
 imports: [
 IonicModule,
 PipesModule,
 DirectivesModule,
 TranslateModule,
 ],
 exports: [
 MainMenuFooterComponent,
 QuestionarioFooterBarComponent,
 QuestionarioNavBarComponent,
 GridQuestionarioComponent,
 LocalizacaoDorTouchComponent,
 ItemSumarioComponent,
 ItemSumarioComumComponent,
 ItemSumarioDuracaoComponent,
 ItemSumarioIntensidadeComponent,
 ItemSumarioLocalizacaoComponent,
 ItemSumarioClimaComponent,
 ],
})
export class ComponentsModule { }
