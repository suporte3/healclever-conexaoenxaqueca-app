import { Medicamento } from './../../core/models/medicamento.model';
import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { TestUtils } from './../../app/app.test';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Opcao } from './../../core/models/opcao.model';
import { Crise } from './../../core/models/crise.model';
import { ItemSumarioComumComponent } from './item-sumario-comum';
import { TipoOpcao } from './../../core/models/tipo-opcao.enum';
import { TipoEficiencia } from './../../core/models/tipo-eficiencia.enum';
import { AppHelper } from './../../core/helpers/app.helper';
import * as moment from 'moment';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Item Sumário Comum:', () => {

    let fixture: ComponentFixture<ItemSumarioComumComponent> = null;
    let instance: any = null;
    let crise: Crise = {
        _id: 'criseId0',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    let opcao: Opcao = {
        _id: 'idOpcao0',
        nome: 'nome da opção',
        dataCriacao: null,
        ativo: true,
        tipo: TipoOpcao.DINAMICO,
    };
    let item: ItemSumarioCrise = new ItemSumarioCrise();

    beforeEach(async(() => {

        item.setCrise(crise);

        TestUtils.beforeEachCompiler([ItemSumarioComumComponent, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            },
        );

        spyOn(moment, 'locale').and.stub();
    }));

    afterEach(() => {
        fixture.destroy();
        instance = null;
        item = new ItemSumarioCrise();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeDefined();
        expect(instance).toBeDefined();
        done();
    });

    it('Deve iniciar o componente ao carregar o componente', () => {
        expect(instance._appHelper).toEqual(new AppHelper());
    });

    it('#ngOnInit - Deve iniciar o componente', () => {
        spyOn(instance, '_iniciar').and.stub();
        instance.item = item;
        instance.ngOnInit();
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#onClickIrParaQuestao - Deve acessar questão', () => {
        let spyPush: jasmine.Spy = spyOn(instance._navCtrl, 'push').and.stub();
        spyOn(instance, '_getConteudoPorItem').and.returnValue('conteudo');

        item.setTipo(ItemSumarioCrise.MEDICAMENTO);
        item.setCrise(crise);
        instance.item = item;

        let paramRota: any = {
            'com.healclever.myMigraine.crise_param': instance.item.getCrise(),
            'com.healclever.myMigraine.route': 'sumario',
        };

        instance._iniciar();
        instance.onClickIrParaQuestao();
        expect(spyPush).toHaveBeenCalledWith(instance.item.getPage(), paramRota);

        spyPush.calls.reset();
        instance.item = new ItemSumarioCrise();
        instance.item.setPage(null);
        instance.onClickIrParaQuestao();
        expect(spyPush).not.toHaveBeenCalled();
    });

    it('Deve configurar o item - com conteúdo', () => {
        let spyGetConteudoPorItem: jasmine.Spy = spyOn(instance, '_getConteudoPorItem');

        spyGetConteudoPorItem.and.returnValue('conteudo');
        item.setTipo(ItemSumarioCrise.MEDICAMENTO);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.MEDICAMENTO');
        expect(instance.item.getPage()).toBe('MedicamentoPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-medicamento');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoMedicamento);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.QUALIDADE_DOR);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.SENSACAO');
        expect(instance.item.getPage()).toBe('QualidadeDorPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-sensacao');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.SINTOMA);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.SINTOMA');
        expect(instance.item.getPage()).toBe('SintomasPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-sintoma');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.GATILHO);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.GATILHO');
        expect(instance.item.getPage()).toBe('GatilhosPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-gatilho');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.ESFORCO_FISICO);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.ESFORCO_FISICO');
        expect(instance.item.getPage()).toBe('EsforcoFisicoPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-esforco-fisico');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.BEBIDA);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.BEBIDA');
        expect(instance.item.getPage()).toBe('BebidaPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-bebida');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.COMIDA);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.COMIDA');
        expect(instance.item.getPage()).toBe('ComidaPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-comida');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.CICLO_MENSTRUAL);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.MENSTRUACAO');
        expect(instance.item.getPage()).toBe('CicloMenstrualPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-menstruacao');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.METODO_ALIVIO);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.METODO_ALIVIO');
        expect(instance.item.getPage()).toBe('MetodoAlivioPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-metodo-alivio');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.METODO_EFICIENTE);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.METODO_EFICIENTE');
        expect(instance.item.getPage()).toBe('MetodoAlivioEficientePage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-metodo-eficiente');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemMetodoEficiente);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.CONSEQUENCIAS);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.CONSEQUENCIA');
        expect(instance.item.getPage()).toBe('ConsequenciaPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-consequencia');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.LOCAIS);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.LOCAL');
        expect(instance.item.getPage()).toBe('LocalPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-local');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(spyGetConteudoPorItem).toHaveBeenCalledWith(instance._getConteudoItemComum);

        spyGetConteudoPorItem.calls.reset();
        crise.anotacoes = 'conteúdo anotações';
        item = new ItemSumarioCrise();
        item.setTipo(ItemSumarioCrise.ANOTACOES);
        item.setCrise(crise);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(true);
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.ANOTACAO');
        expect(instance.item.getPage()).toBe('NotasPage');
        expect(instance.item.getIcone()).toBe('rosie-sumario-anotacoes');
        expect(instance.item.getConteudo()).toBe('conteúdo anotações');
    });

    it('Deve configurar o item - sem conteúdo', () => {
        let spyGetConteudoPorItem: jasmine.Spy = spyOn(instance, '_getConteudoPorItem');

        spyGetConteudoPorItem.and.returnValue(undefined);
        item.setTipo(ItemSumarioCrise.MEDICAMENTO);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(false);
        expect(instance.item.getConteudo()).toBe(instance.TEXTO_SEM_DADOS_PADRAO);

        spyGetConteudoPorItem.and.returnValue(undefined);
        crise.metodosAlivio = [];
        crise.medicamentos = [];
        item.setTipo(ItemSumarioCrise.METODO_EFICIENTE);
        item.setCrise(crise);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(false);
        expect(instance.item.getConteudo()).toBe(instance.TEXTO_SEM_DADOS_METODO_EFICIENTE);

        spyGetConteudoPorItem.and.returnValue(undefined);
        item.setTipo(ItemSumarioCrise.COMIDA);
        instance._iniciar();
        expect(instance.item.isComplete()).toBe(false);
        expect(instance.item.getConteudo()).toBe(instance.TEXTO_SEM_DADOS_SUB_QUESTAO);
    });

    it('#_getConteudoPorItem - Deve obter o conteúdo por item', () => {
        crise.qualidadesDor = [];
        item.setTipo(ItemSumarioCrise.QUALIDADE_DOR);
        item.setCrise(crise);
        instance.item = item;
        expect(instance._getConteudoPorItem(instance._getConteudoItemComum)).toBeUndefined();

        spyOn(instance, '_getConteudoItemComum').and.returnValue('conteudo');
        crise.qualidadesDor = [opcao];
        item.setTipo(ItemSumarioCrise.QUALIDADE_DOR);
        item.setCrise(crise);
        instance.item = item;
        expect(instance._getConteudoPorItem(instance._getConteudoItemComum)).toBe('conteudo');
    });

    it('#_getConteudoItemComum - Deve obter conteúdo', () => {
        let appHelper: AppHelper = new AppHelper();
        let opcao1: Opcao = {
            _id: 'idOpcao0',
            nome: 'consequencia1',
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        let opcao2: Opcao = {
            _id: 'idOpcao1',
            nome: 'consequencia2',
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        crise.consequencias = [opcao1, opcao2];
        item.setTipo(ItemSumarioCrise.CONSEQUENCIAS);
        item.setCrise(crise);
        instance.item = item;
        let conteudo: string = appHelper.textToCapitalize(opcao1.nome) + ', ' + appHelper.textToCapitalize(opcao2.nome) + '.';
        expect(instance._getConteudoItemComum()).toBe(conteudo);
    });

    it('#_getConteudoMedicamento - Deve obter conteúdo', () => {
        let appHelper: AppHelper = new AppHelper();
        let medicamento1: Medicamento = {
            _id: 'idMedicamento0',
            nome: 'tylenol',
            quantidade: 1,
            dataCriacao: null,
        };
        let medicamento2: Medicamento = {
            _id: 'idMedicamento0',
            nome: 'propanolol',
            quantidade: 2,
            dataCriacao: null,
        };
        crise.medicamentos = [medicamento1, medicamento2];
        item.setCrise(crise);
        instance.item = item;
        let conteudo: string;
        conteudo = '(' + medicamento1.quantidade + ') ';
        conteudo += appHelper.textToCapitalize(medicamento1.nome) + ', ';
        conteudo += '(' + medicamento2.quantidade + ') ';
        conteudo += appHelper.textToCapitalize(medicamento2.nome) + '.';
        expect(instance._getConteudoMedicamento()).toEqual(conteudo);
    });

    it('#_getConteudoItemMetodoEficiente - Deve obter conteúdo', () => {
        let appHelper: AppHelper = new AppHelper();
        let opcao1: any = {
            _id: 'idOpcao0',
            nome: 'tylenol',
            dataCriacao: null,
            tipoEficiencia: TipoEficiencia.NADA_UTIL,
        };
        let opcao2: any = {
            _id: 'idOpcao1',
            nome: 'relaxamento',
            dataCriacao: null,
            tipoEficiencia: TipoEficiencia.UTIL,
        };
        crise.metodosAlivioEficiente = [opcao1, opcao2];
        item.setCrise(crise);
        instance.item = item;
        let conteudo: string;
        conteudo = appHelper.textToCapitalize(opcao1.nome) + ' ' + appHelper.getClassificacaoEficiencia(opcao1.tipoEficiencia) + ', ';
        conteudo += appHelper.textToCapitalize(opcao2.nome) + ' ' + appHelper.getClassificacaoEficiencia(opcao2.tipoEficiencia) + '.';
        expect(instance._getConteudoItemMetodoEficiente()).toEqual(conteudo);
    });
});
