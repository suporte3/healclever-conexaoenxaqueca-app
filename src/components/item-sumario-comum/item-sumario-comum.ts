import { TranslateService } from '@ngx-translate/core';
import { Opcao } from './../../core/models/opcao.model';
import { Medicamento } from './../../core/models/medicamento.model';
import { NavController } from 'ionic-angular';
import { AppHelper } from './../../core/helpers/app.helper';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ib-item-sumario-comum',
  templateUrl: 'item-sumario-comum.html',
})
export class ItemSumarioComumComponent implements OnInit {

  @Input() item: ItemSumarioCrise;
  private TEXTO_SEM_DADOS_PADRAO: string = null;
  private TEXTO_SEM_DADOS_SUB_QUESTAO: string = null;
  private TEXTO_SEM_DADOS_METODO_EFICIENTE: string = null;

  private _appHelper: AppHelper;
  private _titulo: string;
  private _itemIcone: string;
  private _page: any;
  private _conteudo: string;

  constructor(private _translate: TranslateService, private _navCtrl: NavController) {
    this._appHelper = new AppHelper();
    this._obterMensagens();
  }

  ngOnInit() {
    if (this.item) {
      this._iniciar();
    }
  }

  public onClickIrParaQuestao() {
    if (this.item.getPage()) {
      this._navCtrl.push(this.item.getPage(), {
        'com.healclever.myMigraine.crise_param': this.item.getCrise(),
        'com.healclever.myMigraine.route': 'sumario',
      });
    }
  }

  private _obterMensagens() {
    this._translate.get('SUMARIO.MSG.SEM_DADOS_PADRAO').subscribe(
      (value: string) => {
        this.TEXTO_SEM_DADOS_PADRAO = value;
      },
    );

    this._translate.get('SUMARIO.MSG.SEM_DADOS_SUB_QUESTAO').subscribe(
      (value: string) => {
        this.TEXTO_SEM_DADOS_SUB_QUESTAO = value;
      },
    );

    this._translate.get('SUMARIO.MSG.SEM_DADOS_METODO_EFICIENTE').subscribe(
      (value: string) => {
        this.TEXTO_SEM_DADOS_METODO_EFICIENTE = value;
      },
    );
  }

  private _iniciar() {
    let isComplete: boolean = true;

    this._configurarItem();

    if (!this._conteudo) {
      isComplete = false;
      this._configurarItemSemDados();
    }

    this.item.setComplete(isComplete);
    this.item.setTitulo(this._titulo);
    this.item.setPage(this._page);
    this.item.setIcone(this._itemIcone);
    this.item.setConteudo(this._conteudo);
  }

  private _configurarItemSemDados() {
    if (this.item.getTipo() === ItemSumarioCrise.METODO_EFICIENTE &&
      (!this.item.getCrise().medicamentos[0] && !this.item.getCrise().metodosAlivio[0])) {
      this._conteudo = this.TEXTO_SEM_DADOS_METODO_EFICIENTE;
      this._page = null;
    } else if (this.item.getTipo() === ItemSumarioCrise.ESFORCO_FISICO ||
      this.item.getTipo() === ItemSumarioCrise.BEBIDA ||
      this.item.getTipo() === ItemSumarioCrise.COMIDA) {
      this._conteudo = this.TEXTO_SEM_DADOS_SUB_QUESTAO;
    } else {
      this._conteudo = this.TEXTO_SEM_DADOS_PADRAO;
    }
  }

  private _configurarItem() {
    switch (this.item.getTipo()) {
      case ItemSumarioCrise.MEDICAMENTO:
        this._titulo = 'SUMARIO.ITEM.MEDICAMENTO';
        this._itemIcone = 'rosie-sumario-medicamento';
        this._page = 'MedicamentoPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoMedicamento);
        break;
      case ItemSumarioCrise.QUALIDADE_DOR:
        this._titulo = 'SUMARIO.ITEM.SENSACAO';
        this._itemIcone = 'rosie-sumario-sensacao';
        this._page = 'QualidadeDorPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.SINTOMA:
        this._titulo = 'SUMARIO.ITEM.SINTOMA';
        this._itemIcone = 'rosie-sumario-sintoma';
        this._page = 'SintomasPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.GATILHO:
        this._titulo = 'SUMARIO.ITEM.GATILHO';
        this._itemIcone = 'rosie-sumario-gatilho';
        this._page = 'GatilhosPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.ESFORCO_FISICO:
        this._titulo = 'SUMARIO.ITEM.ESFORCO_FISICO';
        this._itemIcone = 'rosie-sumario-esforco-fisico';
        this._page = 'EsforcoFisicoPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.BEBIDA:
        this._titulo = 'SUMARIO.ITEM.BEBIDA';
        this._itemIcone = 'rosie-sumario-bebida';
        this._page = 'BebidaPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.COMIDA:
        this._titulo = 'SUMARIO.ITEM.COMIDA';
        this._itemIcone = 'rosie-sumario-comida';
        this._page = 'ComidaPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.CICLO_MENSTRUAL:
        this._titulo = 'SUMARIO.ITEM.MENSTRUACAO';
        this._itemIcone = 'rosie-sumario-menstruacao';
        this._page = 'CicloMenstrualPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.METODO_ALIVIO:
        this._titulo = 'SUMARIO.ITEM.METODO_ALIVIO';
        this._itemIcone = 'rosie-sumario-metodo-alivio';
        this._page = 'MetodoAlivioPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.METODO_EFICIENTE:
        this._titulo = 'SUMARIO.ITEM.METODO_EFICIENTE';
        this._itemIcone = 'rosie-sumario-metodo-eficiente';
        this._page = 'MetodoAlivioEficientePage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemMetodoEficiente);
        break;
      case ItemSumarioCrise.CONSEQUENCIAS:
        this._titulo = 'SUMARIO.ITEM.CONSEQUENCIA';
        this._itemIcone = 'rosie-sumario-consequencia';
        this._page = 'ConsequenciaPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.LOCAIS:
        this._titulo = 'SUMARIO.ITEM.LOCAL';
        this._itemIcone = 'rosie-sumario-local';
        this._page = 'LocalPage';
        this._conteudo = this._getConteudoPorItem(this._getConteudoItemComum);
        break;
      case ItemSumarioCrise.ANOTACOES:
        this._titulo = 'SUMARIO.ITEM.ANOTACAO';
        this._itemIcone = 'rosie-sumario-anotacoes';
        this._page = 'NotasPage';
        this._conteudo = this.item.getCrise().anotacoes;
        break;
      default:
        console.log('TRACK ERR');
    }
  }

  private _getConteudoPorItem(functionConteudo: Function): string {
    let conteudo: string = '';
    let crise: any = this.item.getCrise();

    if (crise[this.item.getTipo()][0]) {
      conteudo = functionConteudo.call(this);
    } else {
      conteudo = undefined;
    }

    return conteudo;
  }

  private _getConteudoItemComum() {
    let conteudo: string = '';
    let crise: any = this.item.getCrise();

    crise[this.item.getTipo()].forEach(
      (opcao: Opcao, indice: number) => {
        conteudo += this._appHelper.textToCapitalize(opcao.nome);

        if (indice === (crise[this.item.getTipo()].length - 1)) {
          conteudo += '.';
        } else {
          conteudo += ', ';
        }
      },
    );

    return conteudo;
  }

  private _getConteudoMedicamento(): string {
    let conteudo: string = '';

    this.item.getCrise().medicamentos.forEach(
      (medicamento: Medicamento, indice: number) => {
        conteudo += '(' + medicamento.quantidade + ') ';
        conteudo += this._appHelper.textToCapitalize(medicamento.nome);

        if (indice === (this.item.getCrise().medicamentos.length - 1)) {
          conteudo += '.';
        } else {
          conteudo += ', ';
        }
      },
    );

    return conteudo;
  }

  private _getConteudoItemMetodoEficiente(): string {
    let conteudo: string = '';

    this.item.getCrise().metodosAlivioEficiente.forEach(
      (metodoEficiente: any, indice: number) => {
        conteudo += this._appHelper.textToCapitalize(metodoEficiente.nome) + ' ';
        conteudo += this._appHelper.getClassificacaoEficiencia(metodoEficiente.tipoEficiencia);

        if (indice === (this.item.getCrise().metodosAlivioEficiente.length - 1)) {
          conteudo += '.';
        } else {
          conteudo += ', ';
        }
      },
    );

    return conteudo;
  }

}
