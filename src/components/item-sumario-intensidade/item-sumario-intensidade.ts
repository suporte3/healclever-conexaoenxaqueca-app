import { TranslateService } from '@ngx-translate/core';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { NavController } from 'ionic-angular';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ib-item-sumario-intensidade',
  templateUrl: 'item-sumario-intensidade.html',
})
export class ItemSumarioIntensidadeComponent implements OnInit {

  @Input() item: ItemSumarioCrise;
  private TEXTO_SEM_DADOS: string = null;

  constructor(private _translate: TranslateService, private _navCtrl: NavController) {
    this._obterMensagens();
  }

  ngOnInit() {

    if (this.item) {
      this._iniciar();
    }
  }

  public onClickIrParaQuestao() {
    this._navCtrl.push(this.item.getPage(), {
      'com.healclever.myMigraine.crise_param': this.item.getCrise(),
      'com.healclever.myMigraine.route': 'sumario',
    });
  }

  private _obterMensagens() {
    this._translate.get('SUMARIO.MSG.SEM_DADOS_PADRAO').subscribe(
      (value: string) => {
        this.TEXTO_SEM_DADOS = value;
      },
    );
  }

  private _iniciar() {
    this.item.setComplete(true);
    this.item.setTitulo('SUMARIO.ITEM.INTENSIDADE.TITULO');
    this.item.setIcone('rosie-sumario-intensidade');
    this.item.setPage('IntensidadePage');
    let conteudo: string = this._getConteudo();

    if (!conteudo) {
      this.item.setComplete(false);
      conteudo = this.TEXTO_SEM_DADOS;
    }

    this.item.setConteudo(conteudo);
  }

  private _getConteudo(): string {
    let conteudo: string;

    if (this.item.getCrise().intensidade[0]) {
      let qtdRegistros: number = 0;
      let valorIntensidade: number = 0;
      this.item.getCrise().intensidade.forEach(
        (intensidade) => {
          valorIntensidade += intensidade.valor;
          qtdRegistros++;
        },
      );

      conteudo = (valorIntensidade / qtdRegistros).toFixed(2);
    } else {
      conteudo = undefined;
    }

    return conteudo;
  }

}
