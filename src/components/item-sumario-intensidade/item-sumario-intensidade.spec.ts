import { Intensidade } from './../../core/models/intensidade.model';
import { TestUtils } from './../../app/app.test';
import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Crise } from './../../core/models/crise.model';
import { ItemSumarioIntensidadeComponent } from './item-sumario-intensidade';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Item Sumário Intensidade:', () => {

    let fixture: ComponentFixture<ItemSumarioIntensidadeComponent> = null;
    let instance: any = null;
    let crise: Crise = {
        _id: 'criseId0',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    let item: ItemSumarioCrise = new ItemSumarioCrise();

    beforeEach(async(() => {

        item.setTipo(ItemSumarioCrise.DURACAO);
        item.setCrise(crise);

        TestUtils.beforeEachCompiler([ItemSumarioIntensidadeComponent, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            },
        );
    }));

    afterEach(() => {
        fixture.destroy();
        instance = null;
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeDefined();
        expect(instance).toBeDefined();
        done();
    });

    it('#ngOnInit - Deve iniciar o componente', () => {
        spyOn(instance, '_iniciar').and.stub();
        instance.item = item;
        instance.ngOnInit();
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#onClickIrParaQuestao - Deve acessar questão', () => {
        spyOn(instance._navCtrl, 'push').and.stub();
        spyOn(instance, '_getConteudo').and.returnValue('');

        item.setCrise(crise);
        instance.item = item;

        let paramRota: any = {
            'com.healclever.myMigraine.crise_param': instance.item.getCrise(),
            'com.healclever.myMigraine.route': 'sumario',
        };

        instance._iniciar();
        instance.onClickIrParaQuestao();
        expect(instance._navCtrl.push).toHaveBeenCalledWith(instance.item.getPage(), paramRota);
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        let spyGetConteudo: jasmine.Spy = spyOn(instance, '_getConteudo').and.returnValue('conteudo');
        instance.item = item;
        instance._iniciar();
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.INTENSIDADE.TITULO');
        expect(instance.item.getIcone()).toBe('rosie-sumario-intensidade');
        expect(instance.item.getPage()).toBe('IntensidadePage');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(instance.item.isComplete()).toBe(true);

        spyGetConteudo.calls.reset();
        spyGetConteudo.and.returnValue(undefined);
        instance._iniciar();
        expect(instance.item.getConteudo()).toBe(instance.TEXTO_SEM_DADOS);
        expect(instance.item.isComplete()).toBe(false);
    });

    it('#_getConteudo - Deve obter conteúdo', () => {
        instance.item = item;
        expect(instance._getConteudo()).toBeUndefined();

        let intensidade1: Intensidade = {
            valor: 1,
            dataCriacao: null,
        };
        let intensidade2: Intensidade = {
            valor: 2,
            dataCriacao: null,
        };
        let intensidade3: Intensidade = {
            valor: 10,
            dataCriacao: null,
        };
        item.getCrise().intensidade = [intensidade1, intensidade2, intensidade3];
        instance.item = item;

        let intensidade: string = ((intensidade1.valor + intensidade2.valor + intensidade3.valor) / 3).toFixed(2);
        expect(instance._getConteudo()).toEqual(intensidade);
    });
});
