import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { AppParam } from './../../core/models/pojo/app-param.pojo';
import { TipoQuestao } from './../../core/models/pojo/tipo-questao.pojo';
import { ConfiguracaoNavBar } from './../../core/models/pojo/configuracao-nav-bar.pojo';
import { TestUtils } from './../../app/app.test';
import { QuestionarioNavBarComponent } from './questionario-nav-bar';
import { ComponentFixture, async } from '@angular/core/testing';

describe('QuestionarioNavBar Componente:', () => {

    let fixture: ComponentFixture<QuestionarioNavBarComponent> = null;
    let instance: any = null;

    beforeEach(async(() => {

        TestUtils.addProvider(QuestionarioNavBarComponent);

        TestUtils.beforeEachCompiler([QuestionarioNavBarComponent, CapitalizePipe]).then(compiled => {
            fixture = compiled.fixture;
            instance = compiled.instance;
            // fixture.detectChanges();
        });
    }));

    afterEach(() => {
        instance.configuracaoNavBar = new ConfiguracaoNavBar();
        fixture.destroy();
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeTruthy();
        expect(instance).toBeTruthy();
        done();
    });

    it('#ngOnInit - Deve iniciar o componente', () => {
        let configuracaoNavBar: ConfiguracaoNavBar = new ConfiguracaoNavBar();
        configuracaoNavBar.setIdentificadorGrid(TipoQuestao.LOCAL);

        instance.configuracaoNavBar = configuracaoNavBar;

        let spyEventsSubscribe: jasmine.Spy = spyOn(instance._events, 'subscribe').and.stub();
        instance.ngOnInit();

        expect(instance.bindModoRemover).toBeFalsy();
        expect(spyEventsSubscribe.calls.argsFor(0)[0]).toEqual(
            'ativarModoRemover:' + instance.configuracaoNavBar.getIdentificadorGrid(),
        );
        expect(spyEventsSubscribe.calls.argsFor(1)[0]).toEqual(
            'ativarModoEdicao:' + instance.configuracaoNavBar.getIdentificadorGrid(),
        );
        expect(spyEventsSubscribe.calls.argsFor(2)[0]).toEqual(
            AppParam.ATIVAR_MODO_GPS,
        );
    });

    it('#isModoNormal - Deve validar modo normal', () => {
        instance.bindModoRemover = false;
        instance.bindModoGPS = false;
        instance.bindModoEdicao = false;
        expect(instance.isModoNormal()).toBe(true);

        instance.bindModoRemover = true;
        instance.bindModoGPS = false;
        instance.bindModoEdicao = false;
        expect(instance.isModoNormal()).toBe(false);

        instance.bindModoRemover = false;
        instance.bindModoGPS = true;
        instance.bindModoEdicao = false;
        expect(instance.isModoNormal()).toBe(false);

        instance.bindModoRemover = false;
        instance.bindModoGPS = false;
        instance.bindModoEdicao = true;
        expect(instance.isModoNormal()).toBe(false);
    });

    it('#isModoRemover - Deve validar o modo remover', () => {
        instance.bindModoRemover = true;
        instance.bindModoGPS = false;
        instance.bindModoEdicao = false;
        expect(instance.isModoRemover()).toBe(true);

        instance.bindModoRemover = false;
        instance.bindModoGPS = true;
        instance.bindModoEdicao = false;
        expect(instance.isModoRemover()).toBe(false);

        instance.bindModoRemover = false;
        instance.bindModoGPS = false;
        instance.bindModoEdicao = true;
        expect(instance.isModoRemover()).toBe(false);

        instance.bindModoRemover = false;
        instance.bindModoGPS = false;
        instance.bindModoEdicao = false;
        expect(instance.isModoRemover()).toBe(false);
    });

    it('#isModoEdicao - Deve validar o modo edição', () => {
        instance.bindModoEdicao = true;
        instance.bindModoRemover = false;
        instance.bindModoGPS = false;
        expect(instance.isModoEdicao()).toBe(true);

        instance.bindModoEdicao = false;
        instance.bindModoRemover = true;
        instance.bindModoGPS = false;
        expect(instance.isModoEdicao()).toBe(false);

        instance.bindModoEdicao = false;
        instance.bindModoRemover = false;
        instance.bindModoGPS = true;
        expect(instance.isModoEdicao()).toBe(false);

        instance.bindModoEdicao = false;
        instance.bindModoRemover = false;
        instance.bindModoGPS = false;
        expect(instance.isModoEdicao()).toBe(false);
    });

    it('#isModoGPS - Deve validar o modo GPS', () => {
        instance.bindModoGPS = true;
        instance.bindModoEdicao = false;
        instance.bindModoRemover = false;
        expect(instance.isModoGPS()).toBe(true);

        instance.bindModoGPS = false;
        instance.bindModoEdicao = true;
        instance.bindModoRemover = false;
        expect(instance.isModoGPS()).toBe(false);

        instance.bindModoGPS = false;
        instance.bindModoEdicao = false;
        instance.bindModoRemover = true;
        expect(instance.isModoGPS()).toBe(false);

        instance.bindModoGPS = false;
        instance.bindModoEdicao = false;
        instance.bindModoRemover = false;
        expect(instance.isModoGPS()).toBe(false);
    });

    it('#_ativarModoRemover - Deve ativar o modo Remover', () => {
        instance.bindModoRemover = false;
        instance.bindModoGPS = true;
        instance.bindModoEdicao = true;
        instance._ativarModoRemover();

        expect(instance.bindModoRemover).toBe(true);
        expect(instance.bindModoGPS).toBe(false);
        expect(instance.bindModoEdicao).toBe(false);
    });

    it('#_ativarModoRemover - Deve desativar o modo Remover', () => {
        instance.bindModoRemover = true;
        instance.bindModoGPS = true;
        instance.bindModoEdicao = true;
        instance._ativarModoRemover();

        expect(instance.bindModoRemover).toBe(false);
        expect(instance.bindModoGPS).toBe(false);
        expect(instance.bindModoEdicao).toBe(false);
    });

    it('#_ativarModoGPS - Deve ativar o modo GPS', () => {
        instance.bindModoGPS = false;
        instance.bindModoRemover = true;
        instance.bindModoEdicao = true;
        instance._ativarModoGPS();

        expect(instance.bindModoGPS).toBe(true);
        expect(instance.bindModoRemover).toBe(false);
        expect(instance.bindModoEdicao).toBe(false);
    });

    it('#_ativarModoGPS - Deve desativar o modo GPS', () => {
        instance.bindModoGPS = true;
        instance.bindModoRemover = true;
        instance.bindModoEdicao = true;
        instance._ativarModoGPS();

        expect(instance.bindModoGPS).toBe(false);
        expect(instance.bindModoRemover).toBe(false);
        expect(instance.bindModoEdicao).toBe(false);
    });

    it('#_ativarModoEdicao - Deve ativar o modo Edição', () => {
        instance.bindModoEdicao = false;
        instance.bindModoGPS = true;
        instance.bindModoRemover = true;
        instance._ativarModoEdicao();

        expect(instance.bindModoEdicao).toBe(true);
        expect(instance.bindModoRemover).toBe(false);
        expect(instance.bindModoGPS).toBe(false);
    });

    it('#_ativarModoEdicao - Deve desativar o modo Edição', () => {
        instance.bindModoEdicao = true;
        instance.bindModoGPS = true;
        instance.bindModoRemover = true;
        instance._ativarModoEdicao();

        expect(instance.bindModoEdicao).toBe(false);
        expect(instance.bindModoRemover).toBe(false);
        expect(instance.bindModoGPS).toBe(false);
    });

});
