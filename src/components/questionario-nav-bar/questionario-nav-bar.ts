import { AppParam } from './../../core/models/pojo/app-param.pojo';
import { TipoQuestao } from './../../core/models/pojo/tipo-questao.pojo';
import { ConfiguracaoNavBar } from './../../core/models/pojo/configuracao-nav-bar.pojo';
import { Events } from 'ionic-angular';
import { Component, OnInit, OnDestroy, Input } from '@angular/core';

@Component({
  selector: 'ib-questionario-nav-bar',
  templateUrl: 'questionario-nav-bar.html',
})
export class QuestionarioNavBarComponent implements OnInit, OnDestroy {

  public bindModoRemover: boolean = false;
  public bindModoEdicao: boolean = false;
  public bindModoGPS: boolean = false;
  public navBarcolor: string = 'bg-toolbar-modo-normal';

  @Input() configuracaoNavBar: ConfiguracaoNavBar;

  constructor(private _events: Events) {
    this.bindModoRemover = false;
    this.bindModoGPS = false;
  }

  ngOnDestroy() {
    this.bindModoRemover = false;
    this.bindModoGPS = false;
    this._events.unsubscribe('ativarModoRemover:' + this.configuracaoNavBar.getIdentificadorGrid());
  }

  onClickHelp() {
    console.log('click help');
  }

  ngOnInit() {
    this.bindModoRemover = false;
    this.bindModoGPS = false;
    this.bindModoEdicao = false;

    this._events.subscribe('ativarModoRemover:' + this.configuracaoNavBar.getIdentificadorGrid(), () => {
      this._ativarModoRemover();
    });

    this._events.subscribe('ativarModoEdicao:' + this.configuracaoNavBar.getIdentificadorGrid(), () => {
      this._ativarModoEdicao();
    });

    if (this.configuracaoNavBar.getIdentificadorGrid()) {
      if (this.configuracaoNavBar.getIdentificadorGrid() === TipoQuestao.LOCAL) {
        this._events.subscribe(AppParam.ATIVAR_MODO_GPS, () => {
          this._ativarModoGPS();
        });
      }
    }
  }

  public isModoRemover(): boolean {
    return (this.bindModoRemover && !this.bindModoGPS && !this.bindModoEdicao);
  }

  public isModoEdicao(): boolean {
    return (this.bindModoEdicao && !this.bindModoGPS && !this.bindModoRemover);
  }

  public isModoNormal(): boolean {
    return (!this.bindModoRemover && !this.bindModoGPS && !this.bindModoEdicao);
  }

  public isModoGPS(): boolean {
    return (this.bindModoGPS && !this.bindModoRemover && !this.bindModoEdicao);
  }

  private _ativarModoRemover() {
    this.bindModoRemover = (!this.bindModoRemover);
    this.bindModoGPS = false;
    this.bindModoEdicao = false;
  }

  private _ativarModoGPS() {
    this.bindModoGPS = (!this.bindModoGPS);
    this.bindModoRemover = false;
    this.bindModoEdicao = false;
  }

  private _ativarModoEdicao() {
    this.bindModoEdicao = (!this.bindModoEdicao);
    this.bindModoGPS = false;
    this.bindModoRemover = false;
  }

}
