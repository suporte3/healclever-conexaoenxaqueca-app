import { TipoOpcao } from './../../core/models/tipo-opcao.enum';
import { LocalizacaoPart } from './../../core/models/pojo/localizacao-dor-part.pojo';
import { ConfiguracaoLocalizacaoDorTouch } from './../../core/models/pojo/configuracao-localizacao-dor-touch.pojo';
import { TestUtils } from './../../app/app.test';
import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Crise } from './../../core/models/crise.model';
import { ItemSumarioLocalizacaoComponent } from './item-sumario-localizacao';
import { Opcao } from './../../core/models/opcao.model';
import { AppHelper } from './../../core/helpers/app.helper';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Item Sumário Localização:', () => {

    let fixture: ComponentFixture<ItemSumarioLocalizacaoComponent> = null;
    let instance: any = null;
    let crise: Crise = {
        _id: 'criseId0',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    let item: ItemSumarioCrise = new ItemSumarioCrise();

    beforeEach(async(() => {

        item.setCrise(crise);

        TestUtils.beforeEachCompiler([ItemSumarioLocalizacaoComponent, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            },
        );
    }));

    afterEach(() => {
        fixture.destroy();
        instance = null;

        item = new ItemSumarioCrise();
        crise.localizacoesDeDor = [];
        item.setCrise(crise);
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeDefined();
        expect(instance).toBeDefined();
        done();
    });

    it('Deve iniciar o componente localização ao carregar o componente', () => {
        expect(instance.configuracaoLocalizacaoCanvasTouch).toEqual(new ConfiguracaoLocalizacaoDorTouch());
    });

    it('#ngOnInit - Deve iniciar o componente', () => {
        spyOn(instance, '_iniciarLocalizacao').and.stub();
        spyOn(instance, '_iniciar').and.stub();
        instance.item = item;
        instance.ngOnInit();
        expect(instance._iniciarLocalizacao).toHaveBeenCalled();
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#onClickIrParaQuestao - Deve acessar questão', () => {
        spyOn(instance._navCtrl, 'push').and.stub();
        spyOn(instance, '_getConteudo').and.returnValue('');

        item.setCrise(crise);
        instance.item = item;

        let paramRota: any = {
            'com.healclever.myMigraine.crise_param': instance.item.getCrise(),
            'com.healclever.myMigraine.route': 'sumario',
        };

        instance._iniciar();
        instance.onClickIrParaQuestao();
        expect(instance._navCtrl.push).toHaveBeenCalledWith(instance.item.getPage(), paramRota);
    });

    it('#_iniciarLocalizacao - Deve iniciar o componente localização corretamente', () => {
        let opcao: Opcao = {
            _id: '1',
            nome: 'localização 1',
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        let localizacaoPart: LocalizacaoPart = new LocalizacaoPart('assets/img/localizacao-dor/localizacao_dor_' + opcao._id + '.svg', true);
        item.getCrise().localizacoesDeDor = [opcao];
        instance.item = item;
        instance._iniciarLocalizacao();
        expect(instance.configuracaoLocalizacaoCanvasTouch.getImagemInicial()).toEqual('assets/img/localizacao-dor/localizacao_dor_0.svg');
        expect(instance.configuracaoLocalizacaoCanvasTouch.getImagemMapa()).toEqual('assets/img/localizacao-dor/localizacao_dor_map.svg');
        expect(instance.configuracaoLocalizacaoCanvasTouch.getLocalizacoes()).toContain(localizacaoPart);
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        let spyGetConteudo: jasmine.Spy = spyOn(instance, '_getConteudo').and.returnValue('conteudo');
        instance.item = item;
        instance._iniciar();
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.LOCALIZACAO.TITULO');
        expect(instance.item.getIcone()).toBe('rosie-sumario-localizacao-dor');
        expect(instance.item.getPage()).toBe('LocalizacaoDorPage');
        expect(instance.item.getConteudo()).toBe('conteudo');
        expect(instance.item.isComplete()).toBe(true);

        spyGetConteudo.calls.reset();
        spyGetConteudo.and.returnValue(undefined);
        instance._iniciar();
        expect(instance.item.getConteudo()).toBe(instance.TEXTO_SEM_DADOS);
        expect(instance.item.isComplete()).toBe(false);
    });

    it('#_getConteudo - Deve obter conteúdo', () => {
        instance.item = item;
        expect(instance._getConteudo()).toBeUndefined();

        let localizacao1: Opcao = {
            _id: '1',
            nome: 'localização 1',
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        let localizacao2: Opcao = {
            _id: '2',
            nome: 'localização 2',
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        let localizacao3: Opcao = {
            _id: '3',
            nome: 'localização 3',
            dataCriacao: null,
            ativo: true,
            tipo: TipoOpcao.DINAMICO,
        };
        item.getCrise().localizacoesDeDor = [localizacao1, localizacao2, localizacao3];
        instance.item = item;

        let appHelper: AppHelper = new AppHelper();
        let conteudo: string;
        conteudo = appHelper.textToCapitalize(localizacao1.nome) + ', ';
        conteudo += appHelper.textToCapitalize(localizacao2.nome) + ', ';
        conteudo += appHelper.textToCapitalize(localizacao3.nome) + '.';
        expect(instance._getConteudo()).toEqual(conteudo);
    });
});
