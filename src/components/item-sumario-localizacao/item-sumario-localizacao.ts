import { TranslateService } from '@ngx-translate/core';
import { LocalizacaoPart } from './../../core/models/pojo/localizacao-dor-part.pojo';
import { Opcao } from './../../core/models/opcao.model';
import { AppHelper } from './../../core/helpers/app.helper';
import { NavController } from 'ionic-angular';
import { ConfiguracaoLocalizacaoDorTouch } from './../../core/models/pojo/configuracao-localizacao-dor-touch.pojo';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Component, Input, OnInit } from '@angular/core';
import filter from 'lodash-es/filter';

@Component({
  selector: 'ib-item-sumario-localizacao',
  templateUrl: 'item-sumario-localizacao.html',
})
export class ItemSumarioLocalizacaoComponent implements OnInit {

  @Input() item: ItemSumarioCrise;
  public configuracaoLocalizacaoCanvasTouch: ConfiguracaoLocalizacaoDorTouch;
  private TEXTO_SEM_DADOS: string = null;

  constructor(private _translate: TranslateService, private _navCtrl: NavController) {
    this.configuracaoLocalizacaoCanvasTouch = new ConfiguracaoLocalizacaoDorTouch();
    this._obterMensagens();
  }

  ngOnInit() {

    if (this.item) {
      this._iniciarLocalizacao();
      this._iniciar();
    }
  }

  public onClickIrParaQuestao() {
    this._navCtrl.push(this.item.getPage(), {
      'com.healclever.myMigraine.crise_param': this.item.getCrise(),
      'com.healclever.myMigraine.route': 'sumario',
    });
  }

  private _obterMensagens() {
    this._translate.get('SUMARIO.MSG.SEM_DADOS_PADRAO').subscribe(
      (value: string) => {
        this.TEXTO_SEM_DADOS = value;
      },
    );
  }

  private _iniciarLocalizacao() {
    let extensao: string = '.svg';
    let localizacoes: Array<LocalizacaoPart> = [];
    for (let i = 1; i < 20; i++) {
      let caminhoImagem = 'assets/img/localizacao-dor/localizacao_dor_' + i + extensao;
      let stringAreaId: string = i.toString();
      let isSelecionado: boolean = false;

      let localizacao: Opcao[] = (filter(this.item.getCrise().localizacoesDeDor, o => o._id === stringAreaId));
      if (localizacao.length > 0) {
        isSelecionado = true;
      }
      localizacoes[i] = new LocalizacaoPart(caminhoImagem, isSelecionado);
    }

    this.configuracaoLocalizacaoCanvasTouch.setImagemInicial('assets/img/localizacao-dor/localizacao_dor_0' + extensao);
    this.configuracaoLocalizacaoCanvasTouch.setImagemMapa('assets/img/localizacao-dor/localizacao_dor_map' + extensao);
    this.configuracaoLocalizacaoCanvasTouch.setLocalizacoes(localizacoes);
  }

  private _iniciar() {
    this.item.setComplete(true);
    this.item.setTitulo('SUMARIO.ITEM.LOCALIZACAO.TITULO');
    this.item.setIcone('rosie-sumario-localizacao-dor');
    this.item.setPage('LocalizacaoDorPage');
    let conteudo: string = this._getConteudo();

    if (!conteudo) {
      this.item.setComplete(false);
      conteudo = this.TEXTO_SEM_DADOS;
    }

    this.item.setConteudo(conteudo);
  }

  private _getConteudo(): string {
    let conteudo: string = '';

    if (this.item.getCrise().localizacoesDeDor[0]) {

      this.item.getCrise().localizacoesDeDor.forEach(
        (localizacao: Opcao, indice: number) => {
          conteudo += new AppHelper().textToCapitalize(localizacao.nome);

          if (indice === (this.item.getCrise().localizacoesDeDor.length - 1)) {
            conteudo += '.';
          } else {
            conteudo += ', ';
          }
        },
      );
    } else {
      conteudo = undefined;
    }

    return conteudo;
  }

}
