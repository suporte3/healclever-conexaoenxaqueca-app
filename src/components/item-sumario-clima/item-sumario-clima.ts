import { NavController } from 'ionic-angular';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'ib-item-sumario-clima',
  templateUrl: 'item-sumario-clima.html',
})
export class ItemSumarioClimaComponent implements OnInit {

  @Input() item: ItemSumarioCrise;
  public configurado: boolean;

  constructor(private _navCtrl: NavController) {

    this.configurado = false;
  }

  ngOnInit() {

    if (this.item) {
      this._iniciar();
    }
  }

  public onClickIrParaQuestao() {
    this._navCtrl.push(this.item.getPage(), {
      'com.healclever.myMigraine.crise_param': this.item.getCrise(),
      'com.healclever.myMigraine.route': 'sumario',
    });
  }

  private _iniciar() {
    this.item.setTitulo('clima');
    this.item.setIcone('rosie-sumario-clima');
    // this.item.setPage('DuracaoDorPage');

    if (this.item.getCrise().clima) {
      this.item.setClima(this.item.getCrise().clima);
      this.item.setComplete(true);
    } else {
      this.item.setComplete(false);
    }

    this.configurado = true;
  }

}
