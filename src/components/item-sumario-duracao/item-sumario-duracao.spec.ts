import { CapitalizePipe } from './../../pipes/capitalize/capitalize';
import { TestUtils } from './../../app/app.test';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Crise } from './../../core/models/crise.model';
import { ItemSumarioDuracaoComponent } from './item-sumario-duracao';
import { ComponentFixture, async } from '@angular/core/testing';
import * as moment from 'moment';

describe('Item Sumário Duração:', () => {

    let fixture: ComponentFixture<ItemSumarioDuracaoComponent> = null;
    let instance: any = null;
    let crise: Crise = {
        _id: 'criseId0',
        dataInicial: null,
        dataFinal: null,
        intensidade: [],
        medicamentos: [],
        qualidadesDor: [],
        sintomas: [],
        gatilhos: [],
        esforcosFisicos: [],
        bebidas: [],
        comidas: [],
        cicloMestrual: [],
        metodosAlivio: [],
        metodosAlivioEficiente: [],
        consequencias: [],
        locais: [],
        localizacoesDeDor: [],
        anotacoes: null,
        clima: null,
        gps: null,
        type: 'crise',
    };
    let item: ItemSumarioCrise = new ItemSumarioCrise();

    beforeEach(async(() => {

        item.setCrise(crise);

        TestUtils.beforeEachCompiler([ItemSumarioDuracaoComponent, CapitalizePipe]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            },
        );

        spyOn(moment, 'locale').and.stub();
    }));

    afterEach(() => {
        fixture.destroy();
        instance = null;
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeDefined();
        expect(instance).toBeDefined();
        done();
    });

    it('Deve iniciar o componente ao carregar o componente', () => {
        expect(moment.locale).toHaveBeenCalledWith('pt-br');
        expect(instance.configurado).toBe(false);
    });

    it('#ngOnInit - Deve iniciar o componente', () => {
        spyOn(instance, '_iniciar').and.stub();
        instance.item = item;
        instance.ngOnInit();
        expect(instance._iniciar).toHaveBeenCalled();
    });

    it('#onClickIrParaQuestao - Deve acessar questão', () => {
        spyOn(instance._navCtrl, 'push').and.stub();

        item.setCrise(crise);
        instance.item = item;

        let paramRota: any = {
            'com.healclever.myMigraine.crise_param': instance.item.getCrise(),
            'com.healclever.myMigraine.route': 'sumario',
        };

        instance._iniciar();
        instance.onClickIrParaQuestao();
        expect(instance._navCtrl.push).toHaveBeenCalledWith(instance.item.getPage(), paramRota);
    });

    it('#_iniciar - Deve iniciar corretamente', () => {
        crise.dataInicial = '2017-02-28T10:49:17.610Z';
        crise.dataFinal = '2017-02-28T11:49:19.135Z';
        item.setCrise(crise);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.getTitulo()).toBe('SUMARIO.ITEM.DURACAO.TITULO');
        expect(instance.item.getIcone()).toBe('rosie-sumario-duracao');
        expect(instance.item.getPage()).toBe('DuracaoDorPage');
        expect(instance.item.getDataInicial()).toBe(moment(crise.dataInicial).utc().format('DD/MM/YYYY'));
        expect(instance.item.getTempoInicial()).toBe(moment(crise.dataInicial).utc().format('HH:mm'));
        expect(instance.item.getDataFinal()).toBe(moment(crise.dataFinal).utc().format('DD/MM/YYYY'));
        expect(instance.item.getTempoFinal()).toBe(moment(crise.dataFinal).utc().format('HH:mm'));

        let duracaoEmMilisegundos: number = moment(crise.dataInicial).diff(crise.dataFinal);
        expect(instance.item.getDuracao()).toBe(moment.duration(duracaoEmMilisegundos).humanize());
        expect(instance.item.isComplete()).toBe(true);

        crise.dataFinal = null;
        item = new ItemSumarioCrise();
        item.setCrise(crise);
        instance.item = item;
        instance._iniciar();
        expect(instance.item.getDataFinal()).toBeUndefined();
        expect(instance.item.getTempoFinal()).toBeUndefined();
        expect(instance.item.getDuracao()).toBeUndefined();
        expect(instance.item.isComplete()).toBe(false);
    });
});
