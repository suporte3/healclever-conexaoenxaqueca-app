import { NavController } from 'ionic-angular';
import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import 'moment/min/locales';

@Component({
  selector: 'ib-item-sumario-duracao',
  templateUrl: 'item-sumario-duracao.html',
})
export class ItemSumarioDuracaoComponent implements OnInit {

  @Input() item: ItemSumarioCrise;
  public configurado: boolean;

  constructor(private _navCtrl: NavController) {
    moment.locale('pt-br');
    this.configurado = false;
  }

  ngOnInit() {

    if (this.item) {
      this._iniciar();
    }
  }

  public onClickIrParaQuestao() {
    this._navCtrl.push(this.item.getPage(), {
      'com.healclever.myMigraine.crise_param': this.item.getCrise(),
      'com.healclever.myMigraine.route': 'sumario',
    });
  }

  private _iniciar() {
    let dataInicial: string = this.item.getCrise().dataInicial;
    let dataFinal: string = this.item.getCrise().dataFinal;

    this.item.setTitulo('SUMARIO.ITEM.DURACAO.TITULO');
    this.item.setIcone('rosie-sumario-duracao');
    this.item.setPage('DuracaoDorPage');

    this.item.setDataInicial(moment(dataInicial).utc().format('DD/MM/YYYY'));
    this.item.setTempoInicial(moment(dataInicial).utc().format('HH:mm'));

    if (dataFinal) {
      this.item.setDataFinal(moment(dataFinal).utc().format('DD/MM/YYYY'));
      this.item.setTempoFinal(moment(dataFinal).utc().format('HH:mm'));

      let duracaoEmMilisegundos: number = moment(dataInicial).diff(dataFinal);
      this.item.setDuracao(moment.duration(duracaoEmMilisegundos).humanize());
      this.item.setComplete(true);
    } else {
      this.item.setComplete(false);
    }

    this.configurado = true;
  }

}
