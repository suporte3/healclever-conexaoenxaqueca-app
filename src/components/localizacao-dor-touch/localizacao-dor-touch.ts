import { LocalizacaoPart } from './../../core/models/pojo/localizacao-dor-part.pojo';
import { ConfiguracaoLocalizacaoDorTouch } from './../../core/models/pojo/configuracao-localizacao-dor-touch.pojo';
import { Component, Input, ViewChild, Renderer, ElementRef, OnInit, AfterViewInit, OnDestroy } from '@angular/core';

@Component({
  selector: 'ib-localizacao-dor-touch',
  template: `<div *ngIf="configuracoes.hasTouch()" #localizacaoCanvasWrapper ibZoomPan></div>
  <div *ngIf="!configuracoes.hasTouch()" #localizacaoCanvasWrapper></div>`,
})
export class LocalizacaoDorTouchComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() configuracoes: ConfiguracaoLocalizacaoDorTouch;
  @ViewChild('localizacaoCanvasWrapper') localizacaoCanvasWrapper: ElementRef;

  public listenLocalizacaoCanvasClick: Function;
  public divWrapperLocalizacaoCanvas: HTMLDivElement;

  private cacheObjetoImagens: any = [];
  private idAtual: number;
  private contextoLocalizacaoDor: CanvasRenderingContext2D;
  private contextoCamadaIdentificacao: CanvasRenderingContext2D;
  private readonly MAX_SIZE_CANVAS: number = 400;

  constructor(private _renderer: Renderer) { }

  ngOnInit() {

    let canvasLocalizacaoDor: HTMLCanvasElement = document.createElement('canvas');
    let canvasDeIdentificacao: HTMLCanvasElement = document.createElement('canvas');

    this.contextoLocalizacaoDor = canvasLocalizacaoDor.getContext('2d');
    this.contextoCamadaIdentificacao = canvasDeIdentificacao.getContext('2d');

    if (this.configuracoes.hasTouch()) {
      this.listenLocalizacaoCanvasClick = this._renderer.listen(this.contextoLocalizacaoDor.canvas, 'click',
        (event: any) => {
          this.onClickLocalizacaoPart(event);
        },
      );
    }
  }

  ngAfterViewInit() {
    this.divWrapperLocalizacaoCanvas = this.localizacaoCanvasWrapper.nativeElement;

    this._smoothAliasingImage();

    this.contextoLocalizacaoDor.canvas.width = this.getSizeCanvas();
    this.contextoLocalizacaoDor.canvas.height = this.getSizeCanvas();
    this.contextoCamadaIdentificacao.canvas.width = this.getSizeCanvas();
    this.contextoCamadaIdentificacao.canvas.height = this.getSizeCanvas();

    this.build().then(
      () => {
        for (let i = 1; i < this.configuracoes.getLocalizacoes().length; i++) {
          let localizacaoPart: LocalizacaoPart = this.configuracoes.getLocalizacoes()[i];
          if (localizacaoPart.isSelecionado()) {
            this.setSelecionarArea(localizacaoPart.getCaminhoImagem(), localizacaoPart.isSelecionado());
          }
        }
      },
    );

    this.divWrapperLocalizacaoCanvas.insertAdjacentElement('beforeend', this.contextoLocalizacaoDor.canvas);
  }

  ngOnDestroy() {
    if (this.configuracoes.hasTouch()) {
      this.listenLocalizacaoCanvasClick();
    }
  }

  setSelecionarArea(area: string, isSelecionada: boolean) {

    if (isSelecionada) {
      this.desenharImagem(this.contextoLocalizacaoDor, area);
    } else {
      this.contextoLocalizacaoDor.clearRect(0, 0, this.contextoLocalizacaoDor.canvas.width, this.contextoLocalizacaoDor.canvas.height);
      this.desenharImagem(this.contextoLocalizacaoDor, this.configuracoes.getImagemInicial());

      if (this.existeNoCache(area)) {
        this.cacheObjetoImagens.splice(area);
      }

      for (let i = 1; i < this.configuracoes.getLocalizacoes().length; i++) {
        let localizacaoPart: LocalizacaoPart = this.configuracoes.getLocalizacoes()[i];
        if (localizacaoPart.isSelecionado()) {
          this.desenharImagem(this.contextoLocalizacaoDor, localizacaoPart.getCaminhoImagem());
        }
      }
    }
  }

  getCoordenadas(event: any) {
    let rect = this.contextoLocalizacaoDor.canvas.getBoundingClientRect();
    let x: number = event.clientX - rect.left;
    let y: number = event.clientY - rect.top;

    if (this.isZoom()) {
      let scale: number = this.getScale();

      x = x / scale;
      y = y / scale;
    }

    return { eixoX: x, eixoY: y };
  }

  public getSizeCanvas() {
    let size: number = this.divWrapperLocalizacaoCanvas.clientWidth;

    if (size > this.MAX_SIZE_CANVAS) {
      size = this.MAX_SIZE_CANVAS;
    }

    return size;
  }

  public isZoom() {
    let isZoomed: boolean = (this.divWrapperLocalizacaoCanvas.getAttribute('zoomed') !== 'false');

    return isZoomed;
  }

  public getScale(): number {
    let scale: number = parseFloat(this.divWrapperLocalizacaoCanvas.getAttribute('scale'));

    return scale;
  }

  public onClickLocalizacaoPart(event: Event) {
    let coordenadasDoToque: any = this.getCoordenadas(event);
    let imgData = this.contextoCamadaIdentificacao.getImageData(
      coordenadasDoToque.eixoX,
      coordenadasDoToque.eixoY,
      1,
      1,
    );
    let idArea = imgData.data[2];
    if (idArea > 0 && idArea < 20) {
      let localizacaoPart: LocalizacaoPart = this.configuracoes.getLocalizacoes()[idArea];
      let isSelecionada: boolean = !localizacaoPart.isSelecionado();
      localizacaoPart.setSelecionado(isSelecionada);
      this.idAtual = idArea;
      this.setSelecionarArea(localizacaoPart.getCaminhoImagem(), isSelecionada);
      this.configuracoes.getPage().onClickArea(this.idAtual, isSelecionada);
    }
  }

  private build(): Promise<any> {
    let _this: LocalizacaoDorTouchComponent = this;

    return new Promise(
      function (resolve) {
        _this.desenharImagem(_this.contextoLocalizacaoDor, _this.configuracoes.getImagemInicial()).then(
          () => {
            _this.desenharImagem(_this.contextoCamadaIdentificacao, _this.configuracoes.getImagemMapa()).then(
              () => {
                resolve();
              },
            );
          },
        );
      },
    );
  }

  private existeNoCache(caminhoImagem: string) {
    let existe: boolean = false;
    for (let item in this.cacheObjetoImagens) {
      if (item === caminhoImagem) {
        existe = true;
        break;
      }
    }

    return existe;
  }

  private desenharImagem(contextoCanvas: CanvasRenderingContext2D, caminhoImagem: string): Promise<any> {
    let _this: LocalizacaoDorTouchComponent = this;
    let size: number = this.getSizeCanvas();

    return new Promise(
      function (resolve) {

        let existeNoCache: boolean = _this.existeNoCache(caminhoImagem);
        if (existeNoCache) {
          contextoCanvas.drawImage(_this.cacheObjetoImagens[caminhoImagem], 0, 0, size, size);
          resolve();
        } else {

          let image = new Image();
          image.crossOrigin = 'Anonymous';
          image.onload = () => {
            contextoCanvas.drawImage(image, 0, 0, size, size);
            _this.cacheObjetoImagens[caminhoImagem] = image;

            resolve();
          };

          image.src = caminhoImagem;
        }
      },
    );
  }

  private _smoothAliasingImage() {
    this.contextoLocalizacaoDor.imageSmoothingEnabled = true;
    this.contextoLocalizacaoDor.mozImageSmoothingEnabled = true;
    this.contextoLocalizacaoDor.webkitImageSmoothingEnabled = true;
    this.contextoLocalizacaoDor.oImageSmoothingEnabled = true;

    this.contextoCamadaIdentificacao.imageSmoothingEnabled = true;
    this.contextoCamadaIdentificacao.mozImageSmoothingEnabled = true;
    this.contextoCamadaIdentificacao.webkitImageSmoothingEnabled = true;
    this.contextoCamadaIdentificacao.oImageSmoothingEnabled = true;
  }

}
