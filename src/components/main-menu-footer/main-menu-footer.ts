import { NavController } from 'ionic-angular';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'ib-main-menu-footer',
  templateUrl: 'main-menu-footer.html',
})
export class MainMenuFooterComponent {

  @Input() sessaoAtual: string;
  constructor(private _navCtrl: NavController) {

  }

  public onClickDiario() {
    if (this.sessaoAtual !== 'diario') {
      this._navCtrl.setRoot('DiarioPage');
    }
  }

  public onClickChat() {
    if (this.sessaoAtual !== 'chat') {
      this._navCtrl.setRoot('ChatPage');
    }
  }

  public onClickApoio() {
    if (this.sessaoAtual !== 'apoio') {
      // this._navCtrl.setRoot(ApoioPage);
    }
  }

  public onClickMais() {
    if (this.sessaoAtual !== 'mais') {
      // this._navCtrl.setRoot(MenuMaisPage);
    }
  }

  public onClickPerfil() {
    if (this.sessaoAtual !== 'perfil') {
      this._navCtrl.setRoot('EuPage');
    }
  }

  public isSelecionada(tipoBtn: string): boolean {
    return tipoBtn === this.sessaoAtual;
  }

}
