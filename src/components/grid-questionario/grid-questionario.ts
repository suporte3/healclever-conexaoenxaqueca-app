import { AppParam } from './../../core/models/pojo/app-param.pojo';
import { Cor } from './../../core/models/pojo/cor.pojo';
import { ModeloOpcao } from './../../core/models/pojo/opcao.pojo';
import { Network } from '@ionic-native/network';
import { AppHelper } from './../../core/helpers/app.helper';
import { GridManagerService } from './../../services/grid-manager/grid-manager.service';
import { SyncDataProvider } from './../../providers/sync-data/sync-data';
import { Content, NavController, ToastController, Events, ModalController, ViewController } from 'ionic-angular';
import { TipoQuestao } from './../../core/models/pojo/tipo-questao.pojo';
import { TipoOpcao } from './../../core/models/tipo-opcao.enum';
import { ConfiguracaoGrid } from './../../core/models/pojo/configuracao-grid.pojo';
import { Component, Input } from '@angular/core';
import clone from 'lodash-es/clone';

@Component({
  selector: 'ib-grid-questionario',
  templateUrl: 'grid-questionario.html',
})
export class GridQuestionarioComponent extends GridManagerService {

  @Input() configuracaoQuestao: ConfiguracaoGrid;
  public tipoQuestaoMedicamento: string = TipoQuestao.MEDICAMENTO;
  public tipoQuestaoLocal: string = TipoQuestao.LOCAL;
  private _content: Content = null;
  private _houveInteracaoUsuario: boolean = false;

  constructor(private navCtrl: NavController, private _syncDataService: SyncDataProvider, private viewController: ViewController,
    private toastCtrl: ToastController, private events: Events, public modalCtrl: ModalController) {
    super();

    this._content = this.viewController.getContent();
  }

  public setHouveInteracaoUsuario(interacao: boolean) {
    this._houveInteracaoUsuario = interacao;
  }

  public isHouveInteracaoUsuario() {
    return this._houveInteracaoUsuario;
  }

  public iniciar() {
    this.setEvents(this.events);
    this.configuracaoQuestao.setSyncDataServiceReference(this._syncDataService);
    this.setConfiguracao(this.configuracaoQuestao);
    this.bindIsModoRemover = false;

    this._montarGrid();

    if (this.configuracaoQuestao.getIdentificadorGrid() === TipoQuestao.LOCAL) {
      this.events.subscribe(AppParam.ATIVAR_MODO_GPS, () => {
        this._ativarModoGPS();
      });
    }

    this._content.resize();
    this._iniciouQuestao = true;
  }

  public refresh() {
    this._needRefresh = true;

    this._montarGrid();

    this._needRefresh = false;
  }

  public onClickAcoesGridQuestionario(modeloOpcao: ModeloOpcao) {

    switch (modeloOpcao.getId()) {
      case this.ADICIONAR_OPCAO:
        if (!this.bindIsModoRemover && !this.bindIsModoGPS) {
          this.abrirModal();
        } else {
          let msg: string = 'Para adicionar saia do modo ';
          msg += (this.bindIsModoRemover ? 'excluir' : 'GPS');
          new AppHelper().toastMensagem(this.toastCtrl, msg);
        }
        break;
      case this.REMOVER_OPCAO:
        this._ativarDesativarModoRemover();
        break;
      default:
        if (this.configuracaoQuestao.getIdentificadorGrid() === TipoQuestao.LOCAL && this.bindIsModoGPS) {
          let network = new Network();
          if (network.type === 'none') {
            new AppHelper().toastMensagem(this.toastCtrl, 'Sem conexão com internet');
          } else {
            let isLocalFixo: boolean = modeloOpcao.isLocalFixo();
            this.criarModalEventoClique(
              this.configuracaoQuestao.getModalEventoClique(), {
                'modeloOpcao': modeloOpcao,
                'isLocalFixo': isLocalFixo,
              },
            );
            this.abrirModalAdicionarDetalhe();
          }
        } else {
          this._removerOuSelecionar(modeloOpcao);
        }
    }
  }

  public abrirModal(): void {
    this.getModalAdicionarOpcoes().onDidDismiss((data: any) => {
      this._abrirModalOnDismiss(data);
    });
    this.getModalAdicionarOpcoes().present();
  }

  public abrirModalAdicionarDetalhe(): void {
    this.getModalAdicionarEvento().onDidDismiss(
      (data: any) => {
        this._abrirModalAdicionarDetalheOnDismiss(data);
      });
    this.getModalAdicionarEvento().present();
  }

  private _abrirModalAdicionarDetalheOnDismiss(data: any) {
    if (data) {
      if (data.modeloOpcao) {
        this.setHouveInteracaoUsuario(true);

        switch (this.configuracaoQuestao.getIdentificadorGrid()) {
          case TipoQuestao.MEDICAMENTO:
            this._addQuantidadeMedicamento(data.modeloOpcao);
            break;
          case TipoQuestao.METODO_ALIVIO_EFICIENTE:
            this._addEficiencia(data.modeloOpcao);
            break;
          case TipoQuestao.LOCAL:
            this._addLocalGPS(data.modeloOpcao);
            break;
          default:
            console.log('TRACK ERROR');
        }
      }
    }
  }

  private _abrirModalOnDismiss(data: any) {
    let parametro = {
      'opcoes': this.getDocumentOptions(),
    };

    if (data) {
      this.setHouveInteracaoUsuario(true);

      this.setDocumentOptions(data.opcao);
      let novaOpcao: ModeloOpcao = data.modeloOpcao;
      if (novaOpcao) {
        if (this.configuracaoQuestao.isMarcarSomenteUmaOpcaoNoGrid()) {
          this._desmarcarUltimaOpcaoSelecionada();
        }

        this.getOpcoesSelecionadas().push(novaOpcao);

        if (this.configuracaoQuestao.getIdentificadorGrid() === TipoQuestao.MEDICAMENTO) {
          this.events.publish('medicamento:onPrimeiroCadastroMedicamento', false);
        }
      }

      // Usar this.atualizarOpcoesGrid(novaOpcao);
      this.atualizarOpcoes();
    }

    this.criarModalAdicionarOpcoes(parametro);
  }

  private _addQuantidadeMedicamento(modeloOpcao: ModeloOpcao) {
    let selecionado: boolean = false;
    this.getOpcoesSelecionadas().every(
      (opcao: ModeloOpcao) => {
        if (opcao.getId() === modeloOpcao.getId()) {
          selecionado = true;
          return false;
        } else {
          return true;
        }
      },
    );

    if (!selecionado) {
      this._selecionarOpcao(modeloOpcao);
    } else if (selecionado && modeloOpcao.getQuantidade()) {
      this._desmarcarOpcao(modeloOpcao);
      this._selecionarOpcao(modeloOpcao);
    } else {
      this._desmarcarOpcao(modeloOpcao);
    }
  }

  private _addEficiencia(modeloOpcao: ModeloOpcao) {
    modeloOpcao.definirIconeEficiencia();

    let selecionado: boolean = false;
    this.getOpcoesSelecionadas().every(
      (opcao: ModeloOpcao) => {
        if (opcao.getId() === modeloOpcao.getId()) {
          selecionado = true;
          return false;
        } else {
          return true;
        }
      },
    );

    if (!selecionado) {
      this._selecionarOpcao(modeloOpcao);
    } else if (selecionado && modeloOpcao.getTipoEficiencia()) {
      this.retirarModeloOpcaoDoArraySelecionado(modeloOpcao);
      this._selecionarOpcao(modeloOpcao);
    }
  }

  private _addLocalGPS(modeloOpcaoModal: ModeloOpcao) {
    this._desmarcarUltimaOpcaoSelecionada();
    this._selecionarOpcao(modeloOpcaoModal);
    this._atualizarArrayOpcoesLocalFixo(modeloOpcaoModal);
    // this.atualizarOpcaoDoGrid(modeloOpcaoModal);
  }

  private _atualizarArrayOpcoesLocalFixo(modeloOpcaoModal: ModeloOpcao) {
    if (modeloOpcaoModal.isLocalFixo()) {
      this._atualizarLocalFixo(modeloOpcaoModal);
    } else {
      this._excluirLocalFixo(modeloOpcaoModal);
    }
  }

  private _atualizarLocalFixo(modeloOpcaoModal: ModeloOpcao) {
    let indice: number = -1;

    this.getOpcoesLocalFixo().every(
      (modeloOpcao: ModeloOpcao, i: number) => {
        if (modeloOpcao.getId() === modeloOpcaoModal.getId()) {
          indice = i;
          return false;
        } else {
          return true;
        }
      },
    );

    if (indice > -1) {
      this.getOpcoesLocalFixo()[indice] = modeloOpcaoModal;
    } else {
      this.getOpcoesLocalFixo().push(modeloOpcaoModal);
    }
  }

  private _excluirLocalFixo(modeloOpcaoModal: ModeloOpcao) {
    this.getOpcoesLocalFixoDatabase().every(
      (localFixoDatabase: ModeloOpcao) => {
        if (localFixoDatabase.getId() === modeloOpcaoModal.getId()) {
          this.getOpcoesLocalFixo().push(modeloOpcaoModal);
          return false;
        } else {
          return true;
        }
      },
    );
  }

  private _montarGrid() {
    this.setCrise(this.configuracaoQuestao.getCrise());
    this.setDocumentOptions(this.configuracaoQuestao.getOpcoesDocumento());

    if (this.configuracaoQuestao.getIdentificadorGrid() === TipoQuestao.METODO_ALIVIO_EFICIENTE) {
      this.montarGridMetodoAlivio();
    } else {
      this.atualizarOpcoes();
      this.configurarModal();
    }
  }

  private _removerOuSelecionar(modeloOpcao: ModeloOpcao) {

    if (this.configuracaoQuestao.getIdentificadorGrid() === TipoQuestao.GATILHO && !this.bindIsModoRemover) {
      this._selecionarOpcaoGatilho(modeloOpcao);
    } else {
      this._removerOuSelecionarOpcaoNormal(modeloOpcao);
    }
  }

  private _removerOuSelecionarOpcaoNormal(modeloOpcao: ModeloOpcao) {
    if (this.bindIsModoRemover) {
      this._removerOpcao(modeloOpcao);
    } else {
      if (this.configuracaoQuestao.getPossuiEventoClique()) {
        this.criarModalEventoClique(this.configuracaoQuestao.getModalEventoClique(), { 'modeloOpcao': modeloOpcao });
        this.abrirModalAdicionarDetalhe();
      } else {
        this._selecionarOuDesmarcarOpcao(modeloOpcao);
      }
    }
  }

  private _selecionarOpcaoGatilho(modeloOpcao: ModeloOpcao) {
    this.setHouveInteracaoUsuario(true);
    let param: any = null;

    if (this.configuracaoQuestao.getRota() === 'sumario') {
      param = { 'com.healclever.myMigraine.crise_param': this.getCrise() };
    }

    switch (modeloOpcao.getId()) {
      case 'mm-gatilho-esforco-fisico':
        this.navCtrl.push('EsforcoFisicoPage', param);
        break;
      case 'mm-gatilho-bebida':
        this.navCtrl.push('BebidaPage', param);
        break;
      case 'mm-gatilho-comida':
        this.navCtrl.push('ComidaPage', param);
        break;
      default:
        this._removerOuSelecionarOpcaoNormal(modeloOpcao);
    }
  }

  private _removerOpcao(modeloOpcao: ModeloOpcao) {
    if (modeloOpcao.getTipo() === TipoOpcao.DINAMICO) {
      this.setHouveInteracaoUsuario(true);
      this.removerOpcao(modeloOpcao);
    } else {
      new AppHelper().toastMensagem(this.toastCtrl, 'Opção fixa, não é possível excluir');
    }
  }

  private _selecionarOuDesmarcarOpcao(modeloOpcao: ModeloOpcao) {
    modeloOpcao.setSelecionado(!modeloOpcao.isSelecionado());
    if (modeloOpcao.isSelecionado()) {
      if (this.configuracaoQuestao.isMarcarSomenteUmaOpcaoNoGrid()) {
        this._desmarcarUltimaOpcaoSelecionada();
      }
      this._selecionarOpcao(modeloOpcao);
    } else {
      this._desmarcarOpcao(modeloOpcao);
    }
  }

  private _desmarcarUltimaOpcaoSelecionada() {
    if (this.getOpcoesSelecionadas().length > 0) {
      this._desmarcarOpcao(this.getOpcoesSelecionadas()[0]);
      this.setOpcoesSelecionadas([]);
    }
  }

  private _selecionarOpcao(modeloOpcao: ModeloOpcao) {
    this.setHouveInteracaoUsuario(true);

    let cloneModeloOpcao: ModeloOpcao = clone(modeloOpcao);

    cloneModeloOpcao.setCor(Cor.LARANJA);
    this.getOpcoesSelecionadas().push(cloneModeloOpcao);

    this.atualizarOpcaoDoGrid(cloneModeloOpcao);
  }

  private _desmarcarOpcao(modeloOpcao: ModeloOpcao) {

    let cloneModeloOpcao: ModeloOpcao = clone(modeloOpcao);

    this.setHouveInteracaoUsuario(true);
    this.retirarModeloOpcaoDoArraySelecionado(cloneModeloOpcao);

    cloneModeloOpcao.setCor(Cor.VERDE_CLARO);
    cloneModeloOpcao.setSelecionado(false);

    if (!cloneModeloOpcao.isLocalFixo()) {
      cloneModeloOpcao.setValorGPS(undefined);
    }

    this.atualizarOpcaoDoGrid(cloneModeloOpcao);
  }

  private _ativarDesativarModoRemover() {

    this.bindIsModoGPS = false;
    this.bindIsModoRemover = (!this.bindIsModoRemover);
    this.events.publish('ativarModoRemover:' + this.configuracaoQuestao.getIdentificadorGrid());

    this.arrayOpcoesDoGrid.forEach(elementModeloOpcao => {
      if (elementModeloOpcao.getId() !== this.REMOVER_OPCAO) {
        this.definirCorOpcao(elementModeloOpcao);
      }
    });

    this._content.resize();
  }

  private _ativarModoGPS() {
    this.bindIsModoRemover = false;
    this.bindIsModoGPS = (!this.bindIsModoGPS);
    this.arrayOpcoesDoGrid.forEach(elementModeloOpcao => {
      if (elementModeloOpcao.getId() !== this.REMOVER_OPCAO) {
        this.definirCorOpcao(elementModeloOpcao);
      }
    });

    this._content.resize();
  }

}
