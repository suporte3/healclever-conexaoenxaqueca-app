import { ItemSumarioCrise } from './../../core/models/pojo/item-sumario.pojo';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'ib-item-sumario',
  templateUrl: 'item-sumario.html',
})
export class ItemSumarioComponent {

  @Input() item: ItemSumarioCrise;

}
