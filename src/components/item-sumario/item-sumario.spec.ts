import { TestUtils } from './../../app/app.test';
import { ItemSumarioComponent } from './item-sumario';
import { ComponentFixture, async } from '@angular/core/testing';

describe('Item Sumário:', () => {

    let fixture: ComponentFixture<ItemSumarioComponent> = null;
    let instance: any = null;

    beforeEach(async(() => {

        TestUtils.beforeEachCompiler([ItemSumarioComponent]).then(
            (compiled: any) => {
                fixture = compiled.fixture;
                instance = compiled.instance;
                fixture.detectChanges();
            },
        );
    }));

    afterEach(() => {
        fixture.destroy();
        instance = null;
    });

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve carregar o component', (done) => {
        expect(fixture).toBeDefined();
        expect(instance).toBeDefined();
        done();
    });
});
