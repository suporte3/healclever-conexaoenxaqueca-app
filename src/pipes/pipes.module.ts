import { NgModule } from '@angular/core';
import { CapitalizePipe } from './../pipes/capitalize/capitalize';
import { CapitalizeAllPipe } from './../pipes/capitalize-all/capitalize-all';

@NgModule({
 declarations: [
 CapitalizePipe,
 CapitalizeAllPipe,
 ],
 imports: [],
 exports: [
  CapitalizePipe,
  CapitalizeAllPipe,
 ],
})
export class PipesModule { }
