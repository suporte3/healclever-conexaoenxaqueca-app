import { TestUtils } from './../../app/app.test';
import { CapitalizePipe } from './capitalize';
import { async, inject } from '@angular/core/testing';

describe('Pipe: CapitalizePipe:', () => {

    let pipe: CapitalizePipe;

    beforeEach(async(() => {

        TestUtils.addProvider(CapitalizePipe);
        TestUtils.beforeEachCompiler([]);
    }));

    beforeEach(inject([CapitalizePipe], (p: CapitalizePipe) => {
        pipe = p;
    }));

    afterAll(() => {
        TestUtils.destruirProviders();
    });

    it('Deve trabalhar com string vazia', () => {
        expect(pipe.transform('')).toEqual('');
    });

    it('Deve fazer capitalize', () => {
        expect(pipe.transform('wow')).toEqual('Wow');
    });

});
