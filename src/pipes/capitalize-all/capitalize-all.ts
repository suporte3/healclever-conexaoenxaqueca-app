import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'capitalizeAll',
})
export class CapitalizeAllPipe implements PipeTransform {

  transform(value: string) {
    if (!value) return value;

    return value.replace(/\w\S*/g, function (txt: string) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }
}
