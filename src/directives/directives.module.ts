import { NgModule } from '@angular/core';
import { ZoomPanDirective } from './zoom-pan/zoom-pan';
@NgModule({
 declarations: [ZoomPanDirective],
 imports: [],
 exports: [ZoomPanDirective],
})
export class DirectivesModule { }
