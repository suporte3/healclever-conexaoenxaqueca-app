import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[ibZoomPan]',
})
export class ZoomPanDirective {

  public isZoomed: boolean;
  private nativeElement: any;

  constructor(public element: ElementRef) {

    this.nativeElement = element.nativeElement;
    this.setZoomed(false);

    this.hammerIt(this.nativeElement);
  }

  private setZoomed(zoomed: boolean) {
    this.isZoomed = zoomed;
    this.nativeElement.setAttribute('zoomed', this.isZoomed);
  }

  private setScale(scale: number) {
    this.nativeElement.setAttribute('scale', scale);
  }

  private hammerIt(elm: any) {
    let hammertime = new Hammer(elm, {});
    hammertime.get('pinch').set({
      enable: true,
    });
    let posX = 0,
      posY = 0,
      scale = 1,
      lastScale = 1,
      lastPosX = 0,
      lastPosY = 0,
      maxPosX = 0,
      maxPosY = 0,
      transform = '',
      el = elm;

    this.setScale(scale);

    hammertime.on('doubletap pan pinch panend pinchend', (ev: any) => {
      if (ev.type === 'doubletap') {
        transform =
          'translate3d(0, 0, 0) ' +
          'scale3d(2, 2, 1) ';
        scale = 2;
        lastScale = 2;
        try {
          if (window.getComputedStyle(el, null).getPropertyValue('-webkit-transform').toString() !== 'matrix(1, 0, 0, 1, 0, 0)') {
            transform =
              'translate3d(0, 0, 0) ' +
              'scale3d(1, 1, 1) ';
            scale = 1;
            lastScale = 1;
          }
        } catch (err) { }
        el.style.webkitTransform = transform;
        transform = '';
      }

      // pan
      if (scale !== 1) {
        posX = lastPosX + ev.deltaX;
        posY = lastPosY + ev.deltaY;
        maxPosX = Math.ceil((scale - 1) * el.clientWidth / 2);
        maxPosY = Math.ceil((scale - 1) * el.clientHeight / 2);
        if (posX > maxPosX) {
          posX = maxPosX;
        }
        if (posX < -maxPosX) {
          posX = -maxPosX;
        }
        if (posY > maxPosY) {
          posY = maxPosY;
        }
        if (posY < -maxPosY) {
          posY = -maxPosY;
        }
      }

      // pinch
      if (ev.type === 'pinch') {
        scale = Math.max(.999, Math.min(lastScale * (ev.scale), 4));
      }
      if (ev.type === 'pinchend') { lastScale = scale; }

      // panend
      if (ev.type === 'panend') {
        lastPosX = posX < maxPosX ? posX : maxPosX;
        lastPosY = posY < maxPosY ? posY : maxPosY;
      }

      if (scale !== 1) {
        transform =
          'translate3d(' + posX + 'px,' + posY + 'px, 0) ' +
          'scale3d(' + scale + ', ' + scale + ', 1)';
      }

      if (transform) {
        el.style.webkitTransform = transform;
      }

      if (scale <= 1) {
        this.setZoomed(false);
      } else {
        this.setZoomed(true);
      }

      this.setScale(scale);
    });
  }

}
