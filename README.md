## MyMigraine
Projeto oficial do primeiro assistente virtual para o autocuidado de Cefaleias -  MyMigraine.
##### Produção
[![prod build status](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/badges/master/build.svg)](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/pipelines?scope=branches) [![prod coverage report](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/badges/master/coverage.svg)](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/commits/master)
##### Desenvolvimento
[![dev build status](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/badges/development/build.svg)](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/pipelines?scope=branches) [![dev coverage report](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/badges/development/coverage.svg)](https://gitlab.com/suporte3/healclever-conexaoenxaqueca-app/commits/development)
## Instalação
```bash
git checkout {branch}
git merge origin/development
npm install
npm install -g typings ou npm install typings --global
typings install --global --save dt~pouchdb dt~pouchdb-adapter-websql dt~pouchdb-browser dt~pouchdb-core dt~pouchdb-http dt~pouchdb-mapreduce dt~pouchdb-node dt~pouchdb-replication dt~pouchdb-find dt~require dt~socket.io-client
mkdir www
```
[ionic 2 cordova bug](https://github.com/driftyco/ionic-cli/issues/935)
### Instalar Yarn:
[Instalar Yarn](https://yarnpkg.com/docs/install)
### Instalar plugins cordova:
```bash
cordova plugin add cordova-sqlite-storage --save
ionic plugin add cordova-plugin-splashscreen
ionic plugin add cordova-plugin-facebook4 --variable APP_ID="1508314849485284" --variable APP_NAME="ConexaoEnxaqueca"
ionic plugin add cordova-plugin-network-information
ionic plugin add cordova-plugin-camera
ionic plugin add de.appplant.cordova.plugin.local-notification
```
1. **SQLite:** cordova plugin add cordova-sqlite-storage --save
2. **Splashscreen:** ionic plugin add cordova-plugin-splashscreen
3. **Facebook:** ionic plugin add cordova-plugin-facebook4 --variable APP_ID="123456789" --variable APP_NAME="myApplication"
4. **Network:** ionic plugin add cordova-plugin-network-information
4. **Camera:** ionic plugin add cordova-plugin-camera
5. **Local Notifications:** ionic plugin add de.appplant.cordova.plugin.local-notification

## Tests
Descrever worflow...

## Release
Descrever a [semantica de versionamento](http://semver.org/) 1.x.x MAJOR.MINOR.PATCH